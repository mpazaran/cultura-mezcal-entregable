-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: mezcal-5
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_passwords`
--

DROP TABLE IF EXISTS `admin_passwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_passwords` (
  `password_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Password Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User Id',
  `password_hash` varchar(100) DEFAULT NULL COMMENT 'Password Hash',
  `expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Expires',
  `last_updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last Updated',
  PRIMARY KEY (`password_id`),
  KEY `ADMIN_PASSWORDS_USER_ID` (`user_id`),
  CONSTRAINT `ADMIN_PASSWORDS_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin Passwords';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_passwords`
--

LOCK TABLES `admin_passwords` WRITE;
/*!40000 ALTER TABLE `admin_passwords` DISABLE KEYS */;
INSERT INTO `admin_passwords` VALUES (1,2,'1ce1382cc06d9fb065094372466a730e4170f96bb986d2f0bba12901723cdd0b:PVAMC3Ft5GOw4TpGgyuJhJVFRFp177BS:1',1590966784,1583190784);
/*!40000 ALTER TABLE `admin_passwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_system_messages`
--

DROP TABLE IF EXISTS `admin_system_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_system_messages` (
  `identity` varchar(100) NOT NULL COMMENT 'Message id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`identity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin System Messages';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_system_messages`
--

LOCK TABLES `admin_system_messages` WRITE;
/*!40000 ALTER TABLE `admin_system_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_system_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user`
--

DROP TABLE IF EXISTS `admin_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `firstname` varchar(32) DEFAULT NULL COMMENT 'User First Name',
  `lastname` varchar(32) DEFAULT NULL COMMENT 'User Last Name',
  `email` varchar(128) DEFAULT NULL COMMENT 'User Email',
  `username` varchar(40) DEFAULT NULL COMMENT 'User Login',
  `password` varchar(255) NOT NULL COMMENT 'User Password',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'User Created Time',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'User Modified Time',
  `logdate` timestamp NULL DEFAULT NULL COMMENT 'User Last Login Time',
  `lognum` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'User Login Number',
  `reload_acl_flag` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Reload ACL',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'User Is Active',
  `extra` text COMMENT 'User Extra Data',
  `rp_token` text COMMENT 'Reset Password Link Token',
  `rp_token_created_at` timestamp NULL DEFAULT NULL COMMENT 'Reset Password Link Token Creation Date',
  `interface_locale` varchar(16) NOT NULL DEFAULT 'en_US' COMMENT 'Backend interface locale',
  `failures_num` smallint(6) DEFAULT '0' COMMENT 'Failure Number',
  `first_failure` timestamp NULL DEFAULT NULL COMMENT 'First Failure',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Expiration Lock Dates',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ADMIN_USER_USERNAME` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Admin User Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user`
--

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` VALUES (1,'admin','admin','kdodeth@hotmail.com','admin','dfef564657defaa6460be618bfa4d29d22b85c744b6c9534f9a3eff14262079c:ivgFhkPle75s2HIDwoqZa5Hqx1Jtc4JG:1','2020-02-28 15:31:33','2020-03-17 18:13:56','2020-03-17 18:13:56',57,0,1,'a:1:{s:11:\"configState\";a:123:{s:26:\"plazathemes_design_general\";s:1:\"1\";s:25:\"plazathemes_design_header\";s:1:\"1\";s:14:\"mfblog_general\";s:1:\"1\";s:17:\"mfblog_index_page\";s:1:\"1\";s:19:\"testimonial_general\";s:1:\"1\";s:22:\"categorytab_new_status\";s:1:\"1\";s:16:\"mfblog_post_view\";s:1:\"0\";s:16:\"mfblog_post_list\";s:1:\"0\";s:14:\"mfblog_sidebar\";s:1:\"1\";s:15:\"mfblog_rss_feed\";s:1:\"0\";s:22:\"categorytop_new_status\";s:1:\"1\";s:24:\"instagramgallery_general\";s:1:\"1\";s:27:\"newsletterpopup_popup_group\";s:1:\"1\";s:24:\"newproductslider_general\";s:1:\"1\";s:31:\"featureproductslider_new_status\";s:1:\"1\";s:28:\"bestsellerproduct_new_status\";s:1:\"1\";s:15:\"general_country\";s:1:\"0\";s:25:\"general_single_store_mode\";s:1:\"1\";s:25:\"general_store_information\";s:1:\"1\";s:11:\"web_default\";s:1:\"1\";s:14:\"general_region\";s:1:\"0\";s:14:\"general_locale\";s:1:\"0\";s:7:\"web_url\";s:1:\"0\";s:7:\"web_seo\";s:1:\"0\";s:12:\"web_unsecure\";s:1:\"1\";s:10:\"web_secure\";s:1:\"0\";s:10:\"web_cookie\";s:1:\"0\";s:11:\"web_session\";s:1:\"0\";s:24:\"web_browser_capabilities\";s:1:\"0\";s:9:\"dev_debug\";s:1:\"1\";s:34:\"dev_front_end_development_workflow\";s:1:\"0\";s:12:\"dev_restrict\";s:1:\"0\";s:12:\"dev_template\";s:1:\"1\";s:20:\"dev_translate_inline\";s:1:\"0\";s:6:\"dev_js\";s:1:\"0\";s:7:\"dev_css\";s:1:\"0\";s:9:\"dev_image\";s:1:\"0\";s:10:\"dev_static\";s:1:\"0\";s:8:\"dev_grid\";s:1:\"0\";s:14:\"admin_security\";s:1:\"1\";s:12:\"admin_emails\";s:1:\"0\";s:13:\"admin_startup\";s:1:\"0\";s:9:\"admin_url\";s:1:\"0\";s:15:\"admin_dashboard\";s:1:\"1\";s:13:\"admin_captcha\";s:1:\"1\";s:20:\"catalog_fields_masks\";s:1:\"1\";s:16:\"catalog_frontend\";s:1:\"1\";s:14:\"catalog_review\";s:1:\"1\";s:20:\"catalog_productalert\";s:1:\"0\";s:19:\"catalog_placeholder\";s:1:\"0\";s:25:\"catalog_recently_products\";s:1:\"1\";s:26:\"catalog_layered_navigation\";s:1:\"1\";s:14:\"catalog_search\";s:1:\"0\";s:18:\"catalog_navigation\";s:1:\"0\";s:11:\"catalog_seo\";s:1:\"0\";s:22:\"catalog_custom_options\";s:1:\"0\";s:24:\"cataloginventory_options\";s:1:\"1\";s:29:\"cataloginventory_item_options\";s:1:\"1\";s:16:\"wishlist_general\";s:1:\"1\";s:14:\"wishlist_email\";s:1:\"1\";s:22:\"wishlist_wishlist_link\";s:1:\"1\";s:15:\"sales_dashboard\";s:1:\"1\";s:13:\"sales_general\";s:1:\"1\";s:16:\"customer_startup\";s:1:\"1\";s:26:\"customer_address_templates\";s:1:\"0\";s:16:\"customer_address\";s:1:\"1\";s:28:\"customer_account_information\";s:1:\"1\";s:25:\"customer_online_customers\";s:1:\"1\";s:15:\"wordpress_setup\";s:1:\"1\";s:20:\"wordpress_xmlsitemap\";s:1:\"1\";s:17:\"sales_totals_sort\";s:1:\"1\";s:13:\"sales_reorder\";s:1:\"1\";s:14:\"sales_identity\";s:1:\"1\";s:19:\"sales_minimum_order\";s:1:\"1\";s:12:\"sales_orders\";s:1:\"1\";s:10:\"sales_msrp\";s:1:\"1\";s:18:\"sales_gift_options\";s:1:\"1\";s:16:\"checkout_options\";s:1:\"1\";s:15:\"shipping_origin\";s:1:\"1\";s:24:\"shipping_shipping_policy\";s:1:\"1\";s:17:\"carriers_flatrate\";s:1:\"0\";s:21:\"carriers_freeshipping\";s:1:\"1\";s:18:\"carriers_tablerate\";s:1:\"0\";s:12:\"carriers_ups\";s:1:\"0\";s:13:\"carriers_usps\";s:1:\"0\";s:14:\"carriers_fedex\";s:1:\"0\";s:12:\"carriers_dhl\";s:1:\"0\";s:33:\"payment_es_express_checkout_other\";s:1:\"0\";s:23:\"checkout_payment_failed\";s:1:\"1\";s:16:\"checkout_sidebar\";s:1:\"1\";s:18:\"checkout_cart_link\";s:1:\"1\";s:13:\"checkout_cart\";s:1:\"1\";s:29:\"plazathemes_design_footer_top\";s:1:\"1\";s:32:\"plazathemes_design_footer_bottom\";s:1:\"1\";s:19:\"brandslider_general\";s:1:\"1\";s:19:\"sales_email_general\";s:1:\"0\";s:20:\"sales_email_shipment\";s:1:\"0\";s:22:\"customer_account_share\";s:1:\"1\";s:23:\"customer_create_account\";s:1:\"1\";s:17:\"customer_password\";s:1:\"1\";s:18:\"payment_es_account\";s:1:\"1\";s:32:\"payment_es_recommended_solutions\";s:1:\"1\";s:41:\"payment_es_other_paypal_payment_solutions\";s:1:\"0\";s:34:\"payment_es_paypal_group_all_in_one\";s:1:\"1\";s:32:\"payment_es_other_payment_methods\";s:1:\"1\";s:18:\"payment_es_checkmo\";s:1:\"0\";s:23:\"payment_es_banktransfer\";s:1:\"0\";s:25:\"payment_es_cashondelivery\";s:1:\"0\";s:15:\"payment_es_free\";s:1:\"0\";s:24:\"payment_es_purchaseorder\";s:1:\"0\";s:34:\"payment_es_authorizenet_directpost\";s:1:\"0\";s:25:\"trans_email_ident_general\";s:1:\"0\";s:23:\"trans_email_ident_sales\";s:1:\"0\";s:25:\"trans_email_ident_support\";s:1:\"0\";s:25:\"trans_email_ident_custom1\";s:1:\"0\";s:25:\"trans_email_ident_custom2\";s:1:\"0\";s:15:\"contact_contact\";s:1:\"1\";s:13:\"contact_email\";s:1:\"1\";s:31:\"advanced_modules_disable_output\";s:1:\"1\";s:25:\"catalog_productalert_cron\";s:1:\"0\";s:13:\"catalog_price\";s:1:\"0\";s:21:\"catalog_product_video\";s:1:\"0\";s:20:\"catalog_downloadable\";s:1:\"0\";}}',NULL,NULL,'en_US',0,NULL,NULL),(2,'admin','admin','correo@correo.com','Disandat','1ce1382cc06d9fb065094372466a730e4170f96bb986d2f0bba12901723cdd0b:PVAMC3Ft5GOw4TpGgyuJhJVFRFp177BS:1','2020-03-02 23:13:04','2020-03-18 00:59:34','2020-03-18 00:59:34',6,0,1,'N;',NULL,NULL,'en_US',0,NULL,NULL);
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_session`
--

DROP TABLE IF EXISTS `admin_user_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `session_id` varchar(128) NOT NULL COMMENT 'Session id value',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin User ID',
  `status` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Current Session status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `ip` varchar(15) NOT NULL COMMENT 'Remote user IP',
  PRIMARY KEY (`id`),
  KEY `ADMIN_USER_SESSION_SESSION_ID` (`session_id`),
  KEY `ADMIN_USER_SESSION_USER_ID` (`user_id`),
  CONSTRAINT `ADMIN_USER_SESSION_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='Admin User sessions table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user_session`
--

LOCK TABLES `admin_user_session` WRITE;
/*!40000 ALTER TABLE `admin_user_session` DISABLE KEYS */;
INSERT INTO `admin_user_session` VALUES (1,'1ae7tnpi15e4hjk1kc07lbr8q4',1,2,'2020-02-28 15:34:33','2020-03-03 05:05:23','187.189.198.170'),(2,'jr6900328anh84ucrqop1bq4k0',1,2,'2020-02-28 16:13:32','2020-03-03 05:05:23','187.189.198.170'),(3,'gk7d9dgs1bib7cnskqtdtrfk57',1,2,'2020-02-28 17:15:04','2020-03-03 05:05:23','187.189.198.170'),(4,'dg5ba2dldojqalfvp5schipoe3',1,2,'2020-02-28 20:26:07','2020-03-03 05:05:23','187.189.198.170'),(5,'p7nccujsbcavbnpk81u1befr40',1,2,'2020-02-28 21:12:24','2020-03-03 05:05:23','187.189.198.170'),(6,'9ehhtsl7j9sec42h24tsf0atl1',1,2,'2020-02-28 22:44:09','2020-03-03 05:05:23','187.189.198.170'),(7,'ftn8f6487493n5jt0i0ljo4r73',1,2,'2020-03-02 15:30:19','2020-03-03 05:05:23','187.189.198.170'),(8,'lf7fb9bqh00tbf669hgurb6tl4',1,2,'2020-03-02 18:23:15','2020-03-03 05:05:23','187.189.198.170'),(9,'cv4n8fcbj02491u2a0mh063rf1',1,2,'2020-03-02 18:52:54','2020-03-03 05:05:23','187.189.198.170'),(10,'pcs7ck9aal85da42jh19ih2ei0',1,2,'2020-03-02 19:30:29','2020-03-03 05:05:23','187.189.198.170'),(11,'jekq1593i9dvdd4e9v4edu7173',1,2,'2020-03-02 21:03:28','2020-03-02 21:12:33','189.213.163.237'),(12,'o8jt5b3d9hgltgc6rfa40c2l81',1,2,'2020-03-02 21:12:33','2020-03-02 21:27:03','187.189.198.170'),(13,'3lq3metpahbi503lp7pkn0mkv5',1,2,'2020-03-02 21:27:03','2020-03-02 21:38:11','189.213.163.237'),(14,'q4su7dq3askjllv60i5freqcr7',1,2,'2020-03-02 21:38:11','2020-03-03 05:05:23','189.213.163.237'),(15,'rqs8erpraiv85g7a37e00q9dd3',1,2,'2020-03-02 22:14:55','2020-03-02 22:18:41','189.213.163.237'),(16,'424u2agmergepcp2tjdqkfcq93',1,2,'2020-03-02 22:18:41','2020-03-02 22:21:24','187.189.198.170'),(17,'6985jmkmkiusog5r63qri63185',1,2,'2020-03-02 22:21:24','2020-03-02 22:41:57','189.213.163.237'),(18,'017fuem5ve1psnqtvv7238lsh1',1,2,'2020-03-02 22:41:57','2020-03-02 22:51:05','189.213.163.237'),(19,'9ctef75iiubh9tdk8tn70rh4a3',1,2,'2020-03-02 22:51:05','2020-03-02 22:52:27','187.189.198.170'),(20,'4cmak4cjk4u7hc16takm7sfj43',1,2,'2020-03-02 22:52:27','2020-03-02 22:52:42','189.213.163.237'),(21,'nso3qnlhsd2rpn238rk2konh35',1,2,'2020-03-02 22:52:42','2020-03-02 23:00:07','187.189.198.170'),(22,'g9frj81oc3e5c8hlkpcsd1dul3',1,2,'2020-03-02 23:00:07','2020-03-02 23:02:19','189.213.163.237'),(23,'4vn2tremh6qrp4smvb35v6fi21',1,2,'2020-03-02 23:02:19','2020-03-02 23:06:09','187.189.198.170'),(24,'7np6asrhk07848mfssrv367j27',1,2,'2020-03-02 23:06:09','2020-03-02 23:07:21','189.213.163.237'),(25,'ioo997ofp88e60m7tepnlifv46',1,2,'2020-03-02 23:07:21','2020-03-03 05:05:23','187.189.198.170'),(26,'tkorsfj3lgkb5h88eng3tt1ge6',1,2,'2020-03-03 05:05:23','2020-03-03 06:27:54','201.114.112.189'),(27,'6qua9lfjhsmocsavgedj1h04q5',1,2,'2020-03-03 06:27:54','2020-03-03 15:54:54','201.114.112.189'),(28,'or6kvists99s671gflp89mn2b0',1,2,'2020-03-03 15:54:54','2020-03-03 17:41:43','187.189.198.170'),(29,'bgjpvr6ccppq1ke71e4d2d9ss4',1,2,'2020-03-03 17:41:43','2020-03-03 20:16:04','187.189.198.170'),(30,'9p807t35o7hhstb9jliud74ms5',1,2,'2020-03-03 20:16:04','2020-03-04 05:04:59','187.189.198.170'),(31,'36vcivmgnjmm4fv8796231ub90',1,2,'2020-03-04 05:04:59','2020-03-04 15:19:22','189.142.147.89'),(32,'qhhnum8mf7vfdmb80p76kfe8s5',1,2,'2020-03-04 15:19:22','2020-03-04 18:04:21','187.189.198.170'),(33,'m5sebh289g65a042gtbuiivbg5',1,2,'2020-03-04 18:04:21','2020-03-04 18:36:36','201.175.135.138'),(34,'8a5v395lct33usfiojof0mtl15',1,2,'2020-03-04 18:36:36','2020-03-04 19:43:13','187.189.198.170'),(35,'l0u1ff54gedrn8rvjffq230ov0',1,2,'2020-03-04 19:43:13','2020-03-05 15:27:09','187.189.198.170'),(36,'8g2gse8copv91g5j4hvmm5s5f0',1,2,'2020-03-05 15:27:09','2020-03-05 16:16:48','187.189.198.170'),(37,'ci8vq2f2c4ano5hcl7f11p3n33',1,2,'2020-03-05 16:16:48','2020-03-05 21:00:06','187.189.198.170'),(38,'rp1c6s73e49gbfmb8h4qo6bh62',1,2,'2020-03-05 21:00:06','2020-03-06 18:00:47','187.189.198.170'),(39,'ke8rm5dus261dndqsk4r587i35',1,2,'2020-03-06 18:00:47','2020-03-06 18:40:23','187.189.198.170'),(40,'6vbtme752eao4ag5u89oh2d6a0',1,2,'2020-03-06 18:40:22','2020-03-09 18:26:09','187.189.198.170'),(41,'htklnlj15b074sjpqgffo0d2v3',1,0,'2020-03-09 18:26:09','2020-03-09 18:28:33','187.189.91.159'),(42,'ojrlboi74gs8h7pfvl72ca92i3',2,2,'2020-03-09 18:28:43','2020-03-09 18:38:30','187.189.91.159'),(43,'us8qu7ci4m01ojf8nfabtlr7a7',2,2,'2020-03-09 18:38:30','2020-03-09 20:32:09','200.194.11.221'),(44,'5065lqdoqr919hn8o3vcb7u197',2,2,'2020-03-09 20:32:09','2020-03-09 23:40:07','200.194.11.221'),(45,'e89h8anaos8acsn6hgb56uga04',1,2,'2020-03-09 21:23:14','2020-03-09 23:56:48','187.189.91.159'),(46,'c9cpv1d6ssqgg5r0pk0h51q0c3',2,2,'2020-03-09 23:40:07','2020-03-12 20:57:31','200.194.11.221'),(47,'edpj79cl70fenf0e6choinqgr1',1,2,'2020-03-09 23:56:48','2020-03-11 15:30:55','200.194.11.221'),(48,'dmvc3jfbtree6nhj4s865tuf13',1,2,'2020-03-11 15:30:55','2020-03-11 17:35:44','187.189.198.170'),(49,'ui21scbtmmfkdiuldgsufuak31',1,2,'2020-03-11 17:35:44','2020-03-11 18:57:32','187.189.198.170'),(50,'rha4levej1p51i8itjg41nke80',1,2,'2020-03-11 18:57:32','2020-03-11 20:19:01','187.189.198.170'),(51,'fs9l5plh4ha94u6mn2qkf5ffb3',1,2,'2020-03-11 20:19:01','2020-03-11 23:41:29','187.189.198.170'),(52,'g6jmcglnvudrrt11291i678bn6',1,2,'2020-03-11 23:41:29','2020-03-12 02:47:57','187.189.198.170'),(53,'qvb5pg5unkefcshn47hrjj9627',1,2,'2020-03-12 02:47:57','2020-03-12 03:51:39','187.189.91.159'),(54,'cn9u4rguu5p7d37jgni8qageg4',1,2,'2020-03-12 03:51:39','2020-03-12 22:21:32','187.189.91.159'),(55,'90f3scgdu847ieg6dhlm4e9cf1',2,2,'2020-03-12 20:57:31','2020-03-18 00:59:34','200.194.11.221'),(56,'0nsibn6k1m49k7tdnflogmg6k6',1,2,'2020-03-12 22:21:32','2020-03-13 02:14:30','187.189.198.170'),(57,'eca3qu5ajsm3f4j417h7l87c32',1,2,'2020-03-13 02:14:30','2020-03-13 16:35:32','187.189.91.159'),(58,'5p3vj8al3d2ouk38cb0csop771',1,2,'2020-03-13 16:35:32','2020-03-13 18:43:25','187.189.91.159'),(59,'6dqbrfp0dkogqasrq63l6vq0m0',1,2,'2020-03-13 18:43:25','2020-03-13 20:18:16','187.189.91.159'),(60,'nahfjej3le4p955c6c8hkqhg11',1,2,'2020-03-13 20:18:16','2020-03-13 21:47:38','187.189.91.159'),(61,'n8mh2dodg0b32q8ili0ao0m2h6',1,2,'2020-03-13 21:47:38','2020-03-17 18:13:56','187.189.91.159'),(62,'qpg23esii4nqtvnotjgmd5vbf3',1,1,'2020-03-17 18:13:56','2020-03-17 18:49:27','189.142.55.183'),(63,'pats4ibdmm2rb69vom59r7tcm7',2,1,'2020-03-18 00:59:34','2020-03-18 03:34:31','201.141.84.44');
/*!40000 ALTER TABLE `admin_user_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adminnotification_inbox`
--

DROP TABLE IF EXISTS `adminnotification_inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminnotification_inbox` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Notification id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `description` text COMMENT 'Description',
  `url` varchar(255) DEFAULT NULL COMMENT 'Url',
  `is_read` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification read',
  `is_remove` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification might be removed',
  PRIMARY KEY (`notification_id`),
  KEY `ADMINNOTIFICATION_INBOX_SEVERITY` (`severity`),
  KEY `ADMINNOTIFICATION_INBOX_IS_READ` (`is_read`),
  KEY `ADMINNOTIFICATION_INBOX_IS_REMOVE` (`is_remove`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Adminnotification Inbox';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adminnotification_inbox`
--

LOCK TABLES `adminnotification_inbox` WRITE;
/*!40000 ALTER TABLE `adminnotification_inbox` DISABLE KEYS */;
INSERT INTO `adminnotification_inbox` VALUES (1,1,'2020-02-28 16:13:32','Attention! You use an ancient version of Blog Extension. It is strongly recommended to update Blog Extension to the newest version 2.9.2',' \n                                    https://magefan.com/magento2-blog-extension?ancient=2.8.0                                 ','https://magefan.com/magento2-blog-extension?ancient=2.8.0',0,1),(2,4,'2020-02-28 16:13:32','FAQ: How to configure &quot;read more&quot; in your Magento 2 blog post',' If you still don\'t know how to configure read more functionality in your magento 2 blog post, please read this article.\n                    Online documentation for Magento 2 blog extension is also available on our website. http://goo.gl/XOEdpy ','http://magefan.com/blog/add-read-more-tag-to-blog-post-content/?from=notification1',0,0),(3,4,'2020-02-28 16:13:32','NEW Magento 2 Alternate Hreflang Tags Extension with Better Store Switcher\n    ',' Do you manage Magento multi-language store without using Hreflang tags? You should definitely enable them. ','https://magefan.com/magento2-alternate-hreflang-extension',0,0),(4,1,'2020-03-05 21:25:23','Retrieving data. Wait a few seconds and try to cut or copy again.','&quot;We have discovered an error in Magento 2.3.4 that can prevent a shopper from completing an order when using PayPal Express Checkout. The issue occurs for any country where the region field in the address section is a text field (as opposed to a drop-down menu).\nWe have released a hotfix to resolve this issue and recommend that merchants on Magento version 2.3.4 who are also using PayPal Express Checkout download and apply this hotfix as soon as possible to prevent further errors. Please refer to our help center for instructions and troubleshooting. A fix for this issue will be included in Magento 2.3.5.','https://support.magento.com/hc/en-us/articles/360039451972',0,1),(5,1,'2020-03-17 18:13:56','Attention! You use an ancient version of Blog Extension. It is strongly recommended to update Blog Extension to the newest version 2.9.3',' \n                                    https://magefan.com/magento2-blog-extension?ancient=2.8.0                                 ','https://magefan.com/magento2-blog-extension?ancient=2.8.0',0,0);
/*!40000 ALTER TABLE `adminnotification_inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorization_role`
--

DROP TABLE IF EXISTS `authorization_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorization_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Role ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Role ID',
  `tree_level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Tree Level',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Sort Order',
  `role_type` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Role Type',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `user_type` varchar(16) DEFAULT NULL COMMENT 'User Type',
  `role_name` varchar(50) DEFAULT NULL COMMENT 'Role Name',
  PRIMARY KEY (`role_id`),
  KEY `AUTHORIZATION_ROLE_PARENT_ID_SORT_ORDER` (`parent_id`,`sort_order`),
  KEY `AUTHORIZATION_ROLE_TREE_LEVEL` (`tree_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Admin Role Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorization_role`
--

LOCK TABLES `authorization_role` WRITE;
/*!40000 ALTER TABLE `authorization_role` DISABLE KEYS */;
INSERT INTO `authorization_role` VALUES (1,0,1,1,'G',0,'2','Administrators'),(2,1,2,0,'U',1,'2','admin'),(3,1,2,0,'U',2,'2','admin');
/*!40000 ALTER TABLE `authorization_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorization_rule`
--

DROP TABLE IF EXISTS `authorization_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorization_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule ID',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Role ID',
  `resource_id` varchar(255) DEFAULT NULL COMMENT 'Resource ID',
  `privileges` varchar(20) DEFAULT NULL COMMENT 'Privileges',
  `permission` varchar(10) DEFAULT NULL COMMENT 'Permission',
  PRIMARY KEY (`rule_id`),
  KEY `AUTHORIZATION_RULE_RESOURCE_ID_ROLE_ID` (`resource_id`,`role_id`),
  KEY `AUTHORIZATION_RULE_ROLE_ID_RESOURCE_ID` (`role_id`,`resource_id`),
  CONSTRAINT `AUTHORIZATION_RULE_ROLE_ID_AUTHORIZATION_ROLE_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `authorization_role` (`role_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Admin Rule Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorization_rule`
--

LOCK TABLES `authorization_rule` WRITE;
/*!40000 ALTER TABLE `authorization_rule` DISABLE KEYS */;
INSERT INTO `authorization_rule` VALUES (1,1,'Magento_Backend::all',NULL,'allow');
/*!40000 ALTER TABLE `authorization_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `id` varchar(200) NOT NULL COMMENT 'Cache Id',
  `data` mediumblob COMMENT 'Cache Data',
  `create_time` int(11) DEFAULT NULL COMMENT 'Cache Creation Time',
  `update_time` int(11) DEFAULT NULL COMMENT 'Time of Cache Updating',
  `expire_time` int(11) DEFAULT NULL COMMENT 'Cache Expiration Time',
  PRIMARY KEY (`id`),
  KEY `CACHE_EXPIRE_TIME` (`expire_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Caches';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache`
--

LOCK TABLES `cache` WRITE;
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_tag`
--

DROP TABLE IF EXISTS `cache_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache_tag` (
  `tag` varchar(100) NOT NULL COMMENT 'Tag',
  `cache_id` varchar(200) NOT NULL COMMENT 'Cache Id',
  PRIMARY KEY (`tag`,`cache_id`),
  KEY `CACHE_TAG_CACHE_ID` (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Caches';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_tag`
--

LOCK TABLES `cache_tag` WRITE;
/*!40000 ALTER TABLE `cache_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `captcha_log`
--

DROP TABLE IF EXISTS `captcha_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `captcha_log` (
  `type` varchar(32) NOT NULL COMMENT 'Type',
  `value` varchar(32) NOT NULL COMMENT 'Value',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Count',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Update Time',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Count Login Attempts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `captcha_log`
--

LOCK TABLES `captcha_log` WRITE;
/*!40000 ALTER TABLE `captcha_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `captcha_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity`
--

DROP TABLE IF EXISTS `catalog_category_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attriute Set ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Category ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `path` varchar(255) NOT NULL COMMENT 'Tree Path',
  `position` int(11) NOT NULL COMMENT 'Position',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT 'Tree Level',
  `children_count` int(11) NOT NULL COMMENT 'Child Count',
  PRIMARY KEY (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_LEVEL` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_entity`
--

LOCK TABLES `catalog_category_entity` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity` DISABLE KEYS */;
INSERT INTO `catalog_category_entity` VALUES (1,3,0,'2020-02-28 15:31:29','2020-02-28 23:06:49','1',0,0,5),(2,3,1,'2020-02-28 15:31:30','2020-02-28 23:06:49','1/2',1,1,4),(3,3,2,'2020-02-28 20:39:29','2020-02-28 20:39:29','1/2/3',1,2,0),(4,3,2,'2020-02-28 20:45:14','2020-02-28 20:45:14','1/2/4',2,2,0),(5,3,2,'2020-02-28 22:49:23','2020-02-28 22:49:23','1/2/5',3,2,0),(6,3,2,'2020-02-28 23:06:49','2020-02-28 23:06:49','1/2/6',4,2,0);
/*!40000 ALTER TABLE `catalog_category_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_datetime`
--

DROP TABLE IF EXISTS `catalog_category_entity_datetime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_DTIME_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Datetime Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_entity_datetime`
--

LOCK TABLES `catalog_category_entity_datetime` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_datetime` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_datetime` VALUES (1,61,0,4,NULL),(2,62,0,4,NULL),(3,61,0,3,NULL),(4,62,0,3,NULL),(5,61,0,5,NULL),(6,62,0,5,NULL),(7,61,0,6,NULL),(8,62,0,6,NULL);
/*!40000 ALTER TABLE `catalog_category_entity_datetime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_decimal`
--

DROP TABLE IF EXISTS `catalog_category_entity_decimal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_DEC_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Decimal Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_entity_decimal`
--

LOCK TABLES `catalog_category_entity_decimal` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_decimal` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_category_entity_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_int`
--

DROP TABLE IF EXISTS `catalog_category_entity_int`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_INT_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_INT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Integer Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_entity_int`
--

LOCK TABLES `catalog_category_entity_int` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_int` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_int` VALUES (1,69,0,1,1),(2,46,0,2,1),(3,69,0,2,1),(4,46,0,3,1),(5,54,0,3,1),(6,69,0,3,1),(7,70,0,3,0),(8,71,0,3,0),(9,137,0,3,1),(10,140,0,3,1),(11,141,0,3,1),(12,46,0,4,1),(13,54,0,4,1),(14,69,0,4,0),(15,70,0,4,0),(16,71,0,4,0),(17,137,0,4,1),(18,140,0,4,0),(19,141,0,4,1),(20,46,0,5,1),(21,54,0,5,1),(22,69,0,5,0),(23,70,0,5,0),(24,71,0,5,0),(25,137,0,5,1),(26,140,0,5,0),(27,141,0,5,0),(28,46,0,6,1),(29,54,0,6,0),(30,69,0,6,0),(31,70,0,6,0),(32,71,0,6,0),(33,137,0,6,1),(34,140,0,6,0),(35,141,0,6,0),(36,53,0,4,NULL),(37,53,0,3,NULL),(38,53,0,5,NULL),(39,53,0,6,NULL);
/*!40000 ALTER TABLE `catalog_category_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_text`
--

DROP TABLE IF EXISTS `catalog_category_entity_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_TEXT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Text Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_entity_text`
--

LOCK TABLES `catalog_category_entity_text` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_text` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_text` VALUES (1,47,0,6,'<div><img src=\"http://culturamezcal.com/mezcal-3/admin_mezcal/cms/wysiwyg/directive/___directive/e3ttZWRpYSB1cmw9Ind5c2l3eWcvdGllbmRhX2V4cGVyaWVuY2lhcy5qcGcifX0,/key/92f04dcde1f48ba0cb999874fd6d7888e443f57835746e3eaf03b815571b1037/\" alt=\"\" /></div>\r\n<p>Dise&ntilde;amos experiencias personalizadas para grupos. <br /> Cont&aacute;ctanos <a href=\"mailto:info@culturamezcal.mx\">info@culturamezcal.mx</a></p>'),(2,47,0,4,NULL),(3,50,0,4,NULL),(4,51,0,4,NULL),(5,64,0,4,NULL),(6,47,0,3,NULL),(7,50,0,3,NULL),(8,51,0,3,NULL),(9,64,0,3,NULL),(10,47,0,5,NULL),(11,50,0,5,NULL),(12,51,0,5,NULL),(13,64,0,5,NULL),(14,50,0,6,NULL),(15,51,0,6,NULL),(16,64,0,6,NULL);
/*!40000 ALTER TABLE `catalog_category_entity_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_entity_varchar`
--

DROP TABLE IF EXISTS `catalog_category_entity_varchar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_ENTT_VCHR_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='Catalog Category Varchar Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_entity_varchar`
--

LOCK TABLES `catalog_category_entity_varchar` WRITE;
/*!40000 ALTER TABLE `catalog_category_entity_varchar` DISABLE KEYS */;
INSERT INTO `catalog_category_entity_varchar` VALUES (1,45,0,1,'Root Catalog'),(2,45,0,2,'Default Category'),(3,52,0,2,'PRODUCTS'),(4,45,0,3,'Mezcal'),(5,52,0,3,'PRODUCTS_AND_PAGE'),(6,60,0,3,'6'),(7,63,0,3,'2columns-left'),(8,117,0,3,'mezcal'),(9,118,0,3,'mezcal'),(10,45,0,4,'Compra'),(11,52,0,4,'PRODUCTS_AND_PAGE'),(12,117,0,4,'compra'),(13,118,0,4,'compra'),(14,45,0,5,'Accessorios'),(15,52,0,5,'PAGE'),(16,117,0,5,'accesorios'),(17,118,0,5,'accesorios'),(18,45,0,6,'Experiencias'),(19,48,0,6,'culturamezcal_tienda_inicio_03_01_1.jpg'),(20,52,0,6,'PAGE'),(21,117,0,6,'experiencias'),(22,118,0,6,'experiencias'),(24,48,0,4,'culturamezcal_tienda_inicio_03_01_1.jpg'),(25,49,0,4,NULL),(26,60,0,4,'6'),(27,63,0,4,'2columns-left'),(30,48,0,3,'culturamezcal_tienda_inicio_03_01_1.jpg'),(31,49,0,3,NULL),(33,48,0,5,'culturamezcal_tienda_inicio_03_01_1.jpg'),(34,49,0,5,NULL),(35,60,0,5,'6'),(36,63,0,5,'2columns-left'),(38,49,0,6,NULL),(39,60,0,6,'6'),(40,63,0,6,'2columns-left');
/*!40000 ALTER TABLE `catalog_category_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product`
--

DROP TABLE IF EXISTS `catalog_category_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_product` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`entity_id`,`category_id`,`product_id`),
  UNIQUE KEY `CATALOG_CATEGORY_PRODUCT_CATEGORY_ID_PRODUCT_ID` (`category_id`,`product_id`),
  KEY `CATALOG_CATEGORY_PRODUCT_PRODUCT_ID` (`product_id`),
  CONSTRAINT `CAT_CTGR_PRD_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_CTGR_PRD_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Category Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_product`
--

LOCK TABLES `catalog_category_product` WRITE;
/*!40000 ALTER TABLE `catalog_category_product` DISABLE KEYS */;
INSERT INTO `catalog_category_product` VALUES (1,3,1,1),(2,4,1,1),(3,3,2,1),(4,4,2,1),(5,3,3,1),(6,4,3,1),(7,3,4,1),(8,4,4,1),(9,3,5,1),(10,4,5,1),(11,3,6,1),(12,4,6,1),(13,3,7,1),(14,4,7,1),(15,3,8,1),(16,4,8,1),(17,3,9,1),(18,4,9,1),(19,3,10,1),(20,4,10,1),(21,3,11,1),(22,4,11,1),(23,3,12,1),(24,4,12,1),(25,3,13,1),(26,4,13,1),(27,3,14,1),(28,4,14,1),(29,3,15,1),(30,4,15,1),(31,3,16,1),(32,4,16,1),(33,3,17,1),(34,4,17,1),(35,3,18,1),(36,4,18,1),(37,3,19,1),(38,4,19,1),(39,3,20,1),(40,4,20,1),(41,3,21,1),(42,4,21,1),(43,3,22,1),(44,4,22,1),(45,3,23,1),(46,4,23,1),(47,3,24,1),(48,4,24,1),(49,3,25,1),(50,4,25,1),(51,3,26,1),(52,4,26,1),(53,3,27,1),(54,4,27,1),(55,3,28,1),(56,4,28,1),(57,3,29,1),(58,4,29,1),(59,3,30,1),(60,4,30,1),(61,3,31,1),(62,4,31,1),(63,3,32,1),(64,4,32,1),(65,3,33,1),(66,4,33,1),(67,3,34,1),(68,4,34,1),(69,3,35,1),(70,4,35,1),(71,3,36,1),(72,4,36,1);
/*!40000 ALTER TABLE `catalog_category_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product_index`
--

DROP TABLE IF EXISTS `catalog_category_product_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_product_index` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) DEFAULT NULL COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  PRIMARY KEY (`category_id`,`product_id`,`store_id`),
  KEY `CAT_CTGR_PRD_IDX_PRD_ID_STORE_ID_CTGR_ID_VISIBILITY` (`product_id`,`store_id`,`category_id`,`visibility`),
  KEY `CAT_CTGR_PRD_IDX_STORE_ID_CTGR_ID_VISIBILITY_IS_PARENT_POSITION` (`store_id`,`category_id`,`visibility`,`is_parent`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Index';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_product_index`
--

LOCK TABLES `catalog_category_product_index` WRITE;
/*!40000 ALTER TABLE `catalog_category_product_index` DISABLE KEYS */;
INSERT INTO `catalog_category_product_index` VALUES (2,1,1,1,1,4),(2,2,1,1,1,4),(2,3,1,1,1,4),(2,4,1,1,1,4),(2,5,1,1,1,4),(2,6,1,1,1,4),(2,7,1,1,1,4),(2,8,1,1,1,4),(2,9,1,1,1,4),(2,10,1,1,1,4),(2,11,1,1,1,4),(2,12,1,1,1,4),(2,13,1,1,1,4),(2,14,1,1,1,4),(2,15,1,1,1,4),(2,16,1,1,1,4),(2,17,1,1,1,4),(2,18,1,1,1,4),(2,19,1,1,1,4),(2,20,1,1,1,4),(2,21,1,1,1,4),(2,22,1,1,1,4),(2,23,1,1,1,4),(2,24,1,1,1,4),(2,25,1,1,1,4),(2,26,1,1,1,4),(2,27,1,1,1,4),(2,28,1,1,1,4),(2,29,1,1,1,4),(2,30,1,1,1,4),(2,31,1,1,1,4),(2,32,1,1,1,4),(2,33,1,1,1,4),(2,34,1,1,1,4),(2,35,1,1,1,4),(2,36,1,1,1,4),(3,1,1,1,1,4),(3,2,1,1,1,4),(3,3,1,1,1,4),(3,4,1,1,1,4),(3,5,1,1,1,4),(3,6,1,1,1,4),(3,7,1,1,1,4),(3,8,1,1,1,4),(3,9,1,1,1,4),(3,10,1,1,1,4),(3,11,1,1,1,4),(3,12,1,1,1,4),(3,13,1,1,1,4),(3,14,1,1,1,4),(3,15,1,1,1,4),(3,16,1,1,1,4),(3,17,1,1,1,4),(3,18,1,1,1,4),(3,19,1,1,1,4),(3,20,1,1,1,4),(3,21,1,1,1,4),(3,22,1,1,1,4),(3,23,1,1,1,4),(3,24,1,1,1,4),(3,25,1,1,1,4),(3,26,1,1,1,4),(3,27,1,1,1,4),(3,28,1,1,1,4),(3,29,1,1,1,4),(3,30,1,1,1,4),(3,31,1,1,1,4),(3,32,1,1,1,4),(3,33,1,1,1,4),(3,34,1,1,1,4),(3,35,1,1,1,4),(3,36,1,1,1,4),(4,1,1,1,1,4),(4,2,1,1,1,4),(4,3,1,1,1,4),(4,4,1,1,1,4),(4,5,1,1,1,4),(4,6,1,1,1,4),(4,7,1,1,1,4),(4,8,1,1,1,4),(4,9,1,1,1,4),(4,10,1,1,1,4),(4,11,1,1,1,4),(4,12,1,1,1,4),(4,13,1,1,1,4),(4,14,1,1,1,4),(4,15,1,1,1,4),(4,16,1,1,1,4),(4,17,1,1,1,4),(4,18,1,1,1,4),(4,19,1,1,1,4),(4,20,1,1,1,4),(4,21,1,1,1,4),(4,22,1,1,1,4),(4,23,1,1,1,4),(4,24,1,1,1,4),(4,25,1,1,1,4),(4,26,1,1,1,4),(4,27,1,1,1,4),(4,28,1,1,1,4),(4,29,1,1,1,4),(4,30,1,1,1,4),(4,31,1,1,1,4),(4,32,1,1,1,4),(4,33,1,1,1,4),(4,34,1,1,1,4),(4,35,1,1,1,4),(4,36,1,1,1,4);
/*!40000 ALTER TABLE `catalog_category_product_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_category_product_index_tmp`
--

DROP TABLE IF EXISTS `catalog_category_product_index_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_category_product_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  KEY `CAT_CTGR_PRD_IDX_TMP_PRD_ID_CTGR_ID_STORE_ID` (`product_id`,`category_id`,`store_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Indexer Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_category_product_index_tmp`
--

LOCK TABLES `catalog_category_product_index_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_category_product_index_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_category_product_index_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_compare_item`
--

DROP TABLE IF EXISTS `catalog_compare_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_compare_item` (
  `catalog_compare_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Compare Item ID',
  `visitor_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Visitor ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`catalog_compare_item_id`),
  KEY `CATALOG_COMPARE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `CATALOG_COMPARE_ITEM_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  KEY `CATALOG_COMPARE_ITEM_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `CATALOG_COMPARE_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_COMPARE_ITEM_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CATALOG_COMPARE_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CATALOG_COMPARE_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Catalog Compare Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_compare_item`
--

LOCK TABLES `catalog_compare_item` WRITE;
/*!40000 ALTER TABLE `catalog_compare_item` DISABLE KEYS */;
INSERT INTO `catalog_compare_item` VALUES (1,79,NULL,1,1);
/*!40000 ALTER TABLE `catalog_compare_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_eav_attribute`
--

DROP TABLE IF EXISTS `catalog_eav_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `frontend_input_renderer` varchar(255) DEFAULT NULL COMMENT 'Frontend Input Renderer',
  `is_global` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Global',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `is_searchable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable',
  `is_filterable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable',
  `is_comparable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Comparable',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `is_html_allowed_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is HTML Allowed On Front',
  `is_used_for_price_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Price Rules',
  `is_filterable_in_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable In Search',
  `used_in_product_listing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used In Product Listing',
  `used_for_sort_by` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Sorting',
  `apply_to` varchar(255) DEFAULT NULL COMMENT 'Apply To',
  `is_visible_in_advanced_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible In Advanced Search',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_wysiwyg_enabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is WYSIWYG Enabled',
  `is_used_for_promo_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Promo Rules',
  `is_required_in_admin_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Required In Admin Store',
  `is_used_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used in Grid',
  `is_visible_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible in Grid',
  `is_filterable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable in Grid',
  `search_weight` float NOT NULL DEFAULT '1' COMMENT 'Search Weight',
  `additional_data` text COMMENT 'Additional swatch attributes data',
  PRIMARY KEY (`attribute_id`),
  KEY `CATALOG_EAV_ATTRIBUTE_USED_FOR_SORT_BY` (`used_for_sort_by`),
  KEY `CATALOG_EAV_ATTRIBUTE_USED_IN_PRODUCT_LISTING` (`used_in_product_listing`),
  CONSTRAINT `CATALOG_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog EAV Attribute Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_eav_attribute`
--

LOCK TABLES `catalog_eav_attribute` WRITE;
/*!40000 ALTER TABLE `catalog_eav_attribute` DISABLE KEYS */;
INSERT INTO `catalog_eav_attribute` VALUES (45,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(46,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(47,NULL,0,1,0,0,0,0,1,0,0,0,0,NULL,0,0,1,0,0,0,0,0,1,NULL),(48,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(49,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(50,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(51,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(52,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(53,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(54,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(55,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(56,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(57,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(58,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(59,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(60,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(61,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(62,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(63,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(64,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(65,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(66,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(67,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Sortby\\Available',0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(68,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Sortby\\DefaultSortby',0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(69,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(70,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(71,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(72,'Magento\\Catalog\\Block\\Adminhtml\\Category\\Helper\\Pricestep',0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(73,NULL,0,1,1,0,0,0,0,0,0,1,1,NULL,1,0,0,0,0,0,0,0,5,NULL),(74,NULL,1,1,1,0,1,0,0,0,0,0,0,NULL,1,0,0,0,0,0,0,0,6,NULL),(75,NULL,0,1,1,0,1,0,1,0,0,0,0,NULL,1,0,1,0,0,0,0,0,1,NULL),(76,NULL,0,1,1,0,1,0,1,0,0,1,0,NULL,1,0,1,0,0,1,0,0,1,NULL),(77,NULL,1,1,1,1,0,0,0,0,0,1,1,'simple,virtual,bundle,downloadable,configurable',1,0,0,0,0,0,0,0,1,NULL),(78,NULL,1,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,1,1,NULL),(79,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,0,1,NULL),(80,NULL,2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,0,1,NULL),(81,NULL,1,1,0,0,0,0,0,0,0,0,0,'simple,virtual,downloadable',0,0,0,0,0,1,0,1,1,NULL),(82,'Magento\\Catalog\\Block\\Adminhtml\\Product\\Helper\\Form\\Weight',1,1,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,1,1,NULL),(83,NULL,1,1,1,2,1,1,0,0,1,0,0,NULL,1,3,0,0,0,1,0,1,1,NULL),(84,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(85,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(86,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(87,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(88,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(89,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(90,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(91,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(92,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,0,0,0,1,NULL),(93,NULL,1,1,1,1,1,0,0,0,0,0,0,'simple,virtual,configurable',1,0,0,0,0,1,0,1,1,NULL),(94,NULL,2,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(95,NULL,2,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(96,NULL,1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(97,'Magento\\Framework\\Data\\Form\\Element\\Hidden',2,1,1,0,0,0,0,0,0,1,0,NULL,0,0,0,0,1,0,0,0,1,NULL),(98,NULL,1,0,0,0,0,0,0,0,0,0,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,0,0,0,1,NULL),(99,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,1,0,0,0,1,NULL),(100,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(101,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(102,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(103,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(104,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(105,'Magento\\Catalog\\Block\\Adminhtml\\Product\\Helper\\Form\\Category',1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(106,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(107,NULL,1,0,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(108,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(109,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(110,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(111,NULL,0,0,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(112,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(113,NULL,1,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(114,NULL,2,1,0,0,0,0,0,0,0,0,0,'simple,bundle,grouped,configurable',0,0,0,0,0,1,0,1,1,NULL),(115,'Magento\\CatalogInventory\\Block\\Adminhtml\\Form\\Field\\Stock',1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(116,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(117,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(118,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(119,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,1,0,1,1,NULL),(120,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(121,'Magento\\Msrp\\Block\\Adminhtml\\Product\\Helper\\Form\\Type',1,1,0,0,0,0,0,0,0,1,0,'simple,virtual,downloadable,bundle,configurable',0,0,0,0,0,1,0,1,1,NULL),(122,'Magento\\Msrp\\Block\\Adminhtml\\Product\\Helper\\Form\\Type\\Price',2,1,0,0,0,0,0,0,0,1,0,'simple,virtual,downloadable,bundle,configurable',0,0,0,0,0,0,0,0,1,NULL),(123,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(124,NULL,1,1,0,0,0,0,0,0,0,0,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(125,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(126,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(127,NULL,1,1,0,0,0,0,0,0,0,1,0,'bundle',0,0,0,0,0,0,0,0,1,NULL),(128,NULL,1,0,0,0,0,0,0,0,0,1,0,'downloadable',0,0,0,0,0,0,0,0,1,NULL),(129,NULL,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,0,0,0,0,0,0,0,1,NULL),(130,NULL,0,0,0,0,0,0,0,0,0,0,0,'downloadable',0,0,0,0,0,0,0,0,1,NULL),(131,NULL,1,0,0,0,0,0,0,0,0,1,0,'downloadable',0,0,0,0,0,0,0,0,1,NULL),(132,NULL,0,1,0,0,0,0,0,0,0,1,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(133,NULL,2,1,1,0,0,0,0,0,0,1,0,'simple,virtual,bundle,downloadable,configurable',0,0,0,0,0,1,0,1,1,NULL),(134,'Magento\\GiftMessage\\Block\\Adminhtml\\Product\\Helper\\Form\\Config',1,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,1,0,0,1,NULL),(135,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(136,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(137,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(138,NULL,0,1,0,0,0,0,0,0,0,1,0,'simple,virtual,configurable',0,0,0,0,0,1,0,1,1,NULL),(139,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(140,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(141,NULL,0,1,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,1,NULL),(142,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(143,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(144,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(145,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(146,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(148,NULL,1,1,0,0,0,0,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(149,NULL,1,1,0,2,0,0,1,0,1,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(150,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(152,NULL,1,1,1,1,0,1,1,0,1,1,0,NULL,1,3,0,0,0,1,1,1,1,NULL),(153,NULL,0,1,0,2,0,0,1,0,1,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(154,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(155,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(156,NULL,1,1,1,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(157,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(158,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(159,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(160,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(161,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(162,NULL,1,1,0,0,0,0,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(163,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(164,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(165,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(166,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(167,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(168,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(169,NULL,1,1,0,0,0,1,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(170,NULL,1,1,0,0,0,0,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL),(171,NULL,1,1,0,0,0,0,1,0,0,0,0,NULL,0,0,0,0,0,1,1,1,1,NULL);
/*!40000 ALTER TABLE `catalog_eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_bundle_option`
--

DROP TABLE IF EXISTS `catalog_product_bundle_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_bundle_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `type` varchar(255) DEFAULT NULL COMMENT 'Type',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_OPTION_PARENT_ID` (`parent_id`),
  CONSTRAINT `CAT_PRD_BNDL_OPT_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_bundle_option`
--

LOCK TABLES `catalog_product_bundle_option` WRITE;
/*!40000 ALTER TABLE `catalog_product_bundle_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_bundle_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_bundle_option_value`
--

DROP TABLE IF EXISTS `catalog_product_bundle_option_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_bundle_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_BUNDLE_OPTION_VALUE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  CONSTRAINT `CAT_PRD_BNDL_OPT_VAL_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option Value';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_bundle_option_value`
--

LOCK TABLES `catalog_product_bundle_option_value` WRITE;
/*!40000 ALTER TABLE `catalog_product_bundle_option_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_bundle_option_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_bundle_price_index`
--

DROP TABLE IF EXISTS `catalog_product_bundle_price_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_bundle_price_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `min_price` decimal(12,4) NOT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) NOT NULL COMMENT 'Max Price',
  PRIMARY KEY (`entity_id`,`website_id`,`customer_group_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_BNDL_PRICE_IDX_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Price Index';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_bundle_price_index`
--

LOCK TABLES `catalog_product_bundle_price_index` WRITE;
/*!40000 ALTER TABLE `catalog_product_bundle_price_index` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_bundle_price_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_bundle_selection`
--

DROP TABLE IF EXISTS `catalog_product_bundle_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_bundle_selection` (
  `selection_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Selection Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `parent_product_id` int(10) unsigned NOT NULL COMMENT 'Parent Product Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  `selection_qty` decimal(12,4) DEFAULT NULL COMMENT 'Selection Qty',
  `selection_can_change_qty` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Selection Can Change Qty',
  PRIMARY KEY (`selection_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_OPTION_ID` (`option_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_PRODUCT_ID` (`product_id`),
  CONSTRAINT `CAT_PRD_BNDL_SELECTION_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_BNDL_SELECTION_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_bundle_selection`
--

LOCK TABLES `catalog_product_bundle_selection` WRITE;
/*!40000 ALTER TABLE `catalog_product_bundle_selection` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_bundle_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_bundle_selection_price`
--

DROP TABLE IF EXISTS `catalog_product_bundle_selection_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_bundle_selection_price` (
  `selection_id` int(10) unsigned NOT NULL COMMENT 'Selection Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  PRIMARY KEY (`selection_id`,`website_id`),
  KEY `CATALOG_PRODUCT_BUNDLE_SELECTION_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CAT_PRD_BNDL_SELECTION_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_DCF37523AA05D770A70AA4ED7C2616E4` FOREIGN KEY (`selection_id`) REFERENCES `catalog_product_bundle_selection` (`selection_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection Price';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_bundle_selection_price`
--

LOCK TABLES `catalog_product_bundle_selection_price` WRITE;
/*!40000 ALTER TABLE `catalog_product_bundle_selection_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_bundle_selection_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_bundle_stock_index`
--

DROP TABLE IF EXISTS `catalog_product_bundle_stock_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_bundle_stock_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `stock_status` smallint(6) DEFAULT '0' COMMENT 'Stock Status',
  PRIMARY KEY (`entity_id`,`website_id`,`stock_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Stock Index';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_bundle_stock_index`
--

LOCK TABLES `catalog_product_bundle_stock_index` WRITE;
/*!40000 ALTER TABLE `catalog_product_bundle_stock_index` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_bundle_stock_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity`
--

DROP TABLE IF EXISTS `catalog_product_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set ID',
  `type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'Type ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `has_options` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Has Options',
  `required_options` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required Options',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  PRIMARY KEY (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_ATTRIBUTE_SET_ID` (`attribute_set_id`),
  KEY `CATALOG_PRODUCT_ENTITY_SKU` (`sku`),
  CONSTRAINT `CAT_PRD_ENTT_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity`
--

LOCK TABLES `catalog_product_entity` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity` DISABLE KEYS */;
INSERT INTO `catalog_product_entity` VALUES (1,9,'simple','MT01',1,1,'2020-03-02 17:16:03','2020-03-11 22:28:50'),(2,9,'simple','MX01',1,1,'2020-03-02 22:22:21','2020-03-11 19:05:57'),(3,9,'simple','SP01',1,1,'2020-03-09 19:04:40','2020-03-11 22:47:22'),(4,9,'simple','SP02',1,1,'2020-03-09 21:08:08','2020-03-11 22:51:18'),(5,9,'simple','SP03',1,1,'2020-03-09 23:51:56','2020-03-11 22:53:04'),(6,9,'simple','MY02',1,1,'2020-03-10 00:01:17','2020-03-11 22:54:32'),(7,9,'simple','MX02',1,1,'2020-03-10 01:05:55','2020-03-11 22:55:57'),(8,9,'simple','CH01',1,1,'2020-03-10 01:09:58','2020-03-11 22:57:13'),(9,9,'simple','CH02',1,1,'2020-03-10 01:13:33','2020-03-11 22:58:56'),(10,9,'simple','CH04',0,0,'2020-03-12 21:08:08','2020-03-12 21:08:08'),(11,9,'simple','CH05',0,0,'2020-03-12 21:12:41','2020-03-12 21:12:41'),(12,9,'simple','CH06',0,0,'2020-03-12 21:22:15','2020-03-12 21:22:15'),(13,9,'simple','BX01',0,0,'2020-03-12 21:38:21','2020-03-12 21:38:21'),(14,9,'simple','BX02',0,0,'2020-03-12 21:45:46','2020-03-12 21:45:46'),(15,9,'simple','BX03',0,0,'2020-03-12 22:36:00','2020-03-12 22:36:00'),(16,9,'simple','BX04',0,0,'2020-03-12 23:18:01','2020-03-12 23:18:01'),(17,9,'simple','IB1',0,0,'2020-03-12 23:47:59','2020-03-12 23:47:59'),(18,9,'simple','DN01',0,0,'2020-03-12 23:52:23','2020-03-12 23:52:23'),(19,9,'simple','MN01',0,0,'2020-03-12 23:56:23','2020-03-12 23:56:23'),(20,9,'simple','MN02',0,0,'2020-03-13 00:00:13','2020-03-13 00:00:13'),(21,9,'simple','MN03',0,0,'2020-03-13 00:03:50','2020-03-13 00:03:50'),(22,9,'simple','MN04',0,0,'2020-03-13 00:16:21','2020-03-13 00:16:21'),(23,9,'simple','PS01',0,0,'2020-03-18 01:07:00','2020-03-18 01:07:00'),(24,9,'simple','PS02',0,0,'2020-03-18 01:10:38','2020-03-18 01:10:38'),(25,9,'simple','PS03',0,0,'2020-03-18 01:15:17','2020-03-18 01:15:17'),(26,9,'simple','Pescador De Sueños Coyote',0,0,'2020-03-18 01:19:37','2020-03-18 01:19:37'),(27,9,'simple','PS05',0,0,'2020-03-18 01:24:01','2020-03-18 01:24:01'),(28,9,'simple','PS06',0,0,'2020-03-18 02:08:07','2020-03-18 02:08:07'),(29,9,'simple','PD01',0,0,'2020-03-18 02:15:15','2020-03-18 02:15:15'),(30,9,'simple','PD02',0,0,'2020-03-18 02:18:52','2020-03-18 02:18:52'),(31,9,'simple','NA01',0,0,'2020-03-18 02:35:53','2020-03-18 02:35:53'),(32,9,'simple','IB01',0,0,'2020-03-18 02:41:25','2020-03-18 02:41:25'),(33,9,'simple','MCH01',0,0,'2020-03-18 02:56:48','2020-03-18 02:56:48'),(34,9,'simple','HS01',0,0,'2020-03-18 03:00:19','2020-03-18 03:00:19'),(35,9,'simple','CH03',0,0,'2020-03-18 03:07:28','2020-03-18 03:07:28'),(36,9,'simple','CH10',0,0,'2020-03-18 03:22:18','2020-03-18 03:22:18');
/*!40000 ALTER TABLE `catalog_product_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_datetime`
--

DROP TABLE IF EXISTS `catalog_product_entity_datetime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_DTIME_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Datetime Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_datetime`
--

LOCK TABLES `catalog_product_entity_datetime` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_datetime` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_datetime` VALUES (1,79,0,2,NULL),(2,80,0,2,NULL),(3,94,0,2,NULL),(4,95,0,2,NULL),(5,101,0,2,NULL),(6,102,0,2,NULL),(7,79,0,1,NULL),(8,80,0,1,NULL),(9,94,0,1,NULL),(10,95,0,1,NULL),(11,101,0,1,NULL),(12,102,0,1,NULL),(13,79,0,3,NULL),(14,80,0,3,NULL),(15,94,0,3,NULL),(16,95,0,3,NULL),(17,101,0,3,NULL),(18,102,0,3,NULL),(19,79,0,5,NULL),(20,80,0,5,NULL),(21,94,0,5,NULL),(22,95,0,5,NULL),(23,101,0,5,NULL),(24,102,0,5,NULL),(25,79,0,6,NULL),(26,80,0,6,NULL),(27,94,0,6,NULL),(28,95,0,6,NULL),(29,101,0,6,NULL),(30,102,0,6,NULL),(31,79,0,4,NULL),(32,80,0,4,NULL),(33,94,0,4,NULL),(34,95,0,4,NULL),(35,101,0,4,NULL),(36,102,0,4,NULL),(37,79,0,7,NULL),(38,80,0,7,NULL),(39,94,0,7,NULL),(40,95,0,7,NULL),(41,101,0,7,NULL),(42,102,0,7,NULL),(43,79,0,8,NULL),(44,80,0,8,NULL),(45,94,0,8,NULL),(46,95,0,8,NULL),(47,101,0,8,NULL),(48,102,0,8,NULL),(49,79,0,9,NULL),(50,80,0,9,NULL),(51,94,0,9,NULL),(52,95,0,9,NULL),(53,101,0,9,NULL),(54,102,0,9,NULL),(55,79,0,15,NULL),(56,80,0,15,NULL),(57,94,0,15,NULL),(58,95,0,15,NULL),(59,101,0,15,NULL),(60,102,0,15,NULL),(61,79,0,11,NULL),(62,80,0,11,NULL),(63,94,0,11,NULL),(64,95,0,11,NULL),(65,101,0,11,NULL),(66,102,0,11,NULL),(67,79,0,28,NULL),(68,80,0,28,NULL),(69,94,0,28,NULL),(70,95,0,28,NULL),(71,101,0,28,NULL),(72,102,0,28,NULL),(73,79,0,31,NULL),(74,80,0,31,NULL),(75,94,0,31,NULL),(76,95,0,31,NULL),(77,101,0,31,NULL),(78,102,0,31,NULL);
/*!40000 ALTER TABLE `catalog_product_entity_datetime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_decimal`
--

DROP TABLE IF EXISTS `catalog_product_entity_decimal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_DEC_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Decimal Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_decimal`
--

LOCK TABLES `catalog_product_entity_decimal` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_decimal` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_decimal` VALUES (1,77,0,1,39.8000),(2,82,0,1,NULL),(3,77,0,2,24.9500),(4,78,0,2,NULL),(5,81,0,2,NULL),(6,82,0,2,NULL),(7,78,0,1,NULL),(8,81,0,1,NULL),(9,77,0,3,27.0000),(10,78,0,3,NULL),(11,81,0,3,NULL),(12,82,0,3,NULL),(13,77,0,4,39.7000),(14,77,0,5,12.4500),(15,78,0,5,NULL),(16,81,0,5,NULL),(17,82,0,5,NULL),(18,77,0,6,39.0000),(19,78,0,6,NULL),(20,81,0,6,NULL),(21,82,0,6,NULL),(22,77,0,7,12.5000),(23,77,0,8,55.0000),(24,77,0,9,45.0000),(25,78,0,4,NULL),(26,81,0,4,NULL),(27,82,0,4,NULL),(28,78,0,7,NULL),(29,81,0,7,NULL),(30,82,0,7,NULL),(31,78,0,8,NULL),(32,81,0,8,NULL),(33,82,0,8,NULL),(34,78,0,9,NULL),(35,81,0,9,NULL),(36,82,0,9,NULL),(37,77,0,10,28.0000),(38,77,0,11,54.0000),(39,77,0,12,48.0000),(40,77,0,13,34.0000),(41,77,0,14,42.4400),(42,77,0,15,50.8500),(43,78,0,15,NULL),(44,81,0,15,NULL),(45,82,0,15,NULL),(46,77,0,16,60.1400),(47,77,0,17,49.0000),(48,77,0,18,121.6300),(49,77,0,19,70.0000),(50,77,0,20,81.2500),(51,77,0,21,81.2500),(52,77,0,22,70.0000),(53,78,0,11,NULL),(54,81,0,11,NULL),(55,82,0,11,NULL),(56,77,0,23,148.2400),(57,77,0,24,126.3200),(58,77,0,25,136.8200),(59,77,0,26,126.5800),(60,77,0,27,137.2900),(61,77,0,28,136.5000),(62,78,0,28,NULL),(63,81,0,28,NULL),(64,82,0,28,NULL),(65,77,0,29,51.4100),(66,77,0,30,70.5600),(67,77,0,31,0.0000),(68,78,0,31,NULL),(69,81,0,31,NULL),(70,82,0,31,NULL),(71,77,0,32,0.0000),(72,77,0,33,0.0000),(73,77,0,34,0.0000),(74,77,0,35,75.1000),(75,77,0,36,64.0000);
/*!40000 ALTER TABLE `catalog_product_entity_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_gallery`
--

DROP TABLE IF EXISTS `catalog_product_entity_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_gallery` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_GLR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Gallery Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_gallery`
--

LOCK TABLES `catalog_product_entity_gallery` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_entity_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_int`
--

DROP TABLE IF EXISTS `catalog_product_entity_int`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_INT_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_INT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Integer Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_int`
--

LOCK TABLES `catalog_product_entity_int` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_int` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_int` VALUES (1,83,0,1,15),(2,97,0,1,1),(3,99,0,1,4),(4,115,0,1,1),(5,133,0,1,2),(6,138,0,1,1),(7,149,0,1,8),(8,97,0,2,1),(9,99,0,2,4),(10,115,0,2,1),(11,133,0,2,2),(12,138,0,2,1),(13,83,0,2,18),(14,149,0,2,8),(15,170,0,1,1),(16,83,0,3,16),(17,97,0,3,1),(18,99,0,3,4),(19,115,0,3,1),(20,133,0,3,2),(21,138,0,3,1),(22,149,0,3,8),(23,170,0,3,0),(24,170,0,2,1),(25,83,0,4,16),(26,97,0,4,1),(27,99,0,4,4),(28,115,0,4,1),(29,133,0,4,2),(30,138,0,4,1),(31,149,0,4,8),(32,170,0,4,0),(33,83,0,5,16),(34,97,0,5,1),(35,99,0,5,4),(36,115,0,5,1),(37,133,0,5,2),(38,138,0,5,1),(39,149,0,5,8),(40,170,0,5,1),(41,97,0,6,1),(42,99,0,6,4),(43,115,0,6,1),(44,133,0,6,2),(45,138,0,6,1),(46,170,0,6,0),(47,83,0,6,17),(48,149,0,6,7),(49,83,0,7,18),(50,97,0,7,1),(51,99,0,7,4),(52,115,0,7,1),(53,133,0,7,2),(54,138,0,7,1),(55,149,0,7,8),(56,170,0,7,0),(57,83,0,8,19),(58,97,0,8,1),(59,99,0,8,4),(60,115,0,8,1),(61,133,0,8,2),(62,138,0,8,1),(63,149,0,8,8),(64,170,0,8,0),(65,83,0,9,19),(66,97,0,9,1),(67,99,0,9,4),(68,115,0,9,1),(69,133,0,9,2),(70,138,0,9,0),(71,149,0,9,8),(72,170,0,9,0),(73,83,0,10,19),(74,97,0,10,1),(75,99,0,10,4),(76,115,0,10,1),(77,133,0,10,2),(78,138,0,10,1),(79,149,0,10,8),(80,170,0,10,0),(81,83,0,11,19),(82,97,0,11,1),(83,99,0,11,4),(84,115,0,11,1),(85,133,0,11,2),(86,138,0,11,1),(87,149,0,11,8),(88,170,0,11,1),(89,83,0,12,19),(90,97,0,12,1),(91,99,0,12,4),(92,115,0,12,1),(93,133,0,12,2),(94,138,0,12,1),(95,149,0,12,8),(96,170,0,12,0),(97,83,0,13,20),(98,97,0,13,1),(99,99,0,13,4),(100,115,0,13,1),(101,133,0,13,2),(102,138,0,13,1),(103,149,0,13,8),(104,170,0,13,0),(105,83,0,14,20),(106,97,0,14,1),(107,99,0,14,4),(108,115,0,14,1),(109,133,0,14,2),(110,138,0,14,1),(111,149,0,14,8),(112,170,0,14,0),(113,97,0,15,1),(114,99,0,15,4),(115,115,0,15,1),(116,133,0,15,2),(117,138,0,15,1),(118,170,0,15,0),(119,83,0,15,20),(120,149,0,15,8),(121,83,0,16,20),(122,97,0,16,1),(123,99,0,16,4),(124,115,0,16,1),(125,133,0,16,2),(126,138,0,16,1),(127,149,0,16,8),(128,170,0,16,0),(129,83,0,17,21),(130,97,0,17,1),(131,99,0,17,4),(132,115,0,17,1),(133,133,0,17,2),(134,138,0,17,1),(135,149,0,17,8),(136,170,0,17,0),(137,83,0,18,22),(138,97,0,18,1),(139,99,0,18,4),(140,115,0,18,1),(141,133,0,18,2),(142,138,0,18,1),(143,149,0,18,6),(144,170,0,18,0),(145,83,0,19,23),(146,97,0,19,1),(147,99,0,19,4),(148,115,0,19,1),(149,133,0,19,2),(150,138,0,19,1),(151,149,0,19,8),(152,170,0,19,0),(153,83,0,20,23),(154,97,0,20,1),(155,99,0,20,4),(156,115,0,20,1),(157,133,0,20,2),(158,138,0,20,1),(159,149,0,20,8),(160,170,0,20,0),(161,83,0,21,23),(162,97,0,21,1),(163,99,0,21,4),(164,115,0,21,1),(165,133,0,21,2),(166,138,0,21,1),(167,149,0,21,8),(168,170,0,21,0),(169,83,0,22,23),(170,97,0,22,1),(171,99,0,22,4),(172,115,0,22,1),(173,133,0,22,2),(174,138,0,22,1),(175,149,0,22,8),(176,170,0,22,0),(177,83,0,23,24),(178,97,0,23,1),(179,99,0,23,4),(180,115,0,23,1),(181,133,0,23,2),(182,138,0,23,1),(183,149,0,23,8),(184,170,0,23,0),(185,83,0,24,24),(186,97,0,24,1),(187,99,0,24,4),(188,115,0,24,1),(189,133,0,24,2),(190,138,0,24,1),(191,149,0,24,8),(192,170,0,24,0),(193,83,0,25,24),(194,97,0,25,1),(195,99,0,25,4),(196,115,0,25,1),(197,133,0,25,2),(198,138,0,25,1),(199,149,0,25,8),(200,170,0,25,0),(201,83,0,26,24),(202,97,0,26,1),(203,99,0,26,4),(204,115,0,26,1),(205,133,0,26,2),(206,138,0,26,1),(207,149,0,26,8),(208,170,0,26,0),(209,83,0,27,24),(210,97,0,27,1),(211,99,0,27,4),(212,115,0,27,1),(213,133,0,27,2),(214,138,0,27,1),(215,149,0,27,8),(216,170,0,27,0),(217,97,0,28,1),(218,99,0,28,4),(219,115,0,28,1),(220,133,0,28,2),(221,138,0,28,1),(222,170,0,28,0),(223,83,0,28,24),(224,149,0,28,8),(225,83,0,29,25),(226,97,0,29,1),(227,99,0,29,4),(228,115,0,29,1),(229,133,0,29,2),(230,138,0,29,1),(231,149,0,29,8),(232,170,0,29,0),(233,83,0,30,25),(234,97,0,30,1),(235,99,0,30,4),(236,115,0,30,1),(237,133,0,30,2),(238,138,0,30,1),(239,149,0,30,8),(240,170,0,30,0),(241,83,0,31,26),(242,97,0,31,1),(243,99,0,31,4),(244,115,0,31,1),(245,133,0,31,2),(246,138,0,31,1),(247,149,0,31,8),(248,170,0,31,0),(249,83,0,32,21),(250,97,0,32,1),(251,99,0,32,4),(252,115,0,32,1),(253,133,0,32,2),(254,138,0,32,1),(255,149,0,32,8),(256,170,0,32,0),(257,83,0,33,27),(258,97,0,33,1),(259,99,0,33,4),(260,115,0,33,1),(261,133,0,33,2),(262,138,0,33,1),(263,149,0,33,7),(264,170,0,33,0),(265,83,0,34,28),(266,97,0,34,1),(267,99,0,34,4),(268,115,0,34,1),(269,133,0,34,2),(270,138,0,34,1),(271,149,0,34,8),(272,170,0,34,0),(273,83,0,35,19),(274,97,0,35,1),(275,99,0,35,4),(276,115,0,35,1),(277,133,0,35,2),(278,138,0,35,1),(279,149,0,35,8),(280,170,0,35,0),(281,83,0,36,19),(282,97,0,36,1),(283,99,0,36,4),(284,115,0,36,1),(285,133,0,36,2),(286,138,0,36,1),(287,149,0,36,6),(288,170,0,36,0);
/*!40000 ALTER TABLE `catalog_product_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_media_gallery` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  `media_type` varchar(32) NOT NULL DEFAULT 'image' COMMENT 'Media entry type',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visibility status',
  PRIMARY KEY (`value_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_media_gallery`
--

LOCK TABLES `catalog_product_entity_media_gallery` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery` VALUES (1,90,'/m/t/mt01.jpg','image',0),(2,90,'/m/x/mx01_2.jpg','image',0),(3,90,'/s/p/sp01.jpg','image',0),(4,90,'/s/p/sp02.jpg','image',0),(5,90,'/s/p/sp03.jpg','image',0),(6,90,'/m/y/my02.jpg','image',0),(7,90,'/m/x/mx02.jpg','image',0),(8,90,'/c/h/ch01.jpg','image',0),(9,90,'/c/h/ch02.jpg','image',0),(10,90,'/c/h/ch04.jpg','image',0),(11,90,'/c/h/ch05.jpg','image',0),(12,90,'/c/h/ch06.jpg','image',0),(13,90,'/b/x/bx01.jpg','image',0),(14,90,'/b/x/bx02.jpg','image',0),(15,90,'/b/x/bx03.jpg','image',0),(16,90,'/b/x/bx04.jpg','image',0),(17,90,'/i/b/ib02.jpg','image',0),(18,90,'/d/n/dn01.jpg','image',0),(19,90,'/m/n/mn01.jpg','image',0),(20,90,'/m/n/mn02.jpg','image',0),(21,90,'/m/n/mn03.jpg','image',0),(22,90,'/m/n/mn04.jpg','image',0),(23,90,'/p/s/ps01.jpg','image',0),(24,90,'/p/s/ps02.jpg','image',0),(25,90,'/p/s/ps03.jpg','image',0),(26,90,'/p/s/ps04.jpg','image',0),(27,90,'/p/s/ps05.jpg','image',0),(28,90,'/p/s/ps06.jpg','image',0),(29,90,'/p/d/pd01.jpg','image',0),(30,90,'/p/d/pd02.jpg','image',0),(31,90,'/n/a/na01.jpg','image',0),(32,90,'/i/b/ib01.jpg','image',0),(33,90,'/m/c/mch01.jpg','image',0),(34,90,'/h/s/hs01.jpg','image',0),(35,90,'/c/h/ch06_1.jpg','image',0),(36,90,'/c/h/ch06_2.jpg','image',0);
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_media_gallery_value` (
  `value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Value ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Disabled',
  `record_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Record Id',
  PRIMARY KEY (`record_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_ENTITY_ID` (`entity_id`),
  KEY `CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_VALUE_ID` (`value_id`),
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_VAL_ID_CAT_PRD_ENTT_MDA_GLR_VAL_ID` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Value Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_media_gallery_value`
--

LOCK TABLES `catalog_product_entity_media_gallery_value` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery_value` VALUES (2,0,2,NULL,1,0,33),(1,0,1,NULL,1,0,34),(3,0,3,NULL,1,0,35),(4,0,4,NULL,1,0,36),(5,0,5,NULL,1,0,37),(6,0,6,NULL,1,0,38),(7,0,7,NULL,1,0,39),(8,0,8,NULL,1,0,40),(9,0,9,NULL,1,0,41),(10,0,10,NULL,1,0,42),(12,0,12,NULL,1,0,44),(13,0,13,NULL,1,0,45),(14,0,14,NULL,1,0,46),(15,0,15,NULL,1,0,48),(16,0,16,NULL,1,0,49),(17,0,17,NULL,1,0,50),(18,0,18,NULL,1,0,51),(19,0,19,NULL,1,0,52),(20,0,20,NULL,1,0,53),(21,0,21,NULL,1,0,54),(22,0,22,NULL,1,0,55),(11,0,11,NULL,1,0,56),(23,0,23,NULL,1,0,57),(24,0,24,NULL,1,0,58),(25,0,25,NULL,1,0,59),(26,0,26,NULL,1,0,60),(27,0,27,NULL,1,0,61),(28,0,28,NULL,1,0,63),(29,0,29,NULL,1,0,64),(30,0,30,NULL,1,0,65),(31,0,31,NULL,1,0,67),(32,0,32,NULL,1,0,68),(33,0,33,NULL,1,0,69),(34,0,34,NULL,1,0,70),(35,0,35,NULL,1,0,71),(36,0,36,NULL,1,0,72);
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value_to_entity`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value_to_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_media_gallery_value_to_entity` (
  `value_id` int(10) unsigned NOT NULL COMMENT 'Value media Entry ID',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Product entity ID',
  UNIQUE KEY `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_VAL_ID_ENTT_ID` (`value_id`,`entity_id`),
  KEY `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` (`entity_id`),
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_TO_ENTT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A6C6C8FAA386736921D3A7C4B50B1185` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Media value to Product entity table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_media_gallery_value_to_entity`
--

LOCK TABLES `catalog_product_entity_media_gallery_value_to_entity` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value_to_entity` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_media_gallery_value_to_entity` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12),(13,13),(14,14),(15,15),(16,16),(17,17),(18,18),(19,19),(20,20),(21,21),(22,22),(23,23),(24,24),(25,25),(26,26),(27,27),(28,28),(29,29),(30,30),(31,31),(32,32),(33,33),(34,34),(35,35),(36,36);
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value_to_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_media_gallery_value_video`
--

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_media_gallery_value_video` (
  `value_id` int(10) unsigned NOT NULL COMMENT 'Media Entity ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `provider` varchar(32) DEFAULT NULL COMMENT 'Video provider ID',
  `url` text COMMENT 'Video URL',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `description` text COMMENT 'Page Meta Description',
  `metadata` text COMMENT 'Video meta data',
  UNIQUE KEY `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_VAL_ID_STORE_ID` (`value_id`,`store_id`),
  KEY `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_STORE_ID_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `CAT_PRD_ENTT_MDA_GLR_VAL_VIDEO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_6FDF205946906B0E653E60AA769899F8` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Video Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_media_gallery_value_video`
--

LOCK TABLES `catalog_product_entity_media_gallery_value_video` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_entity_media_gallery_value_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_text`
--

DROP TABLE IF EXISTS `catalog_product_entity_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_TEXT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Text Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_text`
--

LOCK TABLES `catalog_product_entity_text` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_text` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_text` VALUES (1,75,0,1,'<p>Hace muchos a&ntilde;os seg&uacute;n la leyenda un destello ilumin&oacute; el cielo de la sierra sur de Oaxaca, se escuch&oacute; un rugido y se vieron caer &ldquo;ojitos de fuego&rdquo; por todos lados. Uno de ellos hizo un peque&ntilde;o cr&aacute;ter que un maestro mezcalillero decidi&oacute; usar para cocer sus pi&ntilde;as de espadin y produjo as&iacute;, el mejor mezcal que la regi&oacute;n jam&aacute;s hab&iacute;a probado.<br />&ldquo; C&oacute;mo lo hizo?&rdquo; le preguntaban y el viejo solamente respond&iacute;a: &ldquo;cay&oacute; del cielo&rdquo;.<br />As&iacute; como pasa con el vino, las variedades tienen propiedades muy distintas una de la otra las cuales producen una increible gama de sabores y aromas y el terreno y la mano del maestro mezcalillero tambi&eacute;n juegan un papel fundamental en las caracter&iacute;sticas y personalidad del producto final. Mezcal Meteoro est&aacute; hecho 100% de Agave Espad&iacute;n.</p>'),(2,76,0,1,'<p>Mezcal Meteoro</p>'),(3,85,0,1,'Meteoro'),(4,85,0,2,'Mexxico Joven'),(5,75,0,2,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;MeXXIco es una empresa 100% mexicana, fundada con el objetivo de producir un mezcal de la m&aacute;s alta calidad.\\nNuestro logotipo, inspirado en la historia constituye el simbolo de la majestuosidad y de la victoria, que nos representa como empresa.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">MeXXIco es una empresa 100% mexicana, fundada con el objetivo de producir un mezcal de la m&aacute;s alta calidad.<br />Nuestro logotipo, inspirado en la historia constituye el simbolo de la majestuosidad y de la victoria, que nos representa como empresa.</span></p>'),(6,76,0,2,NULL),(7,103,0,2,NULL),(8,103,0,1,NULL),(9,85,0,3,'Sin Piedad 44'),(10,75,0,3,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.\\nDado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.\\nLa mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.<br />Dado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.<br />La mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.</span></p>'),(11,76,0,3,NULL),(12,103,0,3,NULL),(13,75,0,4,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.\\nDado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.\\nLa mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.<br />Dado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.<br />La mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.</span></p>'),(14,85,0,4,'Sin Piedad Cirial'),(15,75,0,5,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.\\nDado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.\\nLa mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.<br />Dado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.<br />La mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.</span></p>'),(16,85,0,5,'Sin Piedad 44 “Petaca”'),(17,76,0,5,NULL),(18,103,0,5,NULL),(19,75,0,6,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;El mezcal resurge como parte vital en la comida gourmet en respuesta al redescubrimiento de las tradiciones culinarias mexicanas. A trav&eacute;s de nuestro mezcal,\\nMayalen Agua de Juventud preserva el patrimonio incrustado en las costumbres\\ngeneracionales a&uacute;n vivas en el estado de Guerrero.\\nNuestro producto es 100% artesanal y org&aacute;nico. Las condiciones clim&aacute;ticas, el agua, y la\\ntierra conjugan un efecto directo en la calidad final del mezcal. Por esta raz&oacute;n, la ubicaci&oacute;n\\nde nuestro palenque es ideal, toda vez que contamos con acceso al agua de manantial.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">El mezcal resurge como parte vital en la comida gourmet en respuesta al redescubrimiento de las tradiciones culinarias mexicanas. A trav&eacute;s de nuestro mezcal,<br />Mayalen Agua de Juventud preserva el patrimonio incrustado en las costumbres generacionales a&uacute;n vivas en el estado de Guerrero.<br />Nuestro producto es 100% artesanal y org&aacute;nico. Las condiciones clim&aacute;ticas, el agua, y la tierra conjugan un efecto directo en la calidad final del mezcal. Por esta raz&oacute;n, la ubicaci&oacute;n de nuestro palenque es ideal, toda vez que contamos con acceso al agua de manantial.</span></p>'),(20,85,0,6,'Mayalen Etiqueta Negra'),(21,76,0,6,NULL),(22,103,0,6,NULL),(23,75,0,7,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;MeXXIco es una empresa 100% mexicana, fundada con el objetivo de producir un mezcal de la m&aacute;s alta calidad.\\nNuestro logotipo, inspirado en la historia constituye el simbolo de la majestuosidad y de la victoria, que nos representa como empresa.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">MeXXIco es una empresa 100% mexicana, fundada con el objetivo de producir un mezcal de la m&aacute;s alta calidad.<br />Nuestro logotipo, inspirado en la historia constituye el simbolo de la majestuosidad y de la victoria, que nos representa como empresa.</span></p>'),(24,85,0,7,'Mexxico “Petaca”'),(25,75,0,8,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.</span></p>'),(26,85,0,8,'Mezcal Chaneque Madrecuixe'),(27,75,0,9,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.</span></p>'),(28,85,0,9,'Mezcal Chaneque Tobaziche'),(29,76,0,4,NULL),(30,103,0,4,NULL),(31,76,0,7,NULL),(32,103,0,7,NULL),(33,76,0,8,NULL),(34,103,0,8,NULL),(35,76,0,9,NULL),(36,103,0,9,NULL),(37,75,0,10,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.</span></p>'),(38,85,0,10,'Mezcal Chaneque Espadín'),(39,75,0,11,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.</span></p>'),(40,85,0,11,'Mezcal Chaneque Tobalá Silvestre'),(41,75,0,12,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.</span></p>'),(42,85,0,12,'Mezcal Chaneque Ensamble'),(43,75,0,13,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.\\nBRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.\\nBrujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.\\nBRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.<br />BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.<br />Brujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.<br />BRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.</span></p>'),(44,85,0,13,'Bruxo 1. Espadín'),(45,75,0,14,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.\\nBRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.\\nBrujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.\\nBRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.<br />BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.<br />Brujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.<br />BRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.</span></p>'),(46,85,0,14,'Bruxo 2. Pechuga De Maguey'),(47,75,0,15,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.\\nBRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.\\nBrujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.\\nBRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.<br />BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.<br />Brujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.<br />BRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.</span></p>'),(48,85,0,15,'Bruxo 3. Barril'),(49,76,0,15,NULL),(50,103,0,15,NULL),(51,75,0,16,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.\\nBRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.\\nBrujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.\\nBRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.<br />BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.<br />Brujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.<br />BRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.</span></p>'),(52,85,0,16,'Bruxo 4. Ensamble'),(53,75,0,17,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;La palabra IBA significa CIELO en el dialecto ZAPOTECO (lengua indigena del estado de OAXACA) de ahi nuestro slogan EL MEZCAL DEL CIELO.\\nMezcal Iba es una marca que nace de la uni&oacute;n de una empresa BEBIDAS PROACTIVAS DE M&Eacute;XICO y del palenque El dos de Oros ubicado en San Pablo Huixtepec Oaxaca, cuyo pilar, Don Fortino Ramos, legendario maestro mezcalero y pionero en la elaboraci&oacute;n de mezcales artesanales.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">La palabra IBA significa CIELO en el dialecto ZAPOTECO (lengua indigena del estado de OAXACA) de ahi nuestro slogan EL MEZCAL DEL CIELO.<br />Mezcal Iba es una marca que nace de la uni&oacute;n de una empresa BEBIDAS PROACTIVAS DE M&Eacute;XICO y del palenque El dos de Oros ubicado en San Pablo Huixtepec Oaxaca, cuyo pilar, Don Fortino Ramos, legendario maestro mezcalero y pionero en la elaboraci&oacute;n de mezcales artesanales.</span></p>'),(54,85,0,17,'IBÁ 55. Mezcal Joven'),(55,75,0,18,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Las manos expertas de nuestros maestros mezcaleros escogen cuidadosamente el agave silvestre y cultivado en el estado de Durango y otros estados del pais esperando con paciencia que cada agave est&eacute; en su punto perfecto de madurez.\\nAl finalizar la jima, los corazones son horneados en horno de tierra con piedra volc&aacute;nica, al calor de la le&ntilde;a de mezquite. Ya cocido, la dulzura del agave es extra&iacute;do mediante el machacado a mano y despu&eacute;s fermentado en tinas de madera con agua de manantial.\\nNuestro proceso de destilaci&oacute;n es mediante el m&eacute;todo de montera de madera de encino, cobre o barro.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Las manos expertas de nuestros maestros mezcaleros escogen cuidadosamente el agave silvestre y cultivado en el estado de Durango y otros estados del pais esperando con paciencia que cada agave est&eacute; en su punto perfecto de madurez.<br />Al finalizar la jima, los corazones son horneados en horno de tierra con piedra volc&aacute;nica, al calor de la le&ntilde;a de mezquite. Ya cocido, la dulzura del agave es extra&iacute;do mediante el machacado a mano y despu&eacute;s fermentado en tinas de madera con agua de manantial.<br />Nuestro proceso de destilaci&oacute;n es mediante el m&eacute;todo de montera de madera de encino, cobre o barro.</span></p>'),(56,85,0,18,'Doña Natalia'),(57,75,0,19,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.\\nLocalizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.<br />Localizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.</span></p>'),(58,85,0,19,'Marca Negra Ensamble'),(59,75,0,20,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.\\nLocalizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.<br />Localizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.</span></p>'),(60,85,0,20,'Marca Negra Tobalá'),(61,75,0,21,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.\\nLocalizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.<br />Localizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.</span></p>'),(62,85,0,21,'Marca Negra Tepextate'),(63,75,0,22,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.\\nLocalizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.<br />Localizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.</span></p>'),(64,85,0,22,'Marca Negra Dobadán'),(65,76,0,11,NULL),(66,103,0,11,NULL),(67,75,0,23,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.\\nNuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12675,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;4&quot;:[null,2,13421772],&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.<br />Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.</span></p>'),(68,85,0,23,'Pescador De Sueños Arroqueño'),(69,75,0,24,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.\\nNuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.<br />Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.</span></p>'),(70,85,0,24,'Pescador De Sueños Cuishe'),(71,75,0,25,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.\\nNuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.<br />Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.</span></p>'),(72,85,0,25,'Pescador De Sueños Tobalá'),(73,75,0,26,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.\\nNuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.<br />Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.</span></p>'),(74,85,0,26,'Pescador De Sueños Coyote'),(75,75,0,27,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.\\nNuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.<br />Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.</span></p>'),(76,85,0,27,'Pescador De Sueños Tepextate'),(77,75,0,28,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.\\nNuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.<br />Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.</span></p>'),(78,85,0,28,'Pescador De Sueños Bicuixe'),(79,76,0,28,NULL),(80,103,0,28,NULL),(81,75,0,29,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;El nombre PAPADIABLO nace de la uni&oacute;n de dos opuestos que se complementan uno al otro para formar una unidad m&aacute;xima.\\nLa representaci&oacute;n de esta unidad m&aacute;xima traducida en nuestro mezcal, la encontramos de manera perfecta endos cartas del tarot.\\nEstos dos extremos esenciales, representan de manera precisa el campo de acci&oacute;n de nuestro mezcal. En sustancia y con medida puede aportar una gota de inspiraci&oacute;n, puede ayudar a accesar a nuestra fuente creativa o simplemente ponernos en consonancia con nuestro verdadero ser para entrar en empat&iacute;a con el mundo.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">El nombre PAPADIABLO nace de la uni&oacute;n de dos opuestos que se complementan uno al otro para formar una unidad m&aacute;xima.<br />La representaci&oacute;n de esta unidad m&aacute;xima traducida en nuestro mezcal, la encontramos de manera perfecta endos cartas del tarot.<br />Estos dos extremos esenciales, representan de manera precisa el campo de acci&oacute;n de nuestro mezcal. En sustancia y con medida puede aportar una gota de inspiraci&oacute;n, puede ayudar a accesar a nuestra fuente creativa o simplemente ponernos en consonancia con nuestro verdadero ser para entrar en empat&iacute;a con el mundo.</span></p>'),(82,85,0,29,'Papadiablo Espadín'),(83,75,0,30,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;El nombre PAPADIABLO nace de la uni&oacute;n de dos opuestos que se complementan uno al otro para formar una unidad m&aacute;xima.\\nLa representaci&oacute;n de esta unidad m&aacute;xima traducida en nuestro mezcal, la encontramos de manera perfecta endos cartas del tarot.\\nEstos dos extremos esenciales, representan de manera precisa el campo de acci&oacute;n de nuestro mezcal. En sustancia y con medida puede aportar una gota de inspiraci&oacute;n, puede ayudar a accesar a nuestra fuente creativa o simplemente ponernos en consonancia con nuestro verdadero ser para entrar en empat&iacute;a con el mundo.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12673,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;11&quot;:4,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">El nombre PAPADIABLO nace de la uni&oacute;n de dos opuestos que se complementan uno al otro para formar una unidad m&aacute;xima.<br />La representaci&oacute;n de esta unidad m&aacute;xima traducida en nuestro mezcal, la encontramos de manera perfecta endos cartas del tarot.<br />Estos dos extremos esenciales, representan de manera precisa el campo de acci&oacute;n de nuestro mezcal. En sustancia y con medida puede aportar una gota de inspiraci&oacute;n, puede ayudar a accesar a nuestra fuente creativa o simplemente ponernos en consonancia con nuestro verdadero ser para entrar en empat&iacute;a con el mundo.</span></p>'),(84,85,0,30,'Papadiablo Ensamble'),(85,75,0,31,'<p>Nakaw&eacute;</p>'),(86,85,0,31,'Nakawé'),(87,76,0,31,NULL),(88,103,0,31,NULL),(89,75,0,32,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;IB&Aacute; 40. Mezcal Joven&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12417,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">IB&Aacute; 40. Mezcal Joven</span></p>'),(90,85,0,32,'IBÁ 40. Mezcal Joven'),(91,75,0,33,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Machetazo Papalote&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12417,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Machetazo Papalote</span></p>'),(92,85,0,33,'Machetazo Papalote'),(93,75,0,34,'<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;El hijo del Santo&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:12417,&quot;3&quot;:{&quot;1&quot;:0,&quot;3&quot;:1},&quot;10&quot;:1,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">El hijo del Santo</span></p>'),(94,85,0,34,'El hijo del Santo'),(95,75,0,35,'<p><span>Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.</span></p>'),(96,85,0,35,'Mezcal Chaneque Mexicano'),(97,75,0,36,'<p><span>Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.</span></p>'),(98,85,0,36,'Mezcal Chaneque Duranguensis');
/*!40000 ALTER TABLE `catalog_product_entity_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_tier_price`
--

DROP TABLE IF EXISTS `catalog_product_entity_tier_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_tier_price` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `all_groups` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Applicable To All Customer Groups',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group ID',
  `qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'QTY',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_E8AB433B9ACB00343ABB312AD2FAB087` (`entity_id`,`all_groups`,`customer_group_id`,`qty`,`website_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_ENTITY_TIER_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_TIER_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_tier_price`
--

LOCK TABLES `catalog_product_entity_tier_price` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_tier_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_entity_tier_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_entity_varchar`
--

DROP TABLE IF EXISTS `catalog_product_entity_varchar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_ENTT_VCHR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1378 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Varchar Attribute Backend Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_entity_varchar`
--

LOCK TABLES `catalog_product_entity_varchar` WRITE;
/*!40000 ALTER TABLE `catalog_product_entity_varchar` DISABLE KEYS */;
INSERT INTO `catalog_product_entity_varchar` VALUES (1,73,0,1,'Meteoro'),(2,84,0,1,'Meteoro'),(3,86,0,1,'Meteoro Hace muchos años según la leyenda un destello iluminó el cielo de la sierra sur de Oaxaca, se escuchó un rugido y se vieron caer “ojitos de fuego” por todos lados. Uno de ellos hizo un pequeño cráter que un maestro mezcalillero decidió usar para c'),(4,87,0,1,'/m/t/mt01.jpg'),(5,88,0,1,'/m/t/mt01.jpg'),(6,89,0,1,'/m/t/mt01.jpg'),(7,104,0,1,'1column'),(8,106,0,1,'container2'),(9,114,0,1,'MX'),(10,119,0,1,'meteoro'),(11,134,0,1,'2'),(12,142,0,1,'Angustifolia'),(13,145,0,1,'6'),(14,146,0,1,'Horno cónico de piedra'),(15,148,0,1,'5'),(16,150,0,1,'En tinas de madera'),(17,155,0,1,'7'),(18,156,0,1,'Petronilo Rosario Altamirano'),(19,157,0,1,'Espadín'),(20,158,0,1,'Tahona'),(21,160,0,1,'Doble'),(22,162,0,1,'70'),(23,163,0,1,'Las Margaritas'),(24,167,0,1,'Alambique de cobre'),(25,168,0,1,'Espadín'),(29,73,0,2,'MeXXico Joven'),(30,84,0,2,'Mexxico Joven'),(31,86,0,2,'MeXXIco es una empresa 100% mexicana, fundada con el objetivo de producir un mezcal de la más alta calidad.\r\nNuestro logotipo, inspirado en la historia constituye el símbolo de la majestuosidad y de la victoria, que nos representa como empresa.'),(32,106,0,2,'container2'),(33,119,0,2,'mexxico-joven'),(34,134,0,2,'2'),(35,100,0,2,NULL),(36,104,0,2,'1column'),(37,114,0,2,'MX'),(38,116,0,2,NULL),(39,142,0,2,'Angustifolia'),(40,143,0,2,NULL),(41,144,0,2,NULL),(42,145,0,2,'6'),(43,146,0,2,'Horno cónico de piedra'),(44,148,0,2,'5'),(45,150,0,2,'En tinas de acero inoxidable'),(47,152,0,2,NULL),(48,153,0,2,NULL),(49,154,0,2,NULL),(50,155,0,2,'9'),(51,156,0,2,'Jorge Balderas'),(52,157,0,2,'Espadín'),(53,158,0,2,'Tahona'),(54,159,0,2,NULL),(55,160,0,2,'Doble'),(56,161,0,2,NULL),(57,162,0,2,'70'),(58,163,0,2,'San Francisco Tutla'),(59,164,0,2,NULL),(60,165,0,2,'-'),(61,166,0,2,'-'),(62,167,0,2,'Alambique de cobre'),(63,168,0,2,'Joven'),(64,169,0,2,'44.5'),(65,87,0,2,'/m/x/mx01_2.jpg'),(66,88,0,2,'/m/x/mx01_2.jpg'),(67,89,0,2,'/m/x/mx01_2.jpg'),(68,132,0,2,'/m/x/mx01_2.jpg'),(89,100,0,1,NULL),(90,116,0,1,NULL),(91,143,0,1,NULL),(92,144,0,1,NULL),(94,152,0,1,NULL),(95,153,0,1,NULL),(96,154,0,1,NULL),(97,159,0,1,NULL),(98,161,0,1,NULL),(99,164,0,1,NULL),(100,165,0,1,NULL),(101,166,0,1,NULL),(102,169,0,1,'45'),(113,73,0,3,'Sin Piedad 44'),(114,84,0,3,'Sin Piedad 44'),(115,86,0,3,'En el pueblo de Matatlán en Oaxaca, en un pequeño palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Martínez y Enrique Mendoza donde usando métodos tradicionales nos comparten un excepcional Mezcal.\r\nDado que cre'),(116,87,0,3,'/s/p/sp01.jpg'),(117,88,0,3,'/s/p/sp01.jpg'),(118,89,0,3,'/s/p/sp01.jpg'),(119,106,0,3,'container2'),(120,114,0,3,'MX'),(121,119,0,3,'sin-piedad-44'),(122,132,0,3,'/s/p/sp01.jpg'),(123,134,0,3,'2'),(124,142,0,3,'Angustifolia'),(125,145,0,3,'12'),(126,146,0,3,'Horno cónico de piedra'),(127,150,0,3,'En tinas de madera'),(128,155,0,3,'7'),(129,156,0,3,'Don Celso Martínez y Enrique Mendoza'),(130,157,0,3,'Espadín'),(131,158,0,3,'Tahona'),(132,160,0,3,'Doble'),(133,162,0,3,'70'),(134,163,0,3,'Santiago Matatlán'),(135,167,0,3,'Alambique de cobre'),(136,168,0,3,'44'),(137,169,0,3,'44'),(142,100,0,3,NULL),(143,104,0,3,'1column'),(144,116,0,3,NULL),(145,143,0,3,NULL),(146,144,0,3,NULL),(147,148,0,3,'5'),(149,152,0,3,NULL),(150,153,0,3,NULL),(151,154,0,3,NULL),(152,159,0,3,NULL),(153,161,0,3,NULL),(154,164,0,3,NULL),(155,165,0,3,NULL),(156,166,0,3,NULL),(183,73,0,4,'Sin Piedad Cirial'),(184,84,0,4,'Sin Piedad Cirial'),(185,86,0,4,'Sin Piedad Cirial En el pueblo de Matatlán en Oaxaca, en un pequeño palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Martínez y Enrique Mendoza donde usando métodos tradicionales nos comparten un excepcional Mez'),(186,87,0,4,'/s/p/sp02.jpg'),(187,88,0,4,'/s/p/sp02.jpg'),(188,89,0,4,'/s/p/sp02.jpg'),(189,104,0,4,'1column'),(190,106,0,4,'container2'),(191,114,0,4,'MX'),(192,119,0,4,'sin-piedad-cirial'),(193,132,0,4,'/s/p/sp02.jpg'),(194,134,0,4,'2'),(195,142,0,4,'Karwinskii Zucc'),(196,145,0,4,'12'),(197,146,0,4,'Horno cónico de piedra'),(198,148,0,4,'5'),(199,150,0,4,'En tinas de madera'),(200,155,0,4,'8'),(201,156,0,4,'Don Celso Martínez y Enrique Mendoza'),(202,157,0,4,'Silvestre cirial'),(203,158,0,4,'Tahona'),(204,160,0,4,'Doble'),(205,162,0,4,'70'),(206,163,0,4,'Santiago Matatlán'),(207,167,0,4,'Alambique de cobre'),(208,168,0,4,'Cirial'),(209,169,0,4,'48'),(225,73,0,5,'Sin Piedad 44 “Petaca”'),(226,84,0,5,'Sin Piedad 44 “Petaca”'),(227,86,0,5,'Sin Piedad 44 “Petaca” En el pueblo de Matatlán en Oaxaca, en un pequeño palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Martínez y Enrique Mendoza donde usando métodos tradicionales nos comparten un excepciona'),(228,87,0,5,'/s/p/sp03.jpg'),(229,88,0,5,'/s/p/sp03.jpg'),(230,89,0,5,'/s/p/sp03.jpg'),(231,104,0,5,'1column'),(232,106,0,5,'container2'),(233,119,0,5,'sin-piedad-44-petaca'),(234,132,0,5,'/s/p/sp03.jpg'),(235,134,0,5,'2'),(236,142,0,5,'Angustifolia'),(237,145,0,5,'24'),(238,156,0,5,'Don Celso Martínez y Enrique Mendoza'),(239,157,0,5,'Espadín'),(240,162,0,5,'20'),(241,163,0,5,'Santiago Matatlán'),(242,168,0,5,'44 PETACA'),(247,100,0,5,NULL),(248,114,0,5,'MX'),(249,116,0,5,NULL),(250,143,0,5,NULL),(251,144,0,5,NULL),(252,146,0,5,'Horno cónico de piedra'),(253,148,0,5,NULL),(254,150,0,5,'En tinas de madera'),(255,152,0,5,NULL),(256,153,0,5,NULL),(257,154,0,5,NULL),(258,155,0,5,'7'),(259,158,0,5,'Tahona'),(260,159,0,5,NULL),(261,160,0,5,'Doble'),(262,161,0,5,NULL),(263,164,0,5,NULL),(264,165,0,5,NULL),(265,166,0,5,NULL),(266,167,0,5,'Alambique de cobre'),(267,169,0,5,'44'),(276,73,0,6,'Mayalen Etiqueta Negra'),(277,84,0,6,'Mayalen Etiqueta Negra'),(278,86,0,6,'Mayalen Etiqueta Negra El mezcal resurge como parte vital en la comida gourmet en respuesta al redescubrimiento de las tradiciones culinarias mexicanas. A través de nuestro mezcal,Mayalen Agua de Juventud preserva el patrimonio incrustado en las costumbre'),(279,106,0,6,'container2'),(280,114,0,6,'MX'),(281,119,0,6,'mayalen-etiqueta-negra'),(282,134,0,6,'2'),(283,100,0,6,NULL),(284,104,0,6,'1column'),(285,116,0,6,NULL),(286,142,0,6,'Cupreata'),(287,143,0,6,NULL),(288,144,0,6,NULL),(289,145,0,6,'12'),(290,146,0,6,'Horno cónico de piedra'),(291,148,0,6,NULL),(292,150,0,6,'En tinas de madera'),(293,152,0,6,NULL),(294,153,0,6,NULL),(295,154,0,6,NULL),(296,155,0,6,'8 a 14'),(297,156,0,6,'Don José Morales'),(298,157,0,6,'Papalote'),(299,158,0,6,'Tahona'),(300,159,0,6,NULL),(301,160,0,6,'Doble'),(302,161,0,6,'Silvestre'),(303,162,0,6,'70'),(304,163,0,6,'Mochitlán'),(305,164,0,6,NULL),(306,165,0,6,NULL),(307,166,0,6,NULL),(308,167,0,6,'Alambique de cobre'),(309,168,0,6,'Etiqueta Negra'),(310,169,0,6,'48'),(311,87,0,6,'/m/y/my02.jpg'),(312,88,0,6,'/m/y/my02.jpg'),(313,89,0,6,'/m/y/my02.jpg'),(314,132,0,6,'/m/y/my02.jpg'),(323,73,0,7,'Mexxico “Petaca”'),(324,84,0,7,'Mexxico “Petaca”'),(325,86,0,7,'Mexxico “Petaca” MeXXIco es una empresa 100% mexicana, fundada con el objetivo de producir un mezcal de la más alta calidad.Nuestro logotipo, inspirado en la historia constituye el simbolo de la majestuosidad y de la victoria, que nos representa como empr'),(326,87,0,7,'/m/x/mx02.jpg'),(327,88,0,7,'/m/x/mx02.jpg'),(328,89,0,7,'/m/x/mx02.jpg'),(329,104,0,7,'1column'),(330,106,0,7,'container2'),(331,114,0,7,'MX'),(332,119,0,7,'mexxico-petaca'),(333,132,0,7,'/m/x/mx02.jpg'),(334,134,0,7,'2'),(335,142,0,7,'Angustifolia'),(336,145,0,7,'24'),(337,146,0,7,'Horno cónico de piedra'),(338,150,0,7,'En tinas de madera'),(339,155,0,7,'8'),(340,156,0,7,'Jorge Balderas'),(341,157,0,7,'Espadín silvestre'),(342,158,0,7,'Tahona'),(343,160,0,7,'Doble'),(344,162,0,7,'20'),(345,163,0,7,'San Francisco Tutla'),(346,167,0,7,'Alambique de cobre'),(347,168,0,7,' PETACA'),(348,169,0,7,'45.4'),(353,73,0,8,'Mezcal Chaneque Madrecuixe'),(354,84,0,8,'Mezcal Chaneque Madrecuixe'),(355,86,0,8,'Mezcal Chaneque Madrecuixe Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y e'),(356,87,0,8,'/c/h/ch01.jpg'),(357,88,0,8,'/c/h/ch01.jpg'),(358,89,0,8,'/c/h/ch01.jpg'),(359,104,0,8,'1column'),(360,106,0,8,'container2'),(361,114,0,8,'MX'),(362,119,0,8,'mezcal-chaneque-madrecuixe'),(363,132,0,8,'/c/h/ch01.jpg'),(364,134,0,8,'2'),(365,142,0,8,'Karwinskii'),(366,145,0,8,'12'),(367,146,0,8,'Horno cónico de piedra'),(368,150,0,8,'Natural en tinas de roble'),(369,155,0,8,'18 años en promedio'),(370,156,0,8,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(371,157,0,8,'Madrecuixe'),(372,158,0,8,'Tahona'),(373,160,0,8,'Doble'),(374,162,0,8,'70'),(375,163,0,8,'Santiago Matatlán, , San Agustín Amatengo y Santa María Zoquitlán'),(376,167,0,8,'Alambique de cobre'),(377,168,0,8,'Madrecuixe'),(378,169,0,8,'48'),(383,73,0,9,'Mezcal Chaneque Tobaziche'),(384,84,0,9,'Mezcal Chaneque Tobaziche'),(385,86,0,9,'Mezcal Chaneque Tobaziche Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el'),(386,87,0,9,'/c/h/ch02.jpg'),(387,88,0,9,'/c/h/ch02.jpg'),(388,89,0,9,'/c/h/ch02.jpg'),(389,104,0,9,'1column'),(390,106,0,9,'container2'),(391,114,0,9,'MX'),(392,119,0,9,'mezcal-chaneque-tobaziche'),(393,132,0,9,'/c/h/ch02.jpg'),(394,134,0,9,'2'),(395,142,0,9,'Karwinskii'),(396,145,0,9,'12'),(397,146,0,9,'Horno cónico de piedra'),(398,148,0,9,'5'),(399,150,0,9,'Natural en tinas de roble'),(400,155,0,9,'18 años en promedio'),(401,156,0,9,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(402,157,0,9,'Tobaziche'),(403,158,0,9,'Tahona'),(404,160,0,9,'Doble'),(405,162,0,9,'70'),(406,163,0,9,'Santiago Matatlán, , San Agustín Amatengo y Santa María Zoquitlán'),(407,167,0,9,'Alambique de cobre'),(408,168,0,9,'Tobaziche'),(409,169,0,9,'48'),(437,100,0,4,NULL),(438,116,0,4,NULL),(439,143,0,4,NULL),(440,144,0,4,NULL),(441,152,0,4,NULL),(442,153,0,4,NULL),(443,154,0,4,NULL),(444,159,0,4,NULL),(445,161,0,4,NULL),(446,164,0,4,NULL),(447,165,0,4,NULL),(448,166,0,4,NULL),(461,100,0,7,NULL),(462,116,0,7,NULL),(463,143,0,7,NULL),(464,144,0,7,NULL),(465,148,0,7,NULL),(466,152,0,7,NULL),(467,153,0,7,NULL),(468,154,0,7,NULL),(469,159,0,7,NULL),(470,161,0,7,NULL),(471,164,0,7,NULL),(472,165,0,7,NULL),(473,166,0,7,NULL),(478,100,0,8,NULL),(479,116,0,8,NULL),(480,143,0,8,NULL),(481,144,0,8,NULL),(482,148,0,8,NULL),(483,152,0,8,NULL),(484,153,0,8,NULL),(485,154,0,8,NULL),(486,159,0,8,NULL),(487,161,0,8,NULL),(488,164,0,8,NULL),(489,165,0,8,NULL),(490,166,0,8,NULL),(495,100,0,9,NULL),(496,116,0,9,NULL),(497,143,0,9,NULL),(498,144,0,9,NULL),(499,152,0,9,NULL),(500,153,0,9,NULL),(501,154,0,9,NULL),(502,159,0,9,NULL),(503,161,0,9,NULL),(504,164,0,9,NULL),(505,165,0,9,NULL),(506,166,0,9,NULL),(511,73,0,10,'Mezcal Chaneque Espadín'),(512,84,0,10,'Mezcal Chaneque Espadín'),(513,86,0,10,'Mezcal Chaneque Espadín Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el m'),(514,87,0,10,'/c/h/ch04.jpg'),(515,88,0,10,'/c/h/ch04.jpg'),(516,89,0,10,'/c/h/ch04.jpg'),(517,104,0,10,'1column'),(518,106,0,10,'container2'),(519,114,0,10,'MX'),(520,119,0,10,'mezcal-chaneque-espadin'),(521,132,0,10,'/c/h/ch04.jpg'),(522,134,0,10,'2'),(523,142,0,10,'Angustifolia'),(524,145,0,10,'12'),(525,146,0,10,'Horno cónico de piedra'),(526,150,0,10,'Natural en tinas de roble'),(527,155,0,10,'8 a 10'),(528,156,0,10,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(529,157,0,10,'Espadín'),(530,158,0,10,'Tahona'),(531,160,0,10,'Doble'),(532,162,0,10,'70'),(533,163,0,10,'Santiago Matatlán, , San Agustín Amatengo y Santa María Zoquitlán'),(534,167,0,10,'Alambique de cobre'),(535,168,0,10,'Espadín'),(536,169,0,10,'45.6'),(541,73,0,11,'Mezcal Chaneque Tobalá Silvestre'),(542,84,0,11,'Mezcal Chaneque Tobalá Silvestre'),(543,86,0,11,'Mezcal Chaneque Tobalá Silvestre Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tier'),(544,87,0,11,'/c/h/ch05.jpg'),(545,88,0,11,'/c/h/ch05.jpg'),(546,89,0,11,'/c/h/ch05.jpg'),(547,106,0,11,'container2'),(548,114,0,11,'MX'),(549,119,0,11,'mezcal-chaneque-tobala-silvestre'),(550,132,0,11,'/c/h/ch05.jpg'),(551,134,0,11,'2'),(552,142,0,11,'Potatorum'),(553,145,0,11,'12'),(554,146,0,11,'Horno cónico de piedra'),(555,150,0,11,'Natural en tinas de roble'),(556,155,0,11,'15 años en promedio'),(557,156,0,11,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(558,157,0,11,'Tobalá silvestre'),(559,158,0,11,'Tahona'),(560,160,0,11,'Doble'),(561,162,0,11,'70'),(562,163,0,11,'Santiago Matatlán, , San Agustín Amatengo y Santa María Zoquitlán'),(563,167,0,11,'Alambique de cobre'),(564,168,0,11,'Tobalá'),(565,169,0,11,'45.6'),(570,73,0,12,'Mezcal Chaneque Ensamble'),(571,84,0,12,'Mezcal Chaneque Ensamble'),(572,86,0,12,'Mezcal Chaneque Ensamble Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el '),(573,87,0,12,'/c/h/ch06.jpg'),(574,88,0,12,'/c/h/ch06.jpg'),(575,89,0,12,'/c/h/ch06.jpg'),(576,104,0,12,'1column'),(577,106,0,12,'container2'),(578,114,0,12,'MX'),(579,119,0,12,'mezcal-chaneque-ensamble'),(580,132,0,12,'/c/h/ch06.jpg'),(581,134,0,12,'2'),(582,145,0,12,'12'),(583,146,0,12,'Horno cónico de piedra'),(584,150,0,12,'Natural en tinas de roble'),(585,155,0,12,'10 años espadín y 20 el arroqueño promedio'),(586,156,0,12,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(587,157,0,12,'Espadín y arroqueño'),(588,158,0,12,'Tahona'),(589,160,0,12,'Doble'),(590,162,0,12,'70'),(591,163,0,12,'Santiago Matatlán, , San Agustín Amatengo y Santa María Zoquitlán'),(592,167,0,12,'Alambique de cobre'),(593,168,0,12,'Ensamble (Espadín Arroqueño)'),(594,169,0,12,'47'),(599,73,0,13,'Bruxo 1. Espadín'),(600,84,0,13,'Bruxo 1. Espadín'),(601,86,0,13,'Bruxo 1. Espadín En la sierra aprendimos que el Mezcal se trata con respeto, por ahí alguien nos dijo que con Mezcal no se brinda, se ofrenda.BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro país, que descubrimos en el campo la magia '),(602,87,0,13,'/b/x/bx01.jpg'),(603,88,0,13,'/b/x/bx01.jpg'),(604,89,0,13,'/b/x/bx01.jpg'),(605,104,0,13,'1column'),(606,106,0,13,'container2'),(607,114,0,13,'MX'),(608,119,0,13,'bruxo-1-espadin'),(609,132,0,13,'/b/x/bx01.jpg'),(610,134,0,13,'2'),(611,142,0,13,'Angustifolia'),(612,145,0,13,'6'),(613,146,0,13,'Horno cónico de piedra'),(614,148,0,13,'5'),(615,150,0,13,'Tinas de madera, cuero o plástico'),(616,155,0,13,'7 a 30'),(617,156,0,13,'Lucio Morales López, Pablo Vázquez, Cándido Reyes, Cesáreo Rodríguez y Cándido Reyes'),(618,157,0,13,'Espadín'),(619,158,0,13,'Tahona'),(620,160,0,13,'Doble o triple'),(621,162,0,13,'70'),(622,163,0,13,'San Dionisio Ocotepec, Agua del Espino, San Agustín Amatengo, Las Salinas, San Agustín Amatengo'),(623,167,0,13,'\"Alambique de cobre / olla de barro\"'),(624,168,0,13,'Espadín'),(625,169,0,13,'46'),(630,73,0,14,'Bruxo 2. Pechuga De Maguey'),(631,84,0,14,'Bruxo 2. Pechuga De Maguey'),(632,86,0,14,'Bruxo 2. Pechuga De Maguey En la sierra aprendimos que el Mezcal se trata con respeto, por ahí alguien nos dijo que con Mezcal no se brinda, se ofrenda.BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro país, que descubrimos en el campo'),(633,87,0,14,'/b/x/bx02.jpg'),(634,88,0,14,'/b/x/bx02.jpg'),(635,89,0,14,'/b/x/bx02.jpg'),(636,104,0,14,'1column'),(637,106,0,14,'container2'),(638,114,0,14,'MX'),(639,119,0,14,'bruxo-2-pechuga-de-maguey'),(640,132,0,14,'/b/x/bx02.jpg'),(641,134,0,14,'2'),(642,142,0,14,'Angustifolia'),(643,145,0,14,'6'),(644,146,0,14,'Horno cónico de piedra'),(645,148,0,14,'5'),(646,150,0,14,'Tinas de madera, cuero o plástico'),(647,155,0,14,'7 a 30'),(648,156,0,14,'Lucio Morales López, Pablo Vázquez, Cándido Reyes, Cesáreo Rodríguez y Cándido Reyes'),(649,157,0,14,'Pechuga de Maguey'),(650,158,0,14,'Tahona'),(651,160,0,14,'Doble o triple'),(652,162,0,14,'70'),(653,163,0,14,'San Dionisio Ocotepec, Agua del Espino, San Agustín Amatengo, Las Salinas, San Agustín Amatengo '),(654,167,0,14,'Alambique de cobre / olla de barro'),(655,168,0,14,'Pechuga Maguey'),(656,169,0,14,'46'),(661,73,0,15,'Bruxo 3. Barril'),(662,84,0,15,'Bruxo 3. Barril'),(663,86,0,15,'Bruxo 3. Barril En la sierra aprendimos que el Mezcal se trata con respeto, por ahí alguien nos dijo que con Mezcal no se brinda, se ofrenda.BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro país, que descubrimos en el campo la magia d'),(664,87,0,15,'/b/x/bx03.jpg'),(665,88,0,15,'/b/x/bx03.jpg'),(666,89,0,15,'/b/x/bx03.jpg'),(667,104,0,15,'1column'),(668,106,0,15,'container2'),(669,114,0,15,'MX'),(670,119,0,15,'bruxo-3-barril'),(671,132,0,15,'/b/x/bx03.jpg'),(672,134,0,15,'2'),(677,100,0,15,NULL),(678,116,0,15,NULL),(679,142,0,15,'Karwinskii'),(680,143,0,15,NULL),(681,144,0,15,NULL),(682,145,0,15,'6'),(683,146,0,15,'Horno cónico de piedra'),(684,148,0,15,'5'),(685,150,0,15,'Tinas de madera, cuero o plástico'),(686,152,0,15,NULL),(687,153,0,15,NULL),(688,154,0,15,NULL),(689,155,0,15,'7 a 30'),(690,156,0,15,'Lucio Morales López, Pablo Vázquez, Cándido Reyes, Cesáreo Rodríguez y Cándido Reyes'),(691,157,0,15,'Barril'),(692,158,0,15,'Tahona'),(693,159,0,15,NULL),(694,160,0,15,'Doble o triple'),(695,161,0,15,NULL),(696,162,0,15,'70'),(697,163,0,15,'San Dionisio Ocotepec, Agua del Espino, San Agustín Amatengo, Las Salinas, San Agustín Amatengo '),(698,164,0,15,NULL),(699,165,0,15,NULL),(700,166,0,15,NULL),(701,167,0,15,'Alambique de cobre / olla de barro'),(702,168,0,15,'Barril'),(703,169,0,15,'46'),(708,73,0,16,'Bruxo 4. Ensamble'),(709,84,0,16,'Bruxo 4. Ensamble'),(710,86,0,16,'Bruxo 4. Ensamble En la sierra aprendimos que el Mezcal se trata con respeto, por ahí alguien nos dijo que con Mezcal no se brinda, se ofrenda.BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro país, que descubrimos en el campo la magia'),(711,87,0,16,'/b/x/bx04.jpg'),(712,88,0,16,'/b/x/bx04.jpg'),(713,89,0,16,'/b/x/bx04.jpg'),(714,104,0,16,'1column'),(715,106,0,16,'container2'),(716,114,0,16,'MX'),(717,119,0,16,'bruxo-4-ensamble'),(718,132,0,16,'/b/x/bx04.jpg'),(719,134,0,16,'2'),(720,145,0,16,'6'),(721,146,0,16,'Horno cónico de piedra'),(722,148,0,16,'5'),(723,150,0,16,'Tinas de madera, cuero o plástico'),(724,155,0,16,'7 a 30'),(725,156,0,16,'Lucio Morales López, Pablo Vázquez, Cándido Reyes, Cesáreo Rodríguez y Cándido Reyes'),(726,157,0,16,'Espadín, barril y cuishe'),(727,158,0,16,'Tahona'),(728,160,0,16,'Doble o triple'),(729,162,0,16,'70'),(730,163,0,16,'San Dionisio Ocotepec, Agua del Espino, San Agustín Amatengo, Las Salinas, San Agustín Amatengo '),(731,167,0,16,'Alambique de cobre / olla de barro'),(732,168,0,16,'Ensamble (Espadín, Barril, Cuishe)'),(733,169,0,16,'46'),(738,73,0,17,'IBÁ 55. Mezcal Joven'),(739,84,0,17,'IBÁ 55. Mezcal Joven'),(740,86,0,17,'IBÁ 55. Mezcal Joven La palabra IBA significa CIELO en el dialecto ZAPOTECO (lengua indigena del estado de OAXACA) de ahi nuestro slogan EL MEZCAL DEL CIELO.Mezcal Iba es una marca que nace de la unión de una empresa BEBIDAS PROACTIVAS DE MÉXICO y del pal'),(741,87,0,17,'/i/b/ib02.jpg'),(742,88,0,17,'/i/b/ib02.jpg'),(743,89,0,17,'/i/b/ib02.jpg'),(744,104,0,17,'1column'),(745,106,0,17,'container2'),(746,114,0,17,'MX'),(747,119,0,17,'iba-55-mezcal-joven'),(748,132,0,17,'/i/b/ib02.jpg'),(749,134,0,17,'2'),(750,142,0,17,'Angustifolia'),(751,145,0,17,'12'),(752,146,0,17,'Horno cónico de piedra'),(753,150,0,17,'Tinas de madera de encino'),(754,155,0,17,'7 a 14'),(755,156,0,17,'Manuel González'),(756,157,0,17,'Espadín'),(757,158,0,17,'Tahona'),(758,162,0,17,'70'),(759,163,0,17,'San Pablo Huiztepec'),(760,167,0,17,'Alambique de cobre'),(761,168,0,17,'Espadín'),(762,169,0,17,'55'),(767,73,0,18,'Doña Natalia'),(768,84,0,18,'Doña Natalia'),(769,86,0,18,'Doña Natalia Las manos expertas de nuestros maestros mezcaleros escogen cuidadosamente el agave silvestre y cultivado en el estado de Durango y otros estados del pais esperando con paciencia que cada agave esté en su punto perfecto de madurez.Al finalizar'),(770,87,0,18,'/d/n/dn01.jpg'),(771,88,0,18,'/d/n/dn01.jpg'),(772,89,0,18,'/d/n/dn01.jpg'),(773,104,0,18,'1column'),(774,106,0,18,'container2'),(775,114,0,18,'MX'),(776,119,0,18,'do-a-natalia'),(777,132,0,18,'/d/n/dn01.jpg'),(778,134,0,18,'2'),(779,145,0,18,'6'),(780,146,0,18,'Horno cónico de piedra'),(781,148,0,18,'4'),(782,150,0,18,'Tinas de madera de encino'),(783,155,0,18,'12'),(784,156,0,18,'Agustín Salas padre e hijo'),(785,157,0,18,'Silvestre durangensis'),(786,158,0,18,'Tahona'),(787,160,0,18,'Doble'),(788,162,0,18,'70'),(789,163,0,18,'Nombre de Dios'),(790,167,0,18,'Alambique de cobre'),(791,168,0,18,'Cenizo'),(792,169,0,18,'38'),(797,73,0,19,'Marca Negra Ensamble'),(798,84,0,19,'Marca Negra Ensamble'),(799,86,0,19,'Marca Negra Ensamble MN es una selección de mezcales de alta calidad, de diferentes regiones de México, en donde se produce y embotella a mano.Localizamos el mejor mezcal tradicional donde quiera que esté y sin importar que tan lejos se encuentre su produ'),(800,87,0,19,'/m/n/mn01.jpg'),(801,88,0,19,'/m/n/mn01.jpg'),(802,89,0,19,'/m/n/mn01.jpg'),(803,104,0,19,'1column'),(804,106,0,19,'container2'),(805,114,0,19,'MX'),(806,119,0,19,'marca-negra-ensamble'),(807,132,0,19,'/m/n/mn01.jpg'),(808,134,0,19,'2'),(809,145,0,19,'6'),(810,146,0,19,'Horno cónico de piedra'),(811,148,0,19,'4'),(812,150,0,19,'En tinas de madera'),(813,155,0,19,'7'),(814,156,0,19,'Abel Nolasco V. y Jorge Méndez'),(815,157,0,19,'Madrecuixe, bicusixe, espadín'),(816,158,0,19,'Tahona'),(817,160,0,19,'Doble'),(818,162,0,19,'70'),(819,163,0,19,'San Luis del Río. Tlacolula'),(820,167,0,19,'Alambique de cobre'),(821,168,0,19,'Ensamble (Madrecuixr, Bicusixe, Espadín)'),(822,169,0,19,'48-50'),(827,73,0,20,'Marca Negra Tobalá'),(828,84,0,20,'Marca Negra Tobalá'),(829,86,0,20,'Marca Negra Tobalá MN es una selección de mezcales de alta calidad, de diferentes regiones de México, en donde se produce y embotella a mano.Localizamos el mejor mezcal tradicional donde quiera que esté y sin importar que tan lejos se encuentre su product'),(830,87,0,20,'/m/n/mn02.jpg'),(831,88,0,20,'/m/n/mn02.jpg'),(832,89,0,20,'/m/n/mn02.jpg'),(833,104,0,20,'1column'),(834,106,0,20,'container2'),(835,114,0,20,'MX'),(836,119,0,20,'marca-negra-tobala'),(837,132,0,20,'/m/n/mn02.jpg'),(838,134,0,20,'2'),(839,142,0,20,'Potatorum'),(840,145,0,20,'6'),(841,146,0,20,'Horno cónico de piedra'),(842,148,0,20,'4'),(843,150,0,20,'En tinas de madera'),(844,155,0,20,'7'),(845,156,0,20,'Abel Nolasco V. y Jorge Méndez'),(846,157,0,20,'Silvestre tobalá'),(847,158,0,20,'Tahona'),(848,160,0,20,'Doble'),(849,162,0,20,'70'),(850,163,0,20,'San Luis del Río. Tlacolula'),(851,167,0,20,'Alambique de cobre'),(852,168,0,20,'Tobalá'),(853,169,0,20,'48'),(858,73,0,21,'Marca Negra Tepextate'),(859,84,0,21,'Marca Negra Tepextate'),(860,86,0,21,'Marca Negra Tepextate MN es una selección de mezcales de alta calidad, de diferentes regiones de México, en donde se produce y embotella a mano.Localizamos el mejor mezcal tradicional donde quiera que esté y sin importar que tan lejos se encuentre su prod'),(861,87,0,21,'/m/n/mn03.jpg'),(862,88,0,21,'/m/n/mn03.jpg'),(863,89,0,21,'/m/n/mn03.jpg'),(864,104,0,21,'1column'),(865,106,0,21,'container2'),(866,114,0,21,'MX'),(867,119,0,21,'marca-negra-tepextate'),(868,132,0,21,'/m/n/mn03.jpg'),(869,134,0,21,'2'),(870,142,0,21,'Marmorata'),(871,145,0,21,'6'),(872,146,0,21,'Horno cónico de piedra'),(873,148,0,21,'4'),(874,150,0,21,'En tinas de madera'),(875,155,0,21,'7'),(876,156,0,21,'Abel Nolasco V. y Jorge Méndez'),(877,157,0,21,'Tepextate'),(878,158,0,21,'Tahona'),(879,160,0,21,'Doble'),(880,162,0,21,'70'),(881,163,0,21,'San Luis del Río. Tlacolula'),(882,167,0,21,'Alambique de cobre'),(883,168,0,21,'Tepextate'),(884,169,0,21,'48.7'),(889,73,0,22,'Marca Negra Dobadán'),(890,84,0,22,'Marca Negra Dobadán'),(891,86,0,22,'Marca Negra Dobadán MN es una selección de mezcales de alta calidad, de diferentes regiones de México, en donde se produce y embotella a mano.Localizamos el mejor mezcal tradicional donde quiera que esté y sin importar que tan lejos se encuentre su produc'),(892,87,0,22,'/m/n/mn04.jpg'),(893,88,0,22,'/m/n/mn04.jpg'),(894,89,0,22,'/m/n/mn04.jpg'),(895,104,0,22,'1column'),(896,106,0,22,'container2'),(897,114,0,22,'MX'),(898,119,0,22,'marca-negra-dobadan'),(899,132,0,22,'/m/n/mn04.jpg'),(900,134,0,22,'2'),(901,142,0,22,'Rhodacanta'),(902,145,0,22,'6'),(903,146,0,22,'Horno cónico de piedra'),(904,150,0,22,'En tinas de madera'),(905,155,0,22,'7'),(906,156,0,22,'Abel Nolasco V. y Jorge Méndez'),(907,157,0,22,'Dobadán'),(908,158,0,22,'Tahona'),(909,160,0,22,'Doble'),(910,162,0,22,'70'),(911,163,0,22,'San Luis del Río. Tlacolula'),(912,167,0,22,'Alambique de cobre'),(913,168,0,22,'Dobadán'),(914,169,0,22,'49.3'),(919,100,0,11,NULL),(920,104,0,11,NULL),(921,116,0,11,NULL),(922,143,0,11,NULL),(923,144,0,11,NULL),(924,148,0,11,NULL),(925,152,0,11,NULL),(926,153,0,11,NULL),(927,154,0,11,NULL),(928,159,0,11,NULL),(929,161,0,11,NULL),(930,164,0,11,NULL),(931,165,0,11,NULL),(932,166,0,11,NULL),(937,73,0,23,'Pescador De Sueños Arroqueño'),(938,84,0,23,'Pescador De Sueños Arroqueño'),(939,86,0,23,'Pescador De Sueños Arroqueño Pescador de Sueños es un proyecto de Destilería Santa Sabia donde nacen colecciones únicas de selectos mezcales rescatando así la mística y folklore de esta tradición mexicana.Nuestros productos están elaborados con la máxima '),(940,87,0,23,'/p/s/ps01.jpg'),(941,88,0,23,'/p/s/ps01.jpg'),(942,89,0,23,'/p/s/ps01.jpg'),(943,104,0,23,'1column'),(944,106,0,23,'container2'),(945,114,0,23,'MX'),(946,119,0,23,'pescador-de-sue-os-arroque-o'),(947,132,0,23,'/p/s/ps01.jpg'),(948,134,0,23,'2'),(949,142,0,23,'Oaxaquensis'),(950,145,0,23,'4'),(951,146,0,23,'Horno cónico de piedra'),(952,150,0,23,'En tinas de madera'),(953,155,0,23,'20'),(954,156,0,23,'Leoncio, Fernando Mitra, Felipe Cortés'),(955,157,0,23,'Arroqueño'),(956,158,0,23,'Tahona'),(957,160,0,23,'Doble'),(958,162,0,23,'70'),(959,163,0,23,'Santiago Matatlán'),(960,167,0,23,'Alambique de cobre'),(961,168,0,23,'Arroqueño'),(962,169,0,23,'48.4'),(967,73,0,24,'Pescador De Sueños Cuishe'),(968,84,0,24,'Pescador De Sueños Cuishe'),(969,86,0,24,'Pescador De Sueños Cuishe Pescador de Sueños es un proyecto de Destilería Santa Sabia donde nacen colecciones únicas de selectos mezcales rescatando así la mística y folklore de esta tradición mexicana.Nuestros productos están elaborados con la máxima cal'),(970,87,0,24,'/p/s/ps02.jpg'),(971,88,0,24,'/p/s/ps02.jpg'),(972,89,0,24,'/p/s/ps02.jpg'),(973,104,0,24,'1column'),(974,106,0,24,'container2'),(975,114,0,24,'MX'),(976,119,0,24,'pescador-de-sue-os-cuishe'),(977,132,0,24,'/p/s/ps02.jpg'),(978,134,0,24,'2'),(979,142,0,24,'Karwinskii'),(980,145,0,24,'4'),(981,146,0,24,'Horno cónico de piedra'),(982,148,0,24,'4'),(983,150,0,24,'En tinas de madera'),(984,155,0,24,'12'),(985,156,0,24,'Leoncio, Fernando Mitra, Felipe Cortés'),(986,157,0,24,'Cuishe'),(987,158,0,24,'Tahona'),(988,160,0,24,'Doble'),(989,162,0,24,'70'),(990,163,0,24,'Santiago Matatlán'),(991,167,0,24,'Alambique de cobre'),(992,168,0,24,'Cuishe'),(993,169,0,24,'49'),(998,73,0,25,'Pescador De Sueños Tobalá'),(999,84,0,25,'Pescador De Sueños Tobalá'),(1000,86,0,25,'Pescador De Sueños Tobalá Pescador de Sueños es un proyecto de Destilería Santa Sabia donde nacen colecciones únicas de selectos mezcales rescatando así la mística y folklore de esta tradición mexicana.Nuestros productos están elaborados con la máxima cal'),(1001,87,0,25,'/p/s/ps03.jpg'),(1002,88,0,25,'/p/s/ps03.jpg'),(1003,89,0,25,'/p/s/ps03.jpg'),(1004,104,0,25,'1column'),(1005,106,0,25,'container2'),(1006,114,0,25,'MX'),(1007,119,0,25,'pescador-de-sue-os-tobala'),(1008,132,0,25,'/p/s/ps03.jpg'),(1009,134,0,25,'2'),(1010,142,0,25,'Potatorum'),(1011,145,0,25,'4'),(1012,146,0,25,'Horno cónico de piedra'),(1013,148,0,25,'4'),(1014,150,0,25,'En tinas de madera'),(1015,155,0,25,'14'),(1016,156,0,25,'Leoncio, Fernando Mitra, Felipe Cortés'),(1017,157,0,25,'Tobalá'),(1018,158,0,25,'Tahona'),(1019,160,0,25,'Doble'),(1020,162,0,25,'Tobalá'),(1021,163,0,25,'Santiago Matatlán'),(1022,167,0,25,'Alambique de cobre'),(1023,168,0,25,'Tobalá'),(1024,169,0,25,'50.7'),(1029,73,0,26,'Pescador De Sueños Coyote'),(1030,84,0,26,'Pescador De Sueños Coyote'),(1031,86,0,26,'Pescador De Sueños Coyote Pescador de Sueños es un proyecto de Destilería Santa Sabia donde nacen colecciones únicas de selectos mezcales rescatando así la mística y folklore de esta tradición mexicana.Nuestros productos están elaborados con la máxima cal'),(1032,87,0,26,'/p/s/ps04.jpg'),(1033,88,0,26,'/p/s/ps04.jpg'),(1034,89,0,26,'/p/s/ps04.jpg'),(1035,104,0,26,'1column'),(1036,106,0,26,'container2'),(1037,114,0,26,'MX'),(1038,119,0,26,'pescador-de-sue-os-coyote'),(1039,132,0,26,'/p/s/ps04.jpg'),(1040,134,0,26,'2'),(1041,142,0,26,'Oaxaquensis'),(1042,145,0,26,'4'),(1043,146,0,26,'Horno cónico de piedra'),(1044,150,0,26,'En tinas de madera'),(1045,155,0,26,'17'),(1046,156,0,26,'Leoncio, Fernando Mitra, Felipe Cortés'),(1047,157,0,26,'Coyote'),(1048,158,0,26,'Tahona'),(1049,160,0,26,'Doble'),(1050,162,0,26,'70'),(1051,163,0,26,'Santiago Matatlán'),(1052,167,0,26,'Alambique de cobre'),(1053,168,0,26,'Coyote'),(1054,169,0,26,'51'),(1059,73,0,27,'Pescador De Sueños Tepextate'),(1060,84,0,27,'Pescador De Sueños Tepextate'),(1061,86,0,27,'Pescador De Sueños Tepextate Pescador de Sueños es un proyecto de Destilería Santa Sabia donde nacen colecciones únicas de selectos mezcales rescatando así la mística y folklore de esta tradición mexicana.Nuestros productos están elaborados con la máxima '),(1062,87,0,27,'/p/s/ps05.jpg'),(1063,88,0,27,'/p/s/ps05.jpg'),(1064,89,0,27,'/p/s/ps05.jpg'),(1065,104,0,27,'1column'),(1066,106,0,27,'container2'),(1067,114,0,27,'MX'),(1068,119,0,27,'pescador-de-sue-os-tepextate'),(1069,132,0,27,'/p/s/ps05.jpg'),(1070,134,0,27,'2'),(1071,145,0,27,'4'),(1072,146,0,27,'Horno cónico de piedra'),(1073,148,0,27,'4'),(1074,150,0,27,'En tinas de madera'),(1075,156,0,27,'Leoncio, Fernando Mitra, Felipe Cortés'),(1076,157,0,27,'Tepextate'),(1077,158,0,27,'Tahona'),(1078,160,0,27,'Doble'),(1079,162,0,27,'70'),(1080,163,0,27,'Santiago Matatlán'),(1081,167,0,27,'Alambique de cobre'),(1082,168,0,27,'Tepextate'),(1083,169,0,27,'54.2'),(1088,73,0,28,'Pescador De Sueños Bicuixe'),(1089,84,0,28,'Pescador De Sueños Bicuixe'),(1090,86,0,28,'Pescador De Sueños Bicuixe Pescador de Sueños es un proyecto de Destilería Santa Sabia donde nacen colecciones únicas de selectos mezcales rescatando así la mística y folklore de esta tradición mexicana.Nuestros productos están elaborados con la máxima ca'),(1091,87,0,28,'/p/s/ps06.jpg'),(1092,88,0,28,'/p/s/ps06.jpg'),(1093,89,0,28,'/p/s/ps06.jpg'),(1094,106,0,28,'container2'),(1095,114,0,28,'MX'),(1096,119,0,28,'pescador-de-sue-os-bicuixe'),(1097,132,0,28,'/p/s/ps06.jpg'),(1098,134,0,28,'2'),(1103,100,0,28,NULL),(1104,104,0,28,'1column'),(1105,116,0,28,NULL),(1106,142,0,28,'Karwinskii'),(1107,143,0,28,NULL),(1108,144,0,28,NULL),(1109,145,0,28,'4'),(1110,146,0,28,'Horno cónico de piedra'),(1111,148,0,28,NULL),(1112,150,0,28,'En tinas de madera'),(1113,152,0,28,NULL),(1114,153,0,28,NULL),(1115,154,0,28,NULL),(1116,155,0,28,'14'),(1117,156,0,28,'Leoncio, Fernando Mitra, Felipe Cortés'),(1118,157,0,28,'Bicuixe'),(1119,158,0,28,'Tahona'),(1120,159,0,28,NULL),(1121,160,0,28,'Doble'),(1122,161,0,28,NULL),(1123,162,0,28,'70'),(1124,163,0,28,'Santiago Matatlán'),(1125,164,0,28,NULL),(1126,165,0,28,NULL),(1127,166,0,28,NULL),(1128,167,0,28,'Alambique de cobre'),(1129,168,0,28,'Bicuishe'),(1130,169,0,28,'48.4'),(1135,73,0,29,'Papadiablo Espadín'),(1136,84,0,29,'Papadiablo Espadín'),(1137,86,0,29,'Papadiablo Espadín El nombre PAPADIABLO nace de la unión de dos opuestos que se complementan uno al otro para formar una unidad máxima.La representación de esta unidad máxima traducida en nuestro mezcal, la encontramos de manera perfecta endos cartas del '),(1138,87,0,29,'/p/d/pd01.jpg'),(1139,88,0,29,'/p/d/pd01.jpg'),(1140,89,0,29,'/p/d/pd01.jpg'),(1141,104,0,29,'1column'),(1142,106,0,29,'container2'),(1143,114,0,29,'MX'),(1144,119,0,29,'papadiablo-espadin'),(1145,132,0,29,'/p/d/pd01.jpg'),(1146,134,0,29,'2'),(1147,142,0,29,'Angustifolia'),(1148,145,0,29,'6'),(1149,146,0,29,'Horno cónico de piedra'),(1150,148,0,29,'4'),(1151,150,0,29,'En tinas de madera'),(1152,155,0,29,'10'),(1153,156,0,29,'Don Alberto Ortíz'),(1154,157,0,29,'Espadín'),(1155,158,0,29,'Tahona'),(1156,160,0,29,'Doble'),(1157,162,0,29,'70'),(1158,163,0,29,'Miahuatlán de Porfirio Díaz'),(1159,167,0,29,'Alambique de cobre'),(1160,168,0,29,'Espadín'),(1161,169,0,29,'47.5'),(1166,73,0,30,'Papadiablo Ensamble'),(1167,84,0,30,'Papadiablo Ensamble'),(1168,86,0,30,'Papadiablo Ensamble El nombre PAPADIABLO nace de la unión de dos opuestos que se complementan uno al otro para formar una unidad máxima.La representación de esta unidad máxima traducida en nuestro mezcal, la encontramos de manera perfecta endos cartas del'),(1169,87,0,30,'/p/d/pd02.jpg'),(1170,88,0,30,'/p/d/pd02.jpg'),(1171,89,0,30,'/p/d/pd02.jpg'),(1172,104,0,30,'1column'),(1173,106,0,30,'container2'),(1174,114,0,30,'MX'),(1175,119,0,30,'papadiablo-ensamble'),(1176,132,0,30,'/p/d/pd02.jpg'),(1177,134,0,30,'2'),(1178,145,0,30,'6'),(1179,146,0,30,'Horno cónico de piedra'),(1180,148,0,30,'4'),(1181,150,0,30,'En tinas de madera'),(1182,155,0,30,'10'),(1183,156,0,30,'Don Alberto Ortíz'),(1184,157,0,30,'Mexicano, madrecuixe, bicusixe, y espadín'),(1185,158,0,30,'Tahona'),(1186,160,0,30,'Doble'),(1187,162,0,30,'70'),(1188,163,0,30,'Miahuatlán de Porfirio Díaz'),(1189,167,0,30,'Alambique de cobre'),(1190,168,0,30,'Ensamble (Mexicano, Bicuixe, Madrecuixe, Espadín)'),(1191,169,0,30,'47.5'),(1196,73,0,31,'Nakawé'),(1197,84,0,31,'Nakawé'),(1198,86,0,31,'Nakawé Nakawé'),(1199,87,0,31,'/n/a/na01.jpg'),(1200,88,0,31,'/n/a/na01.jpg'),(1201,89,0,31,'/n/a/na01.jpg'),(1202,104,0,31,'1column'),(1203,106,0,31,'container2'),(1204,114,0,31,'MX'),(1205,119,0,31,'nakawe'),(1206,132,0,31,'/n/a/na01.jpg'),(1207,134,0,31,'2'),(1208,142,0,31,'Angustifolia'),(1209,145,0,31,'6'),(1210,150,0,31,'En tinas de madera'),(1211,155,0,31,'8 a 10'),(1212,156,0,31,'Abel Martínez Morales'),(1213,157,0,31,'Espadín'),(1214,160,0,31,'Doble'),(1215,162,0,31,'70'),(1216,163,0,31,'Santo Domingo Albarradas'),(1217,167,0,31,'Alambique de cobre / olla de barro'),(1218,168,0,31,'Espadín'),(1219,169,0,31,'42'),(1224,100,0,31,NULL),(1225,116,0,31,NULL),(1226,143,0,31,NULL),(1227,144,0,31,NULL),(1228,146,0,31,NULL),(1229,148,0,31,NULL),(1230,152,0,31,NULL),(1231,153,0,31,NULL),(1232,154,0,31,NULL),(1233,158,0,31,NULL),(1234,159,0,31,NULL),(1235,161,0,31,NULL),(1236,164,0,31,NULL),(1237,165,0,31,NULL),(1238,166,0,31,NULL),(1243,73,0,32,'IBÁ 40. Mezcal Joven'),(1244,84,0,32,'IBÁ 40. Mezcal Joven'),(1245,86,0,32,'IBÁ 40. Mezcal Joven IBÁ 40. Mezcal Joven'),(1246,87,0,32,'/i/b/ib01.jpg'),(1247,88,0,32,'/i/b/ib01.jpg'),(1248,89,0,32,'/i/b/ib01.jpg'),(1249,104,0,32,'1column'),(1250,106,0,32,'container2'),(1251,114,0,32,'MX'),(1252,119,0,32,'iba-40-mezcal-joven'),(1253,132,0,32,'/i/b/ib01.jpg'),(1254,134,0,32,'2'),(1255,142,0,32,'Angustifolia'),(1256,145,0,32,'12'),(1257,150,0,32,'Tinas de madera de encino'),(1258,155,0,32,'7 a 14'),(1259,156,0,32,'Manuel González'),(1260,157,0,32,'Espadín'),(1261,160,0,32,'Doble'),(1262,162,0,32,'70'),(1263,163,0,32,'San Pablo Huiztepec'),(1264,167,0,32,'Alambique de cobre'),(1265,168,0,32,'Espadín'),(1266,169,0,32,'40'),(1271,73,0,33,'Machetazo Papalote'),(1272,84,0,33,'Machetazo Papalote'),(1273,86,0,33,'Machetazo Papalote Machetazo Papalote'),(1274,87,0,33,'/m/c/mch01.jpg'),(1275,88,0,33,'/m/c/mch01.jpg'),(1276,89,0,33,'/m/c/mch01.jpg'),(1277,104,0,33,'1column'),(1278,106,0,33,'container2'),(1279,114,0,33,'MX'),(1280,119,0,33,'machetazo-papalote'),(1281,132,0,33,'/m/c/mch01.jpg'),(1282,134,0,33,'2'),(1283,142,0,33,'Cupreata'),(1284,145,0,33,'12'),(1285,150,0,33,'Barricas de madera'),(1286,155,0,33,'7 a 10'),(1287,156,0,33,'José Morales Uribe'),(1288,157,0,33,'Papalote'),(1289,162,0,33,'70'),(1290,163,0,33,'Mochitlán'),(1291,167,0,33,'Alambique de cobre'),(1292,168,0,33,'Papalote'),(1293,169,0,33,'40'),(1298,73,0,34,'El hijo del Santo'),(1299,84,0,34,'El hijo del Santo'),(1300,86,0,34,'El hijo del Santo El hijo del Santo'),(1301,87,0,34,'/h/s/hs01.jpg'),(1302,88,0,34,'/h/s/hs01.jpg'),(1303,89,0,34,'/h/s/hs01.jpg'),(1304,104,0,34,'1column'),(1305,106,0,34,'container2'),(1306,114,0,34,'MX'),(1307,119,0,34,'el-hijo-del-santo'),(1308,132,0,34,'/h/s/hs01.jpg'),(1309,134,0,34,'2'),(1310,142,0,34,'Angustifolia'),(1311,145,0,34,'12'),(1312,150,0,34,'Tinas de madera de encino'),(1313,155,0,34,'7'),(1314,156,0,34,'Gregorio Velasco'),(1315,157,0,34,'Espadín'),(1316,160,0,34,'Doble'),(1317,162,0,34,'70'),(1318,163,0,34,'San Luis del Río'),(1319,167,0,34,'Alambique de cobre'),(1320,168,0,34,'Espadín'),(1321,169,0,34,'40'),(1326,73,0,35,'Mezcal Chaneque Mexicano'),(1327,84,0,35,'Mezcal Chaneque Mexicano'),(1328,86,0,35,'Mezcal Chaneque Mexicano Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el '),(1329,87,0,35,'/c/h/ch06_1.jpg'),(1330,88,0,35,'/c/h/ch06_1.jpg'),(1331,89,0,35,'/c/h/ch06_1.jpg'),(1332,104,0,35,'1column'),(1333,106,0,35,'container2'),(1334,114,0,35,'MX'),(1335,119,0,35,'mezcal-chaneque-mexicano'),(1336,132,0,35,'/c/h/ch06_1.jpg'),(1337,134,0,35,'2'),(1338,142,0,35,'Rhodacanta'),(1339,145,0,35,'12'),(1340,150,0,35,'Natural en tinas de roble'),(1341,155,0,35,'15 años en promedio'),(1342,156,0,35,'Gonzalo Martínez Sernas, Justino Ríos Martínez, Juan Lorenzo García y Pedro Meza'),(1343,157,0,35,'Mexicano'),(1344,160,0,35,'Doble'),(1345,162,0,35,'70'),(1346,163,0,35,'Santiago Matatlán, San Agustín Amatengo y Santa María Zoquitlán'),(1347,167,0,35,'Alambique de cobre'),(1348,168,0,35,'Mexicano'),(1349,169,0,35,'51'),(1354,73,0,36,'Mezcal Chaneque Duranguensis'),(1355,84,0,36,'Mezcal Chaneque Duranguensis'),(1356,86,0,36,'Mezcal Chaneque Duranguensis Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y'),(1357,87,0,36,'/c/h/ch06_2.jpg'),(1358,88,0,36,'/c/h/ch06_2.jpg'),(1359,89,0,36,'/c/h/ch06_2.jpg'),(1360,104,0,36,'1column'),(1361,106,0,36,'container2'),(1362,114,0,36,'MX'),(1363,119,0,36,'mezcal-chaneque-duranguensis'),(1364,132,0,36,'/c/h/ch06_2.jpg'),(1365,134,0,36,'2'),(1366,142,0,36,'Duranguensis'),(1367,145,0,36,'12'),(1368,150,0,36,'Natural en tinas de roble'),(1369,155,0,36,'8 a 10'),(1370,156,0,36,'\"Gonzalo Martínez Sernas, Justino Ríos Martínez, Juan Lorenzo García y Pedro Meza\"'),(1371,157,0,36,'Silvestre tobalá'),(1372,160,0,36,'Doble'),(1373,162,0,36,'70'),(1374,163,0,36,'Nombre De Dios'),(1375,167,0,36,'Alambique de cobre'),(1376,168,0,36,'Duranguensis'),(1377,169,0,36,'48');
/*!40000 ALTER TABLE `catalog_product_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav`
--

DROP TABLE IF EXISTS `catalog_product_index_eav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_eav` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  `source_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Original entity Id for attribute value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`,`source_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_eav`
--

LOCK TABLES `catalog_product_index_eav` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav` DISABLE KEYS */;
INSERT INTO `catalog_product_index_eav` VALUES (1,83,1,15,1),(2,83,1,18,2),(3,83,1,16,3),(4,83,1,16,4),(5,83,1,16,5),(6,83,1,17,6),(7,83,1,18,7),(8,83,1,19,8),(9,83,1,19,9),(10,83,1,19,10),(11,83,1,19,11),(12,83,1,19,12),(13,83,1,20,13),(14,83,1,20,14),(15,83,1,20,15),(16,83,1,20,16),(17,83,1,21,17),(18,83,1,22,18),(19,83,1,23,19),(20,83,1,23,20),(21,83,1,23,21),(22,83,1,23,22),(23,83,1,24,23),(24,83,1,24,24),(25,83,1,24,25),(26,83,1,24,26),(27,83,1,24,27),(28,83,1,24,28),(29,83,1,25,29),(30,83,1,25,30),(31,83,1,26,31),(32,83,1,21,32),(33,83,1,27,33),(34,83,1,28,34),(35,83,1,19,35),(36,83,1,19,36),(1,149,1,8,1),(2,149,1,8,2),(3,149,1,8,3),(4,149,1,8,4),(5,149,1,8,5),(6,149,1,7,6),(7,149,1,8,7),(8,149,1,8,8),(9,149,1,8,9),(10,149,1,8,10),(11,149,1,8,11),(12,149,1,8,12),(13,149,1,8,13),(14,149,1,8,14),(15,149,1,8,15),(16,149,1,8,16),(17,149,1,8,17),(18,149,1,6,18),(19,149,1,8,19),(20,149,1,8,20),(21,149,1,8,21),(22,149,1,8,22),(23,149,1,8,23),(24,149,1,8,24),(25,149,1,8,25),(26,149,1,8,26),(27,149,1,8,27),(28,149,1,8,28),(29,149,1,8,29),(30,149,1,8,30),(31,149,1,8,31),(32,149,1,8,32),(33,149,1,7,33),(34,149,1,8,34),(35,149,1,8,35),(36,149,1,6,36);
/*!40000 ALTER TABLE `catalog_product_index_eav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_decimal`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_eav_decimal` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  `source_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Original entity Id for attribute value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`,`source_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_eav_decimal`
--

LOCK TABLES `catalog_product_index_eav_decimal` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_decimal_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_eav_decimal_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  `source_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Original entity Id for attribute value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`,`source_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_eav_decimal_idx`
--

LOCK TABLES `catalog_product_index_eav_decimal_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_decimal_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_eav_decimal_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  `source_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Original entity Id for attribute value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`,`source_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_eav_decimal_tmp`
--

LOCK TABLES `catalog_product_index_eav_decimal_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_eav_decimal_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_eav_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  `source_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Original entity Id for attribute value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`,`source_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_eav_idx`
--

LOCK TABLES `catalog_product_index_eav_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav_idx` DISABLE KEYS */;
INSERT INTO `catalog_product_index_eav_idx` VALUES (1,83,1,15,1),(2,83,1,18,2),(3,83,1,16,3),(4,83,1,16,4),(5,83,1,16,5),(6,83,1,17,6),(7,83,1,18,7),(8,83,1,19,8),(9,83,1,19,9),(10,83,1,19,10),(11,83,1,19,11),(12,83,1,19,12),(13,83,1,20,13),(14,83,1,20,14),(15,83,1,20,15),(16,83,1,20,16),(17,83,1,21,17),(18,83,1,22,18),(19,83,1,23,19),(20,83,1,23,20),(21,83,1,23,21),(22,83,1,23,22),(1,149,1,8,1),(2,149,1,8,2),(3,149,1,8,3),(4,149,1,8,4),(5,149,1,8,5),(6,149,1,7,6),(7,149,1,8,7),(8,149,1,8,8),(9,149,1,8,9),(10,149,1,8,10),(11,149,1,8,11),(12,149,1,8,12),(13,149,1,8,13),(14,149,1,8,14),(15,149,1,8,15),(16,149,1,8,16),(17,149,1,8,17),(18,149,1,6,18),(19,149,1,8,19),(20,149,1,8,20),(21,149,1,8,21),(22,149,1,8,22);
/*!40000 ALTER TABLE `catalog_product_index_eav_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_eav_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_eav_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_eav_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  `source_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Original entity Id for attribute value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`,`source_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_STORE_ID` (`store_id`),
  KEY `CATALOG_PRODUCT_INDEX_EAV_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_eav_tmp`
--

LOCK TABLES `catalog_product_index_eav_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_eav_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_eav_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price`
--

DROP TABLE IF EXISTS `catalog_product_index_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_MIN_PRICE` (`min_price`),
  KEY `CAT_PRD_IDX_PRICE_WS_ID_CSTR_GROUP_ID_MIN_PRICE` (`website_id`,`customer_group_id`,`min_price`),
  CONSTRAINT `CATALOG_PRODUCT_INDEX_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_IDX_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_IDX_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price`
--

LOCK TABLES `catalog_product_index_price` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price` DISABLE KEYS */;
INSERT INTO `catalog_product_index_price` VALUES (1,0,1,2,39.8000,39.8000,238.8000,238.8000,NULL),(1,1,1,2,39.8000,39.8000,238.8000,238.8000,NULL),(1,2,1,2,39.8000,39.8000,238.8000,238.8000,NULL),(1,3,1,2,39.8000,39.8000,238.8000,238.8000,NULL),(2,0,1,2,24.9500,24.9500,149.7000,149.7000,NULL),(2,1,1,2,24.9500,24.9500,149.7000,149.7000,NULL),(2,2,1,2,24.9500,24.9500,149.7000,149.7000,NULL),(2,3,1,2,24.9500,24.9500,149.7000,149.7000,NULL),(3,0,1,2,27.0000,27.0000,324.0000,324.0000,NULL),(3,1,1,2,27.0000,27.0000,324.0000,324.0000,NULL),(3,2,1,2,27.0000,27.0000,324.0000,324.0000,NULL),(3,3,1,2,27.0000,27.0000,324.0000,324.0000,NULL),(4,0,1,2,39.7000,39.7000,476.4000,476.4000,NULL),(4,1,1,2,39.7000,39.7000,476.4000,476.4000,NULL),(4,2,1,2,39.7000,39.7000,476.4000,476.4000,NULL),(4,3,1,2,39.7000,39.7000,476.4000,476.4000,NULL),(5,0,1,2,12.4500,12.4500,298.8000,298.8000,NULL),(5,1,1,2,12.4500,12.4500,298.8000,298.8000,NULL),(5,2,1,2,12.4500,12.4500,298.8000,298.8000,NULL),(5,3,1,2,12.4500,12.4500,298.8000,298.8000,NULL),(6,0,1,2,39.0000,39.0000,468.0000,468.0000,NULL),(6,1,1,2,39.0000,39.0000,468.0000,468.0000,NULL),(6,2,1,2,39.0000,39.0000,468.0000,468.0000,NULL),(6,3,1,2,39.0000,39.0000,468.0000,468.0000,NULL),(7,0,1,2,12.5000,12.5000,300.0000,300.0000,NULL),(7,1,1,2,12.5000,12.5000,300.0000,300.0000,NULL),(7,2,1,2,12.5000,12.5000,300.0000,300.0000,NULL),(7,3,1,2,12.5000,12.5000,300.0000,300.0000,NULL),(8,0,1,2,55.0000,55.0000,660.0000,660.0000,NULL),(8,1,1,2,55.0000,55.0000,660.0000,660.0000,NULL),(8,2,1,2,55.0000,55.0000,660.0000,660.0000,NULL),(8,3,1,2,55.0000,55.0000,660.0000,660.0000,NULL),(9,0,1,2,45.0000,45.0000,540.0000,540.0000,NULL),(9,1,1,2,45.0000,45.0000,540.0000,540.0000,NULL),(9,2,1,2,45.0000,45.0000,540.0000,540.0000,NULL),(9,3,1,2,45.0000,45.0000,540.0000,540.0000,NULL),(10,0,1,2,28.0000,28.0000,28.0000,28.0000,NULL),(10,1,1,2,28.0000,28.0000,28.0000,28.0000,NULL),(10,2,1,2,28.0000,28.0000,28.0000,28.0000,NULL),(10,3,1,2,28.0000,28.0000,28.0000,28.0000,NULL),(11,0,1,2,54.0000,54.0000,54.0000,54.0000,NULL),(11,1,1,2,54.0000,54.0000,54.0000,54.0000,NULL),(11,2,1,2,54.0000,54.0000,54.0000,54.0000,NULL),(11,3,1,2,54.0000,54.0000,54.0000,54.0000,NULL),(12,0,1,2,48.0000,48.0000,48.0000,48.0000,NULL),(12,1,1,2,48.0000,48.0000,48.0000,48.0000,NULL),(12,2,1,2,48.0000,48.0000,48.0000,48.0000,NULL),(12,3,1,2,48.0000,48.0000,48.0000,48.0000,NULL),(13,0,1,2,34.0000,34.0000,34.0000,34.0000,NULL),(13,1,1,2,34.0000,34.0000,34.0000,34.0000,NULL),(13,2,1,2,34.0000,34.0000,34.0000,34.0000,NULL),(13,3,1,2,34.0000,34.0000,34.0000,34.0000,NULL),(14,0,1,2,42.4400,42.4400,42.4400,42.4400,NULL),(14,1,1,2,42.4400,42.4400,42.4400,42.4400,NULL),(14,2,1,2,42.4400,42.4400,42.4400,42.4400,NULL),(14,3,1,2,42.4400,42.4400,42.4400,42.4400,NULL),(15,0,1,2,50.8500,50.8500,50.8500,50.8500,NULL),(15,1,1,2,50.8500,50.8500,50.8500,50.8500,NULL),(15,2,1,2,50.8500,50.8500,50.8500,50.8500,NULL),(15,3,1,2,50.8500,50.8500,50.8500,50.8500,NULL),(16,0,1,2,60.1400,60.1400,60.1400,60.1400,NULL),(16,1,1,2,60.1400,60.1400,60.1400,60.1400,NULL),(16,2,1,2,60.1400,60.1400,60.1400,60.1400,NULL),(16,3,1,2,60.1400,60.1400,60.1400,60.1400,NULL),(17,0,1,2,49.0000,49.0000,49.0000,49.0000,NULL),(17,1,1,2,49.0000,49.0000,49.0000,49.0000,NULL),(17,2,1,2,49.0000,49.0000,49.0000,49.0000,NULL),(17,3,1,2,49.0000,49.0000,49.0000,49.0000,NULL),(18,0,1,2,121.6300,121.6300,121.6300,121.6300,NULL),(18,1,1,2,121.6300,121.6300,121.6300,121.6300,NULL),(18,2,1,2,121.6300,121.6300,121.6300,121.6300,NULL),(18,3,1,2,121.6300,121.6300,121.6300,121.6300,NULL),(19,0,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(19,1,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(19,2,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(19,3,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(20,0,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(20,1,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(20,2,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(20,3,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(21,0,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(21,1,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(21,2,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(21,3,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(22,0,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(22,1,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(22,2,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(22,3,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(23,0,1,2,148.2400,148.2400,148.2400,148.2400,NULL),(23,1,1,2,148.2400,148.2400,148.2400,148.2400,NULL),(23,2,1,2,148.2400,148.2400,148.2400,148.2400,NULL),(23,3,1,2,148.2400,148.2400,148.2400,148.2400,NULL),(24,0,1,2,126.3200,126.3200,126.3200,126.3200,NULL),(24,1,1,2,126.3200,126.3200,126.3200,126.3200,NULL),(24,2,1,2,126.3200,126.3200,126.3200,126.3200,NULL),(24,3,1,2,126.3200,126.3200,126.3200,126.3200,NULL),(25,0,1,2,136.8200,136.8200,136.8200,136.8200,NULL),(25,1,1,2,136.8200,136.8200,136.8200,136.8200,NULL),(25,2,1,2,136.8200,136.8200,136.8200,136.8200,NULL),(25,3,1,2,136.8200,136.8200,136.8200,136.8200,NULL),(26,0,1,2,126.5800,126.5800,126.5800,126.5800,NULL),(26,1,1,2,126.5800,126.5800,126.5800,126.5800,NULL),(26,2,1,2,126.5800,126.5800,126.5800,126.5800,NULL),(26,3,1,2,126.5800,126.5800,126.5800,126.5800,NULL),(27,0,1,2,137.2900,137.2900,137.2900,137.2900,NULL),(27,1,1,2,137.2900,137.2900,137.2900,137.2900,NULL),(27,2,1,2,137.2900,137.2900,137.2900,137.2900,NULL),(27,3,1,2,137.2900,137.2900,137.2900,137.2900,NULL),(28,0,1,2,136.5000,136.5000,136.5000,136.5000,NULL),(28,1,1,2,136.5000,136.5000,136.5000,136.5000,NULL),(28,2,1,2,136.5000,136.5000,136.5000,136.5000,NULL),(28,3,1,2,136.5000,136.5000,136.5000,136.5000,NULL),(29,0,1,2,51.4100,51.4100,51.4100,51.4100,NULL),(29,1,1,2,51.4100,51.4100,51.4100,51.4100,NULL),(29,2,1,2,51.4100,51.4100,51.4100,51.4100,NULL),(29,3,1,2,51.4100,51.4100,51.4100,51.4100,NULL),(30,0,1,2,70.5600,70.5600,70.5600,70.5600,NULL),(30,1,1,2,70.5600,70.5600,70.5600,70.5600,NULL),(30,2,1,2,70.5600,70.5600,70.5600,70.5600,NULL),(30,3,1,2,70.5600,70.5600,70.5600,70.5600,NULL),(31,0,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(31,1,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(31,2,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(31,3,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(32,0,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(32,1,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(32,2,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(32,3,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(33,0,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(33,1,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(33,2,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(33,3,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(34,0,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(34,1,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(34,2,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(34,3,1,2,0.0000,0.0000,0.0000,0.0000,NULL),(35,0,1,2,75.1000,75.1000,75.1000,75.1000,NULL),(35,1,1,2,75.1000,75.1000,75.1000,75.1000,NULL),(35,2,1,2,75.1000,75.1000,75.1000,75.1000,NULL),(35,3,1,2,75.1000,75.1000,75.1000,75.1000,NULL),(36,0,1,2,64.0000,64.0000,64.0000,64.0000,NULL),(36,1,1,2,64.0000,64.0000,64.0000,64.0000,NULL),(36,2,1,2,64.0000,64.0000,64.0000,64.0000,NULL),(36,3,1,2,64.0000,64.0000,64.0000,64.0000,NULL);
/*!40000 ALTER TABLE `catalog_product_index_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_bundle_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_bundle_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Idx';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_bundle_idx`
--

LOCK TABLES `catalog_product_index_price_bundle_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_bundle_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_bundle_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Idx';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_bundle_opt_idx`
--

LOCK TABLES `catalog_product_index_price_bundle_opt_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_opt_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_opt_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_bundle_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_bundle_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Tmp';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_bundle_opt_tmp`
--

LOCK TABLES `catalog_product_index_price_bundle_opt_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_opt_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_opt_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_bundle_sel_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_bundle_sel_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Idx';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_bundle_sel_idx`
--

LOCK TABLES `catalog_product_index_price_bundle_sel_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_sel_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_sel_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_bundle_sel_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_bundle_sel_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Tmp';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_bundle_sel_tmp`
--

LOCK TABLES `catalog_product_index_price_bundle_sel_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_sel_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_sel_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_bundle_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_bundle_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Tmp';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_bundle_tmp`
--

LOCK TABLES `catalog_product_index_price_bundle_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_bundle_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_cfg_opt_agr_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_cfg_opt_agr_idx` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_cfg_opt_agr_idx`
--

LOCK TABLES `catalog_product_index_price_cfg_opt_agr_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_agr_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_agr_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_cfg_opt_agr_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_cfg_opt_agr_tmp` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_cfg_opt_agr_tmp`
--

LOCK TABLES `catalog_product_index_price_cfg_opt_agr_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_agr_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_agr_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_cfg_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_cfg_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_cfg_opt_idx`
--

LOCK TABLES `catalog_product_index_price_cfg_opt_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_cfg_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_cfg_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_cfg_opt_tmp`
--

LOCK TABLES `catalog_product_index_price_cfg_opt_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_cfg_opt_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_downlod_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_downlod_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_downlod_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indexer Table for price of downloadable products';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_downlod_idx`
--

LOCK TABLES `catalog_product_index_price_downlod_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_downlod_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_downlod_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_downlod_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_downlod_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_downlod_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Temporary Indexer Table for price of downloadable products';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_downlod_tmp`
--

LOCK TABLES `catalog_product_index_price_downlod_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_downlod_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_downlod_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_final_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_final_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_final_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_final_idx`
--

LOCK TABLES `catalog_product_index_price_final_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_final_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_final_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_final_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_final_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_final_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_final_tmp`
--

LOCK TABLES `catalog_product_index_price_final_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_final_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_final_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_IDX_MIN_PRICE` (`min_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_idx`
--

LOCK TABLES `catalog_product_index_price_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_idx` DISABLE KEYS */;
INSERT INTO `catalog_product_index_price_idx` VALUES (1,0,1,2,39.8000,39.8000,238.8000,238.8000,NULL),(1,1,1,2,39.8000,39.8000,238.8000,238.8000,NULL),(1,2,1,2,39.8000,39.8000,238.8000,238.8000,NULL),(1,3,1,2,39.8000,39.8000,238.8000,238.8000,NULL),(2,0,1,2,24.9500,24.9500,149.7000,149.7000,NULL),(2,1,1,2,24.9500,24.9500,149.7000,149.7000,NULL),(2,2,1,2,24.9500,24.9500,149.7000,149.7000,NULL),(2,3,1,2,24.9500,24.9500,149.7000,149.7000,NULL),(3,0,1,2,27.0000,27.0000,324.0000,324.0000,NULL),(3,1,1,2,27.0000,27.0000,324.0000,324.0000,NULL),(3,2,1,2,27.0000,27.0000,324.0000,324.0000,NULL),(3,3,1,2,27.0000,27.0000,324.0000,324.0000,NULL),(4,0,1,2,39.7000,39.7000,476.4000,476.4000,NULL),(4,1,1,2,39.7000,39.7000,476.4000,476.4000,NULL),(4,2,1,2,39.7000,39.7000,476.4000,476.4000,NULL),(4,3,1,2,39.7000,39.7000,476.4000,476.4000,NULL),(5,0,1,2,12.4500,12.4500,298.8000,298.8000,NULL),(5,1,1,2,12.4500,12.4500,298.8000,298.8000,NULL),(5,2,1,2,12.4500,12.4500,298.8000,298.8000,NULL),(5,3,1,2,12.4500,12.4500,298.8000,298.8000,NULL),(6,0,1,2,39.0000,39.0000,468.0000,468.0000,NULL),(6,1,1,2,39.0000,39.0000,468.0000,468.0000,NULL),(6,2,1,2,39.0000,39.0000,468.0000,468.0000,NULL),(6,3,1,2,39.0000,39.0000,468.0000,468.0000,NULL),(7,0,1,2,12.5000,12.5000,300.0000,300.0000,NULL),(7,1,1,2,12.5000,12.5000,300.0000,300.0000,NULL),(7,2,1,2,12.5000,12.5000,300.0000,300.0000,NULL),(7,3,1,2,12.5000,12.5000,300.0000,300.0000,NULL),(8,0,1,2,55.0000,55.0000,660.0000,660.0000,NULL),(8,1,1,2,55.0000,55.0000,660.0000,660.0000,NULL),(8,2,1,2,55.0000,55.0000,660.0000,660.0000,NULL),(8,3,1,2,55.0000,55.0000,660.0000,660.0000,NULL),(9,0,1,2,45.0000,45.0000,540.0000,540.0000,NULL),(9,1,1,2,45.0000,45.0000,540.0000,540.0000,NULL),(9,2,1,2,45.0000,45.0000,540.0000,540.0000,NULL),(9,3,1,2,45.0000,45.0000,540.0000,540.0000,NULL),(10,0,1,2,28.0000,28.0000,28.0000,28.0000,NULL),(10,1,1,2,28.0000,28.0000,28.0000,28.0000,NULL),(10,2,1,2,28.0000,28.0000,28.0000,28.0000,NULL),(10,3,1,2,28.0000,28.0000,28.0000,28.0000,NULL),(11,0,1,2,54.0000,54.0000,54.0000,54.0000,NULL),(11,1,1,2,54.0000,54.0000,54.0000,54.0000,NULL),(11,2,1,2,54.0000,54.0000,54.0000,54.0000,NULL),(11,3,1,2,54.0000,54.0000,54.0000,54.0000,NULL),(12,0,1,2,48.0000,48.0000,48.0000,48.0000,NULL),(12,1,1,2,48.0000,48.0000,48.0000,48.0000,NULL),(12,2,1,2,48.0000,48.0000,48.0000,48.0000,NULL),(12,3,1,2,48.0000,48.0000,48.0000,48.0000,NULL),(13,0,1,2,34.0000,34.0000,34.0000,34.0000,NULL),(13,1,1,2,34.0000,34.0000,34.0000,34.0000,NULL),(13,2,1,2,34.0000,34.0000,34.0000,34.0000,NULL),(13,3,1,2,34.0000,34.0000,34.0000,34.0000,NULL),(14,0,1,2,42.4400,42.4400,42.4400,42.4400,NULL),(14,1,1,2,42.4400,42.4400,42.4400,42.4400,NULL),(14,2,1,2,42.4400,42.4400,42.4400,42.4400,NULL),(14,3,1,2,42.4400,42.4400,42.4400,42.4400,NULL),(15,0,1,2,50.8500,50.8500,50.8500,50.8500,NULL),(15,1,1,2,50.8500,50.8500,50.8500,50.8500,NULL),(15,2,1,2,50.8500,50.8500,50.8500,50.8500,NULL),(15,3,1,2,50.8500,50.8500,50.8500,50.8500,NULL),(16,0,1,2,60.1400,60.1400,60.1400,60.1400,NULL),(16,1,1,2,60.1400,60.1400,60.1400,60.1400,NULL),(16,2,1,2,60.1400,60.1400,60.1400,60.1400,NULL),(16,3,1,2,60.1400,60.1400,60.1400,60.1400,NULL),(17,0,1,2,49.0000,49.0000,49.0000,49.0000,NULL),(17,1,1,2,49.0000,49.0000,49.0000,49.0000,NULL),(17,2,1,2,49.0000,49.0000,49.0000,49.0000,NULL),(17,3,1,2,49.0000,49.0000,49.0000,49.0000,NULL),(18,0,1,2,121.6300,121.6300,121.6300,121.6300,NULL),(18,1,1,2,121.6300,121.6300,121.6300,121.6300,NULL),(18,2,1,2,121.6300,121.6300,121.6300,121.6300,NULL),(18,3,1,2,121.6300,121.6300,121.6300,121.6300,NULL),(19,0,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(19,1,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(19,2,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(19,3,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(20,0,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(20,1,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(20,2,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(20,3,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(21,0,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(21,1,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(21,2,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(21,3,1,2,81.2500,81.2500,81.2500,81.2500,NULL),(22,0,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(22,1,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(22,2,1,2,70.0000,70.0000,70.0000,70.0000,NULL),(22,3,1,2,70.0000,70.0000,70.0000,70.0000,NULL);
/*!40000 ALTER TABLE `catalog_product_index_price_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_opt_agr_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_opt_agr_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_opt_agr_idx`
--

LOCK TABLES `catalog_product_index_price_opt_agr_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_agr_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_agr_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_opt_agr_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_opt_agr_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_opt_agr_tmp`
--

LOCK TABLES `catalog_product_index_price_opt_agr_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_agr_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_agr_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_opt_idx`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_opt_idx`
--

LOCK TABLES `catalog_product_index_price_opt_idx` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_idx` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_opt_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_opt_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_opt_tmp`
--

LOCK TABLES `catalog_product_index_price_opt_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_opt_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_price_tmp`
--

DROP TABLE IF EXISTS `catalog_product_index_price_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_price_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_WEBSITE_ID` (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_PRICE_TMP_MIN_PRICE` (`min_price`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Temp Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_price_tmp`
--

LOCK TABLES `catalog_product_index_price_tmp` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_price_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_price_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_tier_price`
--

DROP TABLE IF EXISTS `catalog_product_index_tier_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_tier_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOG_PRODUCT_INDEX_TIER_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_IDX_TIER_PRICE_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_tier_price`
--

LOCK TABLES `catalog_product_index_tier_price` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_tier_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_index_tier_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_index_website`
--

DROP TABLE IF EXISTS `catalog_product_index_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_index_website` (
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `website_date` date DEFAULT NULL COMMENT 'Website Date',
  `rate` float DEFAULT '1' COMMENT 'Rate',
  PRIMARY KEY (`website_id`),
  KEY `CATALOG_PRODUCT_INDEX_WEBSITE_WEBSITE_DATE` (`website_date`),
  CONSTRAINT `CAT_PRD_IDX_WS_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Website Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_index_website`
--

LOCK TABLES `catalog_product_index_website` WRITE;
/*!40000 ALTER TABLE `catalog_product_index_website` DISABLE KEYS */;
INSERT INTO `catalog_product_index_website` VALUES (1,'2020-03-18',1);
/*!40000 ALTER TABLE `catalog_product_index_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link`
--

DROP TABLE IF EXISTS `catalog_product_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `linked_product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Linked Product ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `CATALOG_PRODUCT_LINK_LINK_TYPE_ID_PRODUCT_ID_LINKED_PRODUCT_ID` (`link_type_id`,`product_id`,`linked_product_id`),
  KEY `CATALOG_PRODUCT_LINK_PRODUCT_ID` (`product_id`),
  KEY `CATALOG_PRODUCT_LINK_LINKED_PRODUCT_ID` (`linked_product_id`),
  CONSTRAINT `CATALOG_PRODUCT_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_LNK_LNKED_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`linked_product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_LNK_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Product Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_link`
--

LOCK TABLES `catalog_product_link` WRITE;
/*!40000 ALTER TABLE `catalog_product_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_attribute`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_link_attribute` (
  `product_link_attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Link Attribute ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  `product_link_attribute_code` varchar(32) DEFAULT NULL COMMENT 'Product Link Attribute Code',
  `data_type` varchar(32) DEFAULT NULL COMMENT 'Data Type',
  PRIMARY KEY (`product_link_attribute_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_LINK_TYPE_ID` (`link_type_id`),
  CONSTRAINT `CAT_PRD_LNK_ATTR_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Attribute Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_link_attribute`
--

LOCK TABLES `catalog_product_link_attribute` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute` DISABLE KEYS */;
INSERT INTO `catalog_product_link_attribute` VALUES (1,1,'position','int'),(2,4,'position','int'),(3,5,'position','int'),(4,3,'position','int'),(5,3,'qty','decimal');
/*!40000 ALTER TABLE `catalog_product_link_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_attribute_decimal`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_decimal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_link_attribute_decimal` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_DEC_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_DECIMAL_LINK_ID` (`link_id`),
  CONSTRAINT `CAT_PRD_LNK_ATTR_DEC_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_AB2EFA9A14F7BCF1D5400056203D14B6` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Decimal Attribute Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_link_attribute_decimal`
--

LOCK TABLES `catalog_product_link_attribute_decimal` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute_decimal` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_link_attribute_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_attribute_int`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_int`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_link_attribute_int` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_INT_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_INT_LINK_ID` (`link_id`),
  CONSTRAINT `CAT_PRD_LNK_ATTR_INT_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D6D878F8BA2A4282F8DDED7E6E3DE35C` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Integer Attribute Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_link_attribute_int`
--

LOCK TABLES `catalog_product_link_attribute_int` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute_int` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_link_attribute_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_attribute_varchar`
--

DROP TABLE IF EXISTS `catalog_product_link_attribute_varchar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_link_attribute_varchar` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_LNK_ATTR_VCHR_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `CATALOG_PRODUCT_LINK_ATTRIBUTE_VARCHAR_LINK_ID` (`link_id`),
  CONSTRAINT `CAT_PRD_LNK_ATTR_VCHR_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_DEE9C4DA61CFCC01DFCF50F0D79CEA51` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Varchar Attribute Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_link_attribute_varchar`
--

LOCK TABLES `catalog_product_link_attribute_varchar` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_attribute_varchar` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_link_attribute_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_link_type`
--

DROP TABLE IF EXISTS `catalog_product_link_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_link_type` (
  `link_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Type ID',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  PRIMARY KEY (`link_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Type Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_link_type`
--

LOCK TABLES `catalog_product_link_type` WRITE;
/*!40000 ALTER TABLE `catalog_product_link_type` DISABLE KEYS */;
INSERT INTO `catalog_product_link_type` VALUES (1,'relation'),(3,'super'),(4,'up_sell'),(5,'cross_sell');
/*!40000 ALTER TABLE `catalog_product_link_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_option`
--

DROP TABLE IF EXISTS `catalog_product_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `type` varchar(50) DEFAULT NULL COMMENT 'Type',
  `is_require` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Required',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `max_characters` int(10) unsigned DEFAULT NULL COMMENT 'Max Characters',
  `file_extension` varchar(50) DEFAULT NULL COMMENT 'File Extension',
  `image_size_x` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size X',
  `image_size_y` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size Y',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRODUCT_ID` (`product_id`),
  CONSTRAINT `CAT_PRD_OPT_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_option`
--

LOCK TABLES `catalog_product_option` WRITE;
/*!40000 ALTER TABLE `catalog_product_option` DISABLE KEYS */;
INSERT INTO `catalog_product_option` VALUES (4,2,'drop_down',1,NULL,0,NULL,0,0,1),(5,1,'drop_down',1,NULL,0,NULL,0,0,1),(6,3,'drop_down',1,NULL,0,NULL,0,0,1),(7,4,'drop_down',1,NULL,0,NULL,0,0,1),(8,5,'drop_down',1,NULL,0,NULL,0,0,1),(9,6,'drop_down',1,NULL,0,NULL,0,0,1),(10,7,'drop_down',1,NULL,0,NULL,0,0,1),(11,8,'drop_down',1,NULL,0,NULL,0,0,1),(12,9,'drop_down',1,NULL,0,NULL,0,0,1);
/*!40000 ALTER TABLE `catalog_product_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_option_price`
--

DROP TABLE IF EXISTS `catalog_product_option_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_option_price` (
  `option_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Price ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_price_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_PRICE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_PRICE_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_OPTION_PRICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_OPT_PRICE_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Price Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_option_price`
--

LOCK TABLES `catalog_product_option_price` WRITE;
/*!40000 ALTER TABLE `catalog_product_option_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_option_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_option_title`
--

DROP TABLE IF EXISTS `catalog_product_option_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_option_title` (
  `option_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Title ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_title_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TITLE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_OPTION_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_OPT_TTL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Title Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_option_title`
--

LOCK TABLES `catalog_product_option_title` WRITE;
/*!40000 ALTER TABLE `catalog_product_option_title` DISABLE KEYS */;
INSERT INTO `catalog_product_option_title` VALUES (4,4,0,'Unidad'),(5,5,0,'Unidad'),(6,6,0,'Unidad'),(7,7,0,'Unidad'),(8,8,0,'Unidad'),(9,9,0,'Unidad'),(10,10,0,'Unidad'),(11,11,0,'Unidad'),(12,12,0,'Unidad');
/*!40000 ALTER TABLE `catalog_product_option_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_option_type_price`
--

DROP TABLE IF EXISTS `catalog_product_option_type_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_option_type_price` (
  `option_type_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Price ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_type_price_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B523E3378E8602F376CC415825576B7F` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Price Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_option_type_price`
--

LOCK TABLES `catalog_product_option_type_price` WRITE;
/*!40000 ALTER TABLE `catalog_product_option_type_price` DISABLE KEYS */;
INSERT INTO `catalog_product_option_type_price` VALUES (4,7,0,500.0000,'percent'),(5,9,0,500.0000,'percent'),(6,11,0,1100.0000,'percent'),(7,13,0,1100.0000,'percent'),(8,15,0,2300.0000,'percent'),(9,17,0,1100.0000,'percent'),(10,19,0,2300.0000,'percent'),(11,21,0,1100.0000,'percent'),(12,23,0,1100.0000,'percent');
/*!40000 ALTER TABLE `catalog_product_option_type_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_option_type_title`
--

DROP TABLE IF EXISTS `catalog_product_option_type_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_option_type_title` (
  `option_type_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Title ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_type_title_id`),
  UNIQUE KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C085B9CF2C2A302E8043FDEA1937D6A2` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Title Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_option_type_title`
--

LOCK TABLES `catalog_product_option_type_title` WRITE;
/*!40000 ALTER TABLE `catalog_product_option_type_title` DISABLE KEYS */;
INSERT INTO `catalog_product_option_type_title` VALUES (7,7,0,'Caja'),(8,8,0,'botella'),(9,9,0,'Caja'),(10,10,0,'Botella'),(11,11,0,'Caja'),(12,12,0,'Botella'),(13,13,0,'Caja'),(14,14,0,'Botella'),(15,15,0,'Caja'),(16,16,0,'Botella'),(17,17,0,'Caja'),(18,18,0,'Botella'),(19,19,0,'Caja'),(20,20,0,'Botella'),(21,21,0,'Caja'),(22,22,0,'Botella'),(23,23,0,'Caja'),(24,24,0,'Botella');
/*!40000 ALTER TABLE `catalog_product_option_type_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_option_type_value`
--

DROP TABLE IF EXISTS `catalog_product_option_type_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_option_type_value` (
  `option_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_type_id`),
  KEY `CATALOG_PRODUCT_OPTION_TYPE_VALUE_OPTION_ID` (`option_id`),
  CONSTRAINT `CAT_PRD_OPT_TYPE_VAL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Value Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_option_type_value`
--

LOCK TABLES `catalog_product_option_type_value` WRITE;
/*!40000 ALTER TABLE `catalog_product_option_type_value` DISABLE KEYS */;
INSERT INTO `catalog_product_option_type_value` VALUES (7,4,NULL,1),(8,4,NULL,2),(9,5,NULL,1),(10,5,NULL,2),(11,6,NULL,1),(12,6,NULL,2),(13,7,NULL,1),(14,7,NULL,2),(15,8,NULL,1),(16,8,NULL,2),(17,9,NULL,1),(18,9,NULL,2),(19,10,NULL,1),(20,10,NULL,2),(21,11,NULL,1),(22,11,NULL,2),(23,12,NULL,1),(24,12,NULL,2);
/*!40000 ALTER TABLE `catalog_product_option_type_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_price_cl`
--

DROP TABLE IF EXISTS `catalog_product_price_cl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_price_cl` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Version ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalog_product_price_cl';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_price_cl`
--

LOCK TABLES `catalog_product_price_cl` WRITE;
/*!40000 ALTER TABLE `catalog_product_price_cl` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_price_cl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_relation`
--

DROP TABLE IF EXISTS `catalog_product_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_relation` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  PRIMARY KEY (`parent_id`,`child_id`),
  KEY `CATALOG_PRODUCT_RELATION_CHILD_ID` (`child_id`),
  CONSTRAINT `CAT_PRD_RELATION_CHILD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`child_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_RELATION_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Relation Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_relation`
--

LOCK TABLES `catalog_product_relation` WRITE;
/*!40000 ALTER TABLE `catalog_product_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_super_attribute`
--

DROP TABLE IF EXISTS `catalog_product_super_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_super_attribute` (
  `product_super_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Super Attribute ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`product_super_attribute_id`),
  UNIQUE KEY `CATALOG_PRODUCT_SUPER_ATTRIBUTE_PRODUCT_ID_ATTRIBUTE_ID` (`product_id`,`attribute_id`),
  CONSTRAINT `CAT_PRD_SPR_ATTR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_super_attribute`
--

LOCK TABLES `catalog_product_super_attribute` WRITE;
/*!40000 ALTER TABLE `catalog_product_super_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_super_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_super_attribute_label`
--

DROP TABLE IF EXISTS `catalog_product_super_attribute_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_super_attribute_label` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Super Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `use_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Use Default Value',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CAT_PRD_SPR_ATTR_LBL_PRD_SPR_ATTR_ID_STORE_ID` (`product_super_attribute_id`,`store_id`),
  KEY `CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_309442281DF7784210ED82B2CC51E5D5` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Label Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_super_attribute_label`
--

LOCK TABLES `catalog_product_super_attribute_label` WRITE;
/*!40000 ALTER TABLE `catalog_product_super_attribute_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_super_attribute_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_super_link`
--

DROP TABLE IF EXISTS `catalog_product_super_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_super_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `CATALOG_PRODUCT_SUPER_LINK_PRODUCT_ID_PARENT_ID` (`product_id`,`parent_id`),
  KEY `CATALOG_PRODUCT_SUPER_LINK_PARENT_ID` (`parent_id`),
  CONSTRAINT `CAT_PRD_SPR_LNK_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_SPR_LNK_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Link Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_super_link`
--

LOCK TABLES `catalog_product_super_link` WRITE;
/*!40000 ALTER TABLE `catalog_product_super_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_product_super_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product_website`
--

DROP TABLE IF EXISTS `catalog_product_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product_website` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`product_id`,`website_id`),
  KEY `CATALOG_PRODUCT_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CATALOG_PRODUCT_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_PRD_WS_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Website Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product_website`
--

LOCK TABLES `catalog_product_website` WRITE;
/*!40000 ALTER TABLE `catalog_product_website` DISABLE KEYS */;
INSERT INTO `catalog_product_website` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1);
/*!40000 ALTER TABLE `catalog_product_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_url_rewrite_product_category`
--

DROP TABLE IF EXISTS `catalog_url_rewrite_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_url_rewrite_product_category` (
  `url_rewrite_id` int(10) unsigned NOT NULL COMMENT 'url_rewrite_id',
  `category_id` int(10) unsigned NOT NULL COMMENT 'category_id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'product_id',
  KEY `CATALOG_URL_REWRITE_PRODUCT_CATEGORY_CATEGORY_ID_PRODUCT_ID` (`category_id`,`product_id`),
  KEY `CAT_URL_REWRITE_PRD_CTGR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` (`product_id`),
  KEY `FK_BB79E64705D7F17FE181F23144528FC8` (`url_rewrite_id`),
  CONSTRAINT `CAT_URL_REWRITE_PRD_CTGR_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CAT_URL_REWRITE_PRD_CTGR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_BB79E64705D7F17FE181F23144528FC8` FOREIGN KEY (`url_rewrite_id`) REFERENCES `url_rewrite` (`url_rewrite_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='url_rewrite_relation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_url_rewrite_product_category`
--

LOCK TABLES `catalog_url_rewrite_product_category` WRITE;
/*!40000 ALTER TABLE `catalog_url_rewrite_product_category` DISABLE KEYS */;
INSERT INTO `catalog_url_rewrite_product_category` VALUES (70,3,2),(78,4,3),(77,3,3),(81,4,4),(80,3,4),(84,4,1),(83,3,1),(89,4,5),(88,3,5),(92,4,6),(91,3,6),(95,4,7),(94,3,7),(98,4,8),(97,3,8),(101,4,9),(100,3,9),(104,4,10),(103,3,10),(107,4,11),(106,3,11),(110,4,12),(109,3,12),(113,4,13),(112,3,13),(116,4,14),(115,3,14),(119,4,15),(118,3,15),(122,4,16),(121,3,16),(125,4,17),(124,3,17),(128,4,18),(127,3,18),(131,4,19),(130,3,19),(134,4,20),(133,3,20),(137,4,21),(136,3,21),(140,4,22),(139,3,22),(143,4,23),(142,3,23),(146,4,24),(145,3,24),(149,4,25),(148,3,25),(152,4,26),(151,3,26),(155,4,27),(154,3,27),(158,4,28),(157,3,28),(161,4,29),(160,3,29),(164,4,30),(163,3,30),(167,4,31),(166,3,31),(170,4,32),(169,3,32),(173,4,33),(172,3,33),(176,4,34),(175,3,34),(179,4,35),(178,3,35),(182,4,36),(181,3,36);
/*!40000 ALTER TABLE `catalog_url_rewrite_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock`
--

DROP TABLE IF EXISTS `cataloginventory_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cataloginventory_stock` (
  `stock_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Stock Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_name` varchar(255) DEFAULT NULL COMMENT 'Stock Name',
  PRIMARY KEY (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cataloginventory_stock`
--

LOCK TABLES `cataloginventory_stock` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock` VALUES (1,0,'Default');
/*!40000 ALTER TABLE `cataloginventory_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_item`
--

DROP TABLE IF EXISTS `cataloginventory_stock_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cataloginventory_stock_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `stock_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Id',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `min_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Min Qty',
  `use_config_min_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Qty',
  `is_qty_decimal` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Qty Decimal',
  `backorders` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Backorders',
  `use_config_backorders` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Backorders',
  `min_sale_qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'Min Sale Qty',
  `use_config_min_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Sale Qty',
  `max_sale_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Max Sale Qty',
  `use_config_max_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Max Sale Qty',
  `is_in_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is In Stock',
  `low_stock_date` timestamp NULL DEFAULT NULL COMMENT 'Low Stock Date',
  `notify_stock_qty` decimal(12,4) DEFAULT NULL COMMENT 'Notify Stock Qty',
  `use_config_notify_stock_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Notify Stock Qty',
  `manage_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Manage Stock',
  `use_config_manage_stock` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Manage Stock',
  `stock_status_changed_auto` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Status Changed Automatically',
  `use_config_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Qty Increments',
  `qty_increments` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Increments',
  `use_config_enable_qty_inc` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Enable Qty Increments',
  `enable_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Enable Qty Increments',
  `is_decimal_divided` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `CATALOGINVENTORY_STOCK_ITEM_PRODUCT_ID_WEBSITE_ID` (`product_id`,`website_id`),
  KEY `CATALOGINVENTORY_STOCK_ITEM_WEBSITE_ID` (`website_id`),
  KEY `CATALOGINVENTORY_STOCK_ITEM_STOCK_ID` (`stock_id`),
  CONSTRAINT `CATINV_STOCK_ITEM_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `CATINV_STOCK_ITEM_STOCK_ID_CATINV_STOCK_STOCK_ID` FOREIGN KEY (`stock_id`) REFERENCES `cataloginventory_stock` (`stock_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Item';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cataloginventory_stock_item`
--

LOCK TABLES `cataloginventory_stock_item` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_item` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_item` VALUES (1,1,1,997.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(2,2,1,999.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(3,3,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(4,4,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(5,5,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(6,6,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(7,7,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(8,8,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(9,9,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(10,10,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(11,11,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(12,12,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(13,13,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(14,14,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(15,15,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(16,16,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(17,17,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(18,18,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(19,19,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(20,20,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(21,21,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(22,22,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(23,23,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(24,24,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(25,25,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(26,26,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(27,27,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(28,28,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(29,29,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(30,30,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(31,31,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(32,32,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(33,33,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(34,34,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(35,35,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0),(36,36,1,1000.0000,0.0000,1,0,0,1,1.0000,1,10000.0000,1,1,NULL,1.0000,1,1,1,0,1,1.0000,1,0,0,0);
/*!40000 ALTER TABLE `cataloginventory_stock_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cataloginventory_stock_status` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cataloginventory_stock_status`
--

LOCK TABLES `cataloginventory_stock_status` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_status` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_status` VALUES (1,0,1,997.0000,1),(2,0,1,999.0000,1),(3,0,1,1000.0000,1),(4,0,1,1000.0000,1),(5,0,1,1000.0000,1),(6,0,1,1000.0000,1),(7,0,1,1000.0000,1),(8,0,1,1000.0000,1),(9,0,1,1000.0000,1),(10,0,1,1000.0000,1),(11,0,1,1000.0000,1),(12,0,1,1000.0000,1),(13,0,1,1000.0000,1),(14,0,1,1000.0000,1),(15,0,1,1000.0000,1),(16,0,1,1000.0000,1),(17,0,1,1000.0000,1),(18,0,1,1000.0000,1),(19,0,1,1000.0000,1),(20,0,1,1000.0000,1),(21,0,1,1000.0000,1),(22,0,1,1000.0000,1),(23,0,1,1000.0000,1),(24,0,1,1000.0000,1),(25,0,1,1000.0000,1),(26,0,1,1000.0000,1),(27,0,1,1000.0000,1),(28,0,1,1000.0000,1),(29,0,1,1000.0000,1),(30,0,1,1000.0000,1),(31,0,1,1000.0000,1),(32,0,1,1000.0000,1),(33,0,1,1000.0000,1),(34,0,1,1000.0000,1),(35,0,1,1000.0000,1),(36,0,1,1000.0000,1);
/*!40000 ALTER TABLE `cataloginventory_stock_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status_idx`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status_idx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cataloginventory_stock_status_idx` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_IDX_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_IDX_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Idx';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cataloginventory_stock_status_idx`
--

LOCK TABLES `cataloginventory_stock_status_idx` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_status_idx` DISABLE KEYS */;
INSERT INTO `cataloginventory_stock_status_idx` VALUES (1,0,1,997.0000,1),(2,0,1,999.0000,1),(3,0,1,1000.0000,1),(4,0,1,1000.0000,1),(5,0,1,1000.0000,1),(6,0,1,1000.0000,1),(7,0,1,1000.0000,1),(8,0,1,1000.0000,1),(9,0,1,1000.0000,1),(10,0,1,1000.0000,1),(11,0,1,1000.0000,1),(12,0,1,1000.0000,1),(13,0,1,1000.0000,1),(14,0,1,1000.0000,1),(15,0,1,1000.0000,1),(16,0,1,1000.0000,1),(17,0,1,1000.0000,1),(18,0,1,1000.0000,1),(19,0,1,1000.0000,1),(20,0,1,1000.0000,1),(21,0,1,1000.0000,1),(22,0,1,1000.0000,1);
/*!40000 ALTER TABLE `cataloginventory_stock_status_idx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cataloginventory_stock_status_tmp`
--

DROP TABLE IF EXISTS `cataloginventory_stock_status_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cataloginventory_stock_status_tmp` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_TMP_STOCK_ID` (`stock_id`),
  KEY `CATALOGINVENTORY_STOCK_STATUS_TMP_WEBSITE_ID` (`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Tmp';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cataloginventory_stock_status_tmp`
--

LOCK TABLES `cataloginventory_stock_status_tmp` WRITE;
/*!40000 ALTER TABLE `cataloginventory_stock_status_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `cataloginventory_stock_status_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogrule`
--

DROP TABLE IF EXISTS `catalogrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  PRIMARY KEY (`rule_id`),
  KEY `CATALOGRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogrule`
--

LOCK TABLES `catalogrule` WRITE;
/*!40000 ALTER TABLE `catalogrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogrule_customer_group`
--

DROP TABLE IF EXISTS `catalogrule_customer_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `CATALOGRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `CATALOGRULE_CUSTOMER_GROUP_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `CATRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Customer Groups Relations';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogrule_customer_group`
--

LOCK TABLES `catalogrule_customer_group` WRITE;
/*!40000 ALTER TABLE `catalogrule_customer_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogrule_customer_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogrule_group_website`
--

DROP TABLE IF EXISTS `catalogrule_group_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogrule_group_website` (
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`,`website_id`),
  KEY `CATALOGRULE_GROUP_WEBSITE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CATALOGRULE_GROUP_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE,
  CONSTRAINT `CATRULE_GROUP_WS_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Group Website';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogrule_group_website`
--

LOCK TABLES `catalogrule_group_website` WRITE;
/*!40000 ALTER TABLE `catalogrule_group_website` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogrule_group_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogrule_product`
--

DROP TABLE IF EXISTS `catalogrule_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogrule_product` (
  `rule_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `from_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'From Time',
  `to_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'To time',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `action_operator` varchar(10) DEFAULT 'to_fixed' COMMENT 'Action Operator',
  `action_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Action Amount',
  `action_stop` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Action Stop',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_product_id`),
  UNIQUE KEY `IDX_EAA51B56FF092A0DCB795D1CEF812B7B` (`rule_id`,`from_time`,`to_time`,`website_id`,`customer_group_id`,`product_id`,`sort_order`),
  KEY `CATALOGRULE_PRODUCT_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_PRODUCT_WEBSITE_ID` (`website_id`),
  KEY `CATALOGRULE_PRODUCT_FROM_TIME` (`from_time`),
  KEY `CATALOGRULE_PRODUCT_TO_TIME` (`to_time`),
  KEY `CATALOGRULE_PRODUCT_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogrule_product`
--

LOCK TABLES `catalogrule_product` WRITE;
/*!40000 ALTER TABLE `catalogrule_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogrule_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogrule_product_price`
--

DROP TABLE IF EXISTS `catalogrule_product_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogrule_product_price` (
  `rule_product_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product PriceId',
  `rule_date` date NOT NULL COMMENT 'Rule Date',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `rule_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rule Price',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `latest_start_date` date DEFAULT NULL COMMENT 'Latest StartDate',
  `earliest_end_date` date DEFAULT NULL COMMENT 'Earliest EndDate',
  PRIMARY KEY (`rule_product_price_id`),
  UNIQUE KEY `CATRULE_PRD_PRICE_RULE_DATE_WS_ID_CSTR_GROUP_ID_PRD_ID` (`rule_date`,`website_id`,`customer_group_id`,`product_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_WEBSITE_ID` (`website_id`),
  KEY `CATALOGRULE_PRODUCT_PRICE_PRODUCT_ID` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product Price';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogrule_product_price`
--

LOCK TABLES `catalogrule_product_price` WRITE;
/*!40000 ALTER TABLE `catalogrule_product_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogrule_product_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogrule_website`
--

DROP TABLE IF EXISTS `catalogrule_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `CATALOGRULE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CATALOGRULE_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `CATALOGRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Websites Relations';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogrule_website`
--

LOCK TABLES `catalogrule_website` WRITE;
/*!40000 ALTER TABLE `catalogrule_website` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogrule_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogsearch_fulltext_scope1`
--

DROP TABLE IF EXISTS `catalogsearch_fulltext_scope1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogsearch_fulltext_scope1` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` int(10) unsigned NOT NULL COMMENT 'Attribute_id',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`entity_id`,`attribute_id`),
  FULLTEXT KEY `FTI_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogsearch_fulltext_scope1';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogsearch_fulltext_scope1`
--

LOCK TABLES `catalogsearch_fulltext_scope1` WRITE;
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope1` DISABLE KEYS */;
INSERT INTO `catalogsearch_fulltext_scope1` VALUES (1,73,'Meteoro'),(1,74,'MT01'),(1,75,'Hace muchos a&ntilde;os seg&uacute;n la leyenda un destello ilumin&oacute; el cielo de la sierra sur de Oaxaca, se escuch&oacute; un rugido y se vieron caer &ldquo;ojitos de fuego&rdquo; por todos lados. Uno de ellos hizo un peque&ntilde;o cr&aacute;ter que un maestro mezcalillero decidi&oacute; usar para cocer sus pi&ntilde;as de espadin y produjo as&iacute;, el mejor mezcal que la regi&oacute;n jam&aacute;s hab&iacute;a probado.&ldquo; C&oacute;mo lo hizo?&rdquo; le preguntaban y el viejo solamente respond&iacute;a: &ldquo;cay&oacute; del cielo&rdquo;.As&iacute; como pasa con el vino, las variedades tienen propiedades muy distintas una de la otra las cuales producen una increible gama de sabores y aromas y el terreno y la mano del maestro mezcalillero tambi&eacute;n juegan un papel fundamental en las caracter&iacute;sticas y personalidad del producto final. Mezcal Meteoro est&aacute; hecho 100% de Agave Espad&iacute;n.'),(1,76,'Mezcal Meteoro'),(1,83,'Meteoro'),(1,156,'Petronilo Rosario Altamirano'),(2,73,'MeXXico Joven'),(2,74,'MX01'),(2,75,'MeXXIco es una empresa 100% mexicana, fundada con el objetivo de producir un mezcal de la m&aacute;s alta calidad.Nuestro logotipo, inspirado en la historia constituye el simbolo de la majestuosidad y de la victoria, que nos representa como empresa.'),(2,83,'Mexxico'),(2,156,'Jorge Balderas'),(3,73,'Sin Piedad 44'),(3,74,'SP01'),(3,75,'En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.Dado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.La mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.'),(3,83,'Sin Piedad'),(3,156,'Don Celso Martínez y Enrique Mendoza'),(4,73,'Sin Piedad Cirial'),(4,74,'SP02'),(4,75,'En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.Dado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.La mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.'),(4,83,'Sin Piedad'),(4,156,'Don Celso Martínez y Enrique Mendoza'),(5,73,'Sin Piedad 44 “Petaca”'),(5,74,'SP03'),(5,75,'En el pueblo de Matatl&aacute;n en Oaxaca, en un peque&ntilde;o palenque la Diosa Mayahuel ha entregado su legado y herencia a los Maestros Mezcaleros Celso Mart&iacute;nez y Enrique Mendoza donde usando m&eacute;todos tradicionales nos comparten un excepcional Mezcal.Dado que creemos que las cosas bien hechas son en las que se pone el coraz&oacute;n y el talento, no contamos con procesos industrializados, de esta manera sabemos que solo cosas bien hechas salen.La mayor&iacute;a de nuestras herramientas, al menos las m&aacute;s importantes, como: alambique, horno, tinas de fermentaci&oacute;n, fueron construidas de manera artesanal en donde se ha excedido la calidad para lograr estar al nivel de los est&aacute;ndares globales.'),(5,83,'Sin Piedad'),(5,156,'Don Celso Martínez y Enrique Mendoza'),(6,73,'Mayalen Etiqueta Negra'),(6,74,'MY02'),(6,75,'El mezcal resurge como parte vital en la comida gourmet en respuesta al redescubrimiento de las tradiciones culinarias mexicanas. A trav&eacute;s de nuestro mezcal,Mayalen Agua de Juventud preserva el patrimonio incrustado en las costumbres generacionales a&uacute;n vivas en el estado de Guerrero.Nuestro producto es 100% artesanal y org&aacute;nico. Las condiciones clim&aacute;ticas, el agua, y la tierra conjugan un efecto directo en la calidad final del mezcal. Por esta raz&oacute;n, la ubicaci&oacute;n de nuestro palenque es ideal, toda vez que contamos con acceso al agua de manantial.'),(6,83,'Mayalen'),(6,156,'Don José Morales'),(7,73,'Mexxico “Petaca”'),(7,74,'MX02'),(7,75,'MeXXIco es una empresa 100% mexicana, fundada con el objetivo de producir un mezcal de la m&aacute;s alta calidad.Nuestro logotipo, inspirado en la historia constituye el simbolo de la majestuosidad y de la victoria, que nos representa como empresa.'),(7,83,'Mexxico'),(7,156,'Jorge Balderas'),(8,73,'Mezcal Chaneque Madrecuixe'),(8,74,'CH01'),(8,75,'Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.'),(8,83,'Chaneque'),(8,156,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(9,73,'Mezcal Chaneque Tobaziche'),(9,74,'CH02'),(9,75,'Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.'),(9,83,'Chaneque'),(9,156,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(10,73,'Mezcal Chaneque Espadín'),(10,74,'CH04'),(10,75,'Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.'),(10,83,'Chaneque'),(10,156,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(11,73,'Mezcal Chaneque Tobalá Silvestre'),(11,74,'CH05'),(11,75,'Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.'),(11,83,'Chaneque'),(11,156,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(12,73,'Mezcal Chaneque Ensamble'),(12,74,'CH06'),(12,75,'Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.'),(12,83,'Chaneque'),(12,156,'Gonzalo Martínez Sernas, Justino Ríos Martínez y Juan Lorenzo García'),(13,73,'Bruxo 1. Espadín'),(13,74,'BX01'),(13,75,'En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.Brujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.BRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.'),(13,83,'Bruxo'),(13,156,'Lucio Morales López, Pablo Vázquez, Cándido Reyes, Cesáreo Rodríguez y Cándido Reyes'),(14,73,'Bruxo 2. Pechuga De Maguey'),(14,74,'BX02'),(14,75,'En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.Brujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.BRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.'),(14,83,'Bruxo'),(14,156,'Lucio Morales López, Pablo Vázquez, Cándido Reyes, Cesáreo Rodríguez y Cándido Reyes'),(15,73,'Bruxo 3. Barril'),(15,74,'BX03'),(15,75,'En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.Brujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.BRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.'),(15,83,'Bruxo'),(15,156,'Lucio Morales López, Pablo Vázquez, Cándido Reyes, Cesáreo Rodríguez y Cándido Reyes'),(16,73,'Bruxo 4. Ensamble'),(16,74,'BX04'),(16,75,'En la sierra aprendimos que el Mezcal se trata con respeto, por ah&iacute; alguien nos dijo que con Mezcal no se brinda, se ofrenda.BRUXO fue creado por un grupo de amigos que amamos y honramos nuestro pa&iacute;s, que descubrimos en el campo la magia del mezcal, pero sobre todo, de su gente.Brujo es el sin&oacute;nimo de cham&aacute;n, de sabio, de iluminado&hellip; la &ldquo;X&rdquo; en BRUXO nos da esencia, pues remite a la pronunciaci&oacute;n de nuestras tierras origen, Oaxaca y M&eacute;xico.BRUXO es creaci&oacute;n y sabidur&iacute;a, libertad y responsabilidad; es erotismo y locura, lealtad y compromiso. BRUXO es tradici&oacute;n y tendencia&hellip; un atardecer, un buen libro. Es igualdad de g&eacute;nero y dignidad humana.'),(16,83,'Bruxo'),(16,156,'Lucio Morales López, Pablo Vázquez, Cándido Reyes, Cesáreo Rodríguez y Cándido Reyes'),(17,73,'IBÁ 55. Mezcal Joven'),(17,74,'IB1'),(17,75,'La palabra IBA significa CIELO en el dialecto ZAPOTECO (lengua indigena del estado de OAXACA) de ahi nuestro slogan EL MEZCAL DEL CIELO.Mezcal Iba es una marca que nace de la uni&oacute;n de una empresa BEBIDAS PROACTIVAS DE M&Eacute;XICO y del palenque El dos de Oros ubicado en San Pablo Huixtepec Oaxaca, cuyo pilar, Don Fortino Ramos, legendario maestro mezcalero y pionero en la elaboraci&oacute;n de mezcales artesanales.'),(17,83,'IBÁ'),(17,156,'Manuel González'),(18,73,'Doña Natalia'),(18,74,'DN01'),(18,75,'Las manos expertas de nuestros maestros mezcaleros escogen cuidadosamente el agave silvestre y cultivado en el estado de Durango y otros estados del pais esperando con paciencia que cada agave est&eacute; en su punto perfecto de madurez.Al finalizar la jima, los corazones son horneados en horno de tierra con piedra volc&aacute;nica, al calor de la le&ntilde;a de mezquite. Ya cocido, la dulzura del agave es extra&iacute;do mediante el machacado a mano y despu&eacute;s fermentado en tinas de madera con agua de manantial.Nuestro proceso de destilaci&oacute;n es mediante el m&eacute;todo de montera de madera de encino, cobre o barro.'),(18,83,'Doña Natalia'),(18,156,'Agustín Salas padre e hijo'),(19,73,'Marca Negra Ensamble'),(19,74,'MN01'),(19,75,'MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.Localizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.'),(19,83,'Marca Negra'),(19,156,'Abel Nolasco V. y Jorge Méndez'),(20,73,'Marca Negra Tobalá'),(20,74,'MN02'),(20,75,'MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.Localizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.'),(20,83,'Marca Negra'),(20,156,'Abel Nolasco V. y Jorge Méndez'),(21,73,'Marca Negra Tepextate'),(21,74,'MN03'),(21,75,'MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.Localizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.'),(21,83,'Marca Negra'),(21,156,'Abel Nolasco V. y Jorge Méndez'),(22,73,'Marca Negra Dobadán'),(22,74,'MN04'),(22,75,'MN es una selecci&oacute;n de mezcales de alta calidad, de diferentes regiones de M&eacute;xico, en donde se produce y embotella a mano.Localizamos el mejor mezcal tradicional donde quiera que est&eacute; y sin importar que tan lejos se encuentre su productor. Si existe una expresi&oacute;n aut&eacute;ntica de mezcal, ya sea en la sierra de Oaxaca o el desierto de Durango, la conseguiremos para hacerla disponible en su forma original y verdadera.'),(22,83,'Marca Negra'),(22,156,'Abel Nolasco V. y Jorge Méndez'),(23,73,'Pescador De Sueños Arroqueño'),(23,74,'PS01'),(23,75,'Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.'),(23,83,'Pescador de Sueños'),(23,156,'Leoncio, Fernando Mitra, Felipe Cortés'),(24,73,'Pescador De Sueños Cuishe'),(24,74,'PS02'),(24,75,'Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.'),(24,83,'Pescador de Sueños'),(24,156,'Leoncio, Fernando Mitra, Felipe Cortés'),(25,73,'Pescador De Sueños Tobalá'),(25,74,'PS03'),(25,75,'Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.'),(25,83,'Pescador de Sueños'),(25,156,'Leoncio, Fernando Mitra, Felipe Cortés'),(26,73,'Pescador De Sueños Coyote'),(26,74,'Pescador De Sueños Coyote'),(26,75,'Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.'),(26,83,'Pescador de Sueños'),(26,156,'Leoncio, Fernando Mitra, Felipe Cortés'),(27,73,'Pescador De Sueños Tepextate'),(27,74,'PS05'),(27,75,'Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.'),(27,83,'Pescador de Sueños'),(27,156,'Leoncio, Fernando Mitra, Felipe Cortés'),(28,73,'Pescador De Sueños Bicuixe'),(28,74,'PS06'),(28,75,'Pescador de Sue&ntilde;os es un proyecto de Destiler&iacute;a Santa Sabia donde nacen colecciones &uacute;nicas de selectos mezcales rescatando as&iacute; la m&iacute;stica y folklore de esta tradici&oacute;n mexicana.Nuestros productos est&aacute;n elaborados con la m&aacute;xima calidad, pasi&oacute;n y entrega hacia una cultura ancestral. Este trabajo inicia desde el cuidado de la planta a cargo de nuestros hermanos agricultores, pasando por las manos de nobles maestros mezcaleros, hasta el proceso de envasado en nuestras representativas botellas de cer&aacute;mica de alta temperatura fabricadas por virtuosos artesanos.'),(28,83,'Pescador de Sueños'),(28,156,'Leoncio, Fernando Mitra, Felipe Cortés'),(29,73,'Papadiablo Espadín'),(29,74,'PD01'),(29,75,'El nombre PAPADIABLO nace de la uni&oacute;n de dos opuestos que se complementan uno al otro para formar una unidad m&aacute;xima.La representaci&oacute;n de esta unidad m&aacute;xima traducida en nuestro mezcal, la encontramos de manera perfecta endos cartas del tarot.Estos dos extremos esenciales, representan de manera precisa el campo de acci&oacute;n de nuestro mezcal. En sustancia y con medida puede aportar una gota de inspiraci&oacute;n, puede ayudar a accesar a nuestra fuente creativa o simplemente ponernos en consonancia con nuestro verdadero ser para entrar en empat&iacute;a con el mundo.'),(29,83,'Papadiablo'),(29,156,'Don Alberto Ortíz'),(30,73,'Papadiablo Ensamble'),(30,74,'PD02'),(30,75,'El nombre PAPADIABLO nace de la uni&oacute;n de dos opuestos que se complementan uno al otro para formar una unidad m&aacute;xima.La representaci&oacute;n de esta unidad m&aacute;xima traducida en nuestro mezcal, la encontramos de manera perfecta endos cartas del tarot.Estos dos extremos esenciales, representan de manera precisa el campo de acci&oacute;n de nuestro mezcal. En sustancia y con medida puede aportar una gota de inspiraci&oacute;n, puede ayudar a accesar a nuestra fuente creativa o simplemente ponernos en consonancia con nuestro verdadero ser para entrar en empat&iacute;a con el mundo.'),(30,83,'Papadiablo'),(30,156,'Don Alberto Ortíz'),(31,73,'Nakawé'),(31,74,'NA01'),(31,75,'Nakaw&eacute;'),(31,83,'Nakawé'),(31,156,'Abel Martínez Morales'),(32,73,'IBÁ 40. Mezcal Joven'),(32,74,'IB01'),(32,75,'IB&Aacute; 40. Mezcal Joven'),(32,83,'IBÁ'),(32,156,'Manuel González'),(33,73,'Machetazo Papalote'),(33,74,'MCH01'),(33,75,'Machetazo Papalote'),(33,83,'Machetazo'),(33,156,'José Morales Uribe'),(34,73,'El hijo del Santo'),(34,74,'HS01'),(34,75,'El hijo del Santo'),(34,83,'El hijo del Santo'),(34,156,'Gregorio Velasco'),(35,73,'Mezcal Chaneque Mexicano'),(35,74,'CH03'),(35,75,'Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.'),(35,83,'Chaneque'),(35,156,'Gonzalo Martínez Sernas, Justino Ríos Martínez, Juan Lorenzo García y Pedro Meza'),(36,73,'Mezcal Chaneque Duranguensis'),(36,74,'CH10'),(36,75,'Existen mas de 30 magueyes con los que se produce mezcal, en chaneque tenemos solo 14, pero seguimos explorando y creciendo con mas variedades. cada gota de mezcal chaneque translada al que la bebe al misticismo de la tierra y el maguey.'),(36,83,'Chaneque'),(36,156,'\"Gonzalo Martínez Sernas, Justino Ríos Martínez, Juan Lorenzo García y Pedro Meza\"');
/*!40000 ALTER TABLE `catalogsearch_fulltext_scope1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkout_agreement`
--

DROP TABLE IF EXISTS `checkout_agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkout_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `content` text COMMENT 'Content',
  `content_height` varchar(25) DEFAULT NULL COMMENT 'Content Height',
  `checkbox_text` text COMMENT 'Checkbox Text',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `is_html` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Html',
  `mode` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Applied mode',
  PRIMARY KEY (`agreement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkout_agreement`
--

LOCK TABLES `checkout_agreement` WRITE;
/*!40000 ALTER TABLE `checkout_agreement` DISABLE KEYS */;
/*!40000 ALTER TABLE `checkout_agreement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkout_agreement_store`
--

DROP TABLE IF EXISTS `checkout_agreement_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkout_agreement_store` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`agreement_id`,`store_id`),
  KEY `CHECKOUT_AGREEMENT_STORE_STORE_ID_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `CHECKOUT_AGREEMENT_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `CHKT_AGRT_STORE_AGRT_ID_CHKT_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `checkout_agreement` (`agreement_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement Store';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkout_agreement_store`
--

LOCK TABLES `checkout_agreement_store` WRITE;
/*!40000 ALTER TABLE `checkout_agreement_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `checkout_agreement_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_block`
--

DROP TABLE IF EXISTS `cms_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_block` (
  `block_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Block ID',
  `title` varchar(255) NOT NULL COMMENT 'Block Title',
  `identifier` varchar(255) NOT NULL COMMENT 'Block String Identifier',
  `content` mediumtext COMMENT 'Block Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Block Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Block Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Block Active',
  PRIMARY KEY (`block_id`),
  FULLTEXT KEY `CMS_BLOCK_TITLE_IDENTIFIER_CONTENT` (`title`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='CMS Block Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_block`
--

LOCK TABLES `cms_block` WRITE;
/*!40000 ALTER TABLE `cms_block` DISABLE KEYS */;
INSERT INTO `cms_block` VALUES (1,'Contact us','contact_us','\n                    \n						<div class=\"contact-us\"><iframe style=\"border: 0;\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6305.678625317825!2d-122.39957047637722!3d37.793805380191266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80858063d63a0215%3A0xcba0af2dfe02048f!2s101+California+Street%2C+San+Francisco%2C+CA+94111%2C+Hoa+K%E1%BB%B3!5e0!3m2!1svi!2s!4v1419099932976\" width=\"100%\" height=\"350\"></iframe></div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(2,'Footer Static','footer_static','<div class=\"container\">\r\n<div class=\"col-sm-3 \"><a class=\"logo\" href=\"#\"><img class=\"footer-logo\" src=\"{{media url=\"wysiwyg/logo-2.png\"}}\" alt=\"\" /></a></div>\r\n<div class=\"col-md-9\">\r\n<div class=\"col-md-12 title padding-left-cero\">Mezcal y destilados de agave</div>\r\n<!-- Simple List -->\r\n<div class=\"col-md-5 col-sm-3 \">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 col-xs-6\">\r\n<h2 class=\"thumb-headline\">Correo electr&oacute;nico</h2>\r\n<ul class=\"list-unstyled simple-list margin-bottom-5\">\r\n<li><a href=\"mailto:info@culturamezcal.mx\">info@culturamezcal.mx</a></li>\r\n</ul>\r\n</div>\r\n<div class=\"col-sm-12 col-xs-7\">\r\n<h2 class=\"thumb-headline\">Skype</h2>\r\n<ul class=\"list-unstyled simple-list margin-bottom-5\">\r\n<li><a href=\"skype:culturamezcal?userinfo\">Cultura Mezcal M&eacute;xico</a></li>\r\n</ul>\r\n</div>\r\n<div class=\"col-sm-12 col-xs-6\">\r\n<h2 class=\"thumb-headline\">Tel&eacute;fonos</h2>\r\n<ul class=\"list-unstyled simple-list margin-bottom-5\">\r\n<li>M&eacute;xico: (+55) 5553 9486</li>\r\n<li>Espa&ntilde;a: (+34) 971090 218</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-md-5 col-sm-3 padding-left-cero contacto \">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 col-xs-6\">\r\n<ul class=\"list-unstyled simple-list margin-bottom-20\">\r\n<li><a href=\"preguntas-frecuentes.html\">Preguntas frecuentes</a></li>\r\n<li><a href=\"politica-privacidad.html\">Pol&iacute;ticas de privacidad</a></li>\r\n<li><a href=\"terminos-condiciones.html\">T&eacute;rminos y condiciones</a></li>\r\n</ul>\r\n</div>\r\n<div class=\"col-sm-12 col-xs-7\">\r\n<h2 class=\"thumb-headline\">Zona de cobertura</h2>\r\n<ul class=\"list-unstyled simple-list margin-bottom-20\">\r\n<li><a>Europa, Rusia y M&eacute;xico</a></li>\r\n</ul>\r\n</div>\r\n<div class=\"col-sm-12 col-xs-7\">\r\n<h2 class=\"thumb-headline\">&copy;2017 Cultura Mezcal</h2>\r\n<ul class=\"list-unstyled simple-list margin-bottom-20\">\r\n<li class=\"derechos\">Todos los derechos reservados</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"col-md-2 col-sm-3 padding-left-cero redes_sociales npr\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 col-xs-6\">\r\n<div></div>\r\n<div></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>','2020-02-28 16:14:56','2020-02-28 18:27:36',1),(3,'Hotline','hotline','\n                    \n						<p><span class=\"pe-7s-call\">(+1)866-540-3229</span> <strong>FREE Shipping on orders over $150</strong></p>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(4,'Footer payment','footer_payment','\n                    \n						<p><img src=\"{{media url=\"wysiwyg/payment.png\"}}\" alt=\"\" /></p>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(5,'Link Follow','link_follow','\n                    \n						<ul class=\"link-follow\">\n						<li class=\"first\"><a class=\"twitter fa fa-twitter\" title=\"Twitter\" href=\"https://twitter.com/plazathemes\"><span>twitter</span></a></li>\n						<li><a class=\"google fa fa-google-plus\" title=\"Google\" href=\"#\"><span>google </span></a></li>\n						<li><a class=\"facebook fa fa-facebook\" title=\"Facebook\" href=\"https://www.facebook.com/plazathemes1\"><span>facebook</span></a></li>\n						<li><a class=\"youtube fa fa-youtube\" title=\"Youtube\" href=\"https://www.youtube.com/user/plazathemes\"><span>youtube </span></a></li>\n						<li><a class=\"flickr fa fa-flickr\" title=\"Flickr\" href=\"https://www.youtube.com/user/plazathemes\"><span>Flickr </span></a></li>\n						</ul>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(6,'Footer Link Tag','footer_links_tag','\n                    \n						<ul>\n						<li>\n						<h2>Mobiles:</h2>\n						<a href=\"#\">Moto E</a> | <a href=\"#\">Samsung Mobile</a> | <a href=\"#\">Micromax Mobile</a> | <a href=\"#\">Nokia Mobile</a> | <a href=\"#\">HTC Mobile</a> | <a href=\"#\">Sony Mobile</a> | <a href=\"#\">Apple Mobile</a> | <a href=\"#\">LG Mobile</a> | <a href=\"#\">Karbonn Mobile</a> | <a href=\"#\">Nokia Mobile</a> | <a href=\"#\"> View all</a></li>\n						<li>\n						<h2>Camera:</h2>\n						<a href=\"#\">Nikon Camera</a> | <a href=\"#\">Canon Camera</a> | <a href=\"#\">Sony Camera</a> | <a href=\"#\">Samsung Camera</a> | <a href=\"#\">Point shoot camera</a> | <a href=\"#\">Camera Lens</a> | <a href=\"#\">Camera Tripod</a> | <a href=\"#\">Camera Bag</a> | <a href=\"#\">Nokia Mobile</a> | <a href=\"#\">View all</a></li>\n						<li>\n						<h2>Laptops:</h2>\n						<a href=\"#\">Apple Laptop</a> | <a href=\"#\">Acer Laptop</a> | <a href=\"#\">Samsung Laptop</a> | <a href=\"#\">Lenovo Laptop</a> | <a href=\"#\">Sony Laptop</a> | <a href=\"#\">Dell Laptop</a> | <a href=\"#\">Asus Laptop</a> | <a href=\"#\">Toshiba Laptop</a> | <a href=\"#\">LG Laptop</a> | <a href=\"#\">HP Laptop</a> | <a href=\"#\">Notebook</a> | <a href=\"#\">View all</a></li>\n						<li></li>\n						</ul>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(7,'Menu Links home1','pt_menu_links_home1','\n                    \n						<div class=\"pt_menu_link pt_menu\">\n						<div class=\"parentMenu\">\n						<ul class=\"mobilemenu\">\n						<li><a href=\"{{store url=\"blog\"}}\">Blog</a></li>\n						<li><a href=\"{{store url=\"contact\"}}\">Contact Us</a></li>\n						</ul>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(8,'Banner category home1 ','banner_category_home1','\n                    \n						<div class=\"block banner-category\"><a href=\"{{store url=\"destop.html\"}}\"><img src=\"{{media url=\"wysiwyg/banner11.jpg\"}}\" alt=\"\" /></a></div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(9,'static1 home1','static1_home1','\n                    \n						<div class=\"static1-home1\">\n						<div class=\"img\">\n						<div class=\"bos box1\">\n						<div class=\"box-col\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/img1_top.jpg\"}}\" alt=\"\" /></a></div>\n						<a class=\"text\" href=\"{{store direct_url=\"smart-phone/apple-phones.html\"}}\">Pendants</a></div>\n						<div class=\"bos box2\">\n						<div class=\"box-col\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/img2_top.jpg\"}}\" alt=\"\" /></a></div>\n						<a class=\"text\" href=\"{{store direct_url=\"smart-phone/motorola-phones.html\"}}\">Rings</a></div>\n						<div class=\"bos box3\">\n						<div class=\"box-col\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/img3_top.jpg\"}}\" alt=\"\" /></a></div>\n						<a class=\"text\" href=\"{{store direct_url=\"smart-phone/samsung-phones.html\"}}\">Bracelets</a></div>\n						<div class=\"bos box4\">\n						<div class=\"box-col\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/img4_top.jpg\"}}\" alt=\"\" /></a></div>\n						<a class=\"text\" href=\"{{store direct_url=\"destop/gaming-desktops.html\"}}\">Necklaces</a></div>\n						<div class=\"bos box5\">\n						<div class=\"box-col\"><a href=\"#\"><img src=\"{{media url=\"wysiwyg/img5_top.jpg\"}}\" alt=\"\" /></a></div>\n						<a class=\"text\" href=\"{{store direct_url=\"smart-phone/apple-phones.html\"}}\">Earrings</a></div>\n						</div>\n						<div class=\"text-bottom\">SALE ON SELECT TODD REED, ARMENTA, ADOLOFO COURRIER AND MORE. <a class=\"shop\" href=\"{{store direct_url=\"smart-phone/smart-phone.html\"}}\">SHOP DENVER\'S TOP JEWELRY BOUTIQUE</a></div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(10,'static2 home1','static2_home1','\n                    \n						<div class=\"static2-home1\">\n						<div class=\"container-inner\">\n						<div class=\"text\">\n						<h2>TRUE PROMISE</h2>\n						<p class=\"text1\">We believe everyone deserves the truth.</p>\n						<p class=\"text2\">That\'s why we\'re bringing radical transparency to modernluxury by providing access to 100% authentic jewelry </p>\n						<p class=\"text3\">and watches at a fraction of the retail price.</p>\n						<a class=\"shop\" href=\"{{store direct_url=\"smart-phone.html\"}}\">\n						FIND OUT MORE\n						</a>\n						</div>\n						</div>	\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(11,'Menu Links home2','pt_menu_links_home2','\n                    \n						<div class=\"pt_menu_link pt_menu\">\n						<div class=\"parentMenu\">\n						<ul class=\"mobilemenu\">\n						<li><a href=\"{{store url=\"new-arrials\"}}\">New Arrials</a></li>\n						<li><a class=\"right\" href=\"{{store url=\"contact\"}}\">Contact Us</a></li>\n						</ul>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(12,'static1 home2','static1_home2','\n                    \n						<div class=\"static1_home2\">\n							<div class=\"row\">\n								<div class=\"col-md-6 col-sm-12 col-xs-12 left\">\n									<div class=\"box box1\">\n										<div class=\"box-col\">\n											<a href=\"{{store direct_url=\"shop-new.html\"}}\"> \n												<img src=\"{{media url=\"wysiwyg/img1_static1.jpg\"}}\" alt=\"\" /> \n											</a>\n										</div>\n									</div>\n									<div class=\"box box2\">\n										<div class=\"box-col\">\n											<a href=\"{{store direct_url=\"women-s.html\"}}\">\n												<img src=\"{{media url=\"wysiwyg/img2_static1.jpg\"}}\" alt=\"\" /> \n											</a>\n										</div>\n									</div>\n								</div>\n								<div class=\"col-md-6 col-sm-12 col-xs-12 right\">\n									<div class=\"box box3\">\n										<div class=\"box-col\">\n											<a href=\"{{store direct_url=\"shop-new/dresses.html\"}}\">\n												<img src=\"{{media url=\"wysiwyg/img3_static1.jpg\"}}\" alt=\"\" /> \n											</a>\n										</div>\n									</div>\n									<div class=\"box box4\">\n										<div class=\"box-col\">\n											<a href=\"{{store direct_url=\"shop-new/dresses.html\"}}\">\n												<img src=\"{{media url=\"wysiwyg/img4_static1.jpg\"}}\" alt=\"\" />\n											</a>\n										</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(13,'static2 home2','static2_home2','\n                    \n						<div class=\"static2_home2\">\n						<div class=\"container-inner\">\n						<h3 class=\"ma-title\">PRODUCT CATEGORIES <span class=\"text1\">Variety of product categories, tens of products, only five-stars reviews.</span> <span>Browse the collections right now.</span></h3>\n						<div class=\"row\">\n						<div class=\"col-md-4 col-sm-12 col-xs-12\">\n						<div class=\"bos bos1\">\n						<div class=\"box-iner\"><a href=\"#\"> <img src=\"{{media url=\"wysiwyg/img1_static2.jpg\"}}\" alt=\"\" /> </a></div>\n						<div class=\"text\"><a class=\"shop\" href=\"{{store direct_url=\"shop-new/dresses.html\"}}\">Shop men</a></div>\n						</div>\n						</div>\n						<div class=\"col-md-4 col-sm-12 col-xs-12\">\n						<div class=\"bos bos2\">\n						<div class=\"box-iner\"><a href=\"#\"> <img src=\"{{media url=\"wysiwyg/img2_static2.jpg\"}}\" alt=\"\" /> </a></div>\n						<div class=\"text\"><a class=\"shop\" href=\"{{store direct_url=\"shop-new/shoes.html\"}}\">Shop Kid</a></div>\n						</div>\n						</div>\n						<div class=\"col-md-4 col-sm-12 col-xs-12\">\n						<div class=\"bos bos3\">\n						<div class=\"box-iner\"><a href=\"#\"> <img src=\"{{media url=\"wysiwyg/img3_static2.jpg\"}}\" alt=\"\" /> </a></div>\n						<div class=\"text\"><a class=\"shop\" href=\"{{store direct_url=\"shop-new/handbags.html\"}}\">Shop women</a></div>\n						</div>\n						</div>\n						</div>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(14,'pt menu links home3','pt_menu_links_home3','\n                    \n						<div class=\"pt_menu_link pt_menu\">\n						<div class=\"parentMenu\">\n						<ul class=\"mobilemenu\">\n						<li><a href=\"{{store direct_url=\"about-magento-demo-store\"}}\">About us</a></li>\n						<li><a class=\"right\" href=\"{{store url=\"contact\"}}\">Contact Us</a></li>\n						</ul>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(15,'static1 home3','static1_home3','\n                    \n						<div class=\"static1_home3\">\n						<div class=\"row box1\">\n						<div class=\"col-md-6 col-sm-12 col-xs-12 \">\n						<div class=\"box-col\">\n						<a href=\"{{store direct_url=\"living-room.html\"}}\">\n						<img src=\"{{media url=\"wysiwyg/bg_box1.jpg\"}}\" alt=\"\" /> \n						</a>\n						</div>\n						</div>\n						<div class=\"col-md-6 col-sm-12 col-xs-12 \">\n						<div class=\"box-iner\">\n						<div class=\"text\">\n						<h2>LOVESEAT SOFA</h2>\n						<p>Compact, Cozy and Comfortable</p>\n						<a class=\"shop\" href=\"{{store direct_url=\"living-room.html\"}}\">More details</a>\n						</div>\n						</div>\n						</div>\n						</div>\n						<div class=\"row box2\">\n						<div class=\"col-md-6 col-sm-12 col-xs-12 \">\n						<div class=\"box-iner\">\n						<div class=\"text\">\n						<h2>LOVESEAT SOFA</h2>\n						<p>Compact, Cozy and Comfortable</p>\n						<a class=\"shop\" href=\"{{store direct_url=\"living-room.html\"}}\">More details</a>\n						</div>\n						</div>\n						</div>\n						<div class=\"col-md-6 col-sm-12 col-xs-12 \">\n						<div class=\"box-col\">\n						<a href=\"{{store direct_url=\"living-room.html\"}}\">\n						<img src=\"{{media url=\"wysiwyg/bg_box2.jpg\"}}\" alt=\"\" /> \n						</a>\n						</div>\n						</div>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(16,'static2 home3','static2_home3','\n                    \n						<div class=\"static2_home3\">\n						<div class=\"container-inner\">\n						<div class=\"text\">\n						<h2 class=\"text1\">Save 30% + Free Shipping</h2>\n						<h2>On select Table &amp; Decor</h2>\n						<a class=\"shop\" href=\"{{store direct_url=\"living-room.html\"}}\">More details</a></div>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(17,'pt menu links home4','pt_menu_links_home4','\n                    \n						<div class=\"pt_menu_link pt_menu\">\n						<div class=\"parentMenu\">\n						<ul class=\"mobilemenu\">\n						<li><a href=\"{{store direct_url=\"about-magento-demo-store\"}}\">About us</a></li>\n						<li><a class=\"right\" href=\"{{store url=\"contact\"}}\">Contact Us</a></li>\n						</ul>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(18,'static1 home4','static1_home4','\n                    \n						<div class=\"banner-top-home4\">\n						<div class=\"box1\">\n						<div class=\"box-col\">\n						<a href=\"#\">\n						<img src=\"{{media url=\"wysiwyg/img1_home4.jpg\"}}\" alt=\"\" />\n						</a>\n						</div>\n						<div class=\"text\">\n						<a class=\"text-iner\" href=\"{{store direct_url=\"electric.html\"}}\">\n						Steak Knives\n						</a>\n						<p>Knives</p>\n						</div>\n						</div>\n						<div class=\"box2\">\n						<div class=\"box-col\">\n						<a href=\"#\"><img src=\"{{media url=\"wysiwyg/img2_home4.jpg\"}}\" alt=\"\" /></a>\n						</div>\n						<div class=\"text\">\n						<a class=\"text-iner\" href=\"{{store direct_url=\"electric.html\"}}\">\n						Casseroles &amp; Dutch Ovens\n						</a>\n						<p>Cookware</p>\n\n						</div>\n						</div>\n						<div class=\"box3\">\n						<div class=\"box-col\">\n						<a href=\"#\">\n						<img src=\"{{media url=\"wysiwyg/img3_home4.jpg\"}}\" alt=\"\" />\n						</a>\n						</div>\n						<div class=\"text\">\n						<a class=\"text-iner\" href=\"{{store direct_url=\"grinder.html\"}}\">\n						Coffee Machines\n						</a>\n						<p>Appliances</p>\n						</div>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(19,'categorytab static home4','categorytab_static_home4','\n                    \n						<p>{{widget type=\"Plazathemes\\Categorytab\\Block\\CateWidget\" title=\"SPECIAL OFFERS\" identify=\"special_offrs\" category_id=\"307,308,309,310,311\" items_default=\"3\" items_desktop=\"2\" items_desktop_small=\"2\" items_tablet=\"2\" items_mobile=\"1\" show_addtocart=\"1\" show_wishlist=\"1\" show_compare=\"1\" row_show=\"2\" show_pager=\"1\" template=\"categorytab/grid_top.phtml\" page_var_name=\"psqflr\"}}</p>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(20,'static2 home4','static2_home4','\n                    \n						<div class=\"static2-home4\">\n						<div class=\"row\">\n						<div class=\" col-md-6 box1\">\n						<div class=\"box-col\">\n						<a href=\"{{store direct_url=\"shop.html\"}}\">\n						<img src=\"{{media url=\"wysiwyg/img1_static2_home4.jpg\"}}\" alt=\"\" />\n						</a>\n						</div>\n						</div>\n						<div class=\"col-md-6 box2\">\n						<div class=\"box-col\">\n						<a href=\"{{store direct_url=\"electric.html\"}}\">\n						<img src=\"{{media url=\"wysiwyg/img2_static2_home4.jpg\"}}\" alt=\"\" />\n						</a>\n						</div>\n						</div>\n						</div>\n						</div>\n					\n            ','2020-02-28 16:14:56','2020-02-28 18:15:50',0),(21,'popuphomepage','popuphomepage','<div id=\"popupEdad\" class=\"overlay\" style=\"display: none;\">\r\n<div class=\"popup\">\r\n<div class=\"contenido\">\r\n<h2><span class=\"titulo\">Disfruta el mezcal responsablemente</span></h2>\r\n<p>Para visitar Cultura Mezcal debes tener la edad legal para consumir alcohol dentro de tu pa&iacute;s de residencia. Si no existen tales leyes debes tener m&aacute;s de 21 a&ntilde;os.</p>\r\n<h2><span class=\"titulo\">&iquest;Cu&aacute;l es tu fecha de nacimiento?</span></h2>\r\n<input id=\"is_mayor\" class=\"input-text\" type=\"date\" />\r\n<div class=\"text\"><input id=\"mayor\" class=\"checkbox\" title=\"No volver a preguntarme\" type=\"checkbox\" value=\"1\" /> <label>No volver a preguntarme</label></div>\r\n<button id=\"is_mayorEdad\" class=\"button\" title=\"Entrar\" type=\"button\">Entrar</button>\r\n<p class=\"text\">El abuso del alcohol es peligroso para la salud</p>\r\n</div>\r\n</div>\r\n</div>','2020-02-28 17:29:53','2020-02-28 17:29:53',1),(22,'Banner Administrable Home 1','banner_a_uno','<p><a href=\"compra.html\" target=\"_self\"><img class=\"img-responsive\" src=\"{{media url=\"wysiwyg/para_los_conocedores.jpg\"}}\" alt=\"\" /></a></p>','2020-02-28 18:18:40','2020-03-02 15:31:30',1),(23,'Banner Administrable Home 2','banner_a_dos','<p><a href=\"experiencias.html\" target=\"_self\"><img class=\"img-responsive\" src=\"{{media url=\"wysiwyg/aprende_acatar.jpg\"}}\" alt=\"\" /></a></p>','2020-02-28 18:22:37','2020-03-02 15:35:13',1),(24,'Home_tienda_titulo','Home_tienda_titulo','<div class=\"container content fixmarg\">\r\n<div class=\"heading linea-v3 margin-bottom-20 tienda\">\r\n<div class=\"bloque_lineas\">\r\n<div class=\"linea_izquierda\"></div>\r\n<div class=\"texto_central\">\r\n<h2><span class=\"cmezcal_sprite image_flor dbi izquierdo\"></span><span class=\"titulo\">TIENDA<span class=\"cmezcal_sprite image_flor dbi derecho\"></span></span></h2>\r\n</div>\r\n<div class=\"linea_derecha\"></div>\r\n</div>\r\n</div>\r\n</div>','2020-02-28 18:23:49','2020-02-28 18:23:49',1),(25,'home_products','home_products','<div class=\"productos-destacados\">{{widget type=\"Magento\\CatalogWidget\\Block\\Product\\ProductsList\" show_pager=\"0\" products_count=\"4\" template=\"product/widget/content/grid.phtml\" conditions_encoded=\"a:2:[i:1;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Combine`;s:10:`aggregator`;s:3:`all`;s:5:`value`;s:1:`1`;s:9:`new_child`;s:0:``;]s:4:`1--1`;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Product`;s:9:`attribute`;s:23:`home_producto_destacado`;s:8:`operator`;s:2:`==`;s:5:`value`;s:1:`1`;]]\"}}</div>','2020-03-03 20:34:13','2020-03-17 18:36:25',1);
/*!40000 ALTER TABLE `cms_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_block_store`
--

DROP TABLE IF EXISTS `cms_block_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_block_store` (
  `block_id` smallint(6) NOT NULL COMMENT 'Block ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`block_id`,`store_id`),
  KEY `CMS_BLOCK_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `CMS_BLOCK_STORE_BLOCK_ID_CMS_BLOCK_BLOCK_ID` FOREIGN KEY (`block_id`) REFERENCES `cms_block` (`block_id`) ON DELETE CASCADE,
  CONSTRAINT `CMS_BLOCK_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Block To Store Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_block_store`
--

LOCK TABLES `cms_block_store` WRITE;
/*!40000 ALTER TABLE `cms_block_store` DISABLE KEYS */;
INSERT INTO `cms_block_store` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1);
/*!40000 ALTER TABLE `cms_block_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_page`
--

DROP TABLE IF EXISTS `cms_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_page` (
  `page_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Page ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Page Title',
  `page_layout` varchar(255) DEFAULT NULL COMMENT 'Page Layout',
  `meta_keywords` text COMMENT 'Page Meta Keywords',
  `meta_description` text COMMENT 'Page Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Page String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Page Content Heading',
  `content` mediumtext COMMENT 'Page Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Page Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Page Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Page Active',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Page Sort Order',
  `layout_update_xml` text COMMENT 'Page Layout Update Content',
  `custom_theme` varchar(100) DEFAULT NULL COMMENT 'Page Custom Theme',
  `custom_root_template` varchar(255) DEFAULT NULL COMMENT 'Page Custom Template',
  `custom_layout_update_xml` text COMMENT 'Page Custom Layout Update Content',
  `custom_theme_from` date DEFAULT NULL COMMENT 'Page Custom Theme Active From Date',
  `custom_theme_to` date DEFAULT NULL COMMENT 'Page Custom Theme Active To Date',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Page Meta Title',
  PRIMARY KEY (`page_id`),
  KEY `CMS_PAGE_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `CMS_PAGE_TITLE_META_KEYWORDS_META_DESCRIPTION_IDENTIFIER_CONTENT` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='CMS Page Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_page`
--

LOCK TABLES `cms_page` WRITE;
/*!40000 ALTER TABLE `cms_page` DISABLE KEYS */;
INSERT INTO `cms_page` VALUES (1,'Contenido Exclusivo','empty','Page keywords','Page description','no-route','','<div class=\"page-title\">\r\n<h1>Contenido Exclusivo</h1>\r\n</div>\r\n<p>El contenido que estas tratando de ver es Exclusivo para Miembros Registrados en el Sitio de Cultura Mezcal. Si deseas se parte de nuestra comunidad y acceder a este recurso y muchos otros beneficios por favor:</p>\r\n<p><a href=\"{{config path=\"web/secure/base_url\"}}/customer/account/create/\">Reg&iacute;strate</a></p>','2020-02-28 15:31:29','2020-03-03 18:31:15',1,0,'','','',NULL,NULL,NULL,''),(2,'Home page','1column',NULL,NULL,'home','Home Page','<p>CMS homepage content goes here.</p>\r\n','2020-02-28 15:31:29','2020-02-28 15:31:31',1,0,'<!--\n    <referenceContainer name=\"right\">\n        <action method=\"unsetChild\"><argument name=\"alias\" xsi:type=\"string\">right.reports.product.viewed</argument></action>\n        <action method=\"unsetChild\"><argument name=\"alias\" xsi:type=\"string\">right.reports.product.compared</argument></action>\n    </referenceContainer>-->',NULL,NULL,NULL,NULL,NULL,NULL),(3,'Enable Cookies','1column',NULL,NULL,'enable-cookies','What are Cookies?','<div class=\"enable-cookies cms-content\">\r\n<p>\"Cookies\" are little pieces of data we send when you visit our store. Cookies help us get to know you better and personalize your experience. Plus they help protect you and other shoppers from fraud.</p>\r\n<p style=\"margin-bottom: 20px;\">Set your browser to accept cookies so you can buy items, save items, and receive customized recommendations. Here’s how:</p>\r\n<ul>\r\n<li><a href=\"https://support.google.com/accounts/answer/61416?hl=en\" target=\"_blank\">Google Chrome</a></li>\r\n<li><a href=\"http://windows.microsoft.com/en-us/internet-explorer/delete-manage-cookies\" target=\"_blank\">Internet Explorer</a></li>\r\n<li><a href=\"http://support.apple.com/kb/PH19214\" target=\"_blank\">Safari</a></li>\r\n<li><a href=\"https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences\" target=\"_blank\">Mozilla/Firefox</a></li>\r\n</ul>\r\n</div>','2020-02-28 15:31:29','2020-02-28 15:31:29',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Política de Privacidad','1column','','','politica-privacidad','Politica de Privacidad','<p>RS AGENCY S.A. DE C.V. (En adelante &ldquo;RSA&rdquo;, &ldquo;nuestro&rdquo;, o &ldquo;nosotros&rdquo;) es una empresa comprometida con el mantenimiento de estrictas medidas tendientes a salvaguardar la informaci&oacute;n confidencial contenida en los datos que le son facilitados. La informaci&oacute;n que sus clientes proporcionan como datos personales se encuentra resguardada y bajo custodia, en este sentido, con fundamento en los art&iacute;culos 15 y 16 de la Ley Federal de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de los Particulares, hacemos de su conocimiento que RSA, con domicilio en Amsterdam 115, Piso 5, colonia Hip&oacute;dromo Condesa, C.P. 06100, en la Ciudad de M&eacute;xico, y su marca registrada CULTURA MEZCAL, as&iacute; como el dominio&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a> &nbsp;son responsables de recabar sus datos personales, del uso que se le d&eacute; a los mismos y de su protecci&oacute;n.</p>\r\n<p>RS AGENCY S.A. DE C.V. ser&aacute; responsable de administrar el sitio web&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a>&nbsp;con la finalidad de promover, facilitar y ofrecer un espacio para dar a conocer la cultura del mezcal, as&iacute; como su venta de producto y experiencias, con los l&iacute;mites de responsabilidad establecidos en el presente documento y en el documento complementario denominado T&Eacute;RMINOS Y CONDICIONES DE USO DEL SITIO.</p>\r\n<p>Las presentes POL&Iacute;TICAS DE PRIVACIDAD aplicar&aacute;n para todos los usuarios del domino&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a>, principalmente para el USUARIO COMPRADOR, que se define como sigue:</p>\r\n<ul type=\"circle\">\r\n<li>USUARIO COMPRADOR: Ser&aacute; el usuario que ingrese al sitio con la finalidad de comprar los productos ofertados en el mismo. Dicho usuario acepta y reconoce que los productos ofertados son responsabilidad de los USUARIOS VENDEDORES, reconociendo expresamente que RS AGENCY S.A. DE C.V. no ser&aacute; responsable de la calidad, estado, y procedencia de los mismos.</li>\r\n</ul>\r\n<p>Los clientes y usuarios del dominio&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a>&nbsp;aceptan y reconocen expresamente que al ingresar al sitio de internet en menci&oacute;n aceptan expresamente la transferencia de datos personales en los t&eacute;rminos de las presentes POL&Iacute;TICAS DE PRIVACIDAD as&iacute; como en el documento complementario T&Eacute;RMINOS Y CONDICIONES DE USO DEL SITIO.</p>\r\n<p>Su informaci&oacute;n personal ser&aacute; utilizada para las siguientes finalidades: a) Registrarse en la base de datos de clientes y usuarios de los servicios de RSA; b Hacer llegar promociones y avisos a trav&eacute;s de diversos medios; a) Habilitarlo como usuario activo de los servicios que se ofrezcan en el dominio&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a></p>\r\n<p>Para las finalidades antes mencionadas RSA solicita, entre otros, los siguientes datos personales:</p>\r\n<ol start=\"1\" type=\"a\">\r\n<li>Nombre completo.</li>\r\n<li>Fecha de nacimiento.</li>\r\n<li>G&eacute;nero.</li>\r\n<li>Domicilio.</li>\r\n<li>Correo electr&oacute;nico.</li>\r\n<li>Tel&eacute;fono.</li>\r\n<li>Datos Bancarios.</li>\r\n</ol>\r\n<p>Se hace de su conocimiento que los datos personales que proporcione, podr&aacute;n ser compartidos a terceros, conforme a lo establecido en el art&iacute;culo 36 de la Ley Federal de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de los Particulares.</p>\r\n<p>Los terceros, son aquellas personas f&iacute;sicas y morales que participan con RSA del tratamiento de datos personales, en el desarrollo de sus actividades profesionales, y tendr&aacute;n las mismas obligaciones que RSA, en el tratamiento de datos personales.</p>\r\n<p>De conformidad con lo dispuesto en el art&iacute;culo 8 de la misma Ley, para la transmisi&oacute;n de los datos financieros o patrimoniales se requerir&aacute; del consentimiento expreso de su titular, salvo las excepciones a que se refieren los art&iacute;culos 10 y 37 de la presente Ley.</p>\r\n<p>RSA y los terceros a quienes se les lleguen a transferir y compartir los datos personales previamente proporcionados, se comprometen a guardar la m&aacute;xima protecci&oacute;n y confidencialidad sobre la informaci&oacute;n que sea facilitada y a utilizarla &uacute;nicamente para los fines indicados.</p>\r\n<p>RSA presume que los datos han sido facilitados por su titular o por una persona autorizada por &eacute;ste, siendo en ambos casos mayores de edad, as&iacute; como que son verdaderos, correctos y exactos.</p>\r\n<p>RSA le informa que en el dominio&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a>&nbsp;se utilizan cookies, web beacons u otras tecnolog&iacute;as, a trav&eacute;s de las cuales es posible monitorear el comportamiento del usuario de internet, as&iacute; como brindarle un mejor servicio y experiencia al navegar en la p&aacute;gina&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a>. Los datos personales que recabamos a trav&eacute;s de estas tecnolog&iacute;as, los utilizaremos para los siguientes fines: Para proveer los productos y servicios que ofrecemos, as&iacute; como actividades afines.</p>\r\n<p>Los datos personales que obtenemos de estas tecnolog&iacute;as de rastreo son los siguientes:</p>\r\n<ol start=\"1\" type=\"a\">\r\n<li>Identificadores, nombre de usuario y contrase&ntilde;as de una sesi&oacute;n</li>\r\n<li>Idioma preferido por el usuario</li>\r\n<li>Regi&oacute;n en la que se encuentra el usuario</li>\r\n<li>Tipo de navegador del usuario</li>\r\n<li>Tipo de sistema operativo del usuario</li>\r\n<li>Fecha y hora del inicio y final de una sesi&oacute;n de un usuario</li>\r\n<li>P&aacute;ginas web visitadas por un usuario</li>\r\n<li>B&uacute;squedas realizadas por un usuario</li>\r\n<li>Publicidad revisada por un usuario</li>\r\n<li>Listas y h&aacute;bitos de consumo en p&aacute;ginas de compras</li>\r\n</ol>\r\n<p>RSA ha adoptado los niveles de seguridad adecuados a los datos facilitados por sus clientes, adem&aacute;s, ha instalado todos los medios y medidas a su alcance para evitar, en la medida de lo posible, la p&eacute;rdida, mal uso, alteraci&oacute;n, y extracci&oacute;n de los mismos, sin embargo, al aceptar las presentes Pol&iacute;ticas de Privacidad, el cliente expresamente acepta y reconoce que RSA no ser&aacute; responsable por el acceso y uso no autorizado de dicha informaci&oacute;n, o en violaci&oacute;n a los sistemas de seguridad que RSA ha instalado por parte de terceros.</p>\r\n<p>RSA no ser&aacute; responsable por el uso inadecuado de contrase&ntilde;as y datos de acceso (usuario y contrase&ntilde;a) de los usuarios dentro del sitio web&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a>.</p>\r\n<p>A continuaci&oacute;n, RSA define de manera general los principios que rigen sus pol&iacute;ticas de privacidad y confidencialidad para la informaci&oacute;n proporcionada por sus clientes:</p>\r\n<ol start=\"1\" type=\"a\">\r\n<li>RSA &uacute;nicamente recabar&aacute; la informaci&oacute;n del cliente que sea necesaria para brindarle los servicios que solicite.</li>\r\n<li>RSA se esfuerza por asegurar la calidad de la informaci&oacute;n que recaba sobre sus clientes, particularmente cuando se haya obtenido a trav&eacute;s de alg&uacute;n proveedor de informaci&oacute;n o de servicios.</li>\r\n<li>RSA enfocar&aacute; sus esfuerzos para ofrecer la tecnolog&iacute;a m&aacute;s moderna y actualizada a fin de ofrecer a sus clientes la mayor seguridad posible en el manejo y transferencia de la informaci&oacute;n que sea requerida en los diversos procesos de requerimiento de datos.</li>\r\n<li>RSA evitar&aacute; en todo momento la divulgaci&oacute;n de informaci&oacute;n acerca de sus clientes, haci&eacute;ndolo &uacute;nicamente respecto de aquellos datos que expresamente sean autorizados para ello.</li>\r\n<li>En todo momento, RSA estar&aacute; atento a las inquietudes que manifiesten sus clientes respecto al manejo de la informaci&oacute;n que proporcionen para los diversos servicios requeridos.</li>\r\n<li>RSA cuidar&aacute; que estos principios de privacidad/confidencialidad se extiendan al conjunto de relaciones comerciales al interior del grupo.</li>\r\n<li>RSA compartir&aacute; la responsabilidad del cuidado de la informaci&oacute;n con sus empleados, haci&eacute;ndolos copart&iacute;cipes de los lineamientos expuestos en los documentos de &ldquo;POL&Iacute;TICAS DE PRIVACIDAD&rdquo;.</li>\r\n</ol>\r\n<p>Es importante informarle que usted tiene derecho al Acceso, Rectificaci&oacute;n y Cancelaci&oacute;n de sus datos personales, as&iacute; como a oponerse al tratamiento de los mismos o a revocar el consentimiento que para dicho fin nos haya otorgado (en lo sucesivo los derechos ARCO).</p>\r\n<p>Para ello, es necesario que env&iacute;e la solicitud en los t&eacute;rminos que marca la Ley en su art&iacute;culo 29 en atenci&oacute;n a RS AGENCY S.A. DE C.V., Departamento de Atenci&oacute;n a Clientes, con domicilio en Amsterdam 115, Piso 5, colonia Hip&oacute;dromo Condesa, C.P. 06100, en la Ciudad de M&eacute;xico, o bien, se comunique v&iacute;a correo electr&oacute;nico a&nbsp;<a href=\"mailto:info@culturamezcal.mx\">&nbsp;info@culturamezcal.m</a>x.</p>\r\n<p>La solicitud de acceso, rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n deber&aacute; contener y acompa&ntilde;ar lo siguiente:</p>\r\n<ol start=\"1\" type=\"a\">\r\n<li>El nombre del cliente y su domicilio, u otro medio para comunicarle la respuesta a su solicitud;</li>\r\n<li>Los documentos que acrediten la identidad o, en su caso, la representaci&oacute;n legal del cliente;</li>\r\n<li>La descripci&oacute;n clara y precisa de los datos personales respecto de los que se busca ejercer alguno de los derechos antes mencionados, y;</li>\r\n<li>Cualquier otro elemento o documento que facilite la localizaci&oacute;n de los datos personales.</li>\r\n</ol>\r\n<p>RSA dar&aacute; tr&aacute;mite a las solicitudes de los clientes, para el ejercicio de los derechos a que se refiere la Ley. En el caso de solicitudes de rectificaci&oacute;n de datos personales, el cliente deber&aacute; indicar, adem&aacute;s de lo se&ntilde;alado anteriormente, las modificaciones a realizarse y aportar la documentaci&oacute;n que sustente su petici&oacute;n.</p>\r\n<p>RSA comunicar&aacute; al cliente, en un plazo m&aacute;ximo de 5 (cinco) d&iacute;as h&aacute;biles, contados desde la fecha en que se recibi&oacute; la solicitud de acceso, rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n, la determinaci&oacute;n adoptada, a efecto de que, si resulta procedente, se haga efectiva la misma dentro de los 10 (diez) d&iacute;as h&aacute;biles siguientes a la fecha en que se comunica la respuesta.</p>\r\n<p>Trat&aacute;ndose de solicitudes de acceso a datos personales, proceder&aacute; la entrega, previa acreditaci&oacute;n de la identidad del solicitante o representante legal, seg&uacute;n corresponda.</p>\r\n<p>Los plazos antes referidos podr&aacute;n ser ampliados una sola vez por un periodo igual, siempre y cuando as&iacute; lo justifiquen las circunstancias del caso.</p>\r\n<p>La obligaci&oacute;n de acceso a la informaci&oacute;n se dar&aacute; por cumplida cuando se pongan a disposici&oacute;n del titular los datos personales; o bien, mediante la expedici&oacute;n de copias simples, documentos electr&oacute;nicos o cualquier otro medio que determine RSA en estas POL&Iacute;TICAS DE PRIVACIDAD.</p>\r\n<p>En el caso de que el cliente solicite a RSA el acceso a los datos a una persona que presume es el responsable y resulta no serlo, bastar&aacute; con que as&iacute; se le indique al titular por cualquiera de los medios a que se refiere el p&aacute;rrafo anterior, para tener por cumplida la solicitud.</p>\r\n<p>RSA podr&aacute; negar el acceso a los datos personales, o a realizar la rectificaci&oacute;n o cancelaci&oacute;n o conceder la oposici&oacute;n al tratamiento de los mismos, en los siguientes supuestos:</p>\r\n<ol start=\"1\" type=\"a\">\r\n<li>Cuando el solicitante no sea el titular de los datos personales, o el representante legal no est&eacute; debidamente acreditado para ello;</li>\r\n<li>Cuando en su base de datos, no se encuentren los datos personales del solicitante;</li>\r\n<li>Cuando se lesionen los derechos de un tercero;</li>\r\n<li>Cuando exista un impedimento legal, o la resoluci&oacute;n de una autoridad competente, que restrinja el acceso a los datos personales, o no permita la rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n de los mismos, y</li>\r\n<li>Cuando la rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n haya sido previamente realizada. La negativa a que se refiere este art&iacute;culo podr&aacute; ser parcial en cuyo caso RSA efectuar&aacute; el acceso, rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n requerida por el titular.</li>\r\n</ol>\r\n<p>En todos los casos anteriores, RSA deber&aacute; informar el motivo de su decisi&oacute;n y comunicarla al titular, o en su caso, al representante legal, en los plazos establecidos para tal efecto, por el mismo medio por el que se llev&oacute; a cabo la solicitud, acompa&ntilde;ando, en su caso, las pruebas que resulten pertinentes.</p>\r\n<p>En caso de que no desee recibir mensajes promocionales de nuestra parte, puede enviarnos su solicitud por medio de la direcci&oacute;n electr&oacute;nica: atencionaclientes@vakeva.mx.</p>\r\n<p>RSA se reserva su derecho a realizar cambios en las presentes POL&Iacute;TICAS DE PRIVACIDAD, los cuales ser&aacute;n dados a conocer a trav&eacute;s de medios impresos y el dominio&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a>.</p>\r\n<p>RSA informa a sus clientes que cuentan con el derecho a oponerse al tratamiento de sus datos personales en caso de no estar de acuerdo con las modificaciones que pudieran presentar estas POL&Iacute;TICAS DE PRIVACIDAD, para ello deber&aacute; enviar una solicitud a&nbsp;<a href=\"http://www.culturamezcal.mx/\">http://www.culturamezcal.mx/</a>.</p>\r\n<p>&nbsp;</p>','2020-02-28 15:31:29','2020-03-03 18:19:57',1,0,'','','',NULL,NULL,NULL,''),(5,'Thebell1 - Jewelry Responsive magento theme','1column',NULL,NULL,'thebell1-jewelry-responsive-magento-theme',NULL,'\n				<div class=\"page-content\"><em style=\"display: none;\">page-content</em></div>\n			','2020-02-28 16:14:56','2020-02-28 16:14:56',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Thebell2 - Shoe Responsive magento theme','1column',NULL,NULL,'thebell2-shoe-responsive-magento-theme',NULL,'\n				<div class=\"page-content\"><em style=\"display: none;\">page-content</em></div>\n			','2020-02-28 16:14:56','2020-02-28 16:14:56',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Thebell3 - Furniture Responsive magento theme','1column',NULL,NULL,'thebell3-furniture-responsive-magento-theme',NULL,'\n				<div class=\"page-content\"><em style=\"display: none;\">page-content</em></div>\n			','2020-02-28 16:14:56','2020-02-28 16:14:56',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Cultura Mezcal','1column','','','thebell4-kitchenware-responsive-magento-theme','','\r\n				<div class=\"page-content\"><em style=\"display: none;\">page-content</em></div>\r\n			','2020-02-28 16:14:56','2020-02-28 18:56:43',1,0,'','','',NULL,NULL,NULL,''),(9,'About Us','1column',NULL,NULL,'about-magento-demo-store',NULL,'\n				<div class=\"page-title\">\n				<h1>About Magento Store</h1>\n				</div>\n				<div class=\"col3-set\">\n				<div class=\"col-1\">\n				<p style=\"line-height: 1.2em;\"><small>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.</small></p>\n				<p style=\"color: #888; font: 1.2em/1.4em georgia, serif;\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta.</p>\n				</div>\n				<div class=\"col-2\">\n				<p><strong style=\"color: #de036f;\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.</strong></p>\n				<p>Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</p>\n				<p>Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus. Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi. Vestibulum sapien dolor, aliquet nec, porta ac, malesuada a, libero. Praesent feugiat purus eget est. Nulla facilisi. Vestibulum tincidunt sapien eu velit. Mauris purus. Maecenas eget mauris eu orci accumsan feugiat. Pellentesque eget velit. Nunc tincidunt.</p>\n				</div>\n				<div class=\"col-3\">\n				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper</p>\n				<p><strong style=\"color: #de036f;\">Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus.</strong></p>\n				<p>Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi.</p>\n				<div class=\"divider\">&nbsp;</div>\n				<p>To all of you, from all of us at Magento Store - Thank you and Happy eCommerce!</p>\n				<p style=\"line-height: 1.2em;\"><strong style=\"font: italic 2em Georgia, serif;\">John Doe</strong><br /> <small>Some important guy</small></p>\n				</div>\n				</div>\n			','2020-02-28 16:14:56','2020-02-28 16:14:56',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'New Arrials ','1column',NULL,NULL,'new-arrials',NULL,'\n					<p>{{block class=\"Plazathemes\\Newproductslider\\Block\\Newproductslider\" name=\"newproductslider\" template=\"newproductslider.phtml\" }}</p>\n			','2020-02-28 16:14:56','2020-02-28 16:14:56',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'home_products','1column','','','home-products','','<div class=\"productos-destacados\">{{widget type=\"Magento\\CatalogWidget\\Block\\Product\\ProductsList\" show_pager=\"0\" products_count=\"4\" template=\"product/widget/content/grid.phtml\" conditions_encoded=\"a:2:[i:1;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Combine`;s:10:`aggregator`;s:3:`all`;s:5:`value`;s:1:`1`;s:9:`new_child`;s:0:``;]s:4:`1--1`;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Product`;s:9:`attribute`;s:23:`home_producto_destacado`;s:8:`operator`;s:2:`==`;s:5:`value`;s:1:`1`;]]\"}}&gt;/div&gt;</div>','2020-03-03 07:00:54','2020-03-03 07:02:42',1,0,'','','',NULL,NULL,NULL,''),(12,'Preguntas Frecuentes','1column','','','preguntas-frecuentes','Preguntas Frecuentes','<p>Contenido de preguntas frecuentes.</p>','2020-03-03 16:06:30','2020-03-03 17:57:46',1,0,'','','',NULL,NULL,NULL,'');
/*!40000 ALTER TABLE `cms_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_page_store`
--

DROP TABLE IF EXISTS `cms_page_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_page_store` (
  `page_id` smallint(6) NOT NULL COMMENT 'Page ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`page_id`,`store_id`),
  KEY `CMS_PAGE_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `CMS_PAGE_STORE_PAGE_ID_CMS_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`page_id`) ON DELETE CASCADE,
  CONSTRAINT `CMS_PAGE_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Page To Store Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_page_store`
--

LOCK TABLES `cms_page_store` WRITE;
/*!40000 ALTER TABLE `cms_page_store` DISABLE KEYS */;
INSERT INTO `cms_page_store` VALUES (2,0),(3,0),(1,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1);
/*!40000 ALTER TABLE `cms_page_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_config_data`
--

DROP TABLE IF EXISTS `core_config_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_config_data` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Config Id',
  `scope` varchar(8) NOT NULL DEFAULT 'default' COMMENT 'Config Scope',
  `scope_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Config Scope Id',
  `path` varchar(255) NOT NULL DEFAULT 'general' COMMENT 'Config Path',
  `value` text COMMENT 'Config Value',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `CORE_CONFIG_DATA_SCOPE_SCOPE_ID_PATH` (`scope`,`scope_id`,`path`)
) ENGINE=InnoDB AUTO_INCREMENT=547 DEFAULT CHARSET=utf8 COMMENT='Config Data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_config_data`
--

LOCK TABLES `core_config_data` WRITE;
/*!40000 ALTER TABLE `core_config_data` DISABLE KEYS */;
INSERT INTO `core_config_data` VALUES (1,'default',0,'web/seo/use_rewrites','1'),(2,'default',0,'web/unsecure/base_url','http://culturamezcal.com/mezcal-5/'),(3,'default',0,'web/secure/base_url','https://culturamezcal.com/mezcal-5/'),(4,'default',0,'general/locale/code','en_US'),(5,'default',0,'web/secure/use_in_frontend','0'),(6,'default',0,'web/secure/use_in_adminhtml','0'),(7,'default',0,'general/locale/timezone','Europe/Amsterdam'),(8,'default',0,'currency/options/base','EUR'),(9,'default',0,'currency/options/default','EUR'),(10,'default',0,'currency/options/allow','EUR'),(11,'default',0,'general/region/display_all','1'),(12,'default',0,'general/region/state_required','AT,BR,CA,EE,FI,LV,LT,RO,ES,CH,US'),(13,'default',0,'catalog/category/root_id','2'),(14,'default',0,'web/default/#text','\n	'),(15,'default',0,'web/default/cms_home_page','thebell4-kitchenware-responsive-magento-theme'),(16,'default',0,'web/default/cms_no_route','no-route'),(17,'stores',1,'design/head/default_title','Cultura Mezcal'),(18,'stores',1,'design/head/title_prefix',NULL),(19,'stores',1,'design/head/title_suffix',NULL),(20,'stores',1,'design/head/default_description','Cultura Mezcal'),(21,'stores',1,'design/head/default_keywords','Cultura Mezcal'),(22,'stores',1,'design/head/includes',NULL),(23,'stores',1,'design/header/logo_width',NULL),(24,'stores',1,'design/header/logo_height',NULL),(25,'stores',1,'design/footer/absolute_footer',NULL),(26,'stores',1,'design/theme/theme_id','6'),(27,'stores',1,'design/pagination/pagination_frame_skip',NULL),(28,'stores',1,'design/pagination/anchor_text_for_previous',NULL),(29,'stores',1,'design/pagination/anchor_text_for_next',NULL),(30,'stores',1,'design/watermark/image_size',NULL),(31,'stores',1,'design/watermark/image_imageOpacity',NULL),(32,'stores',1,'design/watermark/small_image_size',NULL),(33,'stores',1,'design/watermark/small_image_imageOpacity',NULL),(34,'stores',1,'design/watermark/thumbnail_size',NULL),(35,'stores',1,'design/watermark/thumbnail_imageOpacity',NULL),(36,'stores',1,'design/email/logo_alt',NULL),(37,'stores',1,'design/email/logo_width',NULL),(38,'stores',1,'design/email/logo_height',NULL),(39,'stores',1,'design/watermark/swatch_image_size',NULL),(40,'stores',1,'design/watermark/swatch_image_imageOpacity',NULL),(41,'default',0,'testimonial/general/enable_frontend','0'),(42,'default',0,'testimonial/general/title','Client says'),(43,'default',0,'testimonial/general/auto','0'),(44,'default',0,'testimonial/general/pagination','500'),(45,'default',0,'testimonial/general/qty','4'),(46,'default',0,'testimonial/general/default','1'),(47,'default',0,'testimonial/general/desktop','1'),(48,'default',0,'testimonial/general/desktop_small','1'),(49,'default',0,'testimonial/general/tablet','1'),(50,'default',0,'testimonial/general/mobile','1'),(51,'default',0,'testimonial/general/rows','1'),(52,'default',0,'testimonial/general/show_description','1'),(53,'default',0,'testimonial/general/show_next_back','0'),(54,'default',0,'testimonial/general/show_navigation_control','1'),(55,'default',0,'testimonial/general/customers_submit','1'),(56,'default',0,'testimonial/general/allow_guest','1'),(57,'default',0,'testimonial/general/approve_testimonials','1'),(58,'default',0,'testimonial/general/per_page',NULL),(59,'default',0,'testimonial/general/footer_link','0'),(60,'default',0,'categorytab/new_status/enabled','0'),(61,'default',0,'categorytab/new_status/items','10'),(62,'default',0,'categorytab/new_status/speed','3000'),(63,'default',0,'categorytab/new_status/qty','10'),(64,'default',0,'categorytab/new_status/navigation','0'),(65,'default',0,'categorytab/new_status/pagination','0'),(66,'default',0,'categorytab/new_status/auto','0'),(67,'default',0,'mfblog/general/enable_frontend','0'),(68,'default',0,'mfblog/general/title','LATEST FROM OUR BLOG'),(69,'default',0,'mfblog/general/des','Aenean commodo ligula eget dolor Aenean massa. Portals seize data-driven, tag expedite.'),(70,'default',0,'mfblog/general/auto','0'),(71,'default',0,'mfblog/general/pagination','500'),(72,'default',0,'mfblog/general/rows','1'),(73,'default',0,'mfblog/general/show_next_back','1'),(74,'default',0,'mfblog/general/show_navigation_control','0'),(75,'default',0,'mfblog/index_page/title','Blog'),(76,'default',0,'mfblog/index_page/meta_keywords',NULL),(77,'default',0,'mfblog/index_page/meta_description',NULL),(78,'default',0,'mfblog/post_view/related_posts/enabled','1'),(79,'default',0,'mfblog/post_view/related_posts/number_of_posts','5'),(80,'default',0,'mfblog/post_view/related_products/enabled','1'),(81,'default',0,'mfblog/post_view/related_products/number_of_products','5'),(82,'default',0,'mfblog/post_view/comments/type','facebook'),(83,'default',0,'mfblog/post_view/comments/number_of_comments','4'),(84,'default',0,'mfblog/post_view/comments/fb_app_id',NULL),(85,'default',0,'mfblog/post_list/posts_per_page','10'),(86,'default',0,'mfblog/sidebar/search/enabled','1'),(87,'default',0,'mfblog/sidebar/search/sort_order','10'),(88,'default',0,'mfblog/sidebar/categories/enabled','1'),(89,'default',0,'mfblog/sidebar/categories/sort_order','20'),(90,'default',0,'mfblog/sidebar/recent_posts/enabled','1'),(91,'default',0,'mfblog/sidebar/recent_posts/posts_per_page','5'),(92,'default',0,'mfblog/sidebar/recent_posts/sort_order','30'),(93,'default',0,'mfblog/sidebar/archive/enabled','1'),(94,'default',0,'mfblog/sidebar/archive/sort_order','40'),(95,'default',0,'mfblog/rss_feed/title','Blog Rss'),(96,'default',0,'mfblog/rss_feed/description',NULL),(97,'default',0,'categorytop/new_status/enabled','0'),(98,'default',0,'categorytop/new_status/items_show','3'),(99,'default',0,'instagramgallery/general/isactive','0'),(100,'default',0,'instagramgallery/general/display_title','1'),(101,'default',0,'instagramgallery/general/title','Instagram'),(102,'default',0,'instagramgallery/general/users_id','4218727808'),(103,'default',0,'instagramgallery/general/access_token','4218727808.1677ed0.24d114a5597c45c2a120a66c3433462d'),(104,'default',0,'instagramgallery/general/limit_image','5'),(105,'default',0,'instagramgallery/general/full_name','1'),(106,'default',0,'instagramgallery/general/target','_self'),(107,'default',0,'instagramgallery/general/type_popup','thumb'),(108,'default',0,'newsletterpopup/popup_group/enabled','0'),(109,'default',0,'newsletterpopup/popup_group/title','NEWSLETTER'),(110,'default',0,'newsletterpopup/popup_group/content','Subscribe to the Thebell mailing list to receive updates on new arrivals, special offers and other discount information.'),(111,'default',0,'newsletterpopup/popup_group/width','790'),(112,'default',0,'newsletterpopup/popup_group/height','390'),(113,'default',0,'newsletterpopup/popup_group/speed',NULL),(114,'default',0,'newsletterpopup/popup_group/margin_top',NULL),(115,'default',0,'newsletterpopup/popup_group/cookie_timeout','1'),(116,'default',0,'newsletterpopup/popup_group/background',NULL),(117,'default',0,'newproductslider/general/enabled','0'),(118,'default',0,'newproductslider/general/row','1'),(119,'default',0,'newproductslider/general/title','New Products'),(120,'default',0,'newproductslider/general/itemsDefault','4'),(121,'default',0,'newproductslider/general/itemsDesktop','4'),(122,'default',0,'newproductslider/general/itemsDesktopSmall','3'),(123,'default',0,'newproductslider/general/itemsTablet','2'),(124,'default',0,'newproductslider/general/itemsMobile','1'),(125,'default',0,'newproductslider/general/speed','1000'),(126,'default',0,'newproductslider/general/qty','20'),(127,'default',0,'newproductslider/general/addtocart','1'),(128,'default',0,'newproductslider/general/wishlist','1'),(129,'default',0,'newproductslider/general/compare','0'),(130,'default',0,'newproductslider/general/navigation','0'),(131,'default',0,'newproductslider/general/pagination','0'),(132,'default',0,'newproductslider/general/auto','0'),(133,'default',0,'featureproductslider/new_status/enabled','0'),(134,'default',0,'featureproductslider/new_status/items','9'),(135,'default',0,'featureproductslider/new_status/row','3'),(136,'default',0,'featureproductslider/new_status/speed','1000'),(137,'default',0,'featureproductslider/new_status/qty','8'),(138,'default',0,'featureproductslider/new_status/addtocart','1'),(139,'default',0,'featureproductslider/new_status/wishlist','1'),(140,'default',0,'featureproductslider/new_status/compare','0'),(141,'default',0,'featureproductslider/new_status/navigation','0'),(142,'default',0,'featureproductslider/new_status/pagination','1'),(143,'default',0,'featureproductslider/new_status/auto','0'),(144,'default',0,'bestsellerproduct/new_status/enabled','0'),(145,'default',0,'bestsellerproduct/new_status/title','best seller'),(146,'default',0,'bestsellerproduct/new_status/row','3'),(147,'default',0,'bestsellerproduct/new_status/speed','1000'),(148,'default',0,'bestsellerproduct/new_status/qty','5'),(149,'default',0,'bestsellerproduct/new_status/navigation','0'),(150,'default',0,'bestsellerproduct/new_status/pagination','1'),(151,'default',0,'bestsellerproduct/new_status/auto','0'),(152,'stores',1,'design/header/logo_src','stores/1/logo.png'),(153,'default',0,'general/store_information/name','Cultura Mezcal'),(154,'default',0,'general/store_information/phone',NULL),(155,'default',0,'general/store_information/hours',NULL),(156,'default',0,'general/store_information/country_id',NULL),(157,'default',0,'general/store_information/region_id',NULL),(158,'default',0,'general/store_information/postcode',NULL),(159,'default',0,'general/store_information/city',NULL),(160,'default',0,'general/store_information/street_line1',NULL),(161,'default',0,'general/store_information/street_line2',NULL),(162,'default',0,'general/store_information/merchant_vat_number',NULL),(163,'default',0,'general/single_store_mode/enabled','0'),(164,'default',0,'web/unsecure/base_static_url',NULL),(165,'default',0,'web/unsecure/base_media_url',NULL),(166,'default',0,'web/secure/base_static_url',NULL),(167,'default',0,'web/secure/base_media_url',NULL),(168,'default',0,'web/cookie/cookie_path',NULL),(169,'default',0,'web/cookie/cookie_domain',NULL),(170,'default',0,'web/cookie/cookie_httponly','1'),(171,'default',0,'dev/restrict/allow_ips',NULL),(172,'default',0,'dev/debug/template_hints_storefront','0'),(173,'default',0,'dev/debug/template_hints_admin','0'),(174,'default',0,'dev/debug/template_hints_blocks','0'),(175,'default',0,'dev/template/allow_symlink','0'),(176,'default',0,'dev/translate_inline/active','0'),(177,'default',0,'dev/translate_inline/active_admin','0'),(178,'default',0,'dev/js/merge_files','0'),(179,'default',0,'dev/js/enable_js_bundling','0'),(180,'default',0,'dev/js/minify_files','0'),(181,'default',0,'dev/css/merge_css_files','0'),(182,'default',0,'dev/css/minify_files','0'),(183,'default',0,'dev/static/sign','1'),(184,'default',0,'admin/security/use_case_sensitive_login','0'),(185,'default',0,'admin/security/session_lifetime','900000'),(186,'stores',1,'design/head/shortcut_icon','stores/1/favicon.ico'),(187,'stores',1,'design/header/logo_alt','Cultura Mezcal'),(188,'default',0,'carriers/flatrate/active','1'),(189,'default',0,'carriers/flatrate/handling_fee',NULL),(190,'default',0,'carriers/flatrate/specificcountry',NULL),(191,'default',0,'carriers/flatrate/showmethod','0'),(192,'default',0,'carriers/flatrate/sort_order',NULL),(193,'default',0,'carriers/freeshipping/free_shipping_subtotal',NULL),(194,'default',0,'carriers/freeshipping/specificcountry',NULL),(195,'default',0,'carriers/freeshipping/showmethod','0'),(196,'default',0,'carriers/freeshipping/sort_order',NULL),(197,'default',0,'carriers/tablerate/handling_fee',NULL),(198,'default',0,'carriers/tablerate/specificcountry',NULL),(199,'default',0,'carriers/tablerate/showmethod','0'),(200,'default',0,'carriers/tablerate/sort_order',NULL),(201,'default',0,'carriers/ups/access_license_number',NULL),(202,'default',0,'carriers/ups/username',NULL),(203,'default',0,'carriers/ups/password',NULL),(204,'default',0,'carriers/ups/shipper_number',NULL),(205,'default',0,'carriers/ups/handling_fee',NULL),(206,'default',0,'carriers/ups/free_shipping_enable','0'),(207,'default',0,'carriers/ups/free_shipping_subtotal',NULL),(208,'default',0,'carriers/ups/specificcountry',NULL),(209,'default',0,'carriers/ups/showmethod','0'),(210,'default',0,'carriers/ups/debug','0'),(211,'default',0,'carriers/ups/sort_order',NULL),(212,'default',0,'carriers/usps/userid',NULL),(213,'default',0,'carriers/usps/password',NULL),(214,'default',0,'carriers/usps/handling_fee',NULL),(215,'default',0,'carriers/usps/free_shipping_enable','0'),(216,'default',0,'carriers/usps/specificcountry',NULL),(217,'default',0,'carriers/usps/debug','0'),(218,'default',0,'carriers/usps/showmethod','0'),(219,'default',0,'carriers/usps/sort_order',NULL),(220,'default',0,'carriers/fedex/account',NULL),(221,'default',0,'carriers/fedex/meter_number',NULL),(222,'default',0,'carriers/fedex/key',NULL),(223,'default',0,'carriers/fedex/password',NULL),(224,'default',0,'carriers/fedex/handling_fee',NULL),(225,'default',0,'carriers/fedex/residence_delivery','0'),(226,'default',0,'carriers/fedex/smartpost_hubid',NULL),(227,'default',0,'carriers/fedex/free_shipping_enable','0'),(228,'default',0,'carriers/fedex/specificcountry',NULL),(229,'default',0,'carriers/fedex/debug','0'),(230,'default',0,'carriers/fedex/showmethod','0'),(231,'default',0,'carriers/fedex/sort_order',NULL),(232,'default',0,'carriers/dhl/id',NULL),(233,'default',0,'carriers/dhl/password',NULL),(234,'default',0,'carriers/dhl/handling_fee',NULL),(235,'default',0,'carriers/dhl/free_method_nondoc',NULL),(236,'default',0,'carriers/dhl/free_shipping_enable','0'),(237,'default',0,'carriers/dhl/specificcountry',NULL),(238,'default',0,'carriers/dhl/showmethod','0'),(239,'default',0,'carriers/dhl/debug','0'),(240,'default',0,'carriers/dhl/sort_order',NULL),(241,'default',0,'checkout/options/enable_agreements','0'),(242,'default',0,'checkout/options/display_billing_address_on','0'),(243,'default',0,'checkout/payment_failed/copy_to',NULL),(244,'default',0,'checkout/payment_failed/copy_method','bcc'),(246,'default',0,'brandslider/general/enable_frontend','1'),(247,'default',0,'brandslider/general/title',NULL),(248,'default',0,'brandslider/general/auto','1'),(249,'default',0,'brandslider/general/speed','5000'),(250,'default',0,'brandslider/general/pagination','500'),(251,'default',0,'brandslider/general/qty','12'),(252,'default',0,'brandslider/general/default','6'),(253,'default',0,'brandslider/general/desktop','5'),(254,'default',0,'brandslider/general/desktop_small','4'),(255,'default',0,'brandslider/general/tablet','3'),(256,'default',0,'brandslider/general/mobile','2'),(257,'default',0,'brandslider/general/rows','1'),(258,'default',0,'brandslider/general/show_description','0'),(259,'default',0,'brandslider/general/show_next_back','0'),(260,'default',0,'brandslider/general/show_navigation_control','0'),(261,'default',0,'shipping/origin/country_id','US'),(262,'default',0,'shipping/origin/region_id','12'),(263,'default',0,'shipping/origin/postcode','90034'),(264,'default',0,'shipping/origin/city',NULL),(265,'default',0,'shipping/origin/street_line1',NULL),(266,'default',0,'shipping/origin/street_line2',NULL),(267,'default',0,'shipping/shipping_policy/enable_shipping_policy','0'),(268,'default',0,'shipping/shipping_policy/shipping_policy_content',NULL),(269,'default',0,'paypal/general/merchant_country','ES'),(270,'default',0,'payment/paypal_express/active','0'),(271,'default',0,'payment/paypal_express/in_context','0'),(272,'default',0,'paypal/general/business_account',NULL),(273,'default',0,'paypal/wpp/api_authentication','0'),(274,'default',0,'paypal/wpp/api_username',NULL),(275,'default',0,'paypal/wpp/api_password',NULL),(276,'default',0,'paypal/wpp/api_signature',NULL),(277,'default',0,'paypal/wpp/sandbox_flag','0'),(278,'default',0,'paypal/wpp/use_proxy','0'),(279,'default',0,'payment/paypal_express/title','PayPal Express Checkout'),(280,'default',0,'payment/paypal_express/sort_order',NULL),(281,'default',0,'payment/paypal_express/payment_action','Authorization'),(282,'default',0,'payment/paypal_express/visible_on_product','1'),(283,'default',0,'payment/paypal_express/visible_on_cart','1'),(284,'default',0,'payment/paypal_express/allowspecific','0'),(285,'default',0,'payment/paypal_express/debug','0'),(286,'default',0,'payment/paypal_express/verify_peer','1'),(287,'default',0,'payment/paypal_express/line_items_enabled','1'),(288,'default',0,'payment/paypal_express/transfer_shipping_options','0'),(289,'default',0,'paypal/wpp/button_flavor','dynamic'),(290,'default',0,'payment/paypal_express/solution_type','Mark'),(291,'default',0,'payment/paypal_express/require_billing_address','0'),(292,'default',0,'payment/paypal_express/allow_ba_signup','never'),(293,'default',0,'payment/paypal_express/skip_order_review_step','1'),(294,'default',0,'payment/paypal_billing_agreement/active','1'),(295,'default',0,'payment/paypal_billing_agreement/title','PayPal Billing Agreement'),(296,'default',0,'payment/paypal_billing_agreement/sort_order',NULL),(297,'default',0,'payment/paypal_billing_agreement/payment_action','Authorization'),(298,'default',0,'payment/paypal_billing_agreement/allowspecific','0'),(299,'default',0,'payment/paypal_billing_agreement/debug','0'),(300,'default',0,'payment/paypal_billing_agreement/verify_peer','1'),(301,'default',0,'payment/paypal_billing_agreement/line_items_enabled','0'),(302,'default',0,'payment/paypal_billing_agreement/allow_billing_agreement_wizard','1'),(303,'default',0,'paypal/fetch_reports/ftp_login',NULL),(304,'default',0,'paypal/fetch_reports/ftp_password',NULL),(305,'default',0,'paypal/fetch_reports/ftp_sandbox','0'),(306,'default',0,'paypal/fetch_reports/ftp_ip',NULL),(307,'default',0,'paypal/fetch_reports/ftp_path',NULL),(308,'default',0,'paypal/fetch_reports/active','0'),(309,'default',0,'paypal/fetch_reports/schedule','1'),(310,'default',0,'paypal/fetch_reports/time','00,00,00'),(311,'default',0,'paypal/style/logo',NULL),(312,'default',0,'paypal/style/page_style',NULL),(313,'default',0,'paypal/style/paypal_hdrimg',NULL),(314,'default',0,'paypal/style/paypal_hdrbackcolor',NULL),(315,'default',0,'paypal/style/paypal_hdrbordercolor',NULL),(316,'default',0,'paypal/style/paypal_payflowcolor',NULL),(317,'default',0,'payment/braintree/active','0'),(318,'default',0,'payment/braintree_paypal/active','0'),(319,'default',0,'payment/braintree_cc_vault/active','0'),(320,'default',0,'payment/braintree/title','Credit Card (Braintree)'),(321,'default',0,'payment/braintree/environment','sandbox'),(322,'default',0,'payment/braintree/payment_action','authorize'),(323,'default',0,'payment/braintree/merchant_id',NULL),(324,'default',0,'payment/braintree/public_key',NULL),(325,'default',0,'payment/braintree/private_key',NULL),(326,'default',0,'payment/braintree_cc_vault/title','Stored Cards (Braintree)'),(327,'default',0,'payment/braintree/merchant_account_id',NULL),(328,'default',0,'payment/braintree/fraudprotection','0'),(329,'default',0,'payment/braintree/debug','0'),(330,'default',0,'payment/braintree/useccv','1'),(331,'default',0,'payment/braintree/cctypes','CUP,AE,VI,MC,DI,JCB,DN,MI'),(332,'default',0,'payment/braintree/sort_order',NULL),(333,'default',0,'payment/braintree/allowspecific','0'),(334,'default',0,'payment/braintree/specificcountry',NULL),(335,'default',0,'payment/braintree/countrycreditcard','a:0:{}'),(336,'default',0,'payment/braintree_paypal/title','PayPal (Braintree)'),(337,'default',0,'payment/braintree_paypal_vault/active','0'),(338,'default',0,'payment/braintree_paypal/sort_order',NULL),(339,'default',0,'payment/braintree_paypal/merchant_name_override',NULL),(340,'default',0,'payment/braintree_paypal/payment_action','authorize'),(341,'default',0,'payment/braintree_paypal/allowspecific','0'),(342,'default',0,'payment/braintree_paypal/specificcountry',NULL),(343,'default',0,'payment/braintree_paypal/require_billing_address','0'),(344,'default',0,'payment/braintree_paypal/allow_shipping_address_override','1'),(345,'default',0,'payment/braintree_paypal/debug','0'),(346,'default',0,'payment/braintree_paypal/display_on_shopping_cart','1'),(347,'default',0,'payment/braintree_paypal/skip_order_review','0'),(348,'default',0,'payment/braintree/verify_3dsecure','0'),(349,'default',0,'payment/braintree/threshold_amount',NULL),(350,'default',0,'payment/braintree/verify_all_countries','0'),(351,'default',0,'payment/braintree/verify_specific_countries',NULL),(352,'default',0,'payment/braintree/descriptor_name',NULL),(353,'default',0,'payment/braintree/descriptor_phone',NULL),(354,'default',0,'payment/braintree/descriptor_url',NULL),(355,'default',0,'payment/hosted_pro/active','0'),(356,'default',0,'payment/hosted_pro/title','Payment by cards or by PayPal account'),(357,'default',0,'payment/hosted_pro/sort_order',NULL),(358,'default',0,'payment/hosted_pro/payment_action','Authorization'),(359,'default',0,'payment/hosted_pro/display_ec','0'),(360,'default',0,'payment/hosted_pro/allowspecific','0'),(361,'default',0,'payment/hosted_pro/debug','0'),(362,'default',0,'payment/hosted_pro/verify_peer','1'),(363,'default',0,'payment/wps_express/active','0'),(364,'default',0,'payment/checkmo/specificcountry',NULL),(365,'default',0,'payment/checkmo/payable_to',NULL),(366,'default',0,'payment/checkmo/mailing_address',NULL),(367,'default',0,'payment/checkmo/min_order_total',NULL),(368,'default',0,'payment/checkmo/max_order_total',NULL),(369,'default',0,'payment/checkmo/sort_order',NULL),(370,'default',0,'payment/banktransfer/specificcountry',NULL),(371,'default',0,'payment/banktransfer/instructions','Cuentas de CulturaMezcal:\r\nBanco\r\nNo. 12345678910121314'),(372,'default',0,'payment/banktransfer/min_order_total',NULL),(373,'default',0,'payment/banktransfer/max_order_total',NULL),(374,'default',0,'payment/banktransfer/sort_order',NULL),(375,'default',0,'payment/cashondelivery/specificcountry',NULL),(376,'default',0,'payment/cashondelivery/instructions',NULL),(377,'default',0,'payment/cashondelivery/min_order_total',NULL),(378,'default',0,'payment/cashondelivery/max_order_total',NULL),(379,'default',0,'payment/cashondelivery/sort_order',NULL),(380,'default',0,'payment/free/specificcountry',NULL),(381,'default',0,'payment/purchaseorder/specificcountry',NULL),(382,'default',0,'payment/purchaseorder/min_order_total',NULL),(383,'default',0,'payment/purchaseorder/max_order_total',NULL),(384,'default',0,'payment/purchaseorder/sort_order',NULL),(385,'default',0,'payment/authorizenet_directpost/login',NULL),(386,'default',0,'payment/authorizenet_directpost/trans_key',NULL),(387,'default',0,'payment/authorizenet_directpost/trans_md5',NULL),(388,'default',0,'payment/authorizenet_directpost/merchant_email',NULL),(389,'default',0,'payment/authorizenet_directpost/useccv','0'),(390,'default',0,'payment/authorizenet_directpost/min_order_total',NULL),(391,'default',0,'payment/authorizenet_directpost/max_order_total',NULL),(392,'default',0,'payment/authorizenet_directpost/sort_order',NULL),(393,'default',0,'web/default/show_cms_breadcrumbs','0'),(394,'default',0,'contact/contact/enabled','1'),(395,'default',0,'advanced/modules_disable_output/FishPig_WordPress','0'),(396,'default',0,'advanced/modules_disable_output/Magento_AdminNotification','0'),(397,'default',0,'advanced/modules_disable_output/Magento_AdvancedPricingImportExport','0'),(398,'default',0,'advanced/modules_disable_output/Magento_Authorization','0'),(399,'default',0,'advanced/modules_disable_output/Magento_Authorizenet','0'),(400,'default',0,'advanced/modules_disable_output/Magento_Backup','0'),(401,'default',0,'advanced/modules_disable_output/Magento_Braintree','0'),(402,'default',0,'advanced/modules_disable_output/Magento_Bundle','0'),(403,'default',0,'advanced/modules_disable_output/Magento_BundleImportExport','0'),(404,'default',0,'advanced/modules_disable_output/Magento_CacheInvalidate','0'),(405,'default',0,'advanced/modules_disable_output/Magento_Captcha','0'),(406,'default',0,'advanced/modules_disable_output/Magento_Catalog','0'),(407,'default',0,'advanced/modules_disable_output/Magento_CatalogImportExport','0'),(408,'default',0,'advanced/modules_disable_output/Magento_CatalogInventory','0'),(409,'default',0,'advanced/modules_disable_output/Magento_CatalogRule','0'),(410,'default',0,'advanced/modules_disable_output/Magento_CatalogRuleConfigurable','0'),(411,'default',0,'advanced/modules_disable_output/Magento_CatalogSearch','0'),(412,'default',0,'advanced/modules_disable_output/Magento_CatalogUrlRewrite','0'),(413,'default',0,'advanced/modules_disable_output/Magento_CatalogWidget','0'),(414,'default',0,'advanced/modules_disable_output/Magento_Checkout','0'),(415,'default',0,'advanced/modules_disable_output/Magento_CheckoutAgreements','0'),(416,'default',0,'advanced/modules_disable_output/Magento_Cms','0'),(417,'default',0,'advanced/modules_disable_output/Magento_CmsUrlRewrite','0'),(418,'default',0,'advanced/modules_disable_output/Magento_Config','0'),(419,'default',0,'advanced/modules_disable_output/Magento_ConfigurableImportExport','0'),(420,'default',0,'advanced/modules_disable_output/Magento_ConfigurableProduct','0'),(421,'default',0,'advanced/modules_disable_output/Magento_Contact','0'),(422,'default',0,'advanced/modules_disable_output/Magento_Cookie','0'),(423,'default',0,'advanced/modules_disable_output/Magento_Cron','0'),(424,'default',0,'advanced/modules_disable_output/Magento_CurrencySymbol','0'),(425,'default',0,'advanced/modules_disable_output/Magento_Customer','0'),(426,'default',0,'advanced/modules_disable_output/Magento_CustomerImportExport','0'),(427,'default',0,'advanced/modules_disable_output/Magento_Deploy','0'),(428,'default',0,'advanced/modules_disable_output/Magento_Developer','0'),(429,'default',0,'advanced/modules_disable_output/Magento_Dhl','0'),(430,'default',0,'advanced/modules_disable_output/Magento_Directory','0'),(431,'default',0,'advanced/modules_disable_output/Magento_Downloadable','0'),(432,'default',0,'advanced/modules_disable_output/Magento_DownloadableImportExport','0'),(433,'default',0,'advanced/modules_disable_output/Magento_Eav','0'),(434,'default',0,'advanced/modules_disable_output/Magento_Email','0'),(435,'default',0,'advanced/modules_disable_output/Magento_EncryptionKey','0'),(436,'default',0,'advanced/modules_disable_output/Magento_Fedex','0'),(437,'default',0,'advanced/modules_disable_output/Magento_GiftMessage','0'),(438,'default',0,'advanced/modules_disable_output/Magento_GoogleAdwords','0'),(439,'default',0,'advanced/modules_disable_output/Magento_GoogleAnalytics','0'),(440,'default',0,'advanced/modules_disable_output/Magento_GoogleOptimizer','0'),(441,'default',0,'advanced/modules_disable_output/Magento_GroupedImportExport','0'),(442,'default',0,'advanced/modules_disable_output/Magento_GroupedProduct','0'),(443,'default',0,'advanced/modules_disable_output/Magento_ImportExport','0'),(444,'default',0,'advanced/modules_disable_output/Magento_Indexer','0'),(445,'default',0,'advanced/modules_disable_output/Magento_Integration','0'),(446,'default',0,'advanced/modules_disable_output/Magento_LayeredNavigation','0'),(447,'default',0,'advanced/modules_disable_output/Magento_Marketplace','0'),(448,'default',0,'advanced/modules_disable_output/Magento_MediaStorage','0'),(449,'default',0,'advanced/modules_disable_output/Magento_Msrp','0'),(450,'default',0,'advanced/modules_disable_output/Magento_Multishipping','0'),(451,'default',0,'advanced/modules_disable_output/Magento_NewRelicReporting','0'),(452,'default',0,'advanced/modules_disable_output/Magento_Newsletter','0'),(453,'default',0,'advanced/modules_disable_output/Magento_OfflinePayments','0'),(454,'default',0,'advanced/modules_disable_output/Magento_OfflineShipping','0'),(455,'default',0,'advanced/modules_disable_output/Magento_PageCache','0'),(456,'default',0,'advanced/modules_disable_output/Magento_Payment','0'),(457,'default',0,'advanced/modules_disable_output/Magento_Paypal','0'),(458,'default',0,'advanced/modules_disable_output/Magento_Persistent','0'),(459,'default',0,'advanced/modules_disable_output/Magento_ProductAlert','0'),(460,'default',0,'advanced/modules_disable_output/Magento_ProductVideo','0'),(461,'default',0,'advanced/modules_disable_output/Magento_Quote','0'),(462,'default',0,'advanced/modules_disable_output/Magento_Reports','0'),(463,'default',0,'advanced/modules_disable_output/Magento_RequireJs','0'),(464,'default',0,'advanced/modules_disable_output/Magento_Review','0'),(465,'default',0,'advanced/modules_disable_output/Magento_Rss','0'),(466,'default',0,'advanced/modules_disable_output/Magento_Rule','0'),(467,'default',0,'advanced/modules_disable_output/Magento_Sales','0'),(468,'default',0,'advanced/modules_disable_output/Magento_SalesInventory','0'),(469,'default',0,'advanced/modules_disable_output/Magento_SalesRule','0'),(470,'default',0,'advanced/modules_disable_output/Magento_SalesSequence','0'),(471,'default',0,'advanced/modules_disable_output/Magento_SampleData','0'),(472,'default',0,'advanced/modules_disable_output/Magento_Search','0'),(473,'default',0,'advanced/modules_disable_output/Magento_Security','0'),(474,'default',0,'advanced/modules_disable_output/Magento_SendFriend','0'),(475,'default',0,'advanced/modules_disable_output/Magento_Shipping','0'),(476,'default',0,'advanced/modules_disable_output/Magento_Sitemap','0'),(477,'default',0,'advanced/modules_disable_output/Magento_Store','0'),(478,'default',0,'advanced/modules_disable_output/Magento_Swagger','0'),(479,'default',0,'advanced/modules_disable_output/Magento_Swatches','0'),(480,'default',0,'advanced/modules_disable_output/Magento_SwatchesLayeredNavigation','0'),(481,'default',0,'advanced/modules_disable_output/Magento_Tax','0'),(482,'default',0,'advanced/modules_disable_output/Magento_TaxImportExport','0'),(483,'default',0,'advanced/modules_disable_output/Magento_Theme','0'),(484,'default',0,'advanced/modules_disable_output/Magento_Translation','0'),(485,'default',0,'advanced/modules_disable_output/Magento_Ui','0'),(486,'default',0,'advanced/modules_disable_output/Magento_Ups','0'),(487,'default',0,'advanced/modules_disable_output/Magento_UrlRewrite','0'),(488,'default',0,'advanced/modules_disable_output/Magento_User','0'),(489,'default',0,'advanced/modules_disable_output/Magento_Usps','0'),(490,'default',0,'advanced/modules_disable_output/Magento_Variable','0'),(491,'default',0,'advanced/modules_disable_output/Magento_Vault','0'),(492,'default',0,'advanced/modules_disable_output/Magento_Version','0'),(493,'default',0,'advanced/modules_disable_output/Magento_Webapi','0'),(494,'default',0,'advanced/modules_disable_output/Magento_WebapiSecurity','0'),(495,'default',0,'advanced/modules_disable_output/Magento_Weee','0'),(496,'default',0,'advanced/modules_disable_output/Magento_Widget','0'),(497,'default',0,'advanced/modules_disable_output/Magento_Wishlist','0'),(498,'default',0,'advanced/modules_disable_output/Plazathemes_Bannerslider','0'),(499,'default',0,'advanced/modules_disable_output/Plazathemes_Bestsellerproduct','0'),(500,'default',0,'advanced/modules_disable_output/Plazathemes_Blog','0'),(501,'default',0,'advanced/modules_disable_output/Plazathemes_Brandslider','0'),(502,'default',0,'advanced/modules_disable_output/Plazathemes_Categorytab','0'),(503,'default',0,'advanced/modules_disable_output/Plazathemes_Categorytop','0'),(504,'default',0,'advanced/modules_disable_output/Plazathemes_Featureproductslider','0'),(505,'default',0,'advanced/modules_disable_output/Plazathemes_Hozmegamenu','0'),(506,'default',0,'advanced/modules_disable_output/Plazathemes_InstagramGallery','0'),(507,'default',0,'advanced/modules_disable_output/Plazathemes_Layout','0'),(508,'default',0,'advanced/modules_disable_output/Plazathemes_Loadjs','0'),(509,'default',0,'advanced/modules_disable_output/Plazathemes_Newproductslider','0'),(510,'default',0,'advanced/modules_disable_output/Plazathemes_Newsletterpopup','0'),(511,'default',0,'advanced/modules_disable_output/Plazathemes_Override','0'),(512,'default',0,'advanced/modules_disable_output/Plazathemes_Recentproductslider','0'),(513,'default',0,'advanced/modules_disable_output/Plazathemes_Template','0'),(514,'default',0,'advanced/modules_disable_output/Plazathemes_Testimonial','0'),(515,'default',0,'advanced/modules_disable_output/Plazathemes_Themeoptions','0'),(516,'default',0,'advanced/modules_disable_output/SendGrid_EmailDeliverySimplified','0'),(517,'default',0,'carriers/flatrate/title','Cultura Mezcal'),(518,'default',0,'carriers/flatrate/name','Tarifa Plana'),(519,'default',0,'carriers/flatrate/type','I'),(520,'default',0,'carriers/flatrate/handling_type','F'),(521,'default',0,'payment/checkmo/active','0'),(522,'default',0,'payment/banktransfer/active','1'),(523,'default',0,'payment/free/active','0'),(524,'default',0,'catalog/frontend/list_allow_all','0'),(525,'default',0,'catalog/frontend/flat_catalog_product','0'),(526,'default',0,'catalog/frontend/show_swatches_in_product_list','0'),(527,'default',0,'catalog/productalert_cron/frequency','D'),(528,'default',0,'crontab/default/jobs/catalog_product_alert/schedule/cron_expr','0 0 * * *'),(529,'default',0,'crontab/default/jobs/catalog_product_alert/run/model',NULL),(530,'default',0,'catalog/productalert_cron/time','00,00,00'),(531,'default',0,'catalog/productalert_cron/error_email',NULL),(532,'default',0,'catalog/product_video/youtube_api_key',NULL),(533,'default',0,'catalog/recently_products/scope','store'),(534,'default',0,'catalog/recently_products/viewed_count','0'),(535,'default',0,'catalog/recently_products/compared_count','0'),(536,'default',0,'catalog/price/scope','0'),(537,'default',0,'catalog/layered_navigation/display_product_count','1'),(538,'default',0,'catalog/layered_navigation/price_range_calculation','auto'),(539,'default',0,'catalog/downloadable/shareable','0'),(540,'default',0,'catalog/downloadable/content_disposition','inline'),(541,'default',0,'catalog/custom_options/use_calendar','0'),(542,'default',0,'catalog/custom_options/year_range',','),(543,'default',0,'catalog/placeholder/image_placeholder',NULL),(544,'default',0,'catalog/placeholder/small_image_placeholder',NULL),(545,'default',0,'catalog/placeholder/swatch_image_placeholder',NULL),(546,'default',0,'catalog/placeholder/thumbnail_placeholder',NULL);
/*!40000 ALTER TABLE `core_config_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cron_schedule`
--

DROP TABLE IF EXISTS `cron_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cron_schedule` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Schedule Id',
  `job_code` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Job Code',
  `status` varchar(7) NOT NULL DEFAULT 'pending' COMMENT 'Status',
  `messages` text COMMENT 'Messages',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `scheduled_at` timestamp NULL DEFAULT NULL COMMENT 'Scheduled At',
  `executed_at` timestamp NULL DEFAULT NULL COMMENT 'Executed At',
  `finished_at` timestamp NULL DEFAULT NULL COMMENT 'Finished At',
  PRIMARY KEY (`schedule_id`),
  KEY `CRON_SCHEDULE_JOB_CODE` (`job_code`),
  KEY `CRON_SCHEDULE_SCHEDULED_AT_STATUS` (`scheduled_at`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Cron Schedule';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cron_schedule`
--

LOCK TABLES `cron_schedule` WRITE;
/*!40000 ALTER TABLE `cron_schedule` DISABLE KEYS */;
INSERT INTO `cron_schedule` VALUES (1,'catalog_index_refresh_price','pending',NULL,'2020-03-05 18:59:21','2020-03-05 19:00:00',NULL,NULL),(2,'catalog_product_outdated_price_values_cleanup','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(3,'sales_clean_orders','pending',NULL,'2020-03-05 18:59:21','2020-03-05 19:00:00',NULL,NULL),(4,'sales_grid_order_async_insert','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(5,'sales_grid_order_invoice_async_insert','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(6,'sales_grid_order_shipment_async_insert','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(7,'sales_grid_order_creditmemo_async_insert','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(8,'sales_send_order_emails','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(9,'sales_send_order_invoice_emails','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(10,'sales_send_order_shipment_emails','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(11,'sales_send_order_creditmemo_emails','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(12,'newsletter_send_all','pending',NULL,'2020-03-05 18:59:21','2020-03-05 19:00:00',NULL,NULL),(13,'captcha_delete_old_attempts','pending',NULL,'2020-03-05 18:59:21','2020-03-05 19:00:00',NULL,NULL),(14,'captcha_delete_expired_images','pending',NULL,'2020-03-05 18:59:21','2020-03-05 19:00:00',NULL,NULL),(15,'outdated_authentication_failures_cleanup','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(16,'expired_tokens_cleanup','pending',NULL,'2020-03-05 18:59:21','2020-03-05 19:00:00',NULL,NULL),(17,'magento_newrelicreporting_cron','pending',NULL,'2020-03-05 18:59:21','2020-03-05 19:00:00',NULL,NULL),(18,'indexer_reindex_all_invalid','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(19,'indexer_update_all_views','pending',NULL,'2020-03-05 18:59:21','2020-03-05 18:59:00',NULL,NULL),(20,'indexer_clean_all_changelogs','pending',NULL,'2020-03-05 18:59:21','2020-03-05 19:00:00',NULL,NULL);
/*!40000 ALTER TABLE `cron_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_address_entity`
--

DROP TABLE IF EXISTS `customer_address_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_address_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `city` varchar(255) NOT NULL COMMENT 'City',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `country_id` varchar(255) NOT NULL COMMENT 'Country',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `firstname` varchar(255) NOT NULL COMMENT 'First Name',
  `lastname` varchar(255) NOT NULL COMMENT 'Last Name',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middle Name',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Zip/Postal Code',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `region` varchar(255) DEFAULT NULL COMMENT 'State/Province',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'State/Province',
  `street` text NOT NULL COMMENT 'Street Address',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `telephone` varchar(255) NOT NULL COMMENT 'Phone Number',
  `vat_id` varchar(255) DEFAULT NULL COMMENT 'VAT number',
  `vat_is_valid` int(10) unsigned DEFAULT NULL COMMENT 'VAT number validity',
  `vat_request_date` varchar(255) DEFAULT NULL COMMENT 'VAT number validation request date',
  `vat_request_id` varchar(255) DEFAULT NULL COMMENT 'VAT number validation request ID',
  `vat_request_success` int(10) unsigned DEFAULT NULL COMMENT 'VAT number validation request success',
  PRIMARY KEY (`entity_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_PARENT_ID` (`parent_id`),
  CONSTRAINT `CUSTOMER_ADDRESS_ENTITY_PARENT_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_address_entity`
--

LOCK TABLES `customer_address_entity` WRITE;
/*!40000 ALTER TABLE `customer_address_entity` DISABLE KEYS */;
INSERT INTO `customer_address_entity` VALUES (1,NULL,4,'2020-03-12 05:01:38','2020-03-12 05:09:10',1,'sdfdsf',NULL,'US',NULL,'Cliente','prueba',NULL,'45644',NULL,'Federated States Of Micronesia',17,'asdasd',NULL,'4575545',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer_address_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_address_entity_datetime`
--

DROP TABLE IF EXISTS `customer_address_entity_datetime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_address_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CSTR_ADDR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_DTIME_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Datetime';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_address_entity_datetime`
--

LOCK TABLES `customer_address_entity_datetime` WRITE;
/*!40000 ALTER TABLE `customer_address_entity_datetime` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_address_entity_datetime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_address_entity_decimal`
--

DROP TABLE IF EXISTS `customer_address_entity_decimal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_address_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CSTR_ADDR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_DEC_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Decimal';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_address_entity_decimal`
--

LOCK TABLES `customer_address_entity_decimal` WRITE;
/*!40000 ALTER TABLE `customer_address_entity_decimal` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_address_entity_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_address_entity_int`
--

DROP TABLE IF EXISTS `customer_address_entity_int`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_address_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CSTR_ADDR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_INT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Int';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_address_entity_int`
--

LOCK TABLES `customer_address_entity_int` WRITE;
/*!40000 ALTER TABLE `customer_address_entity_int` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_address_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_address_entity_text`
--

DROP TABLE IF EXISTS `customer_address_entity_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_address_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CSTR_ADDR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_TEXT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Text';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_address_entity_text`
--

LOCK TABLES `customer_address_entity_text` WRITE;
/*!40000 ALTER TABLE `customer_address_entity_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_address_entity_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_address_entity_varchar`
--

DROP TABLE IF EXISTS `customer_address_entity_varchar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_address_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CSTR_ADDR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_ADDR_ENTT_VCHR_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Varchar';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_address_entity_varchar`
--

LOCK TABLES `customer_address_entity_varchar` WRITE;
/*!40000 ALTER TABLE `customer_address_entity_varchar` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_address_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_eav_attribute`
--

DROP TABLE IF EXISTS `customer_eav_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `input_filter` varchar(255) DEFAULT NULL COMMENT 'Input Filter',
  `multiline_count` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Multiline Count',
  `validate_rules` text COMMENT 'Validate Rules',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `data_model` varchar(255) DEFAULT NULL COMMENT 'Data Model',
  `is_used_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used in Grid',
  `is_visible_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible in Grid',
  `is_filterable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable in Grid',
  `is_searchable_in_grid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable in Grid',
  PRIMARY KEY (`attribute_id`),
  CONSTRAINT `CUSTOMER_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_eav_attribute`
--

LOCK TABLES `customer_eav_attribute` WRITE;
/*!40000 ALTER TABLE `customer_eav_attribute` DISABLE KEYS */;
INSERT INTO `customer_eav_attribute` VALUES (1,1,NULL,0,NULL,1,10,NULL,1,1,1,0),(2,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(3,1,NULL,0,NULL,1,20,NULL,1,1,0,1),(4,0,NULL,0,NULL,0,30,NULL,0,0,0,0),(5,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,40,NULL,0,0,0,0),(6,0,NULL,0,NULL,0,50,NULL,0,0,0,0),(7,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,60,NULL,0,0,0,0),(8,0,NULL,0,NULL,0,70,NULL,0,0,0,0),(9,1,NULL,0,'a:1:{s:16:\"input_validation\";s:5:\"email\";}',1,80,NULL,1,1,1,1),(10,1,NULL,0,NULL,1,25,NULL,1,1,1,0),(11,0,'date',0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',0,90,NULL,1,1,1,0),(12,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(13,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(14,0,NULL,0,'a:1:{s:16:\"input_validation\";s:4:\"date\";}',1,0,NULL,0,0,0,0),(15,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(16,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(17,0,NULL,0,'a:1:{s:15:\"max_text_length\";i:255;}',0,100,NULL,1,1,0,1),(18,0,NULL,0,NULL,1,0,NULL,1,1,1,0),(19,0,NULL,0,NULL,0,0,NULL,1,1,1,0),(20,0,NULL,0,'a:0:{}',0,110,NULL,1,1,1,0),(21,1,NULL,0,NULL,1,28,NULL,0,0,0,0),(22,0,NULL,0,NULL,0,10,NULL,0,0,0,0),(23,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,20,NULL,1,0,0,1),(24,0,NULL,0,NULL,0,30,NULL,0,0,0,0),(25,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,40,NULL,1,0,0,1),(26,0,NULL,0,NULL,0,50,NULL,0,0,0,0),(27,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,60,NULL,1,0,0,1),(28,1,NULL,2,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,70,NULL,1,0,0,1),(29,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,80,NULL,1,0,0,1),(30,1,NULL,0,NULL,1,90,NULL,1,1,1,0),(31,1,NULL,0,NULL,1,100,NULL,1,1,0,1),(32,1,NULL,0,NULL,1,100,NULL,0,0,0,0),(33,1,NULL,0,'a:0:{}',1,110,'Magento\\Customer\\Model\\Attribute\\Data\\Postcode',1,1,1,1),(34,1,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',1,120,NULL,1,1,1,1),(35,0,NULL,0,'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',0,130,NULL,1,0,0,1),(36,1,NULL,0,NULL,1,140,NULL,0,0,0,0),(37,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(38,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(39,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(40,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(41,0,NULL,0,NULL,0,0,NULL,0,0,0,0),(42,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(43,0,NULL,0,NULL,1,0,NULL,0,0,0,0),(44,0,NULL,0,NULL,1,0,NULL,0,0,0,0);
/*!40000 ALTER TABLE `customer_eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_eav_attribute_website`
--

DROP TABLE IF EXISTS `customer_eav_attribute_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_eav_attribute_website` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `is_visible` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Visible',
  `is_required` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Required',
  `default_value` text COMMENT 'Default Value',
  `multiline_count` smallint(5) unsigned DEFAULT NULL COMMENT 'Multiline Count',
  PRIMARY KEY (`attribute_id`,`website_id`),
  KEY `CUSTOMER_EAV_ATTRIBUTE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `CSTR_EAV_ATTR_WS_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CSTR_EAV_ATTR_WS_WS_ID_STORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute Website';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_eav_attribute_website`
--

LOCK TABLES `customer_eav_attribute_website` WRITE;
/*!40000 ALTER TABLE `customer_eav_attribute_website` DISABLE KEYS */;
INSERT INTO `customer_eav_attribute_website` VALUES (1,1,NULL,NULL,NULL,NULL),(3,1,NULL,NULL,NULL,NULL),(9,1,NULL,NULL,NULL,NULL),(10,1,NULL,NULL,NULL,NULL),(11,1,NULL,NULL,NULL,NULL),(17,1,NULL,NULL,NULL,NULL),(18,1,NULL,NULL,NULL,NULL),(19,1,NULL,NULL,NULL,NULL),(20,1,NULL,NULL,NULL,NULL),(21,1,NULL,NULL,NULL,NULL),(23,1,NULL,NULL,NULL,NULL),(25,1,NULL,NULL,NULL,NULL),(27,1,NULL,NULL,NULL,NULL),(28,1,NULL,NULL,NULL,NULL),(29,1,NULL,NULL,NULL,NULL),(30,1,NULL,NULL,NULL,NULL),(31,1,NULL,NULL,NULL,NULL),(32,1,NULL,NULL,NULL,NULL),(33,1,NULL,NULL,NULL,NULL),(34,1,NULL,NULL,NULL,NULL),(35,1,NULL,NULL,NULL,NULL),(36,1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer_eav_attribute_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity`
--

DROP TABLE IF EXISTS `customer_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Website Id',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `disable_auto_group_change` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Disable automatic group change based on VAT ID',
  `created_in` varchar(255) DEFAULT NULL COMMENT 'Created From',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'First Name',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middle Name/Initial',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Last Name',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `dob` date DEFAULT NULL COMMENT 'Date of Birth',
  `password_hash` varchar(128) DEFAULT NULL COMMENT 'Password_hash',
  `rp_token` varchar(128) DEFAULT NULL COMMENT 'Reset password token',
  `rp_token_created_at` datetime DEFAULT NULL COMMENT 'Reset password token creation time',
  `default_billing` int(10) unsigned DEFAULT NULL COMMENT 'Default Billing Address',
  `default_shipping` int(10) unsigned DEFAULT NULL COMMENT 'Default Shipping Address',
  `taxvat` varchar(50) DEFAULT NULL COMMENT 'Tax/VAT Number',
  `confirmation` varchar(64) DEFAULT NULL COMMENT 'Is Confirmed',
  `gender` smallint(5) unsigned DEFAULT NULL COMMENT 'Gender',
  `failures_num` smallint(6) DEFAULT '0' COMMENT 'Failure Number',
  `first_failure` timestamp NULL DEFAULT NULL COMMENT 'First Failure',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Lock Expiration Date',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_EMAIL_WEBSITE_ID` (`email`,`website_id`),
  KEY `CUSTOMER_ENTITY_STORE_ID` (`store_id`),
  KEY `CUSTOMER_ENTITY_WEBSITE_ID` (`website_id`),
  KEY `CUSTOMER_ENTITY_FIRSTNAME` (`firstname`),
  KEY `CUSTOMER_ENTITY_LASTNAME` (`lastname`),
  CONSTRAINT `CUSTOMER_ENTITY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  CONSTRAINT `CUSTOMER_ENTITY_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Customer Entity';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_entity`
--

LOCK TABLES `customer_entity` WRITE;
/*!40000 ALTER TABLE `customer_entity` DISABLE KEYS */;
INSERT INTO `customer_entity` VALUES (1,1,'kdodeth@gmail.com',1,NULL,1,'2020-02-28 15:36:34','2020-03-03 05:41:48',1,0,'Default Store View',NULL,'customer',NULL,'.',NULL,NULL,'0e0a7c7aa7ed3ad89b2cf5d6f7cb19c199e65b0abd3c4816f6177a68d79e6d0b:otVuWLke7BSICgNvfRnFB3uc6CqHgWqN:1','96c9e22566a5fe08191b5ce2e5360a74','2020-03-03 05:41:48',NULL,NULL,NULL,NULL,NULL,1,'2020-03-03 05:39:24',NULL),(2,1,'dev@disandat.mx',1,NULL,1,'2020-02-28 19:06:48','2020-02-28 19:06:48',1,0,'CULTURA MEZCAL',NULL,'Juan',NULL,'Ortiz',NULL,NULL,'1d3d67a6312264990b28ab1b0362d5556a6f828ea6ab1dc23035d97cbc512280:HczZ5YLNVCTrNzqzvroDFlsht8HjiXwU:1','03810b6a414263e464e6e3b896b9234e','2020-02-28 19:06:48',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,'odette.pm@disandat.mx',1,NULL,1,'2020-03-04 20:20:51','2020-03-04 20:20:52',1,0,'CULTURA MEZCAL',NULL,'Odette',NULL,'prueba',NULL,NULL,'aa4b26b840e5f9f5a0a8a9230225f494c08ee5a2b41670c485b0c650d3f0d1c6:XQOSwVqziAy4esTQpqRECwt7UTy4MCSt:1','d2d2440cbbf308272d5aae233dae03be','2020-03-04 20:20:52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,'kdodeth@hotmail.com',1,NULL,1,'2020-03-11 23:54:31','2020-03-12 05:09:10',1,0,'CULTURA MEZCAL',NULL,'Cliente',NULL,'prueba',NULL,NULL,'7ae1df2da17f8c713ea7cf18039efd338dc147609f22c1096dbf35a6f06c1057:FvXUoK6m7qDGHVWts0VOe3HyttO76lSd:1','530b19d0e67d317190b6c34bf4f49801','2020-03-11 23:54:31',NULL,1,NULL,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `customer_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity_datetime`
--

DROP TABLE IF EXISTS `customer_entity_datetime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_DATETIME_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Datetime';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_entity_datetime`
--

LOCK TABLES `customer_entity_datetime` WRITE;
/*!40000 ALTER TABLE `customer_entity_datetime` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_entity_datetime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity_decimal`
--

DROP TABLE IF EXISTS `customer_entity_decimal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Decimal';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_entity_decimal`
--

LOCK TABLES `customer_entity_decimal` WRITE;
/*!40000 ALTER TABLE `customer_entity_decimal` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_entity_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity_int`
--

DROP TABLE IF EXISTS `customer_entity_int`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CUSTOMER_ENTITY_INT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_INT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Int';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_entity_int`
--

LOCK TABLES `customer_entity_int` WRITE;
/*!40000 ALTER TABLE `customer_entity_int` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity_text`
--

DROP TABLE IF EXISTS `customer_entity_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_TEXT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Text';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_entity_text`
--

LOCK TABLES `customer_entity_text` WRITE;
/*!40000 ALTER TABLE `customer_entity_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_entity_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_entity_varchar`
--

DROP TABLE IF EXISTS `customer_entity_varchar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Varchar';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_entity_varchar`
--

LOCK TABLES `customer_entity_varchar` WRITE;
/*!40000 ALTER TABLE `customer_entity_varchar` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_form_attribute`
--

DROP TABLE IF EXISTS `customer_form_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_form_attribute` (
  `form_code` varchar(32) NOT NULL COMMENT 'Form Code',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`form_code`,`attribute_id`),
  KEY `CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Form Attribute';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_form_attribute`
--

LOCK TABLES `customer_form_attribute` WRITE;
/*!40000 ALTER TABLE `customer_form_attribute` DISABLE KEYS */;
INSERT INTO `customer_form_attribute` VALUES ('adminhtml_customer',1),('adminhtml_customer',3),('adminhtml_customer',4),('customer_account_create',4),('customer_account_edit',4),('adminhtml_customer',5),('customer_account_create',5),('customer_account_edit',5),('adminhtml_customer',6),('customer_account_create',6),('customer_account_edit',6),('adminhtml_customer',7),('customer_account_create',7),('customer_account_edit',7),('adminhtml_customer',8),('customer_account_create',8),('customer_account_edit',8),('adminhtml_checkout',9),('adminhtml_customer',9),('customer_account_create',9),('customer_account_edit',9),('adminhtml_checkout',10),('adminhtml_customer',10),('adminhtml_checkout',11),('adminhtml_customer',11),('customer_account_create',11),('customer_account_edit',11),('adminhtml_checkout',17),('adminhtml_customer',17),('customer_account_create',17),('customer_account_edit',17),('adminhtml_customer',19),('customer_account_create',19),('customer_account_edit',19),('adminhtml_checkout',20),('adminhtml_customer',20),('customer_account_create',20),('customer_account_edit',20),('adminhtml_customer',21),('adminhtml_customer_address',22),('customer_address_edit',22),('customer_register_address',22),('adminhtml_customer_address',23),('customer_address_edit',23),('customer_register_address',23),('adminhtml_customer_address',24),('customer_address_edit',24),('customer_register_address',24),('adminhtml_customer_address',25),('customer_address_edit',25),('customer_register_address',25),('adminhtml_customer_address',26),('customer_address_edit',26),('customer_register_address',26),('adminhtml_customer_address',27),('customer_address_edit',27),('customer_register_address',27),('adminhtml_customer_address',28),('customer_address_edit',28),('customer_register_address',28),('adminhtml_customer_address',29),('customer_address_edit',29),('customer_register_address',29),('adminhtml_customer_address',30),('customer_address_edit',30),('customer_register_address',30),('adminhtml_customer_address',31),('customer_address_edit',31),('customer_register_address',31),('adminhtml_customer_address',32),('customer_address_edit',32),('customer_register_address',32),('adminhtml_customer_address',33),('customer_address_edit',33),('customer_register_address',33),('adminhtml_customer_address',34),('customer_address_edit',34),('customer_register_address',34),('adminhtml_customer_address',35),('customer_address_edit',35),('customer_register_address',35),('adminhtml_customer_address',36),('customer_address_edit',36),('customer_register_address',36);
/*!40000 ALTER TABLE `customer_form_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_grid_flat`
--

DROP TABLE IF EXISTS `customer_grid_flat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_grid_flat` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `name` text COMMENT 'Name',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` int(11) DEFAULT NULL COMMENT 'Group_id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created_at',
  `website_id` int(11) DEFAULT NULL COMMENT 'Website_id',
  `confirmation` varchar(255) DEFAULT NULL COMMENT 'Confirmation',
  `created_in` text COMMENT 'Created_in',
  `dob` date DEFAULT NULL COMMENT 'Dob',
  `gender` int(11) DEFAULT NULL COMMENT 'Gender',
  `taxvat` varchar(255) DEFAULT NULL COMMENT 'Taxvat',
  `lock_expires` timestamp NULL DEFAULT NULL COMMENT 'Lock_expires',
  `shipping_full` text COMMENT 'Shipping_full',
  `billing_full` text COMMENT 'Billing_full',
  `billing_firstname` varchar(255) DEFAULT NULL COMMENT 'Billing_firstname',
  `billing_lastname` varchar(255) DEFAULT NULL COMMENT 'Billing_lastname',
  `billing_telephone` varchar(255) DEFAULT NULL COMMENT 'Billing_telephone',
  `billing_postcode` varchar(255) DEFAULT NULL COMMENT 'Billing_postcode',
  `billing_country_id` varchar(255) DEFAULT NULL COMMENT 'Billing_country_id',
  `billing_region` varchar(255) DEFAULT NULL COMMENT 'Billing_region',
  `billing_street` varchar(255) DEFAULT NULL COMMENT 'Billing_street',
  `billing_city` varchar(255) DEFAULT NULL COMMENT 'Billing_city',
  `billing_fax` varchar(255) DEFAULT NULL COMMENT 'Billing_fax',
  `billing_vat_id` varchar(255) DEFAULT NULL COMMENT 'Billing_vat_id',
  `billing_company` varchar(255) DEFAULT NULL COMMENT 'Billing_company',
  PRIMARY KEY (`entity_id`),
  KEY `CUSTOMER_GRID_FLAT_GROUP_ID` (`group_id`),
  KEY `CUSTOMER_GRID_FLAT_CREATED_AT` (`created_at`),
  KEY `CUSTOMER_GRID_FLAT_WEBSITE_ID` (`website_id`),
  KEY `CUSTOMER_GRID_FLAT_CONFIRMATION` (`confirmation`),
  KEY `CUSTOMER_GRID_FLAT_DOB` (`dob`),
  KEY `CUSTOMER_GRID_FLAT_GENDER` (`gender`),
  KEY `CUSTOMER_GRID_FLAT_BILLING_COUNTRY_ID` (`billing_country_id`),
  FULLTEXT KEY `FTI_8746F705702DD5F6D45B8C7CE7FE9F2F` (`name`,`email`,`created_in`,`taxvat`,`shipping_full`,`billing_full`,`billing_firstname`,`billing_lastname`,`billing_telephone`,`billing_postcode`,`billing_region`,`billing_city`,`billing_fax`,`billing_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='customer_grid_flat';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_grid_flat`
--

LOCK TABLES `customer_grid_flat` WRITE;
/*!40000 ALTER TABLE `customer_grid_flat` DISABLE KEYS */;
INSERT INTO `customer_grid_flat` VALUES (1,'customer .','kdodeth@gmail.com',1,'2020-02-28 15:36:34',1,NULL,'Default Store View',NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Juan Ortiz','dev@disandat.mx',1,'2020-02-28 19:06:48',1,NULL,'CULTURA MEZCAL',NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Odette prueba','odette.pm@disandat.mx',1,'2020-03-04 20:20:51',1,NULL,'CULTURA MEZCAL',NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Cliente prueba','kdodeth@hotmail.com',1,'2020-03-11 23:54:31',1,NULL,'CULTURA MEZCAL',NULL,NULL,NULL,NULL,'asdasd sdfdsf Federated States Of Micronesia 45644','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer_grid_flat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_group`
--

DROP TABLE IF EXISTS `customer_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_group` (
  `customer_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Customer Group Id',
  `customer_group_code` varchar(32) NOT NULL COMMENT 'Customer Group Code',
  `tax_class_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Tax Class Id',
  PRIMARY KEY (`customer_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Customer Group';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_group`
--

LOCK TABLES `customer_group` WRITE;
/*!40000 ALTER TABLE `customer_group` DISABLE KEYS */;
INSERT INTO `customer_group` VALUES (0,'NOT LOGGED IN',3),(1,'General',3),(2,'Wholesale',3),(3,'Retailer',3);
/*!40000 ALTER TABLE `customer_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_log`
--

DROP TABLE IF EXISTS `customer_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `customer_id` int(11) NOT NULL COMMENT 'Customer ID',
  `last_login_at` timestamp NULL DEFAULT NULL COMMENT 'Last Login Time',
  `last_logout_at` timestamp NULL DEFAULT NULL COMMENT 'Last Logout Time',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `CUSTOMER_LOG_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Customer Log Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_log`
--

LOCK TABLES `customer_log` WRITE;
/*!40000 ALTER TABLE `customer_log` DISABLE KEYS */;
INSERT INTO `customer_log` VALUES (1,1,'2020-02-28 15:36:35',NULL),(2,2,'2020-02-28 19:06:48',NULL),(3,3,'2020-03-04 20:20:52',NULL),(4,4,'2020-03-12 04:49:44',NULL);
/*!40000 ALTER TABLE `customer_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_visitor`
--

DROP TABLE IF EXISTS `customer_visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_visitor` (
  `visitor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Visitor ID',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `session_id` varchar(64) DEFAULT NULL COMMENT 'Session ID',
  `last_visit_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last Visit Time',
  PRIMARY KEY (`visitor_id`),
  KEY `CUSTOMER_VISITOR_CUSTOMER_ID` (`customer_id`),
  KEY `CUSTOMER_VISITOR_LAST_VISIT_AT` (`last_visit_at`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8 COMMENT='Visitor Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_visitor`
--

LOCK TABLES `customer_visitor` WRITE;
/*!40000 ALTER TABLE `customer_visitor` DISABLE KEYS */;
INSERT INTO `customer_visitor` VALUES (1,1,'rqi6r8ieoemrnvgqmvms4hihg0','2020-02-28 15:36:40'),(2,NULL,'42uc72aro7cg2dd7u03g9c0bi4','2020-02-28 16:17:18'),(3,NULL,'rqi6r8ieoemrnvgqmvms4hihg0','2020-02-28 16:40:23'),(4,NULL,'rqi6r8ieoemrnvgqmvms4hihg0','2020-02-28 19:16:30'),(5,2,'fmnqq7vghstl966osi2eubclf4','2020-02-28 19:07:03'),(6,NULL,'fg748bmfujr3ge69ig8gj48pf7','2020-02-28 20:22:52'),(7,NULL,'rqi6r8ieoemrnvgqmvms4hihg0','2020-02-28 21:19:09'),(8,NULL,'33iugaiag8tgjt49eb59vrghn2','2020-02-28 20:34:30'),(9,NULL,'gcc51e6olslacsifchq8mbqdc7','2020-02-28 23:07:15'),(10,NULL,'fpver4so1uec6s59ikven6l0v3','2020-03-02 15:34:32'),(11,NULL,'aut0l9kqbedfmcngtq0uv85sn4','2020-03-02 17:43:53'),(12,NULL,'ta6png1309764nsc8gfdba1bl7','2020-03-02 18:32:34'),(13,NULL,'mf62k8j490o7hptqgaq2j39f85','2020-03-02 18:32:50'),(14,NULL,'spl1hqsjoiomb9jql7rtaj1t36','2020-03-02 18:32:51'),(15,NULL,'o0botsb8pjf9pkse9unv1lo057','2020-03-02 18:32:52'),(16,NULL,'o0botsb8pjf9pkse9unv1lo057','2020-03-02 19:31:46'),(17,NULL,'lucu91lcinku3j2r3pd4a8tmf2','2020-03-02 19:37:29'),(18,NULL,'c8nqdislhm48jd2chlql2tqd06','2020-03-02 19:35:03'),(19,NULL,'l11qrv17at19qrpooa8oaql2d2','2020-03-02 21:05:31'),(20,NULL,'cka70jn50shcu5am9jo4lsugb1','2020-03-02 22:31:07'),(21,NULL,'uac65j2jdnmplrle5qrjcc4r70','2020-03-02 22:51:31'),(22,NULL,'bmfbpgdpv1eqv6lpmk7vp73gi3','2020-03-02 22:47:54'),(23,NULL,'ie6rmrdb1c0l2vqpcbohbddac2','2020-03-02 22:48:04'),(24,NULL,'ugp6al18hirmavlh2r085fnho0','2020-03-02 22:50:35'),(25,NULL,'duca1752ead0bh5u3v8uikcd31','2020-03-02 22:50:48'),(26,NULL,'h2kpd48v8ab0jtn2csn0edgqp4','2020-03-02 22:53:22'),(27,NULL,'6s9v40b9dei1ch4u49cj84p5h6','2020-03-02 22:53:33'),(28,NULL,'ol1s2of6uib4igfaf17maq0931','2020-03-02 22:56:26'),(29,NULL,'s9hu93t7987brakglssd9hl4u7','2020-03-02 22:56:59'),(30,NULL,'1cjnlhq8nmmvrt2mpa58ncjf07','2020-03-02 22:57:08'),(31,NULL,'817l49oprebicr39spepkg8mt2','2020-03-02 22:57:44'),(32,NULL,'eo0r3kktsttcsectefmfpdp5n1','2020-03-02 23:01:24'),(33,NULL,'26qe33kda0ij7dnlfnodc9kma6','2020-03-02 23:01:33'),(34,NULL,'b4eghfpsulpes56q19lsrfn6f6','2020-03-02 23:04:30'),(35,NULL,'kg7ptpr16rqldnec010t96h4m7','2020-03-02 23:04:39'),(36,NULL,'ffoj40le3u3rq5uasr69tqq6c1','2020-03-02 23:09:15'),(37,NULL,'i2lqrqle2b3fjrqqqa6otl3l72','2020-03-02 23:09:25'),(38,NULL,'3cb3beht2uf3objme7ikimhsg0','2020-03-02 23:09:25'),(39,NULL,'l3s8cvph4t5qsm4b10iujkic43','2020-03-03 00:05:42'),(40,NULL,'bv65bukf1cihct4glqi1u22sa6','2020-03-03 01:31:50'),(41,NULL,'pq3ku422dc9v0otsqaa4b688k7','2020-03-03 01:34:45'),(42,NULL,'pq3ku422dc9v0otsqaa4b688k7','2020-03-03 02:28:40'),(43,NULL,'a2ji3m7jg6tvai1ur5chbvgn24','2020-03-03 06:03:43'),(44,NULL,'2ahb890js02u4bi8utj98uui56','2020-03-03 06:04:15'),(45,NULL,'9vinailjum0emt00ak9abmouk6','2020-03-03 06:04:25'),(46,NULL,'l5gcjitgs7a233ms3bndljrk95','2020-03-03 06:04:26'),(47,NULL,'rah2idjsa8mf82lqe2idtudlm7','2020-03-03 07:02:53'),(48,NULL,'1moitjmeuriegdghseet7ri0s7','2020-03-03 06:47:00'),(49,NULL,'4553flq8anmkee22u7ss6h8ao2','2020-03-03 16:04:51'),(50,NULL,'or6kvists99s671gflp89mn2b0','2020-03-03 16:10:20'),(51,NULL,'4553flq8anmkee22u7ss6h8ao2','2020-03-03 18:42:14'),(52,NULL,'1thm7od6mnhemla8ds8sfu8o90','2020-03-03 18:46:28'),(53,NULL,'4553flq8anmkee22u7ss6h8ao2','2020-03-03 19:09:33'),(54,NULL,'4553flq8anmkee22u7ss6h8ao2','2020-03-03 21:13:59'),(55,NULL,'4553flq8anmkee22u7ss6h8ao2','2020-03-03 22:37:35'),(56,NULL,'4553flq8anmkee22u7ss6h8ao2','2020-03-04 05:07:40'),(57,NULL,'rabc1a4lp0vkknde8njj05v3f7','2020-03-04 15:17:16'),(58,NULL,'c8s9fcaf95no3pjr0949eksai1','2020-03-04 17:50:48'),(59,NULL,'7eeejgfugn09j6pvgifj6ci0h6','2020-03-04 17:51:07'),(60,NULL,'9aehm3n9t09ivpqenc5ka5nnj0','2020-03-04 18:36:13'),(61,NULL,'ioumc8qa27cc8elb74t6bo84p1','2020-03-04 18:36:19'),(62,NULL,'uir3ii7112977hehgpj4fpt0b3','2020-03-04 18:36:25'),(63,NULL,'uir3ii7112977hehgpj4fpt0b3','2020-03-04 19:33:30'),(64,NULL,'h47ku4q8m9030q2kefe3ivsua6','2020-03-04 19:44:58'),(65,NULL,'mc7t1db5q94te4ot5ee3r3mcp0','2020-03-04 19:45:03'),(66,3,'lc6h74undhiga0t22antm33p50','2020-03-04 20:21:43'),(67,NULL,'0ggik9qacb1f3iefhurpt3slj3','2020-03-04 22:13:41'),(68,NULL,'vg44sdsd52ebvpq75ej8rq18k3','2020-03-04 22:13:41'),(69,NULL,'feaiudpnht0q47hhoaf9os2u73','2020-03-05 04:37:59'),(70,NULL,'tq0saddj94rt2ef4e026lgrct1','2020-03-05 15:27:27'),(71,NULL,'o1jvg9pbi2kh41u8pctd1e5gb0','2020-03-05 16:27:04'),(72,NULL,'muunnfafkhalndv2hm469uljv1','2020-03-05 17:14:26'),(73,NULL,'muunnfafkhalndv2hm469uljv1','2020-03-05 17:45:04'),(74,NULL,'g33p1bfra381euju763ka01tl4','2020-03-05 17:46:34'),(75,NULL,'l5uojm5ub7etcdi0fkoh2ltr37','2020-03-05 17:46:44'),(76,NULL,'tju723shovr573inojdiib8tk5','2020-03-05 18:40:09'),(77,NULL,'964sv92oc6oqq8nn45154phu03','2020-03-05 18:41:33'),(78,NULL,'rqk6r6ft4mps1vh6m2vca1aak2','2020-03-05 21:04:09'),(79,NULL,'oobge3neunraedhmfp4p0ir1d2','2020-03-05 22:12:46'),(80,NULL,'42r1dnmbllfomtpr9em94fl8p6','2020-03-05 23:26:39'),(81,NULL,'djdl9u32bim8s1hqtaqqoo8ba0','2020-03-06 00:06:07'),(82,NULL,'rbjuoafq3oj0dqtu0in7ino3b3','2020-03-06 00:02:56'),(83,NULL,'0kuicckvig5m09tul0qppf72g2','2020-03-06 00:02:47'),(84,NULL,'7fkhhe85fbb94kd8pq1434ksh4','2020-03-06 00:02:55'),(85,NULL,'cn2jatkpd7seh6ut4l3sti2nu0','2020-03-06 00:02:55'),(86,NULL,'8h9jr11siq8bv07i5d4gh549d6','2020-03-06 06:03:55'),(87,NULL,'67it6389da29td8o8fv35dm1i5','2020-03-06 06:42:31'),(88,NULL,'d738kqttt5fv00ekbdimm4fmd0','2020-03-06 16:54:51'),(89,NULL,'58hvik2nt67rapkrnhstfegco7','2020-03-06 17:58:36'),(90,NULL,'b934tfa0uub8a8k9cvffipjvh4','2020-03-06 17:57:12'),(91,NULL,'9o6lj8va86hnuqp7odd9slgbn7','2020-03-06 18:57:51'),(92,NULL,'vhlti2nu4vhdrf457h390en635','2020-03-06 19:08:21'),(93,NULL,'eq5h38q4kqma5r5asvac8e6e90','2020-03-06 19:08:31'),(94,NULL,'2nsdhttuiro39updc7q7jah4p3','2020-03-06 19:08:56'),(95,NULL,'7c5893qnu2josl1tp085gbl5c4','2020-03-06 19:08:35'),(96,NULL,'q5u8pa106ek12ksv7kb9sqhf03','2020-03-09 19:50:28'),(97,NULL,'le01qep5058soa7pbfoud0vk64','2020-03-09 18:20:45'),(98,NULL,'1eqju2su3v20l7ec0qtuq9cu35','2020-03-09 18:25:13'),(99,NULL,'qljkv8jom3rvv7nlgs3apkac00','2020-03-09 20:06:22'),(100,NULL,'vng2rvkkn06ud84rd8pakkjpj6','2020-03-09 21:36:58'),(101,NULL,'h5qau5tubdh1052ht0alauj740','2020-03-09 22:07:37'),(102,NULL,'q5u8pa106ek12ksv7kb9sqhf03','2020-03-09 22:51:28'),(103,NULL,'buc62pi24ttb6n04fpq27gg8o5','2020-03-09 22:40:32'),(104,NULL,'q5u8pa106ek12ksv7kb9sqhf03','2020-03-10 00:51:37'),(105,NULL,'g8neni32hbonfrldmmlr4gkk90','2020-03-10 00:36:20'),(106,NULL,'augs34o500ppik420vqp8dufl1','2020-03-10 00:36:29'),(107,NULL,'mtg4bpapv0k8mpbc9c71q75jm6','2020-03-10 00:38:17'),(108,NULL,'phqrbdfhnicp3bbe1o1i0uvtn6','2020-03-10 00:39:05'),(109,NULL,'09p2gvublanu8sble9pm46qvl0','2020-03-10 01:13:46'),(110,NULL,'0514so7k6hdpldklk4nesiq691','2020-03-10 01:14:06'),(111,NULL,'q5u8pa106ek12ksv7kb9sqhf03','2020-03-10 04:18:53'),(112,NULL,'agldqa6lliivitl4273d29d7j2','2020-03-11 17:02:49'),(113,NULL,'rbphmrhi3t9ge8qp54732uccu6','2020-03-11 19:07:37'),(114,NULL,'ii034orloti6shgq14586oleq1','2020-03-11 20:40:02'),(115,NULL,'ptro9hvimb92m4jqrhevqh0b15','2020-03-11 20:40:07'),(116,NULL,'cdllb9h7un9gp5tuh627rhlgo7','2020-03-11 20:40:07'),(117,NULL,'vn7nc8k3clc0f04uuo6mibh3l5','2020-03-11 21:07:48'),(118,NULL,'24gr7969pjua6anjg30o41hnp0','2020-03-11 20:40:08'),(119,NULL,'suqjb94ofk8d2ier7ju88jntm6','2020-03-11 21:49:21'),(120,NULL,'vma9diu8qctvaq1f5cbau1ta50','2020-03-11 22:47:25'),(121,NULL,'17qn9ldlq001ante44616i4um2','2020-03-11 22:49:25'),(122,NULL,'91ejcjca779u8e8mu9ro8m6ji7','2020-03-11 22:49:40'),(123,NULL,'ckhdug8eaa7c5khgkep89ak847','2020-03-11 23:02:13'),(124,NULL,'808aoe4aukj9a5pppnsf5qbb32','2020-03-11 22:49:40'),(125,NULL,'i80csks7fanpdlitul8b72bcc6','2020-03-11 23:53:03'),(126,NULL,'hclscs939dm94047s3aqn02na3','2020-03-11 23:53:09'),(127,NULL,'trqdgjsjnul0ipu97i7m4e6jg7','2020-03-11 23:53:10'),(128,4,'pi2r6e5mm6s4hs3isp7nqnnmn5','2020-03-11 23:55:44'),(129,NULL,'j5f8481gclipk1mto8g54qct16','2020-03-12 03:51:00'),(130,NULL,'e2i1n8ppu7j4moonfm0m1g1u46','2020-03-12 03:55:20'),(131,NULL,'cd8f3is1jaladfg92anvt7qa21','2020-03-12 03:55:20'),(132,NULL,'1klvitk3rh7deg2bf33u0r9e54','2020-03-12 03:55:27'),(133,NULL,'qngvm3ft0vllsv356o4pvjrje1','2020-03-12 03:55:28'),(134,4,'h4v5sbo15718cqg39eji0isan3','2020-03-12 05:09:14'),(135,NULL,'5rf78b1r79v1e464erllhpbim5','2020-03-12 03:55:28'),(136,NULL,'ivq9tn09ptf1fsu409586hlmo4','2020-03-12 15:48:54'),(137,NULL,'3rh4mc3grbbdjsmiogb98gkqs3','2020-03-12 16:35:47'),(138,NULL,'f3865u710qppcigj84belevog5','2020-03-12 20:23:02'),(139,NULL,'db424018u4lga4au2koj7gl023','2020-03-12 21:10:16'),(140,NULL,'0t6kfq0bmvu43596ouic9192m4','2020-03-12 21:10:16'),(141,NULL,'0c6ahjik3o4mk2pjoiv33ohmj6','2020-03-12 21:57:51'),(142,NULL,'4gd8q1s438on3vmh2vlo7bhp13','2020-03-12 21:58:01'),(143,NULL,'elhssfqm5alcgoamij3ngeuhg5','2020-03-12 22:54:11'),(144,NULL,'tjft0tmf6eecbv4vmv6obs5kd5','2020-03-12 21:58:03'),(145,NULL,'gr7qrage4nkrnjur1kstju4ej3','2020-03-12 23:43:45'),(146,NULL,'bnr88cru0ukqoicb78jiuuefd7','2020-03-12 23:09:42'),(147,NULL,'5o786o35n2didtsr2pndupdo73','2020-03-12 23:09:53'),(148,NULL,'ent85hha75kgjd2j37qg2o0be0','2020-03-12 23:09:55'),(149,NULL,'04vhfhmt0743sbrf7umu75ode3','2020-03-13 00:00:52'),(150,NULL,'v5alaj4ai91htbnanfkn1h2jb2','2020-03-13 00:06:18'),(151,NULL,'bmu9pktncbhsapa2sf2j59k0e3','2020-03-13 00:06:25'),(152,NULL,'rbpq9re04qmj4k1k2tru004uc4','2020-03-13 00:06:34'),(153,NULL,'947j5d4lncio7c4sn30scakcd5','2020-03-13 02:01:23'),(154,NULL,'r942ftd3t1kfcb276tmft29ts1','2020-03-13 15:14:06'),(155,NULL,'020dontc54r1bnbnkv7c4eqiu6','2020-03-13 16:30:27'),(156,NULL,'8nfstfdpnugn5abvmocu17cfl2','2020-03-13 16:40:12'),(157,NULL,'tq7mv8vgtobh2lfpruk4u3qv52','2020-03-13 18:25:20'),(158,NULL,'edfua84gb9o0r29rme7be8j0q3','2020-03-13 18:30:28'),(159,NULL,'oob9tu9evjk45ood8de9nj8nl0','2020-03-13 18:31:05'),(160,NULL,'c56iaaqq7jssur7f91gutdjn47','2020-03-13 18:35:15'),(161,NULL,'r942ftd3t1kfcb276tmft29ts1','2020-03-13 18:33:03'),(162,NULL,'7uhb2qg7po4fa1fcc0qfu0rfd1','2020-03-13 20:18:48'),(163,NULL,'rbjet62lfkqam82dls4m27e8e6','2020-03-13 20:19:03'),(164,NULL,'80so9j5i29nkunn6jhnp6ep5a7','2020-03-13 20:21:38'),(165,NULL,'vgpef5vb8r4bcv9b9si0861gu1','2020-03-13 21:17:44'),(166,NULL,'bsr396nt3df6sojq22f9fk0ia5','2020-03-13 21:29:32'),(167,NULL,'5qoo2u8aujhjt8epo7ge7krl70','2020-03-13 21:29:53'),(168,NULL,'j3vbh5c13j2eje76geh3lt0nl3','2020-03-13 23:54:27'),(169,NULL,'h5mu4dchcp2fktvovfrlk9sso0','2020-03-16 21:08:11'),(170,NULL,'gq6bif4fub07h9qcit74ge2sf3','2020-03-16 21:08:46'),(171,NULL,'rq6d5qqsusj0firbrof8udnfv7','2020-03-16 21:08:48'),(172,NULL,'j35d0hlsuu7ang155a299cmrf5','2020-03-16 21:08:48'),(173,NULL,'4cq1t3ku5u871manhbvkad8007','2020-03-17 17:10:16'),(174,NULL,'ae4k38t59i1r56g105g9lof4v6','2020-03-17 18:49:51'),(175,NULL,'sjafq2sbgbu0b3oioda83hia02','2020-03-17 18:27:07'),(176,NULL,'gdjna2nojqj3f40kuc32t78k65','2020-03-17 19:21:00'),(177,NULL,'9f420hmiqf5lberg1p6q0sh091','2020-03-17 19:21:08'),(178,NULL,'ttjfg6ovgtvs5vfube40178si3','2020-03-17 19:21:09'),(179,NULL,'822o1u09bt6ujcpd9ot7sparm4','2020-03-17 20:10:56'),(180,NULL,'dn39gskeb8j4er8trpud4g6fc7','2020-03-17 19:42:57'),(181,NULL,'35lb1qtgipnrgqucdr3kpj4as4','2020-03-17 19:53:00'),(182,NULL,'4dmhbhjkbjhom1uvj4uq2sr4m2','2020-03-17 19:54:22'),(183,NULL,'kjoptahfa4u56hiotgtj509m62','2020-03-17 20:35:23'),(184,NULL,'rrgl2giu7m32se1vqb7jmq6q97','2020-03-17 20:35:23'),(185,NULL,'4dmhbhjkbjhom1uvj4uq2sr4m2','2020-03-17 20:41:13'),(186,NULL,'4dmhbhjkbjhom1uvj4uq2sr4m2','2020-03-17 23:55:00'),(187,NULL,'shta0olko024nrk9d171uim304','2020-03-17 23:55:08'),(188,NULL,'4dmhbhjkbjhom1uvj4uq2sr4m2','2020-03-18 01:52:36'),(189,NULL,'bhn2h7kpfgsao02i87u63cl2i7','2020-03-18 04:29:16'),(190,NULL,'ot3be4vp63aqrb94ha4a1u1v82','2020-03-18 03:39:45'),(191,NULL,'canl3adt02j7pcbtpolcsn34f6','2020-03-18 03:39:45'),(192,NULL,'fdv7vqqm9l88nja236l310tl72','2020-03-18 05:32:15'),(193,NULL,'9huhpe6a53tfm2h3l6rdu5vsm4','2020-03-18 16:38:58'),(194,NULL,'p37cc4p4vccimmnl43kg6plnf5','2020-03-18 16:39:51'),(195,NULL,'msoirka5epq2rm08q7juf3uf32','2020-03-18 17:33:45'),(196,NULL,'qi3ngqd70odlhtivkvu0ci66b5','2020-03-18 17:34:30'),(197,NULL,'shhqfrta4t5cbf272tan5tou46','2020-03-18 22:35:44'),(198,NULL,'c24cqp435c37jlibro8ajk99t4','2020-03-18 22:35:37'),(199,NULL,'ftutkhicc3qb0ulqumtdbeurv1','2020-03-18 22:37:43'),(200,NULL,'jta7ehjpa1ep8g5lav9e6a2p80','2020-03-19 16:26:22'),(201,NULL,'ead0a15n9m1et1ob5ru3b6rf13','2020-03-24 17:54:11'),(202,NULL,'4t6m2r68rqtsck1sciccb6mub6','2020-03-24 22:03:08'),(203,NULL,'qh7n97fu795ojb28j8543rqbf4','2020-03-26 17:41:09');
/*!40000 ALTER TABLE `customer_visitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design_change`
--

DROP TABLE IF EXISTS `design_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design_change` (
  `design_change_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Design Change Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `design` varchar(255) DEFAULT NULL COMMENT 'Design',
  `date_from` date DEFAULT NULL COMMENT 'First Date of Design Activity',
  `date_to` date DEFAULT NULL COMMENT 'Last Date of Design Activity',
  PRIMARY KEY (`design_change_id`),
  KEY `DESIGN_CHANGE_STORE_ID` (`store_id`),
  CONSTRAINT `DESIGN_CHANGE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Design Changes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `design_change`
--

LOCK TABLES `design_change` WRITE;
/*!40000 ALTER TABLE `design_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `design_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design_config_grid_flat`
--

DROP TABLE IF EXISTS `design_config_grid_flat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design_config_grid_flat` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `store_website_id` int(11) DEFAULT NULL COMMENT 'Store_website_id',
  `store_group_id` int(11) DEFAULT NULL COMMENT 'Store_group_id',
  `store_id` int(11) DEFAULT NULL COMMENT 'Store_id',
  `theme_theme_id` varchar(255) DEFAULT NULL COMMENT 'Theme_theme_id',
  PRIMARY KEY (`entity_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_WEBSITE_ID` (`store_website_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_GROUP_ID` (`store_group_id`),
  KEY `DESIGN_CONFIG_GRID_FLAT_STORE_ID` (`store_id`),
  FULLTEXT KEY `DESIGN_CONFIG_GRID_FLAT_THEME_THEME_ID` (`theme_theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='design_config_grid_flat';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `design_config_grid_flat`
--

LOCK TABLES `design_config_grid_flat` WRITE;
/*!40000 ALTER TABLE `design_config_grid_flat` DISABLE KEYS */;
INSERT INTO `design_config_grid_flat` VALUES (0,NULL,NULL,NULL,''),(1,1,NULL,NULL,''),(2,1,1,1,'6');
/*!40000 ALTER TABLE `design_config_grid_flat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country`
--

DROP TABLE IF EXISTS `directory_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_country` (
  `country_id` varchar(2) NOT NULL COMMENT 'Country Id in ISO-2',
  `iso2_code` varchar(2) DEFAULT NULL COMMENT 'Country ISO-2 format',
  `iso3_code` varchar(3) DEFAULT NULL COMMENT 'Country ISO-3',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_country`
--

LOCK TABLES `directory_country` WRITE;
/*!40000 ALTER TABLE `directory_country` DISABLE KEYS */;
INSERT INTO `directory_country` VALUES ('AD','AD','AND'),('AE','AE','ARE'),('AF','AF','AFG'),('AG','AG','ATG'),('AI','AI','AIA'),('AL','AL','ALB'),('AM','AM','ARM'),('AN','AN','ANT'),('AO','AO','AGO'),('AQ','AQ','ATA'),('AR','AR','ARG'),('AS','AS','ASM'),('AT','AT','AUT'),('AU','AU','AUS'),('AW','AW','ABW'),('AX','AX','ALA'),('AZ','AZ','AZE'),('BA','BA','BIH'),('BB','BB','BRB'),('BD','BD','BGD'),('BE','BE','BEL'),('BF','BF','BFA'),('BG','BG','BGR'),('BH','BH','BHR'),('BI','BI','BDI'),('BJ','BJ','BEN'),('BL','BL','BLM'),('BM','BM','BMU'),('BN','BN','BRN'),('BO','BO','BOL'),('BR','BR','BRA'),('BS','BS','BHS'),('BT','BT','BTN'),('BV','BV','BVT'),('BW','BW','BWA'),('BY','BY','BLR'),('BZ','BZ','BLZ'),('CA','CA','CAN'),('CC','CC','CCK'),('CD','CD','COD'),('CF','CF','CAF'),('CG','CG','COG'),('CH','CH','CHE'),('CI','CI','CIV'),('CK','CK','COK'),('CL','CL','CHL'),('CM','CM','CMR'),('CN','CN','CHN'),('CO','CO','COL'),('CR','CR','CRI'),('CU','CU','CUB'),('CV','CV','CPV'),('CX','CX','CXR'),('CY','CY','CYP'),('CZ','CZ','CZE'),('DE','DE','DEU'),('DJ','DJ','DJI'),('DK','DK','DNK'),('DM','DM','DMA'),('DO','DO','DOM'),('DZ','DZ','DZA'),('EC','EC','ECU'),('EE','EE','EST'),('EG','EG','EGY'),('EH','EH','ESH'),('ER','ER','ERI'),('ES','ES','ESP'),('ET','ET','ETH'),('FI','FI','FIN'),('FJ','FJ','FJI'),('FK','FK','FLK'),('FM','FM','FSM'),('FO','FO','FRO'),('FR','FR','FRA'),('GA','GA','GAB'),('GB','GB','GBR'),('GD','GD','GRD'),('GE','GE','GEO'),('GF','GF','GUF'),('GG','GG','GGY'),('GH','GH','GHA'),('GI','GI','GIB'),('GL','GL','GRL'),('GM','GM','GMB'),('GN','GN','GIN'),('GP','GP','GLP'),('GQ','GQ','GNQ'),('GR','GR','GRC'),('GS','GS','SGS'),('GT','GT','GTM'),('GU','GU','GUM'),('GW','GW','GNB'),('GY','GY','GUY'),('HK','HK','HKG'),('HM','HM','HMD'),('HN','HN','HND'),('HR','HR','HRV'),('HT','HT','HTI'),('HU','HU','HUN'),('ID','ID','IDN'),('IE','IE','IRL'),('IL','IL','ISR'),('IM','IM','IMN'),('IN','IN','IND'),('IO','IO','IOT'),('IQ','IQ','IRQ'),('IR','IR','IRN'),('IS','IS','ISL'),('IT','IT','ITA'),('JE','JE','JEY'),('JM','JM','JAM'),('JO','JO','JOR'),('JP','JP','JPN'),('KE','KE','KEN'),('KG','KG','KGZ'),('KH','KH','KHM'),('KI','KI','KIR'),('KM','KM','COM'),('KN','KN','KNA'),('KP','KP','PRK'),('KR','KR','KOR'),('KW','KW','KWT'),('KY','KY','CYM'),('KZ','KZ','KAZ'),('LA','LA','LAO'),('LB','LB','LBN'),('LC','LC','LCA'),('LI','LI','LIE'),('LK','LK','LKA'),('LR','LR','LBR'),('LS','LS','LSO'),('LT','LT','LTU'),('LU','LU','LUX'),('LV','LV','LVA'),('LY','LY','LBY'),('MA','MA','MAR'),('MC','MC','MCO'),('MD','MD','MDA'),('ME','ME','MNE'),('MF','MF','MAF'),('MG','MG','MDG'),('MH','MH','MHL'),('MK','MK','MKD'),('ML','ML','MLI'),('MM','MM','MMR'),('MN','MN','MNG'),('MO','MO','MAC'),('MP','MP','MNP'),('MQ','MQ','MTQ'),('MR','MR','MRT'),('MS','MS','MSR'),('MT','MT','MLT'),('MU','MU','MUS'),('MV','MV','MDV'),('MW','MW','MWI'),('MX','MX','MEX'),('MY','MY','MYS'),('MZ','MZ','MOZ'),('NA','NA','NAM'),('NC','NC','NCL'),('NE','NE','NER'),('NF','NF','NFK'),('NG','NG','NGA'),('NI','NI','NIC'),('NL','NL','NLD'),('NO','NO','NOR'),('NP','NP','NPL'),('NR','NR','NRU'),('NU','NU','NIU'),('NZ','NZ','NZL'),('OM','OM','OMN'),('PA','PA','PAN'),('PE','PE','PER'),('PF','PF','PYF'),('PG','PG','PNG'),('PH','PH','PHL'),('PK','PK','PAK'),('PL','PL','POL'),('PM','PM','SPM'),('PN','PN','PCN'),('PS','PS','PSE'),('PT','PT','PRT'),('PW','PW','PLW'),('PY','PY','PRY'),('QA','QA','QAT'),('RE','RE','REU'),('RO','RO','ROU'),('RS','RS','SRB'),('RU','RU','RUS'),('RW','RW','RWA'),('SA','SA','SAU'),('SB','SB','SLB'),('SC','SC','SYC'),('SD','SD','SDN'),('SE','SE','SWE'),('SG','SG','SGP'),('SH','SH','SHN'),('SI','SI','SVN'),('SJ','SJ','SJM'),('SK','SK','SVK'),('SL','SL','SLE'),('SM','SM','SMR'),('SN','SN','SEN'),('SO','SO','SOM'),('SR','SR','SUR'),('ST','ST','STP'),('SV','SV','SLV'),('SY','SY','SYR'),('SZ','SZ','SWZ'),('TC','TC','TCA'),('TD','TD','TCD'),('TF','TF','ATF'),('TG','TG','TGO'),('TH','TH','THA'),('TJ','TJ','TJK'),('TK','TK','TKL'),('TL','TL','TLS'),('TM','TM','TKM'),('TN','TN','TUN'),('TO','TO','TON'),('TR','TR','TUR'),('TT','TT','TTO'),('TV','TV','TUV'),('TW','TW','TWN'),('TZ','TZ','TZA'),('UA','UA','UKR'),('UG','UG','UGA'),('UM','UM','UMI'),('US','US','USA'),('UY','UY','URY'),('UZ','UZ','UZB'),('VA','VA','VAT'),('VC','VC','VCT'),('VE','VE','VEN'),('VG','VG','VGB'),('VI','VI','VIR'),('VN','VN','VNM'),('VU','VU','VUT'),('WF','WF','WLF'),('WS','WS','WSM'),('YE','YE','YEM'),('YT','YT','MYT'),('ZA','ZA','ZAF'),('ZM','ZM','ZMB'),('ZW','ZW','ZWE');
/*!40000 ALTER TABLE `directory_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country_format`
--

DROP TABLE IF EXISTS `directory_country_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_country_format` (
  `country_format_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Country Format Id',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id in ISO-2',
  `type` varchar(30) DEFAULT NULL COMMENT 'Country Format Type',
  `format` text NOT NULL COMMENT 'Country Format',
  PRIMARY KEY (`country_format_id`),
  UNIQUE KEY `DIRECTORY_COUNTRY_FORMAT_COUNTRY_ID_TYPE` (`country_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Format';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_country_format`
--

LOCK TABLES `directory_country_format` WRITE;
/*!40000 ALTER TABLE `directory_country_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_country_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country_region`
--

DROP TABLE IF EXISTS `directory_country_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_country_region` (
  `region_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Region Id',
  `country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Country Id in ISO-2',
  `code` varchar(32) DEFAULT NULL COMMENT 'Region code',
  `default_name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`region_id`),
  KEY `DIRECTORY_COUNTRY_REGION_COUNTRY_ID` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8 COMMENT='Directory Country Region';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_country_region`
--

LOCK TABLES `directory_country_region` WRITE;
/*!40000 ALTER TABLE `directory_country_region` DISABLE KEYS */;
INSERT INTO `directory_country_region` VALUES (1,'US','AL','Alabama'),(2,'US','AK','Alaska'),(3,'US','AS','American Samoa'),(4,'US','AZ','Arizona'),(5,'US','AR','Arkansas'),(6,'US','AE','Armed Forces Africa'),(7,'US','AA','Armed Forces Americas'),(8,'US','AE','Armed Forces Canada'),(9,'US','AE','Armed Forces Europe'),(10,'US','AE','Armed Forces Middle East'),(11,'US','AP','Armed Forces Pacific'),(12,'US','CA','California'),(13,'US','CO','Colorado'),(14,'US','CT','Connecticut'),(15,'US','DE','Delaware'),(16,'US','DC','District of Columbia'),(17,'US','FM','Federated States Of Micronesia'),(18,'US','FL','Florida'),(19,'US','GA','Georgia'),(20,'US','GU','Guam'),(21,'US','HI','Hawaii'),(22,'US','ID','Idaho'),(23,'US','IL','Illinois'),(24,'US','IN','Indiana'),(25,'US','IA','Iowa'),(26,'US','KS','Kansas'),(27,'US','KY','Kentucky'),(28,'US','LA','Louisiana'),(29,'US','ME','Maine'),(30,'US','MH','Marshall Islands'),(31,'US','MD','Maryland'),(32,'US','MA','Massachusetts'),(33,'US','MI','Michigan'),(34,'US','MN','Minnesota'),(35,'US','MS','Mississippi'),(36,'US','MO','Missouri'),(37,'US','MT','Montana'),(38,'US','NE','Nebraska'),(39,'US','NV','Nevada'),(40,'US','NH','New Hampshire'),(41,'US','NJ','New Jersey'),(42,'US','NM','New Mexico'),(43,'US','NY','New York'),(44,'US','NC','North Carolina'),(45,'US','ND','North Dakota'),(46,'US','MP','Northern Mariana Islands'),(47,'US','OH','Ohio'),(48,'US','OK','Oklahoma'),(49,'US','OR','Oregon'),(50,'US','PW','Palau'),(51,'US','PA','Pennsylvania'),(52,'US','PR','Puerto Rico'),(53,'US','RI','Rhode Island'),(54,'US','SC','South Carolina'),(55,'US','SD','South Dakota'),(56,'US','TN','Tennessee'),(57,'US','TX','Texas'),(58,'US','UT','Utah'),(59,'US','VT','Vermont'),(60,'US','VI','Virgin Islands'),(61,'US','VA','Virginia'),(62,'US','WA','Washington'),(63,'US','WV','West Virginia'),(64,'US','WI','Wisconsin'),(65,'US','WY','Wyoming'),(66,'CA','AB','Alberta'),(67,'CA','BC','British Columbia'),(68,'CA','MB','Manitoba'),(69,'CA','NL','Newfoundland and Labrador'),(70,'CA','NB','New Brunswick'),(71,'CA','NS','Nova Scotia'),(72,'CA','NT','Northwest Territories'),(73,'CA','NU','Nunavut'),(74,'CA','ON','Ontario'),(75,'CA','PE','Prince Edward Island'),(76,'CA','QC','Quebec'),(77,'CA','SK','Saskatchewan'),(78,'CA','YT','Yukon Territory'),(79,'DE','NDS','Niedersachsen'),(80,'DE','BAW','Baden-Württemberg'),(81,'DE','BAY','Bayern'),(82,'DE','BER','Berlin'),(83,'DE','BRG','Brandenburg'),(84,'DE','BRE','Bremen'),(85,'DE','HAM','Hamburg'),(86,'DE','HES','Hessen'),(87,'DE','MEC','Mecklenburg-Vorpommern'),(88,'DE','NRW','Nordrhein-Westfalen'),(89,'DE','RHE','Rheinland-Pfalz'),(90,'DE','SAR','Saarland'),(91,'DE','SAS','Sachsen'),(92,'DE','SAC','Sachsen-Anhalt'),(93,'DE','SCN','Schleswig-Holstein'),(94,'DE','THE','Thüringen'),(95,'AT','WI','Wien'),(96,'AT','NO','Niederösterreich'),(97,'AT','OO','Oberösterreich'),(98,'AT','SB','Salzburg'),(99,'AT','KN','Kärnten'),(100,'AT','ST','Steiermark'),(101,'AT','TI','Tirol'),(102,'AT','BL','Burgenland'),(103,'AT','VB','Vorarlberg'),(104,'CH','AG','Aargau'),(105,'CH','AI','Appenzell Innerrhoden'),(106,'CH','AR','Appenzell Ausserrhoden'),(107,'CH','BE','Bern'),(108,'CH','BL','Basel-Landschaft'),(109,'CH','BS','Basel-Stadt'),(110,'CH','FR','Freiburg'),(111,'CH','GE','Genf'),(112,'CH','GL','Glarus'),(113,'CH','GR','Graubünden'),(114,'CH','JU','Jura'),(115,'CH','LU','Luzern'),(116,'CH','NE','Neuenburg'),(117,'CH','NW','Nidwalden'),(118,'CH','OW','Obwalden'),(119,'CH','SG','St. Gallen'),(120,'CH','SH','Schaffhausen'),(121,'CH','SO','Solothurn'),(122,'CH','SZ','Schwyz'),(123,'CH','TG','Thurgau'),(124,'CH','TI','Tessin'),(125,'CH','UR','Uri'),(126,'CH','VD','Waadt'),(127,'CH','VS','Wallis'),(128,'CH','ZG','Zug'),(129,'CH','ZH','Zürich'),(130,'ES','A Coruсa','A Coruña'),(131,'ES','Alava','Alava'),(132,'ES','Albacete','Albacete'),(133,'ES','Alicante','Alicante'),(134,'ES','Almeria','Almeria'),(135,'ES','Asturias','Asturias'),(136,'ES','Avila','Avila'),(137,'ES','Badajoz','Badajoz'),(138,'ES','Baleares','Baleares'),(139,'ES','Barcelona','Barcelona'),(140,'ES','Burgos','Burgos'),(141,'ES','Caceres','Caceres'),(142,'ES','Cadiz','Cadiz'),(143,'ES','Cantabria','Cantabria'),(144,'ES','Castellon','Castellon'),(145,'ES','Ceuta','Ceuta'),(146,'ES','Ciudad Real','Ciudad Real'),(147,'ES','Cordoba','Cordoba'),(148,'ES','Cuenca','Cuenca'),(149,'ES','Girona','Girona'),(150,'ES','Granada','Granada'),(151,'ES','Guadalajara','Guadalajara'),(152,'ES','Guipuzcoa','Guipuzcoa'),(153,'ES','Huelva','Huelva'),(154,'ES','Huesca','Huesca'),(155,'ES','Jaen','Jaen'),(156,'ES','La Rioja','La Rioja'),(157,'ES','Las Palmas','Las Palmas'),(158,'ES','Leon','Leon'),(159,'ES','Lleida','Lleida'),(160,'ES','Lugo','Lugo'),(161,'ES','Madrid','Madrid'),(162,'ES','Malaga','Malaga'),(163,'ES','Melilla','Melilla'),(164,'ES','Murcia','Murcia'),(165,'ES','Navarra','Navarra'),(166,'ES','Ourense','Ourense'),(167,'ES','Palencia','Palencia'),(168,'ES','Pontevedra','Pontevedra'),(169,'ES','Salamanca','Salamanca'),(170,'ES','Santa Cruz de Tenerife','Santa Cruz de Tenerife'),(171,'ES','Segovia','Segovia'),(172,'ES','Sevilla','Sevilla'),(173,'ES','Soria','Soria'),(174,'ES','Tarragona','Tarragona'),(175,'ES','Teruel','Teruel'),(176,'ES','Toledo','Toledo'),(177,'ES','Valencia','Valencia'),(178,'ES','Valladolid','Valladolid'),(179,'ES','Vizcaya','Vizcaya'),(180,'ES','Zamora','Zamora'),(181,'ES','Zaragoza','Zaragoza'),(182,'FR','1','Ain'),(183,'FR','2','Aisne'),(184,'FR','3','Allier'),(185,'FR','4','Alpes-de-Haute-Provence'),(186,'FR','5','Hautes-Alpes'),(187,'FR','6','Alpes-Maritimes'),(188,'FR','7','Ardèche'),(189,'FR','8','Ardennes'),(190,'FR','9','Ariège'),(191,'FR','10','Aube'),(192,'FR','11','Aude'),(193,'FR','12','Aveyron'),(194,'FR','13','Bouches-du-Rhône'),(195,'FR','14','Calvados'),(196,'FR','15','Cantal'),(197,'FR','16','Charente'),(198,'FR','17','Charente-Maritime'),(199,'FR','18','Cher'),(200,'FR','19','Corrèze'),(201,'FR','2A','Corse-du-Sud'),(202,'FR','2B','Haute-Corse'),(203,'FR','21','Côte-d\'Or'),(204,'FR','22','Côtes-d\'Armor'),(205,'FR','23','Creuse'),(206,'FR','24','Dordogne'),(207,'FR','25','Doubs'),(208,'FR','26','Drôme'),(209,'FR','27','Eure'),(210,'FR','28','Eure-et-Loir'),(211,'FR','29','Finistère'),(212,'FR','30','Gard'),(213,'FR','31','Haute-Garonne'),(214,'FR','32','Gers'),(215,'FR','33','Gironde'),(216,'FR','34','Hérault'),(217,'FR','35','Ille-et-Vilaine'),(218,'FR','36','Indre'),(219,'FR','37','Indre-et-Loire'),(220,'FR','38','Isère'),(221,'FR','39','Jura'),(222,'FR','40','Landes'),(223,'FR','41','Loir-et-Cher'),(224,'FR','42','Loire'),(225,'FR','43','Haute-Loire'),(226,'FR','44','Loire-Atlantique'),(227,'FR','45','Loiret'),(228,'FR','46','Lot'),(229,'FR','47','Lot-et-Garonne'),(230,'FR','48','Lozère'),(231,'FR','49','Maine-et-Loire'),(232,'FR','50','Manche'),(233,'FR','51','Marne'),(234,'FR','52','Haute-Marne'),(235,'FR','53','Mayenne'),(236,'FR','54','Meurthe-et-Moselle'),(237,'FR','55','Meuse'),(238,'FR','56','Morbihan'),(239,'FR','57','Moselle'),(240,'FR','58','Nièvre'),(241,'FR','59','Nord'),(242,'FR','60','Oise'),(243,'FR','61','Orne'),(244,'FR','62','Pas-de-Calais'),(245,'FR','63','Puy-de-Dôme'),(246,'FR','64','Pyrénées-Atlantiques'),(247,'FR','65','Hautes-Pyrénées'),(248,'FR','66','Pyrénées-Orientales'),(249,'FR','67','Bas-Rhin'),(250,'FR','68','Haut-Rhin'),(251,'FR','69','Rhône'),(252,'FR','70','Haute-Saône'),(253,'FR','71','Saône-et-Loire'),(254,'FR','72','Sarthe'),(255,'FR','73','Savoie'),(256,'FR','74','Haute-Savoie'),(257,'FR','75','Paris'),(258,'FR','76','Seine-Maritime'),(259,'FR','77','Seine-et-Marne'),(260,'FR','78','Yvelines'),(261,'FR','79','Deux-Sèvres'),(262,'FR','80','Somme'),(263,'FR','81','Tarn'),(264,'FR','82','Tarn-et-Garonne'),(265,'FR','83','Var'),(266,'FR','84','Vaucluse'),(267,'FR','85','Vendée'),(268,'FR','86','Vienne'),(269,'FR','87','Haute-Vienne'),(270,'FR','88','Vosges'),(271,'FR','89','Yonne'),(272,'FR','90','Territoire-de-Belfort'),(273,'FR','91','Essonne'),(274,'FR','92','Hauts-de-Seine'),(275,'FR','93','Seine-Saint-Denis'),(276,'FR','94','Val-de-Marne'),(277,'FR','95','Val-d\'Oise'),(278,'RO','AB','Alba'),(279,'RO','AR','Arad'),(280,'RO','AG','Argeş'),(281,'RO','BC','Bacău'),(282,'RO','BH','Bihor'),(283,'RO','BN','Bistriţa-Năsăud'),(284,'RO','BT','Botoşani'),(285,'RO','BV','Braşov'),(286,'RO','BR','Brăila'),(287,'RO','B','Bucureşti'),(288,'RO','BZ','Buzău'),(289,'RO','CS','Caraş-Severin'),(290,'RO','CL','Călăraşi'),(291,'RO','CJ','Cluj'),(292,'RO','CT','Constanţa'),(293,'RO','CV','Covasna'),(294,'RO','DB','Dâmboviţa'),(295,'RO','DJ','Dolj'),(296,'RO','GL','Galaţi'),(297,'RO','GR','Giurgiu'),(298,'RO','GJ','Gorj'),(299,'RO','HR','Harghita'),(300,'RO','HD','Hunedoara'),(301,'RO','IL','Ialomiţa'),(302,'RO','IS','Iaşi'),(303,'RO','IF','Ilfov'),(304,'RO','MM','Maramureş'),(305,'RO','MH','Mehedinţi'),(306,'RO','MS','Mureş'),(307,'RO','NT','Neamţ'),(308,'RO','OT','Olt'),(309,'RO','PH','Prahova'),(310,'RO','SM','Satu-Mare'),(311,'RO','SJ','Sălaj'),(312,'RO','SB','Sibiu'),(313,'RO','SV','Suceava'),(314,'RO','TR','Teleorman'),(315,'RO','TM','Timiş'),(316,'RO','TL','Tulcea'),(317,'RO','VS','Vaslui'),(318,'RO','VL','Vâlcea'),(319,'RO','VN','Vrancea'),(320,'FI','Lappi','Lappi'),(321,'FI','Pohjois-Pohjanmaa','Pohjois-Pohjanmaa'),(322,'FI','Kainuu','Kainuu'),(323,'FI','Pohjois-Karjala','Pohjois-Karjala'),(324,'FI','Pohjois-Savo','Pohjois-Savo'),(325,'FI','Etelä-Savo','Etelä-Savo'),(326,'FI','Etelä-Pohjanmaa','Etelä-Pohjanmaa'),(327,'FI','Pohjanmaa','Pohjanmaa'),(328,'FI','Pirkanmaa','Pirkanmaa'),(329,'FI','Satakunta','Satakunta'),(330,'FI','Keski-Pohjanmaa','Keski-Pohjanmaa'),(331,'FI','Keski-Suomi','Keski-Suomi'),(332,'FI','Varsinais-Suomi','Varsinais-Suomi'),(333,'FI','Etelä-Karjala','Etelä-Karjala'),(334,'FI','Päijät-Häme','Päijät-Häme'),(335,'FI','Kanta-Häme','Kanta-Häme'),(336,'FI','Uusimaa','Uusimaa'),(337,'FI','Itä-Uusimaa','Itä-Uusimaa'),(338,'FI','Kymenlaakso','Kymenlaakso'),(339,'FI','Ahvenanmaa','Ahvenanmaa'),(340,'EE','EE-37','Harjumaa'),(341,'EE','EE-39','Hiiumaa'),(342,'EE','EE-44','Ida-Virumaa'),(343,'EE','EE-49','Jõgevamaa'),(344,'EE','EE-51','Järvamaa'),(345,'EE','EE-57','Läänemaa'),(346,'EE','EE-59','Lääne-Virumaa'),(347,'EE','EE-65','Põlvamaa'),(348,'EE','EE-67','Pärnumaa'),(349,'EE','EE-70','Raplamaa'),(350,'EE','EE-74','Saaremaa'),(351,'EE','EE-78','Tartumaa'),(352,'EE','EE-82','Valgamaa'),(353,'EE','EE-84','Viljandimaa'),(354,'EE','EE-86','Võrumaa'),(355,'LV','LV-DGV','Daugavpils'),(356,'LV','LV-JEL','Jelgava'),(357,'LV','Jēkabpils','Jēkabpils'),(358,'LV','LV-JUR','Jūrmala'),(359,'LV','LV-LPX','Liepāja'),(360,'LV','LV-LE','Liepājas novads'),(361,'LV','LV-REZ','Rēzekne'),(362,'LV','LV-RIX','Rīga'),(363,'LV','LV-RI','Rīgas novads'),(364,'LV','Valmiera','Valmiera'),(365,'LV','LV-VEN','Ventspils'),(366,'LV','Aglonas novads','Aglonas novads'),(367,'LV','LV-AI','Aizkraukles novads'),(368,'LV','Aizputes novads','Aizputes novads'),(369,'LV','Aknīstes novads','Aknīstes novads'),(370,'LV','Alojas novads','Alojas novads'),(371,'LV','Alsungas novads','Alsungas novads'),(372,'LV','LV-AL','Alūksnes novads'),(373,'LV','Amatas novads','Amatas novads'),(374,'LV','Apes novads','Apes novads'),(375,'LV','Auces novads','Auces novads'),(376,'LV','Babītes novads','Babītes novads'),(377,'LV','Baldones novads','Baldones novads'),(378,'LV','Baltinavas novads','Baltinavas novads'),(379,'LV','LV-BL','Balvu novads'),(380,'LV','LV-BU','Bauskas novads'),(381,'LV','Beverīnas novads','Beverīnas novads'),(382,'LV','Brocēnu novads','Brocēnu novads'),(383,'LV','Burtnieku novads','Burtnieku novads'),(384,'LV','Carnikavas novads','Carnikavas novads'),(385,'LV','Cesvaines novads','Cesvaines novads'),(386,'LV','Ciblas novads','Ciblas novads'),(387,'LV','LV-CE','Cēsu novads'),(388,'LV','Dagdas novads','Dagdas novads'),(389,'LV','LV-DA','Daugavpils novads'),(390,'LV','LV-DO','Dobeles novads'),(391,'LV','Dundagas novads','Dundagas novads'),(392,'LV','Durbes novads','Durbes novads'),(393,'LV','Engures novads','Engures novads'),(394,'LV','Garkalnes novads','Garkalnes novads'),(395,'LV','Grobiņas novads','Grobiņas novads'),(396,'LV','LV-GU','Gulbenes novads'),(397,'LV','Iecavas novads','Iecavas novads'),(398,'LV','Ikšķiles novads','Ikšķiles novads'),(399,'LV','Ilūkstes novads','Ilūkstes novads'),(400,'LV','Inčukalna novads','Inčukalna novads'),(401,'LV','Jaunjelgavas novads','Jaunjelgavas novads'),(402,'LV','Jaunpiebalgas novads','Jaunpiebalgas novads'),(403,'LV','Jaunpils novads','Jaunpils novads'),(404,'LV','LV-JL','Jelgavas novads'),(405,'LV','LV-JK','Jēkabpils novads'),(406,'LV','Kandavas novads','Kandavas novads'),(407,'LV','Kokneses novads','Kokneses novads'),(408,'LV','Krimuldas novads','Krimuldas novads'),(409,'LV','Krustpils novads','Krustpils novads'),(410,'LV','LV-KR','Krāslavas novads'),(411,'LV','LV-KU','Kuldīgas novads'),(412,'LV','Kārsavas novads','Kārsavas novads'),(413,'LV','Lielvārdes novads','Lielvārdes novads'),(414,'LV','LV-LM','Limbažu novads'),(415,'LV','Lubānas novads','Lubānas novads'),(416,'LV','LV-LU','Ludzas novads'),(417,'LV','Līgatnes novads','Līgatnes novads'),(418,'LV','Līvānu novads','Līvānu novads'),(419,'LV','LV-MA','Madonas novads'),(420,'LV','Mazsalacas novads','Mazsalacas novads'),(421,'LV','Mālpils novads','Mālpils novads'),(422,'LV','Mārupes novads','Mārupes novads'),(423,'LV','Naukšēnu novads','Naukšēnu novads'),(424,'LV','Neretas novads','Neretas novads'),(425,'LV','Nīcas novads','Nīcas novads'),(426,'LV','LV-OG','Ogres novads'),(427,'LV','Olaines novads','Olaines novads'),(428,'LV','Ozolnieku novads','Ozolnieku novads'),(429,'LV','LV-PR','Preiļu novads'),(430,'LV','Priekules novads','Priekules novads'),(431,'LV','Priekuļu novads','Priekuļu novads'),(432,'LV','Pārgaujas novads','Pārgaujas novads'),(433,'LV','Pāvilostas novads','Pāvilostas novads'),(434,'LV','Pļaviņu novads','Pļaviņu novads'),(435,'LV','Raunas novads','Raunas novads'),(436,'LV','Riebiņu novads','Riebiņu novads'),(437,'LV','Rojas novads','Rojas novads'),(438,'LV','Ropažu novads','Ropažu novads'),(439,'LV','Rucavas novads','Rucavas novads'),(440,'LV','Rugāju novads','Rugāju novads'),(441,'LV','Rundāles novads','Rundāles novads'),(442,'LV','LV-RE','Rēzeknes novads'),(443,'LV','Rūjienas novads','Rūjienas novads'),(444,'LV','Salacgrīvas novads','Salacgrīvas novads'),(445,'LV','Salas novads','Salas novads'),(446,'LV','Salaspils novads','Salaspils novads'),(447,'LV','LV-SA','Saldus novads'),(448,'LV','Saulkrastu novads','Saulkrastu novads'),(449,'LV','Siguldas novads','Siguldas novads'),(450,'LV','Skrundas novads','Skrundas novads'),(451,'LV','Skrīveru novads','Skrīveru novads'),(452,'LV','Smiltenes novads','Smiltenes novads'),(453,'LV','Stopiņu novads','Stopiņu novads'),(454,'LV','Strenču novads','Strenču novads'),(455,'LV','Sējas novads','Sējas novads'),(456,'LV','LV-TA','Talsu novads'),(457,'LV','LV-TU','Tukuma novads'),(458,'LV','Tērvetes novads','Tērvetes novads'),(459,'LV','Vaiņodes novads','Vaiņodes novads'),(460,'LV','LV-VK','Valkas novads'),(461,'LV','LV-VM','Valmieras novads'),(462,'LV','Varakļānu novads','Varakļānu novads'),(463,'LV','Vecpiebalgas novads','Vecpiebalgas novads'),(464,'LV','Vecumnieku novads','Vecumnieku novads'),(465,'LV','LV-VE','Ventspils novads'),(466,'LV','Viesītes novads','Viesītes novads'),(467,'LV','Viļakas novads','Viļakas novads'),(468,'LV','Viļānu novads','Viļānu novads'),(469,'LV','Vārkavas novads','Vārkavas novads'),(470,'LV','Zilupes novads','Zilupes novads'),(471,'LV','Ādažu novads','Ādažu novads'),(472,'LV','Ērgļu novads','Ērgļu novads'),(473,'LV','Ķeguma novads','Ķeguma novads'),(474,'LV','Ķekavas novads','Ķekavas novads'),(475,'LT','LT-AL','Alytaus Apskritis'),(476,'LT','LT-KU','Kauno Apskritis'),(477,'LT','LT-KL','Klaipėdos Apskritis'),(478,'LT','LT-MR','Marijampolės Apskritis'),(479,'LT','LT-PN','Panevėžio Apskritis'),(480,'LT','LT-SA','Šiaulių Apskritis'),(481,'LT','LT-TA','Tauragės Apskritis'),(482,'LT','LT-TE','Telšių Apskritis'),(483,'LT','LT-UT','Utenos Apskritis'),(484,'LT','LT-VL','Vilniaus Apskritis'),(485,'BR','AC','Acre'),(486,'BR','AL','Alagoas'),(487,'BR','AP','Amapá'),(488,'BR','AM','Amazonas'),(489,'BR','BA','Bahia'),(490,'BR','CE','Ceará'),(491,'BR','ES','Espírito Santo'),(492,'BR','GO','Goiás'),(493,'BR','MA','Maranhão'),(494,'BR','MT','Mato Grosso'),(495,'BR','MS','Mato Grosso do Sul'),(496,'BR','MG','Minas Gerais'),(497,'BR','PA','Pará'),(498,'BR','PB','Paraíba'),(499,'BR','PR','Paraná'),(500,'BR','PE','Pernambuco'),(501,'BR','PI','Piauí'),(502,'BR','RJ','Rio de Janeiro'),(503,'BR','RN','Rio Grande do Norte'),(504,'BR','RS','Rio Grande do Sul'),(505,'BR','RO','Rondônia'),(506,'BR','RR','Roraima'),(507,'BR','SC','Santa Catarina'),(508,'BR','SP','São Paulo'),(509,'BR','SE','Sergipe'),(510,'BR','TO','Tocantins'),(511,'BR','DF','Distrito Federal');
/*!40000 ALTER TABLE `directory_country_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_country_region_name`
--

DROP TABLE IF EXISTS `directory_country_region_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_country_region_name` (
  `locale` varchar(8) NOT NULL COMMENT 'Locale',
  `region_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Region Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`locale`,`region_id`),
  KEY `DIRECTORY_COUNTRY_REGION_NAME_REGION_ID` (`region_id`),
  CONSTRAINT `DIR_COUNTRY_REGION_NAME_REGION_ID_DIR_COUNTRY_REGION_REGION_ID` FOREIGN KEY (`region_id`) REFERENCES `directory_country_region` (`region_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Region Name';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_country_region_name`
--

LOCK TABLES `directory_country_region_name` WRITE;
/*!40000 ALTER TABLE `directory_country_region_name` DISABLE KEYS */;
INSERT INTO `directory_country_region_name` VALUES ('en_US',1,'Alabama'),('en_US',2,'Alaska'),('en_US',3,'American Samoa'),('en_US',4,'Arizona'),('en_US',5,'Arkansas'),('en_US',6,'Armed Forces Africa'),('en_US',7,'Armed Forces Americas'),('en_US',8,'Armed Forces Canada'),('en_US',9,'Armed Forces Europe'),('en_US',10,'Armed Forces Middle East'),('en_US',11,'Armed Forces Pacific'),('en_US',12,'California'),('en_US',13,'Colorado'),('en_US',14,'Connecticut'),('en_US',15,'Delaware'),('en_US',16,'District of Columbia'),('en_US',17,'Federated States Of Micronesia'),('en_US',18,'Florida'),('en_US',19,'Georgia'),('en_US',20,'Guam'),('en_US',21,'Hawaii'),('en_US',22,'Idaho'),('en_US',23,'Illinois'),('en_US',24,'Indiana'),('en_US',25,'Iowa'),('en_US',26,'Kansas'),('en_US',27,'Kentucky'),('en_US',28,'Louisiana'),('en_US',29,'Maine'),('en_US',30,'Marshall Islands'),('en_US',31,'Maryland'),('en_US',32,'Massachusetts'),('en_US',33,'Michigan'),('en_US',34,'Minnesota'),('en_US',35,'Mississippi'),('en_US',36,'Missouri'),('en_US',37,'Montana'),('en_US',38,'Nebraska'),('en_US',39,'Nevada'),('en_US',40,'New Hampshire'),('en_US',41,'New Jersey'),('en_US',42,'New Mexico'),('en_US',43,'New York'),('en_US',44,'North Carolina'),('en_US',45,'North Dakota'),('en_US',46,'Northern Mariana Islands'),('en_US',47,'Ohio'),('en_US',48,'Oklahoma'),('en_US',49,'Oregon'),('en_US',50,'Palau'),('en_US',51,'Pennsylvania'),('en_US',52,'Puerto Rico'),('en_US',53,'Rhode Island'),('en_US',54,'South Carolina'),('en_US',55,'South Dakota'),('en_US',56,'Tennessee'),('en_US',57,'Texas'),('en_US',58,'Utah'),('en_US',59,'Vermont'),('en_US',60,'Virgin Islands'),('en_US',61,'Virginia'),('en_US',62,'Washington'),('en_US',63,'West Virginia'),('en_US',64,'Wisconsin'),('en_US',65,'Wyoming'),('en_US',66,'Alberta'),('en_US',67,'British Columbia'),('en_US',68,'Manitoba'),('en_US',69,'Newfoundland and Labrador'),('en_US',70,'New Brunswick'),('en_US',71,'Nova Scotia'),('en_US',72,'Northwest Territories'),('en_US',73,'Nunavut'),('en_US',74,'Ontario'),('en_US',75,'Prince Edward Island'),('en_US',76,'Quebec'),('en_US',77,'Saskatchewan'),('en_US',78,'Yukon Territory'),('en_US',79,'Niedersachsen'),('en_US',80,'Baden-Württemberg'),('en_US',81,'Bayern'),('en_US',82,'Berlin'),('en_US',83,'Brandenburg'),('en_US',84,'Bremen'),('en_US',85,'Hamburg'),('en_US',86,'Hessen'),('en_US',87,'Mecklenburg-Vorpommern'),('en_US',88,'Nordrhein-Westfalen'),('en_US',89,'Rheinland-Pfalz'),('en_US',90,'Saarland'),('en_US',91,'Sachsen'),('en_US',92,'Sachsen-Anhalt'),('en_US',93,'Schleswig-Holstein'),('en_US',94,'Thüringen'),('en_US',95,'Wien'),('en_US',96,'Niederösterreich'),('en_US',97,'Oberösterreich'),('en_US',98,'Salzburg'),('en_US',99,'Kärnten'),('en_US',100,'Steiermark'),('en_US',101,'Tirol'),('en_US',102,'Burgenland'),('en_US',103,'Vorarlberg'),('en_US',104,'Aargau'),('en_US',105,'Appenzell Innerrhoden'),('en_US',106,'Appenzell Ausserrhoden'),('en_US',107,'Bern'),('en_US',108,'Basel-Landschaft'),('en_US',109,'Basel-Stadt'),('en_US',110,'Freiburg'),('en_US',111,'Genf'),('en_US',112,'Glarus'),('en_US',113,'Graubünden'),('en_US',114,'Jura'),('en_US',115,'Luzern'),('en_US',116,'Neuenburg'),('en_US',117,'Nidwalden'),('en_US',118,'Obwalden'),('en_US',119,'St. Gallen'),('en_US',120,'Schaffhausen'),('en_US',121,'Solothurn'),('en_US',122,'Schwyz'),('en_US',123,'Thurgau'),('en_US',124,'Tessin'),('en_US',125,'Uri'),('en_US',126,'Waadt'),('en_US',127,'Wallis'),('en_US',128,'Zug'),('en_US',129,'Zürich'),('en_US',130,'A Coruña'),('en_US',131,'Alava'),('en_US',132,'Albacete'),('en_US',133,'Alicante'),('en_US',134,'Almeria'),('en_US',135,'Asturias'),('en_US',136,'Avila'),('en_US',137,'Badajoz'),('en_US',138,'Baleares'),('en_US',139,'Barcelona'),('en_US',140,'Burgos'),('en_US',141,'Caceres'),('en_US',142,'Cadiz'),('en_US',143,'Cantabria'),('en_US',144,'Castellon'),('en_US',145,'Ceuta'),('en_US',146,'Ciudad Real'),('en_US',147,'Cordoba'),('en_US',148,'Cuenca'),('en_US',149,'Girona'),('en_US',150,'Granada'),('en_US',151,'Guadalajara'),('en_US',152,'Guipuzcoa'),('en_US',153,'Huelva'),('en_US',154,'Huesca'),('en_US',155,'Jaen'),('en_US',156,'La Rioja'),('en_US',157,'Las Palmas'),('en_US',158,'Leon'),('en_US',159,'Lleida'),('en_US',160,'Lugo'),('en_US',161,'Madrid'),('en_US',162,'Malaga'),('en_US',163,'Melilla'),('en_US',164,'Murcia'),('en_US',165,'Navarra'),('en_US',166,'Ourense'),('en_US',167,'Palencia'),('en_US',168,'Pontevedra'),('en_US',169,'Salamanca'),('en_US',170,'Santa Cruz de Tenerife'),('en_US',171,'Segovia'),('en_US',172,'Sevilla'),('en_US',173,'Soria'),('en_US',174,'Tarragona'),('en_US',175,'Teruel'),('en_US',176,'Toledo'),('en_US',177,'Valencia'),('en_US',178,'Valladolid'),('en_US',179,'Vizcaya'),('en_US',180,'Zamora'),('en_US',181,'Zaragoza'),('en_US',182,'Ain'),('en_US',183,'Aisne'),('en_US',184,'Allier'),('en_US',185,'Alpes-de-Haute-Provence'),('en_US',186,'Hautes-Alpes'),('en_US',187,'Alpes-Maritimes'),('en_US',188,'Ardèche'),('en_US',189,'Ardennes'),('en_US',190,'Ariège'),('en_US',191,'Aube'),('en_US',192,'Aude'),('en_US',193,'Aveyron'),('en_US',194,'Bouches-du-Rhône'),('en_US',195,'Calvados'),('en_US',196,'Cantal'),('en_US',197,'Charente'),('en_US',198,'Charente-Maritime'),('en_US',199,'Cher'),('en_US',200,'Corrèze'),('en_US',201,'Corse-du-Sud'),('en_US',202,'Haute-Corse'),('en_US',203,'Côte-d\'Or'),('en_US',204,'Côtes-d\'Armor'),('en_US',205,'Creuse'),('en_US',206,'Dordogne'),('en_US',207,'Doubs'),('en_US',208,'Drôme'),('en_US',209,'Eure'),('en_US',210,'Eure-et-Loir'),('en_US',211,'Finistère'),('en_US',212,'Gard'),('en_US',213,'Haute-Garonne'),('en_US',214,'Gers'),('en_US',215,'Gironde'),('en_US',216,'Hérault'),('en_US',217,'Ille-et-Vilaine'),('en_US',218,'Indre'),('en_US',219,'Indre-et-Loire'),('en_US',220,'Isère'),('en_US',221,'Jura'),('en_US',222,'Landes'),('en_US',223,'Loir-et-Cher'),('en_US',224,'Loire'),('en_US',225,'Haute-Loire'),('en_US',226,'Loire-Atlantique'),('en_US',227,'Loiret'),('en_US',228,'Lot'),('en_US',229,'Lot-et-Garonne'),('en_US',230,'Lozère'),('en_US',231,'Maine-et-Loire'),('en_US',232,'Manche'),('en_US',233,'Marne'),('en_US',234,'Haute-Marne'),('en_US',235,'Mayenne'),('en_US',236,'Meurthe-et-Moselle'),('en_US',237,'Meuse'),('en_US',238,'Morbihan'),('en_US',239,'Moselle'),('en_US',240,'Nièvre'),('en_US',241,'Nord'),('en_US',242,'Oise'),('en_US',243,'Orne'),('en_US',244,'Pas-de-Calais'),('en_US',245,'Puy-de-Dôme'),('en_US',246,'Pyrénées-Atlantiques'),('en_US',247,'Hautes-Pyrénées'),('en_US',248,'Pyrénées-Orientales'),('en_US',249,'Bas-Rhin'),('en_US',250,'Haut-Rhin'),('en_US',251,'Rhône'),('en_US',252,'Haute-Saône'),('en_US',253,'Saône-et-Loire'),('en_US',254,'Sarthe'),('en_US',255,'Savoie'),('en_US',256,'Haute-Savoie'),('en_US',257,'Paris'),('en_US',258,'Seine-Maritime'),('en_US',259,'Seine-et-Marne'),('en_US',260,'Yvelines'),('en_US',261,'Deux-Sèvres'),('en_US',262,'Somme'),('en_US',263,'Tarn'),('en_US',264,'Tarn-et-Garonne'),('en_US',265,'Var'),('en_US',266,'Vaucluse'),('en_US',267,'Vendée'),('en_US',268,'Vienne'),('en_US',269,'Haute-Vienne'),('en_US',270,'Vosges'),('en_US',271,'Yonne'),('en_US',272,'Territoire-de-Belfort'),('en_US',273,'Essonne'),('en_US',274,'Hauts-de-Seine'),('en_US',275,'Seine-Saint-Denis'),('en_US',276,'Val-de-Marne'),('en_US',277,'Val-d\'Oise'),('en_US',278,'Alba'),('en_US',279,'Arad'),('en_US',280,'Argeş'),('en_US',281,'Bacău'),('en_US',282,'Bihor'),('en_US',283,'Bistriţa-Năsăud'),('en_US',284,'Botoşani'),('en_US',285,'Braşov'),('en_US',286,'Brăila'),('en_US',287,'Bucureşti'),('en_US',288,'Buzău'),('en_US',289,'Caraş-Severin'),('en_US',290,'Călăraşi'),('en_US',291,'Cluj'),('en_US',292,'Constanţa'),('en_US',293,'Covasna'),('en_US',294,'Dâmboviţa'),('en_US',295,'Dolj'),('en_US',296,'Galaţi'),('en_US',297,'Giurgiu'),('en_US',298,'Gorj'),('en_US',299,'Harghita'),('en_US',300,'Hunedoara'),('en_US',301,'Ialomiţa'),('en_US',302,'Iaşi'),('en_US',303,'Ilfov'),('en_US',304,'Maramureş'),('en_US',305,'Mehedinţi'),('en_US',306,'Mureş'),('en_US',307,'Neamţ'),('en_US',308,'Olt'),('en_US',309,'Prahova'),('en_US',310,'Satu-Mare'),('en_US',311,'Sălaj'),('en_US',312,'Sibiu'),('en_US',313,'Suceava'),('en_US',314,'Teleorman'),('en_US',315,'Timiş'),('en_US',316,'Tulcea'),('en_US',317,'Vaslui'),('en_US',318,'Vâlcea'),('en_US',319,'Vrancea'),('en_US',320,'Lappi'),('en_US',321,'Pohjois-Pohjanmaa'),('en_US',322,'Kainuu'),('en_US',323,'Pohjois-Karjala'),('en_US',324,'Pohjois-Savo'),('en_US',325,'Etelä-Savo'),('en_US',326,'Etelä-Pohjanmaa'),('en_US',327,'Pohjanmaa'),('en_US',328,'Pirkanmaa'),('en_US',329,'Satakunta'),('en_US',330,'Keski-Pohjanmaa'),('en_US',331,'Keski-Suomi'),('en_US',332,'Varsinais-Suomi'),('en_US',333,'Etelä-Karjala'),('en_US',334,'Päijät-Häme'),('en_US',335,'Kanta-Häme'),('en_US',336,'Uusimaa'),('en_US',337,'Itä-Uusimaa'),('en_US',338,'Kymenlaakso'),('en_US',339,'Ahvenanmaa'),('en_US',340,'Harjumaa'),('en_US',341,'Hiiumaa'),('en_US',342,'Ida-Virumaa'),('en_US',343,'Jõgevamaa'),('en_US',344,'Järvamaa'),('en_US',345,'Läänemaa'),('en_US',346,'Lääne-Virumaa'),('en_US',347,'Põlvamaa'),('en_US',348,'Pärnumaa'),('en_US',349,'Raplamaa'),('en_US',350,'Saaremaa'),('en_US',351,'Tartumaa'),('en_US',352,'Valgamaa'),('en_US',353,'Viljandimaa'),('en_US',354,'Võrumaa'),('en_US',355,'Daugavpils'),('en_US',356,'Jelgava'),('en_US',357,'Jēkabpils'),('en_US',358,'Jūrmala'),('en_US',359,'Liepāja'),('en_US',360,'Liepājas novads'),('en_US',361,'Rēzekne'),('en_US',362,'Rīga'),('en_US',363,'Rīgas novads'),('en_US',364,'Valmiera'),('en_US',365,'Ventspils'),('en_US',366,'Aglonas novads'),('en_US',367,'Aizkraukles novads'),('en_US',368,'Aizputes novads'),('en_US',369,'Aknīstes novads'),('en_US',370,'Alojas novads'),('en_US',371,'Alsungas novads'),('en_US',372,'Alūksnes novads'),('en_US',373,'Amatas novads'),('en_US',374,'Apes novads'),('en_US',375,'Auces novads'),('en_US',376,'Babītes novads'),('en_US',377,'Baldones novads'),('en_US',378,'Baltinavas novads'),('en_US',379,'Balvu novads'),('en_US',380,'Bauskas novads'),('en_US',381,'Beverīnas novads'),('en_US',382,'Brocēnu novads'),('en_US',383,'Burtnieku novads'),('en_US',384,'Carnikavas novads'),('en_US',385,'Cesvaines novads'),('en_US',386,'Ciblas novads'),('en_US',387,'Cēsu novads'),('en_US',388,'Dagdas novads'),('en_US',389,'Daugavpils novads'),('en_US',390,'Dobeles novads'),('en_US',391,'Dundagas novads'),('en_US',392,'Durbes novads'),('en_US',393,'Engures novads'),('en_US',394,'Garkalnes novads'),('en_US',395,'Grobiņas novads'),('en_US',396,'Gulbenes novads'),('en_US',397,'Iecavas novads'),('en_US',398,'Ikšķiles novads'),('en_US',399,'Ilūkstes novads'),('en_US',400,'Inčukalna novads'),('en_US',401,'Jaunjelgavas novads'),('en_US',402,'Jaunpiebalgas novads'),('en_US',403,'Jaunpils novads'),('en_US',404,'Jelgavas novads'),('en_US',405,'Jēkabpils novads'),('en_US',406,'Kandavas novads'),('en_US',407,'Kokneses novads'),('en_US',408,'Krimuldas novads'),('en_US',409,'Krustpils novads'),('en_US',410,'Krāslavas novads'),('en_US',411,'Kuldīgas novads'),('en_US',412,'Kārsavas novads'),('en_US',413,'Lielvārdes novads'),('en_US',414,'Limbažu novads'),('en_US',415,'Lubānas novads'),('en_US',416,'Ludzas novads'),('en_US',417,'Līgatnes novads'),('en_US',418,'Līvānu novads'),('en_US',419,'Madonas novads'),('en_US',420,'Mazsalacas novads'),('en_US',421,'Mālpils novads'),('en_US',422,'Mārupes novads'),('en_US',423,'Naukšēnu novads'),('en_US',424,'Neretas novads'),('en_US',425,'Nīcas novads'),('en_US',426,'Ogres novads'),('en_US',427,'Olaines novads'),('en_US',428,'Ozolnieku novads'),('en_US',429,'Preiļu novads'),('en_US',430,'Priekules novads'),('en_US',431,'Priekuļu novads'),('en_US',432,'Pārgaujas novads'),('en_US',433,'Pāvilostas novads'),('en_US',434,'Pļaviņu novads'),('en_US',435,'Raunas novads'),('en_US',436,'Riebiņu novads'),('en_US',437,'Rojas novads'),('en_US',438,'Ropažu novads'),('en_US',439,'Rucavas novads'),('en_US',440,'Rugāju novads'),('en_US',441,'Rundāles novads'),('en_US',442,'Rēzeknes novads'),('en_US',443,'Rūjienas novads'),('en_US',444,'Salacgrīvas novads'),('en_US',445,'Salas novads'),('en_US',446,'Salaspils novads'),('en_US',447,'Saldus novads'),('en_US',448,'Saulkrastu novads'),('en_US',449,'Siguldas novads'),('en_US',450,'Skrundas novads'),('en_US',451,'Skrīveru novads'),('en_US',452,'Smiltenes novads'),('en_US',453,'Stopiņu novads'),('en_US',454,'Strenču novads'),('en_US',455,'Sējas novads'),('en_US',456,'Talsu novads'),('en_US',457,'Tukuma novads'),('en_US',458,'Tērvetes novads'),('en_US',459,'Vaiņodes novads'),('en_US',460,'Valkas novads'),('en_US',461,'Valmieras novads'),('en_US',462,'Varakļānu novads'),('en_US',463,'Vecpiebalgas novads'),('en_US',464,'Vecumnieku novads'),('en_US',465,'Ventspils novads'),('en_US',466,'Viesītes novads'),('en_US',467,'Viļakas novads'),('en_US',468,'Viļānu novads'),('en_US',469,'Vārkavas novads'),('en_US',470,'Zilupes novads'),('en_US',471,'Ādažu novads'),('en_US',472,'Ērgļu novads'),('en_US',473,'Ķeguma novads'),('en_US',474,'Ķekavas novads'),('en_US',475,'Alytaus Apskritis'),('en_US',476,'Kauno Apskritis'),('en_US',477,'Klaipėdos Apskritis'),('en_US',478,'Marijampolės Apskritis'),('en_US',479,'Panevėžio Apskritis'),('en_US',480,'Šiaulių Apskritis'),('en_US',481,'Tauragės Apskritis'),('en_US',482,'Telšių Apskritis'),('en_US',483,'Utenos Apskritis'),('en_US',484,'Vilniaus Apskritis'),('en_US',485,'Acre'),('en_US',486,'Alagoas'),('en_US',487,'Amapá'),('en_US',488,'Amazonas'),('en_US',489,'Bahia'),('en_US',490,'Ceará'),('en_US',491,'Espírito Santo'),('en_US',492,'Goiás'),('en_US',493,'Maranhão'),('en_US',494,'Mato Grosso'),('en_US',495,'Mato Grosso do Sul'),('en_US',496,'Minas Gerais'),('en_US',497,'Pará'),('en_US',498,'Paraíba'),('en_US',499,'Paraná'),('en_US',500,'Pernambuco'),('en_US',501,'Piauí'),('en_US',502,'Rio de Janeiro'),('en_US',503,'Rio Grande do Norte'),('en_US',504,'Rio Grande do Sul'),('en_US',505,'Rondônia'),('en_US',506,'Roraima'),('en_US',507,'Santa Catarina'),('en_US',508,'São Paulo'),('en_US',509,'Sergipe'),('en_US',510,'Tocantins'),('en_US',511,'Distrito Federal');
/*!40000 ALTER TABLE `directory_country_region_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_currency_rate`
--

DROP TABLE IF EXISTS `directory_currency_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_currency_rate` (
  `currency_from` varchar(3) NOT NULL COMMENT 'Currency Code Convert From',
  `currency_to` varchar(3) NOT NULL COMMENT 'Currency Code Convert To',
  `rate` decimal(24,12) NOT NULL DEFAULT '0.000000000000' COMMENT 'Currency Conversion Rate',
  PRIMARY KEY (`currency_from`,`currency_to`),
  KEY `DIRECTORY_CURRENCY_RATE_CURRENCY_TO` (`currency_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Currency Rate';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_currency_rate`
--

LOCK TABLES `directory_currency_rate` WRITE;
/*!40000 ALTER TABLE `directory_currency_rate` DISABLE KEYS */;
INSERT INTO `directory_currency_rate` VALUES ('EUR','EUR',1.000000000000),('EUR','USD',1.415000000000),('USD','EUR',0.706700000000),('USD','USD',1.000000000000);
/*!40000 ALTER TABLE `directory_currency_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_link`
--

DROP TABLE IF EXISTS `downloadable_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadable_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  `number_of_downloads` int(11) DEFAULT NULL COMMENT 'Number of downloads',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(20) DEFAULT NULL COMMENT 'Link Type',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample Url',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample File',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  PRIMARY KEY (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRODUCT_ID_SORT_ORDER` (`product_id`,`sort_order`),
  CONSTRAINT `DOWNLOADABLE_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloadable_link`
--

LOCK TABLES `downloadable_link` WRITE;
/*!40000 ALTER TABLE `downloadable_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloadable_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_link_price`
--

DROP TABLE IF EXISTS `downloadable_link_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadable_link_price` (
  `price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Price ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  PRIMARY KEY (`price_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_LINK_ID` (`link_id`),
  KEY `DOWNLOADABLE_LINK_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `DOWNLOADABLE_LINK_PRICE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `DOWNLOADABLE_LINK_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Price Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloadable_link_price`
--

LOCK TABLES `downloadable_link_price` WRITE;
/*!40000 ALTER TABLE `downloadable_link_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloadable_link_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_link_purchased`
--

DROP TABLE IF EXISTS `downloadable_link_purchased`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadable_link_purchased` (
  `purchased_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Purchased ID',
  `order_id` int(10) unsigned DEFAULT '0' COMMENT 'Order ID',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment ID',
  `order_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Item ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of creation',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of modification',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer ID',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product name',
  `product_sku` varchar(255) DEFAULT NULL COMMENT 'Product sku',
  `link_section_title` varchar(255) DEFAULT NULL COMMENT 'Link_section_title',
  PRIMARY KEY (`purchased_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ORDER_ID` (`order_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `DL_LNK_PURCHASED_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  CONSTRAINT `DOWNLOADABLE_LINK_PURCHASED_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloadable_link_purchased`
--

LOCK TABLES `downloadable_link_purchased` WRITE;
/*!40000 ALTER TABLE `downloadable_link_purchased` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloadable_link_purchased` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_link_purchased_item`
--

DROP TABLE IF EXISTS `downloadable_link_purchased_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadable_link_purchased_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item ID',
  `purchased_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Purchased ID',
  `order_item_id` int(10) unsigned DEFAULT '0' COMMENT 'Order Item ID',
  `product_id` int(10) unsigned DEFAULT '0' COMMENT 'Product ID',
  `link_hash` varchar(255) DEFAULT NULL COMMENT 'Link hash',
  `number_of_downloads_bought` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads bought',
  `number_of_downloads_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads used',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `link_title` varchar(255) DEFAULT NULL COMMENT 'Link Title',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable Flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(255) DEFAULT NULL COMMENT 'Link Type',
  `status` varchar(50) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  PRIMARY KEY (`item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_LINK_HASH` (`link_hash`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_ORDER_ITEM_ID` (`order_item_id`),
  KEY `DOWNLOADABLE_LINK_PURCHASED_ITEM_PURCHASED_ID` (`purchased_id`),
  CONSTRAINT `DL_LNK_PURCHASED_ITEM_ORDER_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`order_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE SET NULL,
  CONSTRAINT `DL_LNK_PURCHASED_ITEM_PURCHASED_ID_DL_LNK_PURCHASED_PURCHASED_ID` FOREIGN KEY (`purchased_id`) REFERENCES `downloadable_link_purchased` (`purchased_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Item Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloadable_link_purchased_item`
--

LOCK TABLES `downloadable_link_purchased_item` WRITE;
/*!40000 ALTER TABLE `downloadable_link_purchased_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloadable_link_purchased_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_link_title`
--

DROP TABLE IF EXISTS `downloadable_link_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadable_link_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `DOWNLOADABLE_LINK_TITLE_LINK_ID_STORE_ID` (`link_id`,`store_id`),
  KEY `DOWNLOADABLE_LINK_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `DOWNLOADABLE_LINK_TITLE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE,
  CONSTRAINT `DOWNLOADABLE_LINK_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Title Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloadable_link_title`
--

LOCK TABLES `downloadable_link_title` WRITE;
/*!40000 ALTER TABLE `downloadable_link_title` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloadable_link_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_sample`
--

DROP TABLE IF EXISTS `downloadable_sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadable_sample` (
  `sample_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sample ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample URL',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample file',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`sample_id`),
  KEY `DOWNLOADABLE_SAMPLE_PRODUCT_ID` (`product_id`),
  CONSTRAINT `DOWNLOADABLE_SAMPLE_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloadable_sample`
--

LOCK TABLES `downloadable_sample` WRITE;
/*!40000 ALTER TABLE `downloadable_sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloadable_sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloadable_sample_title`
--

DROP TABLE IF EXISTS `downloadable_sample_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadable_sample_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `sample_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sample ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `DOWNLOADABLE_SAMPLE_TITLE_SAMPLE_ID_STORE_ID` (`sample_id`,`store_id`),
  KEY `DOWNLOADABLE_SAMPLE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `DL_SAMPLE_TTL_SAMPLE_ID_DL_SAMPLE_SAMPLE_ID` FOREIGN KEY (`sample_id`) REFERENCES `downloadable_sample` (`sample_id`) ON DELETE CASCADE,
  CONSTRAINT `DOWNLOADABLE_SAMPLE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Title Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloadable_sample_title`
--

LOCK TABLES `downloadable_sample_title` WRITE;
/*!40000 ALTER TABLE `downloadable_sample_title` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloadable_sample_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute`
--

DROP TABLE IF EXISTS `eav_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_code` varchar(255) DEFAULT NULL COMMENT 'Attribute Code',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `backend_model` varchar(255) DEFAULT NULL COMMENT 'Backend Model',
  `backend_type` varchar(8) NOT NULL DEFAULT 'static' COMMENT 'Backend Type',
  `backend_table` varchar(255) DEFAULT NULL COMMENT 'Backend Table',
  `frontend_model` varchar(255) DEFAULT NULL COMMENT 'Frontend Model',
  `frontend_input` varchar(50) DEFAULT NULL COMMENT 'Frontend Input',
  `frontend_label` varchar(255) DEFAULT NULL COMMENT 'Frontend Label',
  `frontend_class` varchar(255) DEFAULT NULL COMMENT 'Frontend Class',
  `source_model` varchar(255) DEFAULT NULL COMMENT 'Source Model',
  `is_required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Required',
  `is_user_defined` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is User Defined',
  `default_value` text COMMENT 'Default Value',
  `is_unique` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Unique',
  `note` varchar(255) DEFAULT NULL COMMENT 'Note',
  PRIMARY KEY (`attribute_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_ENTITY_TYPE_ID_ATTRIBUTE_CODE` (`entity_type_id`,`attribute_code`),
  CONSTRAINT `EAV_ATTRIBUTE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_attribute`
--

LOCK TABLES `eav_attribute` WRITE;
/*!40000 ALTER TABLE `eav_attribute` DISABLE KEYS */;
INSERT INTO `eav_attribute` VALUES (1,1,'website_id',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Website','static',NULL,NULL,'select','Associate to Website',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Website',1,0,NULL,0,NULL),(2,1,'store_id',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Store','static',NULL,NULL,'select','Create In',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Store',1,0,NULL,0,NULL),(3,1,'created_in',NULL,NULL,'static',NULL,NULL,'text','Created From',NULL,NULL,0,0,NULL,0,NULL),(4,1,'prefix',NULL,NULL,'static',NULL,NULL,'text','Prefix',NULL,NULL,0,0,NULL,0,NULL),(5,1,'firstname',NULL,NULL,'static',NULL,NULL,'text','First Name',NULL,NULL,1,0,NULL,0,NULL),(6,1,'middlename',NULL,NULL,'static',NULL,NULL,'text','Middle Name/Initial',NULL,NULL,0,0,NULL,0,NULL),(7,1,'lastname',NULL,NULL,'static',NULL,NULL,'text','Last Name',NULL,NULL,1,0,NULL,0,NULL),(8,1,'suffix',NULL,NULL,'static',NULL,NULL,'text','Suffix',NULL,NULL,0,0,NULL,0,NULL),(9,1,'email',NULL,NULL,'static',NULL,NULL,'text','Email',NULL,NULL,1,0,NULL,0,NULL),(10,1,'group_id',NULL,NULL,'static',NULL,NULL,'select','Group',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Source\\Group',1,0,NULL,0,NULL),(11,1,'dob',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','static',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Frontend\\Datetime','date','Date of Birth',NULL,NULL,0,0,NULL,0,NULL),(12,1,'password_hash',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Password','static',NULL,NULL,'hidden',NULL,NULL,NULL,0,0,NULL,0,NULL),(13,1,'rp_token',NULL,NULL,'static',NULL,NULL,'hidden',NULL,NULL,NULL,0,0,NULL,0,NULL),(14,1,'rp_token_created_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,0,0,NULL,0,NULL),(15,1,'default_billing',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Billing','static',NULL,NULL,'text','Default Billing Address',NULL,NULL,0,0,NULL,0,NULL),(16,1,'default_shipping',NULL,'Magento\\Customer\\Model\\Customer\\Attribute\\Backend\\Shipping','static',NULL,NULL,'text','Default Shipping Address',NULL,NULL,0,0,NULL,0,NULL),(17,1,'taxvat',NULL,NULL,'static',NULL,NULL,'text','Tax/VAT Number',NULL,NULL,0,0,NULL,0,NULL),(18,1,'confirmation',NULL,NULL,'static',NULL,NULL,'text','Is Confirmed',NULL,NULL,0,0,NULL,0,NULL),(19,1,'created_at',NULL,NULL,'static',NULL,NULL,'date','Created At',NULL,NULL,0,0,NULL,0,NULL),(20,1,'gender',NULL,NULL,'static',NULL,NULL,'select','Gender',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,0,NULL,0,NULL),(21,1,'disable_auto_group_change',NULL,'Magento\\Customer\\Model\\Attribute\\Backend\\Data\\Boolean','static',NULL,NULL,'boolean','Disable Automatic Group Change Based on VAT ID',NULL,NULL,0,0,NULL,0,NULL),(22,2,'prefix',NULL,NULL,'static',NULL,NULL,'text','Prefix',NULL,NULL,0,0,NULL,0,NULL),(23,2,'firstname',NULL,NULL,'static',NULL,NULL,'text','First Name',NULL,NULL,1,0,NULL,0,NULL),(24,2,'middlename',NULL,NULL,'static',NULL,NULL,'text','Middle Name/Initial',NULL,NULL,0,0,NULL,0,NULL),(25,2,'lastname',NULL,NULL,'static',NULL,NULL,'text','Last Name',NULL,NULL,1,0,NULL,0,NULL),(26,2,'suffix',NULL,NULL,'static',NULL,NULL,'text','Suffix',NULL,NULL,0,0,NULL,0,NULL),(27,2,'company',NULL,NULL,'static',NULL,NULL,'text','Company',NULL,NULL,0,0,NULL,0,NULL),(28,2,'street',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\DefaultBackend','static',NULL,NULL,'multiline','Street Address',NULL,NULL,1,0,NULL,0,NULL),(29,2,'city',NULL,NULL,'static',NULL,NULL,'text','City',NULL,NULL,1,0,NULL,0,NULL),(30,2,'country_id',NULL,NULL,'static',NULL,NULL,'select','Country',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Source\\Country',1,0,NULL,0,NULL),(31,2,'region',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Backend\\Region','static',NULL,NULL,'text','State/Province',NULL,NULL,0,0,NULL,0,NULL),(32,2,'region_id',NULL,NULL,'static',NULL,NULL,'hidden','State/Province',NULL,'Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Source\\Region',0,0,NULL,0,NULL),(33,2,'postcode',NULL,NULL,'static',NULL,NULL,'text','Zip/Postal Code',NULL,NULL,0,0,NULL,0,NULL),(34,2,'telephone',NULL,NULL,'static',NULL,NULL,'text','Phone Number',NULL,NULL,1,0,NULL,0,NULL),(35,2,'fax',NULL,NULL,'static',NULL,NULL,'text','Fax',NULL,NULL,0,0,NULL,0,NULL),(36,2,'vat_id',NULL,NULL,'static',NULL,NULL,'text','VAT number',NULL,NULL,0,0,NULL,0,NULL),(37,2,'vat_is_valid',NULL,NULL,'static',NULL,NULL,'text','VAT number validity',NULL,NULL,0,0,NULL,0,NULL),(38,2,'vat_request_id',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request ID',NULL,NULL,0,0,NULL,0,NULL),(39,2,'vat_request_date',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request date',NULL,NULL,0,0,NULL,0,NULL),(40,2,'vat_request_success',NULL,NULL,'static',NULL,NULL,'text','VAT number validation request success',NULL,NULL,0,0,NULL,0,NULL),(41,1,'updated_at',NULL,NULL,'static',NULL,NULL,'date','Updated At',NULL,NULL,0,0,NULL,0,NULL),(42,1,'failures_num',NULL,NULL,'static',NULL,NULL,'hidden','Failures Number',NULL,NULL,0,0,NULL,0,NULL),(43,1,'first_failure',NULL,NULL,'static',NULL,NULL,'date','First Failure Date',NULL,NULL,0,0,NULL,0,NULL),(44,1,'lock_expires',NULL,NULL,'static',NULL,NULL,'date','Failures Number',NULL,NULL,0,0,NULL,0,NULL),(45,3,'name',NULL,NULL,'varchar',NULL,NULL,'text','Name',NULL,NULL,1,0,NULL,0,NULL),(46,3,'is_active',NULL,NULL,'int',NULL,NULL,'select','Is Active',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,NULL,0,NULL),(47,3,'description',NULL,NULL,'text',NULL,NULL,'textarea','Description',NULL,NULL,0,0,NULL,0,NULL),(48,3,'image',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Image',NULL,NULL,0,0,NULL,0,NULL),(49,3,'meta_title',NULL,NULL,'varchar',NULL,NULL,'text','Page Title',NULL,NULL,0,0,NULL,0,NULL),(50,3,'meta_keywords',NULL,NULL,'text',NULL,NULL,'textarea','Meta Keywords',NULL,NULL,0,0,NULL,0,NULL),(51,3,'meta_description',NULL,NULL,'text',NULL,NULL,'textarea','Meta Description',NULL,NULL,0,0,NULL,0,NULL),(52,3,'display_mode',NULL,NULL,'varchar',NULL,NULL,'select','Display Mode',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Mode',0,0,NULL,0,NULL),(53,3,'landing_page',NULL,NULL,'int',NULL,NULL,'select','CMS Block',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Page',0,0,NULL,0,NULL),(54,3,'is_anchor',NULL,NULL,'int',NULL,NULL,'select','Is Anchor',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,'1',0,NULL),(55,3,'path',NULL,NULL,'static',NULL,NULL,'text','Path',NULL,NULL,0,0,NULL,0,NULL),(56,3,'position',NULL,NULL,'static',NULL,NULL,'text','Position',NULL,NULL,0,0,NULL,0,NULL),(57,3,'all_children',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(58,3,'path_in_store',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(59,3,'children',NULL,NULL,'text',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(60,3,'custom_design',NULL,NULL,'varchar',NULL,NULL,'select','Custom Design',NULL,'Magento\\Theme\\Model\\Theme\\Source\\Theme',0,0,NULL,0,NULL),(61,3,'custom_design_from','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Frontend\\Datetime','date','Active From',NULL,NULL,0,0,NULL,0,NULL),(62,3,'custom_design_to',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Active To',NULL,NULL,0,0,NULL,0,NULL),(63,3,'page_layout',NULL,NULL,'varchar',NULL,NULL,'select','Page Layout',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(64,3,'custom_layout_update',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Customlayoutupdate','text',NULL,NULL,'textarea','Custom Layout Update',NULL,NULL,0,0,NULL,0,NULL),(65,3,'level',NULL,NULL,'static',NULL,NULL,'text','Level',NULL,NULL,0,0,NULL,0,NULL),(66,3,'children_count',NULL,NULL,'static',NULL,NULL,'text','Children Count',NULL,NULL,0,0,NULL,0,NULL),(67,3,'available_sort_by',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Sortby','text',NULL,NULL,'multiselect','Available Product Listing Sort By',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Sortby',1,0,NULL,0,NULL),(68,3,'default_sort_by',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Sortby','varchar',NULL,NULL,'select','Default Product Listing Sort By',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Source\\Sortby',1,0,NULL,0,NULL),(69,3,'include_in_menu',NULL,NULL,'int',NULL,NULL,'select','Include in Navigation Menu',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,'1',0,NULL),(70,3,'custom_use_parent_settings',NULL,NULL,'int',NULL,NULL,'select','Use Parent Category Settings',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(71,3,'custom_apply_to_products',NULL,NULL,'int',NULL,NULL,'select','Apply To Products',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(72,3,'filter_price_range',NULL,NULL,'decimal',NULL,NULL,'text','Layered Navigation Price Step',NULL,NULL,0,0,NULL,0,NULL),(73,4,'name',NULL,NULL,'varchar',NULL,NULL,'text','Product Name','validate-length maximum-length-255',NULL,1,0,NULL,0,NULL),(74,4,'sku',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Sku','static',NULL,NULL,'text','SKU','validate-length maximum-length-64',NULL,1,0,NULL,1,NULL),(75,4,'description',NULL,NULL,'text',NULL,NULL,'textarea','Description',NULL,NULL,0,0,NULL,0,NULL),(76,4,'short_description',NULL,NULL,'text',NULL,NULL,'textarea','Short Description',NULL,NULL,0,0,NULL,0,NULL),(77,4,'price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Price',NULL,NULL,1,0,NULL,0,NULL),(78,4,'special_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Special Price',NULL,NULL,0,0,NULL,0,NULL),(79,4,'special_from_date',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Special Price From Date',NULL,NULL,0,0,NULL,0,NULL),(80,4,'special_to_date',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Special Price To Date',NULL,NULL,0,0,NULL,0,NULL),(81,4,'cost',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Cost',NULL,NULL,0,1,NULL,0,NULL),(82,4,'weight',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Weight','decimal',NULL,NULL,'weight','Weight',NULL,NULL,0,0,NULL,0,NULL),(83,4,'manufacturer',NULL,NULL,'int',NULL,NULL,'select','Casa',NULL,NULL,0,1,'',0,NULL),(84,4,'meta_title',NULL,NULL,'varchar',NULL,NULL,'text','Meta Title',NULL,NULL,0,0,NULL,0,NULL),(85,4,'meta_keyword',NULL,NULL,'text',NULL,NULL,'textarea','Meta Keywords',NULL,NULL,0,0,NULL,0,NULL),(86,4,'meta_description',NULL,NULL,'varchar',NULL,NULL,'textarea','Meta Description',NULL,NULL,0,0,NULL,0,'Maximum 255 chars. Meta Description should optimally be between 150-160 characters'),(87,4,'image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Base',NULL,NULL,0,0,NULL,0,NULL),(88,4,'small_image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Small',NULL,NULL,0,0,NULL,0,NULL),(89,4,'thumbnail',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Thumbnail',NULL,NULL,0,0,NULL,0,NULL),(90,4,'media_gallery',NULL,NULL,'static',NULL,NULL,'gallery','Media Gallery',NULL,NULL,0,0,NULL,0,NULL),(91,4,'old_id',NULL,NULL,'int',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(92,4,'tier_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Tierprice','decimal',NULL,NULL,'text','Tier Price',NULL,NULL,0,0,NULL,0,NULL),(93,4,'color',NULL,NULL,'int',NULL,NULL,'select','Color',NULL,NULL,0,1,NULL,0,NULL),(94,4,'news_from_date',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Set Product as New from Date',NULL,NULL,0,0,NULL,0,NULL),(95,4,'news_to_date',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Set Product as New to Date',NULL,NULL,0,0,NULL,0,NULL),(96,4,'gallery',NULL,NULL,'varchar',NULL,NULL,'gallery','Image Gallery',NULL,NULL,0,0,NULL,0,NULL),(97,4,'status',NULL,NULL,'int',NULL,NULL,'select','Enable Product',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Status',0,0,'1',0,NULL),(98,4,'minimal_price',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Minimal Price',NULL,NULL,0,0,NULL,0,NULL),(99,4,'visibility',NULL,NULL,'int',NULL,NULL,'select','Visibility',NULL,'Magento\\Catalog\\Model\\Product\\Visibility',0,0,'4',0,NULL),(100,4,'custom_design',NULL,NULL,'varchar',NULL,NULL,'select','New Theme',NULL,'Magento\\Theme\\Model\\Theme\\Source\\Theme',0,0,NULL,0,NULL),(101,4,'custom_design_from',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Startdate','datetime',NULL,NULL,'date','Active From',NULL,NULL,0,0,NULL,0,NULL),(102,4,'custom_design_to',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\Datetime','datetime',NULL,NULL,'date','Active To',NULL,NULL,0,0,NULL,0,NULL),(103,4,'custom_layout_update',NULL,'Magento\\Catalog\\Model\\Attribute\\Backend\\Customlayoutupdate','text',NULL,NULL,'textarea','Layout Update XML',NULL,NULL,0,0,NULL,0,NULL),(104,4,'page_layout',NULL,NULL,'varchar',NULL,NULL,'select','Layout',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(105,4,'category_ids',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Category','static',NULL,NULL,'text','Categories',NULL,NULL,0,0,NULL,0,NULL),(106,4,'options_container',NULL,NULL,'varchar',NULL,NULL,'select','Display Product Options In',NULL,'Magento\\Catalog\\Model\\Entity\\Product\\Attribute\\Design\\Options\\Container',0,0,'container2',0,NULL),(107,4,'required_options',NULL,NULL,'static',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(108,4,'has_options',NULL,NULL,'static',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(109,4,'image_label',NULL,NULL,'varchar',NULL,NULL,'text','Image Label',NULL,NULL,0,0,NULL,0,NULL),(110,4,'small_image_label',NULL,NULL,'varchar',NULL,NULL,'text','Small Image Label',NULL,NULL,0,0,NULL,0,NULL),(111,4,'thumbnail_label',NULL,NULL,'varchar',NULL,NULL,'text','Thumbnail Label',NULL,NULL,0,0,NULL,0,NULL),(112,4,'created_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,1,0,NULL,0,NULL),(113,4,'updated_at',NULL,NULL,'static',NULL,NULL,'date',NULL,NULL,NULL,1,0,NULL,0,NULL),(114,4,'country_of_manufacture',NULL,NULL,'varchar',NULL,NULL,'select','Country of Manufacture',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Countryofmanufacture',0,0,NULL,0,NULL),(115,4,'quantity_and_stock_status',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Stock','int',NULL,NULL,'select','Quantity',NULL,'Magento\\CatalogInventory\\Model\\Source\\Stock',0,0,'1',0,NULL),(116,4,'custom_layout',NULL,NULL,'varchar',NULL,NULL,'select','New Layout',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Layout',0,0,NULL,0,NULL),(117,3,'url_key',NULL,NULL,'varchar',NULL,NULL,'text','URL Key',NULL,NULL,0,0,NULL,0,NULL),(118,3,'url_path',NULL,NULL,'varchar',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(119,4,'url_key',NULL,NULL,'varchar',NULL,NULL,'text','URL Key',NULL,NULL,0,0,NULL,0,NULL),(120,4,'url_path',NULL,NULL,'varchar',NULL,NULL,'text',NULL,NULL,NULL,0,0,NULL,0,NULL),(121,4,'msrp',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Price','decimal',NULL,NULL,'price','Manufacturer\'s Suggested Retail Price',NULL,NULL,0,0,NULL,0,NULL),(122,4,'msrp_display_actual_price_type',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Boolean','varchar',NULL,NULL,'select','Display Actual Price',NULL,'Magento\\Msrp\\Model\\Product\\Attribute\\Source\\Type\\Price',0,0,'0',0,NULL),(123,4,'price_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic Price',NULL,NULL,1,0,'0',0,NULL),(124,4,'sku_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic SKU',NULL,NULL,1,0,'0',0,NULL),(125,4,'weight_type',NULL,NULL,'int',NULL,NULL,'boolean','Dynamic Weight',NULL,NULL,1,0,'0',0,NULL),(126,4,'price_view',NULL,NULL,'int',NULL,NULL,'select','Price View',NULL,'Magento\\Bundle\\Model\\Product\\Attribute\\Source\\Price\\View',1,0,NULL,0,NULL),(127,4,'shipment_type',NULL,NULL,'int',NULL,NULL,'select','Ship Bundle Items',NULL,'Magento\\Bundle\\Model\\Product\\Attribute\\Source\\Shipment\\Type',1,0,'0',0,NULL),(128,4,'links_purchased_separately',NULL,NULL,'int',NULL,NULL,NULL,'Links can be purchased separately',NULL,NULL,1,0,NULL,0,NULL),(129,4,'samples_title',NULL,NULL,'varchar',NULL,NULL,NULL,'Samples title',NULL,NULL,1,0,NULL,0,NULL),(130,4,'links_title',NULL,NULL,'varchar',NULL,NULL,NULL,'Links title',NULL,NULL,1,0,NULL,0,NULL),(131,4,'links_exist',NULL,NULL,'int',NULL,NULL,NULL,NULL,NULL,NULL,0,0,'0',0,NULL),(132,4,'swatch_image',NULL,NULL,'varchar',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Frontend\\Image','media_image','Swatch',NULL,NULL,0,0,NULL,0,NULL),(133,4,'tax_class_id',NULL,NULL,'int',NULL,NULL,'select','Tax Class',NULL,'Magento\\Tax\\Model\\TaxClass\\Source\\Product',0,0,'2',0,NULL),(134,4,'gift_message_available',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Backend\\Boolean','varchar',NULL,NULL,'select','Allow Gift Message',NULL,'Magento\\Catalog\\Model\\Product\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(135,3,'thumb_nail_hover',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Thumbnail Hover',NULL,NULL,0,0,NULL,0,NULL),(136,3,'thumb_popular',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Thumbnail Popular',NULL,NULL,0,0,NULL,0,NULL),(137,3,'cate_popular',NULL,NULL,'int',NULL,NULL,'select','Show Category Popular',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,NULL,0,NULL),(138,4,'featured',NULL,NULL,'int',NULL,NULL,'select','Featured',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,0,'0',0,NULL),(139,3,'thumb_nail',NULL,'Magento\\Catalog\\Model\\Category\\Attribute\\Backend\\Image','varchar',NULL,NULL,'image','Thumbnail',NULL,NULL,0,0,NULL,0,NULL),(140,3,'is_new',NULL,NULL,'int',NULL,NULL,'select','Is New',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,NULL,0,NULL),(141,3,'is_sale',NULL,NULL,'int',NULL,NULL,'select','Is Sale',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',1,0,NULL,0,NULL),(142,4,'agave',NULL,NULL,'varchar',NULL,NULL,'text','Agave',NULL,NULL,0,1,NULL,0,NULL),(143,4,'agua_fermentacion',NULL,NULL,'varchar',NULL,NULL,'text','Agua de fermentación',NULL,NULL,0,1,NULL,0,NULL),(144,4,'anio',NULL,NULL,'varchar',NULL,NULL,'text','Año',NULL,NULL,0,1,NULL,0,NULL),(145,4,'botellas_por_caja',NULL,NULL,'varchar',NULL,NULL,'text','Botellas por caja',NULL,NULL,0,1,NULL,0,NULL),(146,4,'cocimiento',NULL,NULL,'varchar',NULL,NULL,'text','Cocimiento',NULL,NULL,0,1,NULL,0,NULL),(148,4,'entrega',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Entrega',NULL,NULL,0,1,'',0,NULL),(149,4,'estado',NULL,NULL,'int',NULL,NULL,'select','Estado',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL),(150,4,'fermentacion',NULL,NULL,'varchar',NULL,NULL,'text','Fermentación',NULL,NULL,0,1,NULL,0,NULL),(152,4,'filter_maguey',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','Filter_Maguey',NULL,NULL,0,1,'',0,NULL),(153,4,'filter_region',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Backend\\ArrayBackend','varchar',NULL,NULL,'multiselect','region',NULL,NULL,0,1,'',0,NULL),(154,4,'litros_destilados',NULL,NULL,'varchar',NULL,NULL,'text','Litros destilados',NULL,NULL,0,1,NULL,0,NULL),(155,4,'maduracion_anios',NULL,NULL,'varchar',NULL,NULL,'text','Maduración (años)',NULL,NULL,0,1,NULL,0,NULL),(156,4,'maestro',NULL,NULL,'varchar',NULL,NULL,'text','Maestro Mezcalero',NULL,NULL,0,1,NULL,0,NULL),(157,4,'maguey',NULL,NULL,'varchar',NULL,NULL,'text','Maguey',NULL,NULL,0,1,NULL,0,NULL),(158,4,'molido',NULL,NULL,'varchar',NULL,NULL,'text','Molido',NULL,NULL,0,1,NULL,0,NULL),(159,4,'notas_de_cata',NULL,NULL,'varchar',NULL,NULL,'text','Notas de Cata',NULL,NULL,0,1,NULL,0,NULL),(160,4,'no_destilaciones',NULL,NULL,'varchar',NULL,NULL,'text','No. de destilaciones',NULL,NULL,0,1,NULL,0,NULL),(161,4,'origen_planta',NULL,NULL,'varchar',NULL,NULL,'text','Origen de planta',NULL,NULL,0,1,NULL,0,NULL),(162,4,'presentacion_cl',NULL,NULL,'varchar',NULL,NULL,'text','Presentación (cl)',NULL,NULL,0,1,NULL,0,NULL),(163,4,'region',NULL,NULL,'varchar',NULL,NULL,'text','Región',NULL,NULL,0,1,NULL,0,NULL),(164,4,'terruno',NULL,NULL,'varchar',NULL,NULL,'text','Terruño',NULL,NULL,0,1,NULL,0,NULL),(165,4,'tipo_abocado',NULL,NULL,'varchar',NULL,NULL,'text','Tipo de Abocado',NULL,NULL,0,1,NULL,0,NULL),(166,4,'tipo_ajuste',NULL,NULL,'varchar',NULL,NULL,'text','Tipo de ajuste',NULL,NULL,0,1,NULL,0,NULL),(167,4,'tipo_destilador',NULL,NULL,'varchar',NULL,NULL,'text','Tipo de destilador',NULL,NULL,0,1,NULL,0,NULL),(168,4,'variedad',NULL,NULL,'varchar',NULL,NULL,'text','Variedad',NULL,NULL,0,1,NULL,0,NULL),(169,4,'grados_alcohol',NULL,NULL,'varchar',NULL,NULL,'text','Grados de alcohol (%)',NULL,NULL,0,1,NULL,0,NULL),(170,4,'home_producto_destacado',NULL,NULL,'int',NULL,NULL,'boolean','Producto Destacado',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Boolean',0,1,'0',0,NULL),(171,4,'paquete',NULL,NULL,'int',NULL,NULL,'select','Paquete',NULL,'Magento\\Eav\\Model\\Entity\\Attribute\\Source\\Table',0,1,'',0,NULL);
/*!40000 ALTER TABLE `eav_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_group`
--

DROP TABLE IF EXISTS `eav_attribute_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_attribute_group` (
  `attribute_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Group Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Group Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Default Id',
  `attribute_group_code` varchar(255) NOT NULL COMMENT 'Attribute Group Code',
  `tab_group_code` varchar(255) DEFAULT NULL COMMENT 'Tab Group Code',
  PRIMARY KEY (`attribute_group_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_ATTRIBUTE_GROUP_NAME` (`attribute_set_id`,`attribute_group_name`),
  KEY `EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`),
  CONSTRAINT `EAV_ATTR_GROUP_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Group';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_attribute_group`
--

LOCK TABLES `eav_attribute_group` WRITE;
/*!40000 ALTER TABLE `eav_attribute_group` DISABLE KEYS */;
INSERT INTO `eav_attribute_group` VALUES (1,1,'General',1,1,'general',NULL),(2,2,'General',1,1,'general',NULL),(3,3,'General',10,1,'general',NULL),(4,3,'General Information',2,0,'general-information',NULL),(5,3,'Display Settings',20,0,'display-settings',NULL),(6,3,'Custom Design',30,0,'custom-design',NULL),(7,4,'Product Details',10,1,'product-details','basic'),(8,4,'Advanced Pricing',40,0,'advanced-pricing','advanced'),(9,4,'Search Engine Optimization',30,0,'search-engine-optimization','basic'),(10,4,'Images',20,0,'image-management','basic'),(11,4,'Design',50,0,'design','advanced'),(12,4,'Autosettings',60,0,'autosettings','advanced'),(13,4,'Content',15,0,'content','basic'),(14,4,'Schedule Design Update',55,0,'schedule-design-update','advanced'),(15,4,'Bundle Items',16,0,'bundle-items',NULL),(16,5,'General',1,1,'general',NULL),(17,6,'General',1,1,'general',NULL),(18,7,'General',1,1,'general',NULL),(19,8,'General',1,1,'general',NULL),(20,4,'Gift Options',61,0,'gift-options',NULL),(21,9,'Gift Options',10,0,'gift-options',NULL),(22,9,'Autosettings',9,0,'autosettings','advanced'),(23,9,'Schedule Design Update',8,0,'schedule-design-update','advanced'),(24,9,'Design',7,0,'design','advanced'),(25,9,'Advanced Pricing',6,0,'advanced-pricing','advanced'),(26,9,'Search Engine Optimization',5,0,'search-engine-optimization','basic'),(27,9,'Images',4,0,'image-management','basic'),(28,9,'Bundle Items',3,0,'bundle-items',NULL),(29,9,'Content',2,0,'content','basic'),(30,9,'Product Details',1,1,'product-details','basic'),(31,9,'Mezcal',11,0,'mezcal',NULL);
/*!40000 ALTER TABLE `eav_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_label`
--

DROP TABLE IF EXISTS `eav_attribute_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_attribute_label` (
  `attribute_label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Label Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`attribute_label_id`),
  KEY `EAV_ATTRIBUTE_LABEL_STORE_ID` (`store_id`),
  KEY `EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_STORE_ID` (`attribute_id`,`store_id`),
  CONSTRAINT `EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ATTRIBUTE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Label';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_attribute_label`
--

LOCK TABLES `eav_attribute_label` WRITE;
/*!40000 ALTER TABLE `eav_attribute_label` DISABLE KEYS */;
INSERT INTO `eav_attribute_label` VALUES (86,142,1,'Agave'),(87,143,1,'Agua de fermentación'),(88,144,1,'Año'),(89,145,1,'Botellas por caja'),(90,146,1,'Cocimiento'),(91,148,1,'Entrega'),(92,149,1,'Estado'),(93,150,1,'Fermentacion'),(96,153,1,'region'),(97,154,1,'Litros destilados'),(98,155,1,'Maduración (años)'),(100,157,1,'Maguey'),(101,158,1,'Molido'),(102,159,1,'Notas de Cata'),(103,160,1,'No. de destilaciones'),(104,161,1,'Origen de planta'),(105,162,1,'Presentación (cl)'),(106,163,1,'Región'),(107,164,1,'Terruño'),(108,165,1,'Tipo de Abocado'),(109,166,1,'Tipo de ajuste'),(110,167,1,'Tipo de destilador'),(111,168,1,'Variedad'),(112,169,1,'Grados de alcohol (%)'),(114,152,1,'Maguey'),(115,83,1,'Casa / Marca'),(117,156,1,'Maestro Mezcalero'),(118,77,1,'Precio');
/*!40000 ALTER TABLE `eav_attribute_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_option`
--

DROP TABLE IF EXISTS `eav_attribute_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_attribute_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_attribute_option`
--

LOCK TABLES `eav_attribute_option` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option` DISABLE KEYS */;
INSERT INTO `eav_attribute_option` VALUES (1,20,0),(2,20,1),(3,20,3),(4,148,1),(5,148,2),(6,149,1),(7,149,2),(8,149,3),(13,152,1),(14,153,1),(15,83,1),(16,83,2),(17,83,3),(18,83,4),(19,83,5),(20,83,6),(21,83,7),(22,83,8),(23,83,9),(24,83,10),(25,83,11),(26,83,12),(27,83,13),(28,83,14),(29,171,1),(30,171,2);
/*!40000 ALTER TABLE `eav_attribute_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_option_swatch`
--

DROP TABLE IF EXISTS `eav_attribute_option_swatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_attribute_option_swatch` (
  `swatch_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Swatch ID',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `type` smallint(5) unsigned NOT NULL COMMENT 'Swatch type: 0 - text, 1 - visual color, 2 - visual image',
  `value` varchar(255) DEFAULT NULL COMMENT 'Swatch Value',
  PRIMARY KEY (`swatch_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_OPTION_SWATCH_STORE_ID_OPTION_ID` (`store_id`,`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_SWATCH_SWATCH_ID` (`swatch_id`),
  KEY `EAV_ATTR_OPT_SWATCH_OPT_ID_EAV_ATTR_OPT_OPT_ID` (`option_id`),
  CONSTRAINT `EAV_ATTRIBUTE_OPTION_SWATCH_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ATTR_OPT_SWATCH_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magento Swatches table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_attribute_option_swatch`
--

LOCK TABLES `eav_attribute_option_swatch` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option_swatch` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_attribute_option_swatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_option_value`
--

DROP TABLE IF EXISTS `eav_attribute_option_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_attribute_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  KEY `EAV_ATTRIBUTE_OPTION_VALUE_OPTION_ID` (`option_id`),
  KEY `EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ATTR_OPT_VAL_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option Value';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_attribute_option_value`
--

LOCK TABLES `eav_attribute_option_value` WRITE;
/*!40000 ALTER TABLE `eav_attribute_option_value` DISABLE KEYS */;
INSERT INTO `eav_attribute_option_value` VALUES (1,1,0,'Male'),(2,2,0,'Female'),(3,3,0,'Not Specified'),(4,4,1,'Especial'),(5,4,0,'Especial'),(6,5,1,'Inmediata'),(7,5,0,'Inmediata'),(8,6,1,'Durango'),(9,6,0,'Durango'),(10,7,1,'Guerrero'),(11,7,0,'Guerrero'),(12,8,1,'Oaxaca'),(13,8,0,'Oaxaca'),(24,14,1,'nombre-de-diosa<'),(25,14,0,'nombre-de-dios'),(56,13,1,'Silvestre tobalá'),(57,13,0,'silvestre-tobala'),(58,15,1,'Meteoro'),(59,15,0,'Meteoro'),(60,16,1,'Sin Piedad'),(61,16,0,'Sin Piedad'),(62,17,1,'Mayalen'),(63,17,0,'Mayalen'),(64,18,1,'Mexxico '),(65,18,0,'Mexxico '),(66,19,1,'Chaneque'),(67,19,0,'Chaneque'),(68,20,1,'Bruxo'),(69,20,0,'Bruxo'),(70,21,1,'IBÁ'),(71,21,0,'IBÁ'),(72,22,1,'Doña Natalia'),(73,22,0,'Doña Natalia'),(74,23,1,'Marca Negra'),(75,23,0,'Marca Negra'),(76,24,1,'Pescador de Sueños'),(77,24,0,'Pescador de Sueños'),(78,25,1,'Papadiablo'),(79,25,0,'Papadiablo'),(80,26,1,'Nakawé'),(81,26,0,'Nakawé'),(82,27,1,'Machetazo'),(83,27,0,'Machetazo'),(84,28,1,'El hijo del Santo'),(85,28,0,'El hijo del Santo'),(86,29,1,'Caja'),(87,29,0,'Caja'),(88,30,1,'Botella'),(89,30,0,'Botella');
/*!40000 ALTER TABLE `eav_attribute_option_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_attribute_set`
--

DROP TABLE IF EXISTS `eav_attribute_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_attribute_set` (
  `attribute_set_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Set Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Set Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`attribute_set_id`),
  UNIQUE KEY `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_ATTRIBUTE_SET_NAME` (`entity_type_id`,`attribute_set_name`),
  KEY `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_SORT_ORDER` (`entity_type_id`,`sort_order`),
  CONSTRAINT `EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Set';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_attribute_set`
--

LOCK TABLES `eav_attribute_set` WRITE;
/*!40000 ALTER TABLE `eav_attribute_set` DISABLE KEYS */;
INSERT INTO `eav_attribute_set` VALUES (1,1,'Default',2),(2,2,'Default',2),(3,3,'Default',1),(4,4,'Default',1),(5,5,'Default',1),(6,6,'Default',1),(7,7,'Default',1),(8,8,'Default',1),(9,4,'Mezcal',0);
/*!40000 ALTER TABLE `eav_attribute_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity`
--

DROP TABLE IF EXISTS `eav_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Entity Active',
  PRIMARY KEY (`entity_id`),
  KEY `EAV_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_ENTITY_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity`
--

LOCK TABLES `eav_entity` WRITE;
/*!40000 ALTER TABLE `eav_entity` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_attribute`
--

DROP TABLE IF EXISTS `eav_entity_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity_attribute` (
  `entity_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`entity_attribute_id`),
  UNIQUE KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_ATTRIBUTE_ID` (`attribute_set_id`,`attribute_id`),
  UNIQUE KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_GROUP_ID_ATTRIBUTE_ID` (`attribute_group_id`,`attribute_id`),
  KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`),
  KEY `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTT_ATTR_ATTR_GROUP_ID_EAV_ATTR_GROUP_ATTR_GROUP_ID` FOREIGN KEY (`attribute_group_id`) REFERENCES `eav_attribute_group` (`attribute_group_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=696 DEFAULT CHARSET=utf8 COMMENT='Eav Entity Attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity_attribute`
--

LOCK TABLES `eav_entity_attribute` WRITE;
/*!40000 ALTER TABLE `eav_entity_attribute` DISABLE KEYS */;
INSERT INTO `eav_entity_attribute` VALUES (1,1,1,1,1,10),(2,1,1,1,2,20),(3,1,1,1,3,20),(4,1,1,1,4,30),(5,1,1,1,5,40),(6,1,1,1,6,50),(7,1,1,1,7,60),(8,1,1,1,8,70),(9,1,1,1,9,80),(10,1,1,1,10,25),(11,1,1,1,11,90),(12,1,1,1,12,81),(13,1,1,1,13,115),(14,1,1,1,14,120),(15,1,1,1,15,82),(16,1,1,1,16,83),(17,1,1,1,17,100),(18,1,1,1,18,85),(19,1,1,1,19,86),(20,1,1,1,20,110),(21,1,1,1,21,121),(22,2,2,2,22,10),(23,2,2,2,23,20),(24,2,2,2,24,30),(25,2,2,2,25,40),(26,2,2,2,26,50),(27,2,2,2,27,60),(28,2,2,2,28,70),(29,2,2,2,29,80),(30,2,2,2,30,90),(31,2,2,2,31,100),(32,2,2,2,32,100),(33,2,2,2,33,110),(34,2,2,2,34,120),(35,2,2,2,35,130),(36,2,2,2,36,131),(37,2,2,2,37,132),(38,2,2,2,38,133),(39,2,2,2,39,134),(40,2,2,2,40,135),(41,1,1,1,41,87),(42,1,1,1,42,100),(43,1,1,1,43,110),(44,1,1,1,44,120),(45,3,3,4,45,1),(46,3,3,4,46,2),(47,3,3,4,47,4),(48,3,3,4,48,5),(49,3,3,4,49,6),(50,3,3,4,50,7),(51,3,3,4,51,8),(52,3,3,5,52,10),(53,3,3,5,53,20),(54,3,3,5,54,30),(55,3,3,4,55,12),(56,3,3,4,56,13),(57,3,3,4,57,14),(58,3,3,4,58,15),(59,3,3,4,59,16),(60,3,3,6,60,10),(61,3,3,6,61,30),(62,3,3,6,62,40),(63,3,3,6,63,50),(64,3,3,6,64,60),(65,3,3,4,65,24),(66,3,3,4,66,25),(67,3,3,5,67,40),(68,3,3,5,68,50),(69,3,3,4,69,10),(70,3,3,6,70,5),(71,3,3,6,71,6),(72,3,3,5,72,51),(73,4,4,7,73,10),(74,4,4,7,74,20),(75,4,4,13,75,90),(76,4,4,13,76,100),(77,4,4,7,77,30),(78,4,4,8,78,3),(79,4,4,8,79,4),(80,4,4,8,80,5),(81,4,4,8,81,6),(82,4,4,7,82,70),(83,4,4,9,84,20),(84,4,4,9,85,30),(85,4,4,9,86,40),(86,4,4,10,87,1),(87,4,4,10,88,2),(88,4,4,10,89,3),(89,4,4,10,90,4),(90,4,4,7,91,6),(91,4,4,8,92,7),(92,4,4,7,94,90),(93,4,4,7,95,100),(94,4,4,10,96,5),(95,4,4,7,97,5),(96,4,4,8,98,8),(97,4,4,7,99,80),(98,4,4,14,100,40),(99,4,4,14,101,20),(100,4,4,14,102,30),(101,4,4,11,103,10),(102,4,4,11,104,5),(103,4,4,7,105,80),(104,4,4,11,106,6),(105,4,4,7,107,14),(106,4,4,7,108,15),(107,4,4,7,109,16),(108,4,4,7,110,17),(109,4,4,7,111,18),(110,4,4,7,112,19),(111,4,4,7,113,20),(112,4,4,7,114,110),(113,4,4,7,115,60),(114,4,4,14,116,50),(115,3,3,4,117,3),(116,3,3,4,118,17),(117,4,4,9,119,10),(118,4,4,7,120,11),(119,4,4,8,121,9),(120,4,4,8,122,10),(121,4,4,7,123,31),(122,4,4,7,124,21),(123,4,4,7,125,71),(124,4,4,8,126,11),(125,4,4,15,127,1),(126,4,4,7,128,111),(127,4,4,7,129,112),(128,4,4,7,130,113),(129,4,4,7,131,114),(130,4,4,10,132,3),(131,4,4,7,133,40),(132,4,4,20,134,10),(133,3,3,4,135,50),(134,3,3,4,136,5),(135,3,3,4,137,5),(136,4,4,7,138,115),(137,3,3,4,139,50),(138,3,3,4,140,51),(139,3,3,4,141,52),(167,4,9,25,98,8),(203,4,9,30,91,6),(207,4,9,30,120,11),(209,4,9,30,107,14),(211,4,9,30,108,15),(213,4,9,30,109,16),(215,4,9,30,110,17),(217,4,9,30,111,18),(219,4,9,30,112,19),(223,4,9,30,113,20),(249,4,9,30,128,111),(251,4,9,30,129,112),(253,4,9,30,130,113),(255,4,9,30,131,114),(549,4,9,30,73,2),(551,4,9,30,74,3),(553,4,9,30,77,5),(555,4,9,30,82,9),(557,4,9,30,94,13),(559,4,9,30,95,14),(561,4,9,30,97,1),(563,4,9,30,99,11),(565,4,9,30,105,12),(567,4,9,30,114,15),(569,4,9,30,115,8),(571,4,9,30,123,6),(573,4,9,30,124,4),(575,4,9,30,125,10),(577,4,9,30,133,7),(579,4,9,30,138,16),(581,4,9,29,75,1),(583,4,9,29,76,2),(585,4,9,28,127,1),(587,4,9,27,87,1),(589,4,9,27,88,2),(591,4,9,27,89,3),(593,4,9,27,90,5),(595,4,9,27,96,6),(597,4,9,27,132,4),(599,4,9,26,84,2),(601,4,9,26,85,3),(603,4,9,26,86,4),(605,4,9,26,119,1),(607,4,9,25,78,1),(609,4,9,25,79,2),(611,4,9,25,80,3),(613,4,9,25,81,4),(615,4,9,25,92,5),(617,4,9,25,121,6),(619,4,9,25,122,7),(621,4,9,25,126,8),(623,4,9,24,103,3),(625,4,9,24,104,1),(627,4,9,24,106,2),(629,4,9,23,100,3),(631,4,9,23,101,1),(633,4,9,23,102,2),(635,4,9,23,116,4),(637,4,9,21,134,1),(639,4,9,31,83,6),(641,4,9,31,142,11),(643,4,9,31,143,24),(645,4,9,31,144,17),(647,4,9,31,145,9),(649,4,9,31,146,22),(651,4,9,31,148,5),(653,4,9,31,149,13),(655,4,9,31,150,25),(659,4,9,31,152,4),(661,4,9,31,153,2),(663,4,9,31,154,18),(665,4,9,31,155,20),(667,4,9,31,156,15),(669,4,9,31,157,12),(671,4,9,31,158,23),(673,4,9,31,159,10),(675,4,9,31,160,27),(677,4,9,31,161,19),(679,4,9,31,162,8),(681,4,9,31,163,14),(683,4,9,31,164,21),(685,4,9,31,165,29),(687,4,9,31,166,28),(689,4,9,31,167,26),(691,4,9,31,168,7),(693,4,9,31,169,16),(695,4,9,31,170,1);
/*!40000 ALTER TABLE `eav_entity_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_datetime`
--

DROP TABLE IF EXISTS `eav_entity_datetime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_DATETIME_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_DATETIME_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_DATETIME_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `EAV_ENTITY_DATETIME_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_DATETIME_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTT_DTIME_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity_datetime`
--

LOCK TABLES `eav_entity_datetime` WRITE;
/*!40000 ALTER TABLE `eav_entity_datetime` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_entity_datetime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_decimal`
--

DROP TABLE IF EXISTS `eav_entity_decimal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_DECIMAL_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `EAV_ENTITY_DECIMAL_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_DECIMAL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity_decimal`
--

LOCK TABLES `eav_entity_decimal` WRITE;
/*!40000 ALTER TABLE `eav_entity_decimal` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_entity_decimal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_int`
--

DROP TABLE IF EXISTS `eav_entity_int`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_INT_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_INT_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_INT_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `EAV_ENTITY_INT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_INT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_INT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity_int`
--

LOCK TABLES `eav_entity_int` WRITE;
/*!40000 ALTER TABLE `eav_entity_int` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_entity_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_store`
--

DROP TABLE IF EXISTS `eav_entity_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity_store` (
  `entity_store_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Store Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `increment_prefix` varchar(20) DEFAULT NULL COMMENT 'Increment Prefix',
  `increment_last_id` varchar(50) DEFAULT NULL COMMENT 'Last Incremented Id',
  PRIMARY KEY (`entity_store_id`),
  KEY `EAV_ENTITY_STORE_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_ENTITY_STORE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Store';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity_store`
--

LOCK TABLES `eav_entity_store` WRITE;
/*!40000 ALTER TABLE `eav_entity_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_entity_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_text`
--

DROP TABLE IF EXISTS `eav_entity_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_TEXT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `EAV_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `EAV_ENTITY_TEXT_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_ENTITY_TEXT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_TEXT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_TEXT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity_text`
--

LOCK TABLES `eav_entity_text` WRITE;
/*!40000 ALTER TABLE `eav_entity_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_entity_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_type`
--

DROP TABLE IF EXISTS `eav_entity_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity_type` (
  `entity_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Type Id',
  `entity_type_code` varchar(50) NOT NULL COMMENT 'Entity Type Code',
  `entity_model` varchar(255) NOT NULL COMMENT 'Entity Model',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `entity_table` varchar(255) DEFAULT NULL COMMENT 'Entity Table',
  `value_table_prefix` varchar(255) DEFAULT NULL COMMENT 'Value Table Prefix',
  `entity_id_field` varchar(255) DEFAULT NULL COMMENT 'Entity Id Field',
  `is_data_sharing` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Data Sharing',
  `data_sharing_key` varchar(100) DEFAULT 'default' COMMENT 'Data Sharing Key',
  `default_attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Attribute Set Id',
  `increment_model` varchar(255) DEFAULT NULL COMMENT 'Increment Model',
  `increment_per_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Increment Per Store',
  `increment_pad_length` smallint(5) unsigned NOT NULL DEFAULT '8' COMMENT 'Increment Pad Length',
  `increment_pad_char` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Increment Pad Char',
  `additional_attribute_table` varchar(255) DEFAULT NULL COMMENT 'Additional Attribute Table',
  `entity_attribute_collection` varchar(255) DEFAULT NULL COMMENT 'Entity Attribute Collection',
  PRIMARY KEY (`entity_type_id`),
  KEY `EAV_ENTITY_TYPE_ENTITY_TYPE_CODE` (`entity_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Eav Entity Type';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity_type`
--

LOCK TABLES `eav_entity_type` WRITE;
/*!40000 ALTER TABLE `eav_entity_type` DISABLE KEYS */;
INSERT INTO `eav_entity_type` VALUES (1,'customer','Magento\\Customer\\Model\\ResourceModel\\Customer','Magento\\Customer\\Model\\Attribute','customer_entity',NULL,NULL,1,'default',1,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',0,8,'0','customer_eav_attribute','Magento\\Customer\\Model\\ResourceModel\\Attribute\\Collection'),(2,'customer_address','Magento\\Customer\\Model\\ResourceModel\\Address','Magento\\Customer\\Model\\Attribute','customer_address_entity',NULL,NULL,1,'default',2,NULL,0,8,'0','customer_eav_attribute','Magento\\Customer\\Model\\ResourceModel\\Address\\Attribute\\Collection'),(3,'catalog_category','Magento\\Catalog\\Model\\ResourceModel\\Category','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','catalog_category_entity',NULL,NULL,1,'default',3,NULL,0,8,'0','catalog_eav_attribute','Magento\\Catalog\\Model\\ResourceModel\\Category\\Attribute\\Collection'),(4,'catalog_product','Magento\\Catalog\\Model\\ResourceModel\\Product','Magento\\Catalog\\Model\\ResourceModel\\Eav\\Attribute','catalog_product_entity',NULL,NULL,1,'default',4,NULL,0,8,'0','catalog_eav_attribute','Magento\\Catalog\\Model\\ResourceModel\\Product\\Attribute\\Collection'),(5,'order','Magento\\Sales\\Model\\ResourceModel\\Order',NULL,'sales_order',NULL,NULL,1,'default',5,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(6,'invoice','Magento\\Sales\\Model\\ResourceModel\\Order',NULL,'sales_invoice',NULL,NULL,1,'default',6,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(7,'creditmemo','Magento\\Sales\\Model\\ResourceModel\\Order\\Creditmemo',NULL,'sales_creditmemo',NULL,NULL,1,'default',7,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL),(8,'shipment','Magento\\Sales\\Model\\ResourceModel\\Order\\Shipment',NULL,'sales_shipment',NULL,NULL,1,'default',8,'Magento\\Eav\\Model\\Entity\\Increment\\NumericValue',1,8,'0',NULL,NULL);
/*!40000 ALTER TABLE `eav_entity_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_entity_varchar`
--

DROP TABLE IF EXISTS `eav_entity_varchar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `EAV_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `EAV_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  KEY `EAV_ENTITY_VARCHAR_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `EAV_ENTITY_VARCHAR_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_ENTITY_VARCHAR_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_entity_varchar`
--

LOCK TABLES `eav_entity_varchar` WRITE;
/*!40000 ALTER TABLE `eav_entity_varchar` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_entity_varchar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_element`
--

DROP TABLE IF EXISTS `eav_form_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_form_element` (
  `element_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Element Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `fieldset_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Fieldset Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`element_id`),
  UNIQUE KEY `EAV_FORM_ELEMENT_TYPE_ID_ATTRIBUTE_ID` (`type_id`,`attribute_id`),
  KEY `EAV_FORM_ELEMENT_FIELDSET_ID` (`fieldset_id`),
  KEY `EAV_FORM_ELEMENT_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `EAV_FORM_ELEMENT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_FORM_ELEMENT_FIELDSET_ID_EAV_FORM_FIELDSET_FIELDSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE SET NULL,
  CONSTRAINT `EAV_FORM_ELEMENT_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Eav Form Element';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_form_element`
--

LOCK TABLES `eav_form_element` WRITE;
/*!40000 ALTER TABLE `eav_form_element` DISABLE KEYS */;
INSERT INTO `eav_form_element` VALUES (1,1,NULL,23,0),(2,1,NULL,25,1),(3,1,NULL,27,2),(4,1,NULL,9,3),(5,1,NULL,28,4),(6,1,NULL,29,5),(7,1,NULL,31,6),(8,1,NULL,33,7),(9,1,NULL,30,8),(10,1,NULL,34,9),(11,1,NULL,35,10),(12,2,NULL,23,0),(13,2,NULL,25,1),(14,2,NULL,27,2),(15,2,NULL,9,3),(16,2,NULL,28,4),(17,2,NULL,29,5),(18,2,NULL,31,6),(19,2,NULL,33,7),(20,2,NULL,30,8),(21,2,NULL,34,9),(22,2,NULL,35,10),(23,3,NULL,23,0),(24,3,NULL,25,1),(25,3,NULL,27,2),(26,3,NULL,28,3),(27,3,NULL,29,4),(28,3,NULL,31,5),(29,3,NULL,33,6),(30,3,NULL,30,7),(31,3,NULL,34,8),(32,3,NULL,35,9),(33,4,NULL,23,0),(34,4,NULL,25,1),(35,4,NULL,27,2),(36,4,NULL,28,3),(37,4,NULL,29,4),(38,4,NULL,31,5),(39,4,NULL,33,6),(40,4,NULL,30,7),(41,4,NULL,34,8),(42,4,NULL,35,9);
/*!40000 ALTER TABLE `eav_form_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_fieldset`
--

DROP TABLE IF EXISTS `eav_form_fieldset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_form_fieldset` (
  `fieldset_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Fieldset Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`fieldset_id`),
  UNIQUE KEY `EAV_FORM_FIELDSET_TYPE_ID_CODE` (`type_id`,`code`),
  CONSTRAINT `EAV_FORM_FIELDSET_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_form_fieldset`
--

LOCK TABLES `eav_form_fieldset` WRITE;
/*!40000 ALTER TABLE `eav_form_fieldset` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_form_fieldset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_fieldset_label`
--

DROP TABLE IF EXISTS `eav_form_fieldset_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_form_fieldset_label` (
  `fieldset_id` smallint(5) unsigned NOT NULL COMMENT 'Fieldset Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`fieldset_id`,`store_id`),
  KEY `EAV_FORM_FIELDSET_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_FORM_FIELDSET_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_FORM_FSET_LBL_FSET_ID_EAV_FORM_FSET_FSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset Label';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_form_fieldset_label`
--

LOCK TABLES `eav_form_fieldset_label` WRITE;
/*!40000 ALTER TABLE `eav_form_fieldset_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `eav_form_fieldset_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_type`
--

DROP TABLE IF EXISTS `eav_form_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_form_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `theme` varchar(64) DEFAULT NULL COMMENT 'Theme',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `EAV_FORM_TYPE_CODE_THEME_STORE_ID` (`code`,`theme`,`store_id`),
  KEY `EAV_FORM_TYPE_STORE_ID` (`store_id`),
  CONSTRAINT `EAV_FORM_TYPE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Eav Form Type';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_form_type`
--

LOCK TABLES `eav_form_type` WRITE;
/*!40000 ALTER TABLE `eav_form_type` DISABLE KEYS */;
INSERT INTO `eav_form_type` VALUES (1,'checkout_onepage_register','checkout_onepage_register',1,'',0),(2,'checkout_onepage_register_guest','checkout_onepage_register_guest',1,'',0),(3,'checkout_onepage_billing_address','checkout_onepage_billing_address',1,'',0),(4,'checkout_onepage_shipping_address','checkout_onepage_shipping_address',1,'',0);
/*!40000 ALTER TABLE `eav_form_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eav_form_type_entity`
--

DROP TABLE IF EXISTS `eav_form_type_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eav_form_type_entity` (
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `entity_type_id` smallint(5) unsigned NOT NULL COMMENT 'Entity Type Id',
  PRIMARY KEY (`type_id`,`entity_type_id`),
  KEY `EAV_FORM_TYPE_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  CONSTRAINT `EAV_FORM_TYPE_ENTITY_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE,
  CONSTRAINT `EAV_FORM_TYPE_ENTT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Type Entity';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eav_form_type_entity`
--

LOCK TABLES `eav_form_type_entity` WRITE;
/*!40000 ALTER TABLE `eav_form_type_entity` DISABLE KEYS */;
INSERT INTO `eav_form_type_entity` VALUES (1,1),(2,1),(1,2),(2,2),(3,2),(4,2);
/*!40000 ALTER TABLE `eav_form_type_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template ID',
  `template_code` varchar(150) NOT NULL COMMENT 'Template Name',
  `template_text` text NOT NULL COMMENT 'Template Content',
  `template_styles` text COMMENT 'Templste Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) NOT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of Template Creation',
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Template Modification',
  `orig_template_code` varchar(200) DEFAULT NULL COMMENT 'Original Template Code',
  `orig_template_variables` text COMMENT 'Original Template Variables',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `EMAIL_TEMPLATE_TEMPLATE_CODE` (`template_code`),
  KEY `EMAIL_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `EMAIL_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Email Templates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_template`
--

LOCK TABLES `email_template` WRITE;
/*!40000 ALTER TABLE `email_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flag`
--

DROP TABLE IF EXISTS `flag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flag` (
  `flag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Flag Id',
  `flag_code` varchar(255) NOT NULL COMMENT 'Flag Code',
  `state` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag State',
  `flag_data` text COMMENT 'Flag Data',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Last Flag Update',
  PRIMARY KEY (`flag_id`),
  KEY `FLAG_LAST_UPDATE` (`last_update`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Flag';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flag`
--

LOCK TABLES `flag` WRITE;
/*!40000 ALTER TABLE `flag` DISABLE KEYS */;
INSERT INTO `flag` VALUES (1,'report_order_aggregated',0,NULL,'2020-03-12 02:53:18'),(2,'report_tax_aggregated',0,NULL,'2020-03-12 02:53:18'),(3,'report_shipping_aggregated',0,NULL,'2020-03-12 02:53:18'),(4,'report_invoiced_aggregated',0,NULL,'2020-03-12 02:53:18'),(5,'report_refunded_aggregated',0,NULL,'2020-03-12 02:53:18'),(6,'report_coupons_aggregated',0,NULL,'2020-03-12 02:53:18'),(7,'report_bestsellers_aggregated',0,NULL,'2020-03-12 02:53:18'),(8,'report_product_viewed_aggregated',0,NULL,'2020-03-12 02:53:18');
/*!40000 ALTER TABLE `flag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_message`
--

DROP TABLE IF EXISTS `gift_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_message` (
  `gift_message_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'GiftMessage Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `sender` varchar(255) DEFAULT NULL COMMENT 'Sender',
  `recipient` varchar(255) DEFAULT NULL COMMENT 'Registrant',
  `message` text COMMENT 'Message',
  PRIMARY KEY (`gift_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Gift Message';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_message`
--

LOCK TABLES `gift_message` WRITE;
/*!40000 ALTER TABLE `gift_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `googleoptimizer_code`
--

DROP TABLE IF EXISTS `googleoptimizer_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleoptimizer_code` (
  `code_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Google experiment code id',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Optimized entity id product id or catalog id',
  `entity_type` varchar(50) DEFAULT NULL COMMENT 'Optimized entity type',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store id',
  `experiment_script` text COMMENT 'Google experiment script',
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `GOOGLEOPTIMIZER_CODE_STORE_ID_ENTITY_ID_ENTITY_TYPE` (`store_id`,`entity_id`,`entity_type`),
  CONSTRAINT `GOOGLEOPTIMIZER_CODE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Google Experiment code';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `googleoptimizer_code`
--

LOCK TABLES `googleoptimizer_code` WRITE;
/*!40000 ALTER TABLE `googleoptimizer_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `googleoptimizer_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hozmegamenu`
--

DROP TABLE IF EXISTS `hozmegamenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hozmegamenu` (
  `hozmegamenu_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'hozmegamenu ID',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'hozmegamenu status',
  `type_menu` smallint(6) NOT NULL DEFAULT '1' COMMENT 'hozmegamenu type_menu',
  `is_home` smallint(6) NOT NULL DEFAULT '1' COMMENT 'hozmegamenu is_home',
  `is_mobile` smallint(6) NOT NULL DEFAULT '1' COMMENT 'hozmegamenu is_home',
  `is_new` varchar(255) DEFAULT '' COMMENT 'hozmegamenu is_new',
  `is_sale` varchar(255) DEFAULT '' COMMENT 'hozmegamenu is_sale',
  `is_level` varchar(255) DEFAULT '' COMMENT 'hozmegamenu Level',
  `is_column` varchar(255) DEFAULT '' COMMENT 'hozmegamenu Column',
  `items` varchar(255) DEFAULT '' COMMENT 'hozmegamenu items',
  `is_link` varchar(255) DEFAULT '' COMMENT 'hozmegamenu is_link',
  `effect` varchar(255) DEFAULT '' COMMENT 'hozmegamenu Effect',
  `image` varchar(255) DEFAULT NULL COMMENT 'hozmegamenu image',
  `store` smallint(6) NOT NULL DEFAULT '0' COMMENT 'store',
  `items_show` varchar(255) DEFAULT NULL COMMENT 'Items Show',
  PRIMARY KEY (`hozmegamenu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='hozmegamenu';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hozmegamenu`
--

LOCK TABLES `hozmegamenu` WRITE;
/*!40000 ALTER TABLE `hozmegamenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `hozmegamenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_history`
--

DROP TABLE IF EXISTS `import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'History record Id',
  `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Started at',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `imported_file` varchar(255) DEFAULT NULL COMMENT 'Imported file',
  `execution_time` varchar(255) DEFAULT NULL COMMENT 'Execution time',
  `summary` varchar(255) DEFAULT NULL COMMENT 'Summary',
  `error_file` varchar(255) NOT NULL COMMENT 'Imported file with errors',
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import history table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_history`
--

LOCK TABLES `import_history` WRITE;
/*!40000 ALTER TABLE `import_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `importexport_importdata`
--

DROP TABLE IF EXISTS `importexport_importdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `importexport_importdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity` varchar(50) NOT NULL COMMENT 'Entity',
  `behavior` varchar(10) NOT NULL DEFAULT 'append' COMMENT 'Behavior',
  `data` longtext COMMENT 'Data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import Data Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `importexport_importdata`
--

LOCK TABLES `importexport_importdata` WRITE;
/*!40000 ALTER TABLE `importexport_importdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `importexport_importdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indexer_state`
--

DROP TABLE IF EXISTS `indexer_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indexer_state` (
  `state_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Indexer State Id',
  `indexer_id` varchar(255) DEFAULT NULL COMMENT 'Indexer Id',
  `status` varchar(16) DEFAULT 'invalid' COMMENT 'Indexer Status',
  `updated` datetime DEFAULT NULL COMMENT 'Indexer Status',
  `hash_config` varchar(32) NOT NULL COMMENT 'Hash of indexer config',
  PRIMARY KEY (`state_id`),
  KEY `INDEXER_STATE_INDEXER_ID` (`indexer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Indexer State';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indexer_state`
--

LOCK TABLES `indexer_state` WRITE;
/*!40000 ALTER TABLE `indexer_state` DISABLE KEYS */;
INSERT INTO `indexer_state` VALUES (1,'design_config_grid','valid','2020-03-13 23:10:29','27baa8fe6a5369f52c8b7cbd54a3c3c4'),(2,'customer_grid','valid','2020-03-13 23:10:29','d572ea00944c9e3f517b3f46bad058a4'),(3,'catalog_category_product','valid','2020-03-13 23:10:29','57b48d3cf1fcd64abe6b01dea3173d02'),(4,'catalog_product_category','valid','2020-02-28 15:35:13','9957f66909342cc58ff2703dcd268bf4'),(5,'catalog_product_price','valid','2020-03-13 23:10:30','15a819a577a149220cd0722c291de721'),(6,'catalog_product_attribute','valid','2020-03-13 23:10:30','77eed0bf72b16099d299d0ab47b74910'),(7,'cataloginventory_stock','valid','2020-03-13 23:10:30','78a405fd852458c326c85096099d7d5e'),(8,'catalogrule_rule','valid','2020-03-13 23:10:30','5afe3cacdcb52ec3a7e68dc245679021'),(9,'catalogrule_product','valid','2020-02-28 15:35:14','0ebee9e52ed424273132e8227fe646f3'),(10,'catalogsearch_fulltext','valid','2020-03-13 23:10:30','4486b57e2021aa78b526c68c9af2dcab');
/*!40000 ALTER TABLE `indexer_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integration`
--

DROP TABLE IF EXISTS `integration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration` (
  `integration_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Integration ID',
  `name` varchar(255) NOT NULL COMMENT 'Integration name is displayed in the admin interface',
  `email` varchar(255) NOT NULL COMMENT 'Email address of the contact person',
  `endpoint` varchar(255) DEFAULT NULL COMMENT 'Endpoint for posting consumer credentials',
  `status` smallint(5) unsigned NOT NULL COMMENT 'Integration status',
  `consumer_id` int(10) unsigned DEFAULT NULL COMMENT 'Oauth consumer',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
  `setup_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Integration type - manual or config file',
  `identity_link_url` varchar(255) DEFAULT NULL COMMENT 'Identity linking Url',
  PRIMARY KEY (`integration_id`),
  UNIQUE KEY `INTEGRATION_NAME` (`name`),
  UNIQUE KEY `INTEGRATION_CONSUMER_ID` (`consumer_id`),
  CONSTRAINT `INTEGRATION_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='integration';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integration`
--

LOCK TABLES `integration` WRITE;
/*!40000 ALTER TABLE `integration` DISABLE KEYS */;
/*!40000 ALTER TABLE `integration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `layout_link`
--

DROP TABLE IF EXISTS `layout_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layout_link` (
  `layout_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Defines whether Layout Update is Temporary',
  PRIMARY KEY (`layout_link_id`),
  KEY `LAYOUT_LINK_LAYOUT_UPDATE_ID` (`layout_update_id`),
  KEY `LAYOUT_LINK_STORE_ID_THEME_ID_LAYOUT_UPDATE_ID_IS_TEMPORARY` (`store_id`,`theme_id`,`layout_update_id`,`is_temporary`),
  KEY `LAYOUT_LINK_THEME_ID_THEME_THEME_ID` (`theme_id`),
  CONSTRAINT `LAYOUT_LINK_LAYOUT_UPDATE_ID_LAYOUT_UPDATE_LAYOUT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE,
  CONSTRAINT `LAYOUT_LINK_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `LAYOUT_LINK_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Layout Link';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layout_link`
--

LOCK TABLES `layout_link` WRITE;
/*!40000 ALTER TABLE `layout_link` DISABLE KEYS */;
INSERT INTO `layout_link` VALUES (3,1,6,3,0);
/*!40000 ALTER TABLE `layout_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `layout_update`
--

DROP TABLE IF EXISTS `layout_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layout_update` (
  `layout_update_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Layout Update Id',
  `handle` varchar(255) DEFAULT NULL COMMENT 'Handle',
  `xml` text COMMENT 'Xml',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Last Update Timestamp',
  PRIMARY KEY (`layout_update_id`),
  KEY `LAYOUT_UPDATE_HANDLE` (`handle`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Layout Updates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layout_update`
--

LOCK TABLES `layout_update` WRITE;
/*!40000 ALTER TABLE `layout_update` DISABLE KEYS */;
INSERT INTO `layout_update` VALUES (3,'catalog_category_view','<body><referenceContainer name=\"footer\"><block class=\"Magento\\Cms\\Block\\Widget\\Block\" name=\"bd5c827707940124e885ae064ecfe8a2\" template=\"widget/static_block/default.phtml\"><action method=\"setData\"><argument name=\"name\" xsi:type=\"string\">block_id</argument><argument name=\"value\" xsi:type=\"string\">25</argument></action></block></referenceContainer></body>',1,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `layout_update` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_state`
--

DROP TABLE IF EXISTS `mview_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mview_state` (
  `state_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'View State Id',
  `view_id` varchar(255) DEFAULT NULL COMMENT 'View Id',
  `mode` varchar(16) DEFAULT 'disabled' COMMENT 'View Mode',
  `status` varchar(16) DEFAULT 'idle' COMMENT 'View Status',
  `updated` datetime DEFAULT NULL COMMENT 'View updated time',
  `version_id` int(10) unsigned DEFAULT NULL COMMENT 'View Version Id',
  PRIMARY KEY (`state_id`),
  KEY `MVIEW_STATE_VIEW_ID` (`view_id`),
  KEY `MVIEW_STATE_MODE` (`mode`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='View State';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_state`
--

LOCK TABLES `mview_state` WRITE;
/*!40000 ALTER TABLE `mview_state` DISABLE KEYS */;
INSERT INTO `mview_state` VALUES (1,'catalog_product_price','disabled','idle','2020-03-13 23:02:19',NULL);
/*!40000 ALTER TABLE `mview_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_problem`
--

DROP TABLE IF EXISTS `newsletter_problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_problem` (
  `problem_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Problem Id',
  `subscriber_id` int(10) unsigned DEFAULT NULL COMMENT 'Subscriber Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `problem_error_code` int(10) unsigned DEFAULT '0' COMMENT 'Problem Error Code',
  `problem_error_text` varchar(200) DEFAULT NULL COMMENT 'Problem Error Text',
  PRIMARY KEY (`problem_id`),
  KEY `NEWSLETTER_PROBLEM_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `NEWSLETTER_PROBLEM_QUEUE_ID` (`queue_id`),
  CONSTRAINT `NEWSLETTER_PROBLEM_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  CONSTRAINT `NLTTR_PROBLEM_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Problems';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_problem`
--

LOCK TABLES `newsletter_problem` WRITE;
/*!40000 ALTER TABLE `newsletter_problem` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_problem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_queue`
--

DROP TABLE IF EXISTS `newsletter_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_queue` (
  `queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Id',
  `template_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Template ID',
  `newsletter_type` int(11) DEFAULT NULL COMMENT 'Newsletter Type',
  `newsletter_text` text COMMENT 'Newsletter Text',
  `newsletter_styles` text COMMENT 'Newsletter Styles',
  `newsletter_subject` varchar(200) DEFAULT NULL COMMENT 'Newsletter Subject',
  `newsletter_sender_name` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Name',
  `newsletter_sender_email` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Email',
  `queue_status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Status',
  `queue_start_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Start At',
  `queue_finish_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Finish At',
  PRIMARY KEY (`queue_id`),
  KEY `NEWSLETTER_QUEUE_TEMPLATE_ID` (`template_id`),
  CONSTRAINT `NEWSLETTER_QUEUE_TEMPLATE_ID_NEWSLETTER_TEMPLATE_TEMPLATE_ID` FOREIGN KEY (`template_id`) REFERENCES `newsletter_template` (`template_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_queue`
--

LOCK TABLES `newsletter_queue` WRITE;
/*!40000 ALTER TABLE `newsletter_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_queue_link`
--

DROP TABLE IF EXISTS `newsletter_queue_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_queue_link` (
  `queue_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Link Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `subscriber_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subscriber Id',
  `letter_sent_at` timestamp NULL DEFAULT NULL COMMENT 'Letter Sent At',
  PRIMARY KEY (`queue_link_id`),
  KEY `NEWSLETTER_QUEUE_LINK_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `NEWSLETTER_QUEUE_LINK_QUEUE_ID_LETTER_SENT_AT` (`queue_id`,`letter_sent_at`),
  CONSTRAINT `NEWSLETTER_QUEUE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  CONSTRAINT `NLTTR_QUEUE_LNK_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Link';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_queue_link`
--

LOCK TABLES `newsletter_queue_link` WRITE;
/*!40000 ALTER TABLE `newsletter_queue_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_queue_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_queue_store_link`
--

DROP TABLE IF EXISTS `newsletter_queue_store_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_queue_store_link` (
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`queue_id`,`store_id`),
  KEY `NEWSLETTER_QUEUE_STORE_LINK_STORE_ID` (`store_id`),
  CONSTRAINT `NEWSLETTER_QUEUE_STORE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE,
  CONSTRAINT `NEWSLETTER_QUEUE_STORE_LINK_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Store Link';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_queue_store_link`
--

LOCK TABLES `newsletter_queue_store_link` WRITE;
/*!40000 ALTER TABLE `newsletter_queue_store_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_queue_store_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_subscriber`
--

DROP TABLE IF EXISTS `newsletter_subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_subscriber` (
  `subscriber_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Subscriber Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `change_status_at` timestamp NULL DEFAULT NULL COMMENT 'Change Status At',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `subscriber_email` varchar(150) DEFAULT NULL COMMENT 'Subscriber Email',
  `subscriber_status` int(11) NOT NULL DEFAULT '0' COMMENT 'Subscriber Status',
  `subscriber_confirm_code` varchar(32) DEFAULT 'NULL' COMMENT 'Subscriber Confirm Code',
  PRIMARY KEY (`subscriber_id`),
  KEY `NEWSLETTER_SUBSCRIBER_CUSTOMER_ID` (`customer_id`),
  KEY `NEWSLETTER_SUBSCRIBER_STORE_ID` (`store_id`),
  CONSTRAINT `NEWSLETTER_SUBSCRIBER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Newsletter Subscriber';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_subscriber`
--

LOCK TABLES `newsletter_subscriber` WRITE;
/*!40000 ALTER TABLE `newsletter_subscriber` DISABLE KEYS */;
INSERT INTO `newsletter_subscriber` VALUES (1,1,NULL,2,'dev@disandat.mx',1,'emgmeatqimip7iq7jyukwaawxee6q3am'),(2,1,NULL,4,'kdodeth@hotmail.com',1,'ilgxxfitlq5ibsf26amas1qu6uh5e2io');
/*!40000 ALTER TABLE `newsletter_subscriber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_template`
--

DROP TABLE IF EXISTS `newsletter_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template ID',
  `template_code` varchar(150) DEFAULT NULL COMMENT 'Template Code',
  `template_text` text COMMENT 'Template Text',
  `template_styles` text COMMENT 'Template Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) DEFAULT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `template_actual` smallint(5) unsigned DEFAULT '1' COMMENT 'Template Actual',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Added At',
  `modified_at` timestamp NULL DEFAULT NULL COMMENT 'Modified At',
  PRIMARY KEY (`template_id`),
  KEY `NEWSLETTER_TEMPLATE_TEMPLATE_ACTUAL` (`template_actual`),
  KEY `NEWSLETTER_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `NEWSLETTER_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Template';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_template`
--

LOCK TABLES `newsletter_template` WRITE;
/*!40000 ALTER TABLE `newsletter_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_consumer`
--

DROP TABLE IF EXISTS `oauth_consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_consumer` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `name` varchar(255) NOT NULL COMMENT 'Name of consumer',
  `key` varchar(32) NOT NULL COMMENT 'Key code',
  `secret` varchar(32) NOT NULL COMMENT 'Secret code',
  `callback_url` text COMMENT 'Callback URL',
  `rejected_callback_url` text NOT NULL COMMENT 'Rejected callback URL',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `OAUTH_CONSUMER_KEY` (`key`),
  UNIQUE KEY `OAUTH_CONSUMER_SECRET` (`secret`),
  KEY `OAUTH_CONSUMER_CREATED_AT` (`created_at`),
  KEY `OAUTH_CONSUMER_UPDATED_AT` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Consumers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_consumer`
--

LOCK TABLES `oauth_consumer` WRITE;
/*!40000 ALTER TABLE `oauth_consumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_consumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_nonce`
--

DROP TABLE IF EXISTS `oauth_nonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_nonce` (
  `nonce` varchar(32) NOT NULL COMMENT 'Nonce String',
  `timestamp` int(10) unsigned NOT NULL COMMENT 'Nonce Timestamp',
  `consumer_id` int(10) unsigned NOT NULL COMMENT 'Consumer ID',
  UNIQUE KEY `OAUTH_NONCE_NONCE_CONSUMER_ID` (`nonce`,`consumer_id`),
  KEY `OAUTH_NONCE_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` (`consumer_id`),
  CONSTRAINT `OAUTH_NONCE_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Nonce';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_nonce`
--

LOCK TABLES `oauth_nonce` WRITE;
/*!40000 ALTER TABLE `oauth_nonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_nonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_token`
--

DROP TABLE IF EXISTS `oauth_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `consumer_id` int(10) unsigned DEFAULT NULL COMMENT 'Oauth Consumer ID',
  `admin_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin user ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer user ID',
  `type` varchar(16) NOT NULL COMMENT 'Token Type',
  `token` varchar(32) NOT NULL COMMENT 'Token',
  `secret` varchar(32) NOT NULL COMMENT 'Token Secret',
  `verifier` varchar(32) DEFAULT NULL COMMENT 'Token Verifier',
  `callback_url` text NOT NULL COMMENT 'Token Callback URL',
  `revoked` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token revoked',
  `authorized` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token authorized',
  `user_type` int(11) DEFAULT NULL COMMENT 'User type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Token creation timestamp',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `OAUTH_TOKEN_TOKEN` (`token`),
  KEY `OAUTH_TOKEN_CONSUMER_ID` (`consumer_id`),
  KEY `OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` (`admin_id`),
  KEY `OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`),
  CONSTRAINT `OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`admin_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `OAUTH_TOKEN_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Tokens';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_token`
--

LOCK TABLES `oauth_token` WRITE;
/*!40000 ALTER TABLE `oauth_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_token_request_log`
--

DROP TABLE IF EXISTS `oauth_token_request_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_token_request_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log Id',
  `user_name` varchar(255) NOT NULL COMMENT 'Customer email or admin login',
  `user_type` smallint(5) unsigned NOT NULL COMMENT 'User type (admin or customer)',
  `failures_count` smallint(5) unsigned DEFAULT '0' COMMENT 'Number of failed authentication attempts in a row',
  `lock_expires_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Lock expiration time',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `OAUTH_TOKEN_REQUEST_LOG_USER_NAME_USER_TYPE` (`user_name`,`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log of token request authentication failures.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_token_request_log`
--

LOCK TABLES `oauth_token_request_log` WRITE;
/*!40000 ALTER TABLE `oauth_token_request_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_token_request_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_request_event`
--

DROP TABLE IF EXISTS `password_reset_request_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_reset_request_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `request_type` smallint(5) unsigned NOT NULL COMMENT 'Type of the event under a security control',
  `account_reference` varchar(255) DEFAULT NULL COMMENT 'An identifier for existing account or another target',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp when the event occurs',
  `ip` varchar(15) NOT NULL COMMENT 'Remote user IP',
  PRIMARY KEY (`id`),
  KEY `PASSWORD_RESET_REQUEST_EVENT_ACCOUNT_REFERENCE` (`account_reference`),
  KEY `PASSWORD_RESET_REQUEST_EVENT_CREATED_AT` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Password Reset Request Event under a security control';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_request_event`
--

LOCK TABLES `password_reset_request_event` WRITE;
/*!40000 ALTER TABLE `password_reset_request_event` DISABLE KEYS */;
INSERT INTO `password_reset_request_event` VALUES (1,1,'kdodeth@gmail.com','2020-03-03 05:39:44','201.114.112.189'),(2,0,'kdodeth@gmail.com','2020-03-03 05:41:48','201.114.112.189');
/*!40000 ALTER TABLE `password_reset_request_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_billing_agreement`
--

DROP TABLE IF EXISTS `paypal_billing_agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_billing_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `method_code` varchar(32) NOT NULL COMMENT 'Method Code',
  `reference_id` varchar(32) NOT NULL COMMENT 'Reference Id',
  `status` varchar(20) NOT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `agreement_label` varchar(255) DEFAULT NULL COMMENT 'Agreement Label',
  PRIMARY KEY (`agreement_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_CUSTOMER_ID` (`customer_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_STORE_ID` (`store_id`),
  CONSTRAINT `PAYPAL_BILLING_AGREEMENT_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PAYPAL_BILLING_AGREEMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_billing_agreement`
--

LOCK TABLES `paypal_billing_agreement` WRITE;
/*!40000 ALTER TABLE `paypal_billing_agreement` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_billing_agreement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_billing_agreement_order`
--

DROP TABLE IF EXISTS `paypal_billing_agreement_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_billing_agreement_order` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  PRIMARY KEY (`agreement_id`,`order_id`),
  KEY `PAYPAL_BILLING_AGREEMENT_ORDER_ORDER_ID` (`order_id`),
  CONSTRAINT `PAYPAL_BILLING_AGREEMENT_ORDER_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PAYPAL_BILLING_AGRT_ORDER_AGRT_ID_PAYPAL_BILLING_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `paypal_billing_agreement` (`agreement_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement Order';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_billing_agreement_order`
--

LOCK TABLES `paypal_billing_agreement_order` WRITE;
/*!40000 ALTER TABLE `paypal_billing_agreement_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_billing_agreement_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_cert`
--

DROP TABLE IF EXISTS `paypal_cert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_cert` (
  `cert_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cert Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `content` text COMMENT 'Content',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`cert_id`),
  KEY `PAYPAL_CERT_WEBSITE_ID` (`website_id`),
  CONSTRAINT `PAYPAL_CERT_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Certificate Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_cert`
--

LOCK TABLES `paypal_cert` WRITE;
/*!40000 ALTER TABLE `paypal_cert` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_cert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_payment_transaction`
--

DROP TABLE IF EXISTS `paypal_payment_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `PAYPAL_PAYMENT_TRANSACTION_TXN_ID` (`txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='PayPal Payflow Link Payment Transaction';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_payment_transaction`
--

LOCK TABLES `paypal_payment_transaction` WRITE;
/*!40000 ALTER TABLE `paypal_payment_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_payment_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_settlement_report`
--

DROP TABLE IF EXISTS `paypal_settlement_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_settlement_report` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Report Id',
  `report_date` timestamp NULL DEFAULT NULL COMMENT 'Report Date',
  `account_id` varchar(64) DEFAULT NULL COMMENT 'Account Id',
  `filename` varchar(24) DEFAULT NULL COMMENT 'Filename',
  `last_modified` timestamp NULL DEFAULT NULL COMMENT 'Last Modified',
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `PAYPAL_SETTLEMENT_REPORT_REPORT_DATE_ACCOUNT_ID` (`report_date`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_settlement_report`
--

LOCK TABLES `paypal_settlement_report` WRITE;
/*!40000 ALTER TABLE `paypal_settlement_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_settlement_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_settlement_report_row`
--

DROP TABLE IF EXISTS `paypal_settlement_report_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_settlement_report_row` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Row Id',
  `report_id` int(10) unsigned NOT NULL COMMENT 'Report Id',
  `transaction_id` varchar(19) DEFAULT NULL COMMENT 'Transaction Id',
  `invoice_id` varchar(127) DEFAULT NULL COMMENT 'Invoice Id',
  `paypal_reference_id` varchar(19) DEFAULT NULL COMMENT 'Paypal Reference Id',
  `paypal_reference_id_type` varchar(3) DEFAULT NULL COMMENT 'Paypal Reference Id Type',
  `transaction_event_code` varchar(5) DEFAULT NULL COMMENT 'Transaction Event Code',
  `transaction_initiation_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Initiation Date',
  `transaction_completion_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Completion Date',
  `transaction_debit_or_credit` varchar(2) NOT NULL DEFAULT 'CR' COMMENT 'Transaction Debit Or Credit',
  `gross_transaction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Gross Transaction Amount',
  `gross_transaction_currency` varchar(3) DEFAULT NULL COMMENT 'Gross Transaction Currency',
  `fee_debit_or_credit` varchar(2) DEFAULT NULL COMMENT 'Fee Debit Or Credit',
  `fee_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Fee Amount',
  `fee_currency` varchar(3) DEFAULT NULL COMMENT 'Fee Currency',
  `custom_field` varchar(255) DEFAULT NULL COMMENT 'Custom Field',
  `consumer_id` varchar(127) DEFAULT NULL COMMENT 'Consumer Id',
  `payment_tracking_id` varchar(255) DEFAULT NULL COMMENT 'Payment Tracking ID',
  `store_id` varchar(50) DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`row_id`),
  KEY `PAYPAL_SETTLEMENT_REPORT_ROW_REPORT_ID` (`report_id`),
  CONSTRAINT `FK_E183E488F593E0DE10C6EBFFEBAC9B55` FOREIGN KEY (`report_id`) REFERENCES `paypal_settlement_report` (`report_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Row Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_settlement_report_row`
--

LOCK TABLES `paypal_settlement_report_row` WRITE;
/*!40000 ALTER TABLE `paypal_settlement_report_row` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_settlement_report_row` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistent_session`
--

DROP TABLE IF EXISTS `persistent_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistent_session` (
  `persistent_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Session id',
  `key` varchar(50) NOT NULL COMMENT 'Unique cookie key',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `info` text COMMENT 'Session Data',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`persistent_id`),
  UNIQUE KEY `PERSISTENT_SESSION_KEY` (`key`),
  UNIQUE KEY `PERSISTENT_SESSION_CUSTOMER_ID` (`customer_id`),
  KEY `PERSISTENT_SESSION_UPDATED_AT` (`updated_at`),
  KEY `PERSISTENT_SESSION_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `PERSISTENT_SESSION_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PERSISTENT_SESSION_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Persistent Session';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistent_session`
--

LOCK TABLES `persistent_session` WRITE;
/*!40000 ALTER TABLE `persistent_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `persistent_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazathemes_blog_category`
--

DROP TABLE IF EXISTS `plazathemes_blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plazathemes_blog_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Category ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Category Title',
  `meta_keywords` text COMMENT 'Category Meta Keywords',
  `meta_description` text COMMENT 'Category Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Category String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Category Content Heading',
  `content` mediumtext COMMENT 'Category Content',
  `path` varchar(255) DEFAULT NULL COMMENT 'Category Path',
  `position` smallint(6) NOT NULL COMMENT 'Category Position',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Category Active',
  PRIMARY KEY (`category_id`),
  KEY `PLAZATHEMES_BLOG_CATEGORY_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `FTI_0E5A42C75595B5A4A5CC4FE14800BBC2` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Category Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazathemes_blog_category`
--

LOCK TABLES `plazathemes_blog_category` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `plazathemes_blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazathemes_blog_category_store`
--

DROP TABLE IF EXISTS `plazathemes_blog_category_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plazathemes_blog_category_store` (
  `category_id` int(11) NOT NULL COMMENT 'Category ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`category_id`,`store_id`),
  KEY `PLAZATHEMES_BLOG_CATEGORY_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_5CE7EA43DAADC8EAFEDD24CB3765DBCB` FOREIGN KEY (`category_id`) REFERENCES `plazathemes_blog_category` (`category_id`) ON DELETE CASCADE,
  CONSTRAINT `PLAZATHEMES_BLOG_CATEGORY_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Category To Store Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazathemes_blog_category_store`
--

LOCK TABLES `plazathemes_blog_category_store` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_category_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `plazathemes_blog_category_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazathemes_blog_post`
--

DROP TABLE IF EXISTS `plazathemes_blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plazathemes_blog_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Post ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Post Title',
  `thumbnailimage` varchar(255) DEFAULT NULL COMMENT 'Thumbnail Image',
  `meta_keywords` text COMMENT 'Post Meta Keywords',
  `meta_description` text COMMENT 'Post Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Post String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Post Content Heading',
  `short_content` text COMMENT 'Short Content',
  `content` mediumtext COMMENT 'Post Content',
  `creation_time` timestamp NULL DEFAULT NULL COMMENT 'Post Creation Time',
  `update_time` timestamp NULL DEFAULT NULL COMMENT 'Post Modification Time',
  `publish_time` timestamp NULL DEFAULT NULL COMMENT 'Post Publish Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Post Active',
  PRIMARY KEY (`post_id`),
  KEY `PLAZATHEMES_BLOG_POST_IDENTIFIER` (`identifier`),
  FULLTEXT KEY `FTI_3A962F890DAF9E9D7B6277086623FD2B` (`title`,`meta_keywords`,`meta_description`,`identifier`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazathemes_blog_post`
--

LOCK TABLES `plazathemes_blog_post` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_post` DISABLE KEYS */;
INSERT INTO `plazathemes_blog_post` VALUES (1,'Hello world!',NULL,'magento 2 blog','Magento 2 blog default post.','hello-world','Hello world!',NULL,'Welcome to <a target=\"_blank\" href=\"http://www.plazathemes.com/\" title=\"plazathemes - solutions for Magento 2\">plazathemes</a> blog extension for Magento&reg; 2. This is your first post. Edit or delete it, then start blogging!','2020-02-28 16:10:59','2020-02-28 16:10:59','2020-02-28 16:10:59',1);
/*!40000 ALTER TABLE `plazathemes_blog_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazathemes_blog_post_category`
--

DROP TABLE IF EXISTS `plazathemes_blog_post_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plazathemes_blog_post_category` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `category_id` int(11) NOT NULL COMMENT 'Category ID',
  PRIMARY KEY (`post_id`,`category_id`),
  KEY `PLAZATHEMES_BLOG_POST_CATEGORY_CATEGORY_ID` (`category_id`),
  CONSTRAINT `PLAZATHEMES_BLOG_POST_CTGR_CTGR_ID_PLAZATHEMES_BLOG_CTGR_CTGR_ID` FOREIGN KEY (`category_id`) REFERENCES `plazathemes_blog_category` (`category_id`) ON DELETE CASCADE,
  CONSTRAINT `PLAZATHEMES_BLOG_POST_CTGR_POST_ID_PLAZATHEMES_BLOG_POST_POST_ID` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post To Category Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazathemes_blog_post_category`
--

LOCK TABLES `plazathemes_blog_post_category` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_post_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `plazathemes_blog_post_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazathemes_blog_post_relatedpost`
--

DROP TABLE IF EXISTS `plazathemes_blog_post_relatedpost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plazathemes_blog_post_relatedpost` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `related_id` int(11) NOT NULL COMMENT 'Related Post ID',
  PRIMARY KEY (`post_id`,`related_id`),
  KEY `PLAZATHEMES_BLOG_POST_RELATEDPRODUCT_RELATED_ID` (`related_id`),
  CONSTRAINT `FK_9F4A4184A025A73B339BA1263D565D1E` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D1DFCD681F1AE23DD14596C624890A62` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post To Post Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazathemes_blog_post_relatedpost`
--

LOCK TABLES `plazathemes_blog_post_relatedpost` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_post_relatedpost` DISABLE KEYS */;
/*!40000 ALTER TABLE `plazathemes_blog_post_relatedpost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazathemes_blog_post_relatedproduct`
--

DROP TABLE IF EXISTS `plazathemes_blog_post_relatedproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plazathemes_blog_post_relatedproduct` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `related_id` int(10) unsigned NOT NULL COMMENT 'Related Product ID',
  PRIMARY KEY (`post_id`,`related_id`),
  KEY `PLAZATHEMES_BLOG_POST_RELATEDPRODUCT_RELATED_ID` (`related_id`),
  CONSTRAINT `FK_F087337CF909C30B9C993D16115A82A6` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE,
  CONSTRAINT `PLAZATHEMES_BLOG_POST_RELATEDPRD_RELATED_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`related_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post To Product Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazathemes_blog_post_relatedproduct`
--

LOCK TABLES `plazathemes_blog_post_relatedproduct` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_post_relatedproduct` DISABLE KEYS */;
/*!40000 ALTER TABLE `plazathemes_blog_post_relatedproduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazathemes_blog_post_store`
--

DROP TABLE IF EXISTS `plazathemes_blog_post_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plazathemes_blog_post_store` (
  `post_id` int(11) NOT NULL COMMENT 'Post ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`post_id`,`store_id`),
  KEY `PLAZATHEMES_BLOG_POST_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_A8F0C30220F3C333078301EB9CD221CC` FOREIGN KEY (`post_id`) REFERENCES `plazathemes_blog_post` (`post_id`) ON DELETE CASCADE,
  CONSTRAINT `PLAZATHEMES_BLOG_POST_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Plazathemes Blog Post To Store Linkage Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazathemes_blog_post_store`
--

LOCK TABLES `plazathemes_blog_post_store` WRITE;
/*!40000 ALTER TABLE `plazathemes_blog_post_store` DISABLE KEYS */;
INSERT INTO `plazathemes_blog_post_store` VALUES (1,0);
/*!40000 ALTER TABLE `plazathemes_blog_post_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_alert_price`
--

DROP TABLE IF EXISTS `product_alert_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_alert_price` (
  `alert_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert price id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price amount',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Product alert add date',
  `last_send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert last send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert send count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_price_id`),
  KEY `PRODUCT_ALERT_PRICE_CUSTOMER_ID` (`customer_id`),
  KEY `PRODUCT_ALERT_PRICE_PRODUCT_ID` (`product_id`),
  KEY `PRODUCT_ALERT_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `PRODUCT_ALERT_PRICE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUCT_ALERT_PRICE_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUCT_ALERT_PRICE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Price';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_alert_price`
--

LOCK TABLES `product_alert_price` WRITE;
/*!40000 ALTER TABLE `product_alert_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_alert_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_alert_stock`
--

DROP TABLE IF EXISTS `product_alert_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_alert_stock` (
  `alert_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert stock id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Product alert add date',
  `send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Send Count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_stock_id`),
  KEY `PRODUCT_ALERT_STOCK_CUSTOMER_ID` (`customer_id`),
  KEY `PRODUCT_ALERT_STOCK_PRODUCT_ID` (`product_id`),
  KEY `PRODUCT_ALERT_STOCK_WEBSITE_ID` (`website_id`),
  CONSTRAINT `PRODUCT_ALERT_STOCK_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUCT_ALERT_STOCK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `PRODUCT_ALERT_STOCK_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Stock';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_alert_stock`
--

LOCK TABLES `product_alert_stock` WRITE;
/*!40000 ALTER TABLE `product_alert_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_alert_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pt_bannerslider`
--

DROP TABLE IF EXISTS `pt_bannerslider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pt_bannerslider` (
  `banner_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Banner ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Banner name',
  `store_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'Store Id',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Banner status',
  `click_url` varchar(255) DEFAULT '' COMMENT 'Banner click url',
  `image` varchar(255) DEFAULT NULL COMMENT 'Banner image',
  `image_alt` varchar(255) DEFAULT NULL COMMENT 'Banner image alt',
  `title1` varchar(255) DEFAULT NULL COMMENT 'Title 1',
  `title2` varchar(255) DEFAULT NULL COMMENT 'Title 2',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Order',
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='pt_bannerslider';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pt_bannerslider`
--

LOCK TABLES `pt_bannerslider` WRITE;
/*!40000 ALTER TABLE `pt_bannerslider` DISABLE KEYS */;
INSERT INTO `pt_bannerslider` VALUES (3,'','1',1,NULL,'Plazathemes/bannerslider/images/b/a/banners_1.jpg',NULL,NULL,NULL,NULL,0),(4,'','1',1,NULL,'Plazathemes/bannerslider/images/b/a/banners_2.jpg',NULL,NULL,NULL,NULL,1),(5,'','1',1,NULL,'Plazathemes/bannerslider/images/b/a/banners_3.jpg',NULL,NULL,NULL,NULL,2),(6,'','1',1,NULL,'Plazathemes/bannerslider/images/b/a/banners_4.jpg',NULL,NULL,NULL,NULL,4);
/*!40000 ALTER TABLE `pt_bannerslider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pt_brandslider`
--

DROP TABLE IF EXISTS `pt_brandslider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pt_brandslider` (
  `brand_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Brand ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'Title',
  `store_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'Store Id',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Brand status',
  `link` varchar(255) DEFAULT '' COMMENT 'Link',
  `image` varchar(255) DEFAULT NULL COMMENT 'Brand image',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Order',
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='pt_brandslider';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pt_brandslider`
--

LOCK TABLES `pt_brandslider` WRITE;
/*!40000 ALTER TABLE `pt_brandslider` DISABLE KEYS */;
INSERT INTO `pt_brandslider` VALUES (1,'meteoro','1',1,NULL,'Plazathemes/brandslider/images/0/1/01_meteoro.png',NULL,1),(2,'sin_piedad','1',1,NULL,'Plazathemes/brandslider/images/0/2/02_sin_piedad.png',NULL,2),(3,'mayalen','1',1,NULL,'Plazathemes/brandslider/images/0/3/03_mayalen.png',NULL,3),(4,'04_mexxico','1',1,NULL,'Plazathemes/brandslider/images/0/4/04_mexxico.png',NULL,4),(5,'05_chaneque','1',1,NULL,'Plazathemes/brandslider/images/0/5/05_chaneque.png',NULL,5),(6,'06_bruxo','1',1,NULL,'Plazathemes/brandslider/images/0/6/06_bruxo.png',NULL,6),(7,'07_iba','1',1,NULL,'Plazathemes/brandslider/images/0/7/07_iba.png',NULL,7),(8,'08_marca_negra','1',1,NULL,'Plazathemes/brandslider/images/0/8/08_marca_negra.png',NULL,8),(9,'natalia','1',1,NULL,'Plazathemes/brandslider/images/0/9/09_do_a_natalia.png',NULL,9);
/*!40000 ALTER TABLE `pt_brandslider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pt_testimonial`
--

DROP TABLE IF EXISTS `pt_testimonial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pt_testimonial` (
  `testimo_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Testimonial ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Name',
  `store_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'Store Id',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `avatar` varchar(255) DEFAULT NULL COMMENT 'Avatar',
  `website` varchar(255) DEFAULT NULL COMMENT 'Website',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `address` varchar(255) DEFAULT NULL COMMENT 'Address',
  `job` varchar(255) DEFAULT NULL COMMENT 'Job',
  `testimonial` varchar(255) DEFAULT NULL COMMENT 'Testimonial',
  `created_time` date DEFAULT NULL COMMENT 'Created Time',
  `update_time` date DEFAULT NULL COMMENT 'Update Time',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Status',
  `order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Order',
  PRIMARY KEY (`testimo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='pt_testimonial';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pt_testimonial`
--

LOCK TABLES `pt_testimonial` WRITE;
/*!40000 ALTER TABLE `pt_testimonial` DISABLE KEYS */;
/*!40000 ALTER TABLE `pt_testimonial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote`
--

DROP TABLE IF EXISTS `quote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `converted_at` timestamp NULL DEFAULT NULL COMMENT 'Converted At',
  `is_active` smallint(5) unsigned DEFAULT '1' COMMENT 'Is Active',
  `is_virtual` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Virtual',
  `is_multi_shipping` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Multi Shipping',
  `items_count` int(10) unsigned DEFAULT '0' COMMENT 'Items Count',
  `items_qty` decimal(12,4) DEFAULT '0.0000' COMMENT 'Items Qty',
  `orig_order_id` int(10) unsigned DEFAULT '0' COMMENT 'Orig Order Id',
  `store_to_base_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Base Rate',
  `store_to_quote_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Quote Rate',
  `base_currency_code` varchar(255) DEFAULT NULL COMMENT 'Base Currency Code',
  `store_currency_code` varchar(255) DEFAULT NULL COMMENT 'Store Currency Code',
  `quote_currency_code` varchar(255) DEFAULT NULL COMMENT 'Quote Currency Code',
  `grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `checkout_method` varchar(255) DEFAULT NULL COMMENT 'Checkout Method',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `customer_tax_class_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Tax Class Id',
  `customer_group_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Group Id',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_prefix` varchar(40) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_firstname` varchar(255) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_middlename` varchar(40) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_lastname` varchar(255) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_suffix` varchar(40) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `customer_note` varchar(255) DEFAULT NULL COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT '1' COMMENT 'Customer Note Notify',
  `customer_is_guest` smallint(5) unsigned DEFAULT '0' COMMENT 'Customer Is Guest',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `applied_rule_ids` varchar(255) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `reserved_order_id` varchar(64) DEFAULT NULL COMMENT 'Reserved Order Id',
  `password_hash` varchar(255) DEFAULT NULL COMMENT 'Password Hash',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `global_currency_code` varchar(255) DEFAULT NULL COMMENT 'Global Currency Code',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_quote_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Quote Rate',
  `customer_taxvat` varchar(255) DEFAULT NULL COMMENT 'Customer Taxvat',
  `customer_gender` varchar(255) DEFAULT NULL COMMENT 'Customer Gender',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal With Discount',
  `is_changed` int(10) unsigned DEFAULT NULL COMMENT 'Is Changed',
  `trigger_recollect` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Trigger Recollect',
  `ext_shipping_info` text COMMENT 'Ext Shipping Info',
  `is_persistent` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Quote Persistent',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  KEY `QUOTE_CUSTOMER_ID_STORE_ID_IS_ACTIVE` (`customer_id`,`store_id`,`is_active`),
  KEY `QUOTE_STORE_ID` (`store_id`),
  CONSTRAINT `QUOTE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote`
--

LOCK TABLES `quote` WRITE;
/*!40000 ALTER TABLE `quote` DISABLE KEYS */;
INSERT INTO `quote` VALUES (1,1,'2020-02-28 15:36:35','0000-00-00 00:00:00',NULL,1,0,0,0,0.0000,0,0.0000,0.0000,'EUR','EUR','EUR',0.0000,0.0000,NULL,1,NULL,1,'kdodeth@gmail.com',NULL,'customer',NULL,'.',NULL,NULL,NULL,1,0,'187.189.198.170',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,1,0,NULL,0,NULL),(2,1,'2020-02-28 19:06:48','0000-00-00 00:00:00',NULL,1,0,0,0,0.0000,0,0.0000,0.0000,'EUR','EUR','EUR',0.0000,0.0000,NULL,2,NULL,1,'dev@disandat.mx',NULL,'Juan',NULL,'Ortiz',NULL,NULL,NULL,1,0,'187.178.88.72',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,1,0,NULL,0,NULL),(3,1,'2020-03-04 20:20:52','2020-03-04 20:21:39',NULL,1,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',56.6000,56.6000,NULL,3,3,1,'odette.pm@disandat.mx',NULL,'Odette',NULL,'prueba',NULL,NULL,NULL,1,0,'187.189.198.170',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,56.6000,56.6000,56.6000,56.6000,1,0,NULL,0,NULL),(4,1,'2020-03-05 17:13:50','0000-00-00 00:00:00',NULL,1,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',56.6000,56.6000,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'187.189.198.170',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,56.6000,56.6000,56.6000,56.6000,1,0,NULL,0,NULL),(5,1,'2020-03-06 00:03:10','2020-03-06 00:04:48',NULL,0,0,0,1,2.0000,0,0.0000,0.0000,'EUR','EUR','EUR',123.2000,123.2000,'guest',NULL,3,0,'juan.op@disandat.mx',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'189.213.163.237',NULL,'000000001',NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,113.2000,113.2000,113.2000,113.2000,1,0,NULL,0,NULL),(6,1,'2020-03-06 16:52:48','2020-03-06 16:53:06',NULL,1,0,0,1,2.0000,0,0.0000,0.0000,'EUR','EUR','EUR',113.2000,113.2000,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'187.189.198.170',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,113.2000,113.2000,113.2000,113.2000,1,0,NULL,0,NULL),(7,1,'2020-03-06 17:57:59','0000-00-00 00:00:00',NULL,1,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',56.6000,56.6000,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'187.189.198.170',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,56.6000,56.6000,56.6000,56.6000,1,0,NULL,0,NULL),(8,1,'2020-03-06 19:08:35','0000-00-00 00:00:00',NULL,1,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',56.6000,56.6000,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'187.189.198.170',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,56.6000,56.6000,56.6000,56.6000,1,0,NULL,0,NULL),(9,1,'2020-03-09 21:17:48','0000-00-00 00:00:00',NULL,1,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',24.9500,24.9500,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'200.194.11.221',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,24.9500,24.9500,24.9500,24.9500,1,0,NULL,0,NULL),(10,1,'2020-03-11 15:30:16','0000-00-00 00:00:00',NULL,1,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',39.8000,39.8000,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'187.189.198.170',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,39.8000,39.8000,39.8000,39.8000,1,0,NULL,0,NULL),(11,1,'2020-03-11 22:14:51','0000-00-00 00:00:00',NULL,1,0,0,1,10.0000,0,0.0000,0.0000,'EUR','EUR','EUR',249.5000,249.5000,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'187.189.198.170',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,249.5000,249.5000,249.5000,249.5000,1,0,NULL,0,NULL),(12,1,'2020-03-11 23:54:31','2020-03-12 05:01:39',NULL,0,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',243.8000,243.8000,NULL,4,3,1,'kdodeth@hotmail.com',NULL,'Cliente',NULL,'prueba',NULL,NULL,NULL,1,0,'187.189.198.170',NULL,'000000002',NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,238.8000,238.8000,238.8000,238.8000,1,0,NULL,0,NULL),(14,1,'2020-03-12 05:03:03','2020-03-12 05:09:10',NULL,0,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',154.7000,154.7000,NULL,4,3,1,'kdodeth@hotmail.com',NULL,'Cliente',NULL,'prueba',NULL,NULL,NULL,1,0,'187.189.91.159',NULL,'000000003',NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,149.7000,149.7000,149.7000,149.7000,1,0,NULL,0,NULL),(15,1,'2020-03-12 23:26:19','0000-00-00 00:00:00',NULL,1,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',54.0000,54.0000,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'200.194.11.221',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,54.0000,54.0000,54.0000,54.0000,1,0,NULL,0,NULL),(16,1,'2020-03-13 23:54:23','0000-00-00 00:00:00',NULL,1,0,0,1,1.0000,0,0.0000,0.0000,'EUR','EUR','EUR',121.6300,121.6300,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'187.189.91.159',NULL,NULL,NULL,NULL,'EUR',1.0000,1.0000,NULL,NULL,121.6300,121.6300,121.6300,121.6300,1,0,NULL,0,NULL);
/*!40000 ALTER TABLE `quote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_address`
--

DROP TABLE IF EXISTS `quote_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_address` (
  `address_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `save_in_address_book` smallint(6) DEFAULT '0' COMMENT 'Save In Address Book',
  `customer_address_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Address Id',
  `address_type` varchar(10) DEFAULT NULL COMMENT 'Address Type',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Firstname',
  `middlename` varchar(40) DEFAULT NULL COMMENT 'Middlename',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Lastname',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(40) DEFAULT NULL COMMENT 'City',
  `region` varchar(40) DEFAULT NULL COMMENT 'Region',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'Region Id',
  `postcode` varchar(20) DEFAULT NULL COMMENT 'Postcode',
  `country_id` varchar(30) DEFAULT NULL COMMENT 'Country Id',
  `telephone` varchar(20) DEFAULT NULL COMMENT 'Phone Number',
  `fax` varchar(20) DEFAULT NULL COMMENT 'Fax',
  `same_as_billing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Same As Billing',
  `collect_shipping_rates` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Collect Shipping Rates',
  `shipping_method` varchar(40) DEFAULT NULL COMMENT 'Shipping Method',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `weight` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Weight',
  `subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal With Discount',
  `tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Shipping Amount',
  `base_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Shipping Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `customer_notes` text COMMENT 'Customer Notes',
  `applied_taxes` text COMMENT 'Applied Taxes',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `free_shipping` smallint(6) DEFAULT NULL,
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_id`),
  KEY `QUOTE_ADDRESS_QUOTE_ID` (`quote_id`),
  CONSTRAINT `QUOTE_ADDRESS_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_address`
--

LOCK TABLES `quote_address` WRITE;
/*!40000 ALTER TABLE `quote_address` DISABLE KEYS */;
INSERT INTO `quote_address` VALUES (1,1,'2020-02-28 15:36:35','0000-00-00 00:00:00',1,0,NULL,'billing','kdodeth@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,'2020-02-28 15:36:35','0000-00-00 00:00:00',1,0,NULL,'shipping','kdodeth@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,2,'2020-02-28 19:06:48','0000-00-00 00:00:00',2,0,NULL,'billing','dev@disandat.mx',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,2,'2020-02-28 19:06:48','0000-00-00 00:00:00',2,0,NULL,'shipping','dev@disandat.mx',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,3,'2020-03-04 20:20:52','0000-00-00 00:00:00',3,0,NULL,'billing','odette.pm@disandat.mx',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,3,'2020-03-04 20:20:52','0000-00-00 00:00:00',3,0,NULL,'shipping','odette.pm@disandat.mx',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,70.0000,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,56.6000,56.6000,NULL,'a:0:{}',NULL,0.0000,0.0000,56.6000,56.6000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,4,'2020-03-05 17:13:50','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,4,'2020-03-05 17:13:50','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,70.0000,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,56.6000,56.6000,NULL,'a:0:{}',NULL,0.0000,0.0000,56.6000,56.6000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(10,5,'2020-03-06 00:03:10','2020-03-06 00:04:48',NULL,0,NULL,'shipping','juan.op@disandat.mx',NULL,'Juan Luis',NULL,'Ortiz',NULL,'Disandat','Laguna de Mayrán 166\nEdificio Copenhague 1202, Anahuac, Miguel Hidalgo','Mexico','Mexico',NULL,'11320','MX','5552176974',NULL,0,0,'flatrate_flatrate','Flat Rate - Fixed',140.0000,113.2000,113.2000,113.2000,113.2000,0.0000,0.0000,10.0000,10.0000,0.0000,0.0000,0.0000,0.0000,123.2000,123.2000,NULL,'a:0:{}',NULL,0.0000,0.0000,113.2000,113.2000,0.0000,0.0000,0.0000,NULL,10.0000,10.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(11,5,'2020-03-06 00:04:48','0000-00-00 00:00:00',NULL,0,NULL,'billing','juan.op@disandat.mx',NULL,'Juan Luis',NULL,'Ortiz',NULL,'Disandat','Laguna de Mayrán 166\nEdificio Copenhague 1202, Anahuac, Miguel Hidalgo','Mexico','Mexico',NULL,'11320','MX','5552176974',NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,6,'2020-03-06 16:52:48','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,6,'2020-03-06 16:52:48','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,140.0000,113.2000,113.2000,113.2000,113.2000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,113.2000,113.2000,NULL,'a:0:{}',NULL,0.0000,0.0000,113.2000,113.2000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(14,7,'2020-03-06 17:57:59','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,7,'2020-03-06 17:57:59','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,70.0000,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,56.6000,56.6000,NULL,'a:0:{}',NULL,0.0000,0.0000,56.6000,56.6000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(16,8,'2020-03-06 19:08:35','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,8,'2020-03-06 19:08:35','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,70.0000,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,56.6000,56.6000,NULL,'a:0:{}',NULL,0.0000,0.0000,56.6000,56.6000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(18,9,'2020-03-09 21:17:48','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,9,'2020-03-09 21:17:48','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0.0000,24.9500,24.9500,24.9500,24.9500,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,24.9500,24.9500,NULL,'a:0:{}',NULL,0.0000,0.0000,24.9500,24.9500,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(20,10,'2020-03-11 15:30:16','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,10,'2020-03-11 15:30:16','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0.0000,39.8000,39.8000,39.8000,39.8000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,39.8000,39.8000,NULL,'a:0:{}',NULL,0.0000,0.0000,39.8000,39.8000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(22,11,'2020-03-11 22:14:51','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,11,'2020-03-11 22:14:51','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0.0000,249.5000,249.5000,249.5000,249.5000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,249.5000,249.5000,NULL,'a:0:{}',NULL,0.0000,0.0000,249.5000,249.5000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(25,12,'2020-03-11 23:54:31','2020-03-12 05:01:38',4,1,1,'shipping','kdodeth@hotmail.com',NULL,'Cliente',NULL,'prueba',NULL,NULL,'asdasd','sdfdsf','Federated States Of Micronesia',17,'45644','US','4575545',NULL,0,0,'flatrate_flatrate','Cultura Mezcal - Tarifa Plana',0.0000,238.8000,238.8000,238.8000,238.8000,0.0000,0.0000,5.0000,5.0000,0.0000,0.0000,0.0000,0.0000,243.8000,243.8000,NULL,'a:0:{}',NULL,0.0000,0.0000,238.8000,238.8000,0.0000,0.0000,0.0000,NULL,5.0000,5.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(28,12,'2020-03-12 05:01:38','0000-00-00 00:00:00',4,0,NULL,'billing','kdodeth@hotmail.com',NULL,'Cliente',NULL,'prueba',NULL,NULL,'asdasd','sdfdsf','Federated States Of Micronesia',17,'45644','US','4575545',NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,14,'2020-03-12 05:03:03','2020-03-12 05:09:10',4,0,1,'shipping','kdodeth@hotmail.com',NULL,'Cliente',NULL,'prueba',NULL,NULL,'asdasd','sdfdsf','Federated States Of Micronesia',17,'45644','US','4575545',NULL,0,0,'flatrate_flatrate','Cultura Mezcal - Tarifa Plana',0.0000,149.7000,149.7000,149.7000,149.7000,0.0000,0.0000,5.0000,5.0000,0.0000,0.0000,0.0000,0.0000,154.7000,154.7000,NULL,'a:0:{}',NULL,0.0000,0.0000,149.7000,149.7000,0.0000,0.0000,0.0000,NULL,5.0000,5.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(31,14,'2020-03-12 05:09:10','0000-00-00 00:00:00',4,0,1,'billing','kdodeth@hotmail.com',NULL,'Cliente',NULL,'prueba',NULL,NULL,'asdasd','sdfdsf','Federated States Of Micronesia',17,'45644','US','4575545',NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,15,'2020-03-12 23:26:19','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,15,'2020-03-12 23:26:19','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0.0000,54.0000,54.0000,54.0000,54.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,54.0000,54.0000,NULL,'a:0:{}',NULL,0.0000,0.0000,54.0000,54.0000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL),(34,16,'2020-03-13 23:54:23','0000-00-00 00:00:00',NULL,0,NULL,'billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,0.0000,0.0000,0.0000,0.0000,NULL,'N;',NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,16,'2020-03-13 23:54:23','0000-00-00 00:00:00',NULL,0,NULL,'shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,0.0000,121.6300,121.6300,121.6300,121.6300,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,121.6300,121.6300,NULL,'a:0:{}',NULL,0.0000,0.0000,121.6300,121.6300,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `quote_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_address_item`
--

DROP TABLE IF EXISTS `quote_address_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_address_item` (
  `address_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Item Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Address Id',
  `quote_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Item Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `super_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Super Product Id',
  `parent_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Product Id',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `is_qty_decimal` int(10) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `discount_percent` decimal(12,4) DEFAULT NULL COMMENT 'Discount Percent',
  `no_discount` int(10) unsigned DEFAULT NULL COMMENT 'No Discount',
  `tax_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tax Percent',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `free_shipping` int(11) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_item_id`),
  KEY `QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID` (`quote_address_id`),
  KEY `QUOTE_ADDRESS_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID` (`quote_item_id`),
  CONSTRAINT `QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID_QUOTE_ADDRESS_ADDRESS_ID` FOREIGN KEY (`quote_address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`quote_item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ADDR_ITEM_PARENT_ITEM_ID_QUOTE_ADDR_ITEM_ADDR_ITEM_ID` FOREIGN KEY (`parent_item_id`) REFERENCES `quote_address_item` (`address_item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address Item';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_address_item`
--

LOCK TABLES `quote_address_item` WRITE;
/*!40000 ALTER TABLE `quote_address_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `quote_address_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_id_mask`
--

DROP TABLE IF EXISTS `quote_id_mask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_id_mask` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `quote_id` int(10) unsigned NOT NULL COMMENT 'Quote ID',
  `masked_id` varchar(32) DEFAULT NULL COMMENT 'Masked ID',
  PRIMARY KEY (`entity_id`,`quote_id`),
  KEY `QUOTE_ID_MASK_QUOTE_ID` (`quote_id`),
  KEY `QUOTE_ID_MASK_MASKED_ID` (`masked_id`),
  CONSTRAINT `QUOTE_ID_MASK_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Quote ID and masked ID mapping';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_id_mask`
--

LOCK TABLES `quote_id_mask` WRITE;
/*!40000 ALTER TABLE `quote_id_mask` DISABLE KEYS */;
INSERT INTO `quote_id_mask` VALUES (1,4,'0e425edfbd14082d883a32f49bb4151b'),(5,8,'1c358db18704cf6315ec91cf36f351ba'),(7,10,'2b9335b619d929e6d1361a28253aeb3b'),(8,11,'4afdceed3bd062696a0939dab0b82cbb'),(2,5,'545af128a034c61db8ec7e48767068ad'),(10,15,'5e324bf40d36585c83109e1c5e34c0d4'),(11,16,'7628f359adc32c5531b558cc9b60c9a2'),(6,9,'b213160e2f6432aa704573c58f6fc190'),(4,7,'c8918b5ab592998d40f0f5c65a501cfc'),(3,6,'eabf634637ef5f155d171352a436bb5f');
/*!40000 ALTER TABLE `quote_id_mask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_item`
--

DROP TABLE IF EXISTS `quote_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned DEFAULT '0' COMMENT 'No Discount',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Custom Price',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `original_custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Custom Price',
  `redirect_url` varchar(255) DEFAULT NULL COMMENT 'Redirect Url',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `free_shipping` smallint(6) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `QUOTE_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `QUOTE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `QUOTE_ITEM_QUOTE_ID` (`quote_id`),
  KEY `QUOTE_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `QUOTE_ITEM_PARENT_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`parent_item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ITEM_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `QUOTE_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_item`
--

LOCK TABLES `quote_item` WRITE;
/*!40000 ALTER TABLE `quote_item` DISABLE KEYS */;
INSERT INTO `quote_item` VALUES (1,3,'2020-03-04 20:21:39','0000-00-00 00:00:00',1,1,NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,70.0000,1.0000,56.6000,56.6000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,56.6000,56.6000,0.0000,70.0000,'simple',NULL,NULL,NULL,NULL,NULL,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,4,'2020-03-05 17:13:50','0000-00-00 00:00:00',1,1,NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,70.0000,1.0000,56.6000,56.6000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,56.6000,56.6000,0.0000,70.0000,'simple',NULL,NULL,NULL,NULL,NULL,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,5,'2020-03-06 00:03:10','2020-03-06 00:03:46',1,1,NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,70.0000,2.0000,56.6000,56.6000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,113.2000,113.2000,0.0000,140.0000,'simple',NULL,NULL,NULL,NULL,NULL,56.6000,56.6000,113.2000,113.2000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,6,'2020-03-06 16:52:48','2020-03-06 16:53:06',1,1,NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,70.0000,2.0000,56.6000,56.6000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,113.2000,113.2000,0.0000,140.0000,'simple',NULL,NULL,NULL,NULL,NULL,56.6000,56.6000,113.2000,113.2000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,7,'2020-03-06 17:57:59','0000-00-00 00:00:00',1,1,NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,70.0000,1.0000,56.6000,56.6000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,56.6000,56.6000,0.0000,70.0000,'simple',NULL,NULL,NULL,NULL,NULL,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,8,'2020-03-06 19:08:35','0000-00-00 00:00:00',1,1,NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,70.0000,1.0000,56.6000,56.6000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,56.6000,56.6000,0.0000,70.0000,'simple',NULL,NULL,NULL,NULL,NULL,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,9,'2020-03-09 21:17:48','0000-00-00 00:00:00',2,1,NULL,0,'MX01','MeXXico Joven',NULL,NULL,NULL,0,0,NULL,1.0000,24.9500,24.9500,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,24.9500,24.9500,0.0000,0.0000,'simple',NULL,NULL,NULL,NULL,NULL,24.9500,24.9500,24.9500,24.9500,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,10,'2020-03-11 15:30:16','0000-00-00 00:00:00',1,1,NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,NULL,1.0000,39.8000,39.8000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,39.8000,39.8000,0.0000,0.0000,'simple',NULL,NULL,NULL,NULL,NULL,39.8000,39.8000,39.8000,39.8000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,11,'2020-03-11 22:14:51','0000-00-00 00:00:00',2,1,NULL,0,'MX01','MeXXico Joven',NULL,NULL,NULL,0,0,NULL,10.0000,24.9500,24.9500,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,249.5000,249.5000,0.0000,0.0000,'simple',NULL,NULL,NULL,NULL,NULL,24.9500,24.9500,249.5000,249.5000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,12,'2020-03-12 04:49:14','0000-00-00 00:00:00',1,1,NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,NULL,1.0000,238.8000,238.8000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,238.8000,238.8000,0.0000,0.0000,'simple',NULL,NULL,NULL,NULL,NULL,238.8000,238.8000,238.8000,238.8000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,14,'2020-03-12 05:03:03','0000-00-00 00:00:00',2,1,NULL,0,'MX01','MeXXico Joven',NULL,NULL,NULL,0,0,NULL,1.0000,149.7000,149.7000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,149.7000,149.7000,0.0000,0.0000,'simple',NULL,NULL,NULL,NULL,NULL,149.7000,149.7000,149.7000,149.7000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,15,'2020-03-12 23:26:19','0000-00-00 00:00:00',11,1,NULL,0,'CH05','Mezcal Chaneque Tobalá Silvestre',NULL,NULL,NULL,0,0,NULL,1.0000,54.0000,54.0000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,54.0000,54.0000,0.0000,0.0000,'simple',NULL,NULL,NULL,NULL,NULL,54.0000,54.0000,54.0000,54.0000,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,16,'2020-03-13 23:54:23','0000-00-00 00:00:00',18,1,NULL,0,'DN01','Doña Natalia',NULL,NULL,NULL,0,0,NULL,1.0000,121.6300,121.6300,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,121.6300,121.6300,0.0000,0.0000,'simple',NULL,NULL,NULL,NULL,NULL,121.6300,121.6300,121.6300,121.6300,0.0000,0.0000,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `quote_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_item_option`
--

DROP TABLE IF EXISTS `quote_item_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `item_id` int(10) unsigned NOT NULL COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `QUOTE_ITEM_OPTION_ITEM_ID` (`item_id`),
  CONSTRAINT `QUOTE_ITEM_OPTION_ITEM_ID_QUOTE_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `quote_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item Option';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_item_option`
--

LOCK TABLES `quote_item_option` WRITE;
/*!40000 ALTER TABLE `quote_item_option` DISABLE KEYS */;
INSERT INTO `quote_item_option` VALUES (1,1,1,'info_buyRequest','a:5:{s:4:\"uenc\";s:72:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21lemNhbC1tZXRlb3JvLmh0bWw,\";s:7:\"product\";s:1:\"1\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),(2,2,1,'info_buyRequest','a:3:{s:4:\"uenc\";s:48:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zLw,,\";s:7:\"product\";s:1:\"1\";s:3:\"qty\";d:1;}'),(3,3,1,'info_buyRequest','a:5:{s:4:\"uenc\";s:72:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21lemNhbC1tZXRlb3JvLmh0bWw,\";s:7:\"product\";s:1:\"1\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),(4,4,1,'info_buyRequest','a:3:{s:4:\"uenc\";s:48:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zLw,,\";s:7:\"product\";s:1:\"1\";s:3:\"qty\";d:1;}'),(5,5,1,'info_buyRequest','a:5:{s:4:\"uenc\";s:72:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21lemNhbC1tZXRlb3JvLmh0bWw,\";s:7:\"product\";s:1:\"1\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),(6,6,1,'info_buyRequest','a:5:{s:4:\"uenc\";s:72:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21lemNhbC1tZXRlb3JvLmh0bWw,\";s:7:\"product\";s:1:\"1\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),(7,7,2,'info_buyRequest','a:3:{s:4:\"uenc\";s:92:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL2NvbXByYS5odG1sP3Byb2R1Y3RfbGlzdF9vcmRlcj1uYW1l\";s:7:\"product\";s:1:\"2\";s:3:\"qty\";d:1;}'),(8,8,1,'info_buyRequest','a:3:{s:4:\"uenc\";s:48:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zLw,,\";s:7:\"product\";s:1:\"1\";s:3:\"qty\";d:1;}'),(9,9,2,'info_buyRequest','a:6:{s:4:\"uenc\";s:72:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21leHhpY28tam92ZW4uaHRtbA,,\";s:7:\"product\";s:1:\"2\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:7:\"options\";a:1:{i:2;s:1:\"4\";}s:3:\"qty\";s:2:\"10\";}'),(10,9,2,'option_ids','2'),(11,9,2,'option_2','4'),(15,11,1,'info_buyRequest','a:6:{s:4:\"uenc\";s:80:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21ldGVvcm8uaHRtbD9vcHRpb25zPWNhcnQ,\";s:7:\"product\";s:1:\"1\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:7:\"options\";a:1:{i:5;s:1:\"9\";}s:3:\"qty\";s:1:\"1\";}'),(16,11,1,'option_ids','5'),(17,11,1,'option_5','9'),(18,12,2,'info_buyRequest','a:6:{s:4:\"uenc\";s:88:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21leHhpY28tam92ZW4uaHRtbD9vcHRpb25zPWNhcnQ,\";s:7:\"product\";s:1:\"2\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:7:\"options\";a:1:{i:4;s:1:\"7\";}s:3:\"qty\";s:1:\"1\";}'),(19,12,2,'option_ids','4'),(20,12,2,'option_4','7'),(21,13,11,'info_buyRequest','a:5:{s:4:\"uenc\";s:96:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21lemNhbC1jaGFuZXF1ZS10b2JhbGEtc2lsdmVzdHJlLmh0bWw,\";s:7:\"product\";s:2:\"11\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}'),(22,14,18,'info_buyRequest','a:5:{s:4:\"uenc\";s:68:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL2RvLWEtbmF0YWxpYS5odG1s\";s:7:\"product\";s:2:\"18\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}');
/*!40000 ALTER TABLE `quote_item_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_payment`
--

DROP TABLE IF EXISTS `quote_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Payment Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `cc_type` varchar(255) DEFAULT NULL COMMENT 'Cc Type',
  `cc_number_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_last_4` varchar(255) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_cid_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Cid Enc',
  `cc_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_exp_month` varchar(255) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_exp_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Exp Year',
  `cc_ss_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Owner',
  `cc_ss_start_month` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Month',
  `cc_ss_start_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Year',
  `po_number` varchar(255) DEFAULT NULL COMMENT 'Po Number',
  `additional_data` text COMMENT 'Additional Data',
  `cc_ss_issue` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `additional_information` text COMMENT 'Additional Information',
  `paypal_payer_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Id',
  `paypal_payer_status` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Status',
  `paypal_correlation_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Correlation Id',
  PRIMARY KEY (`payment_id`),
  KEY `QUOTE_PAYMENT_QUOTE_ID` (`quote_id`),
  CONSTRAINT `QUOTE_PAYMENT_QUOTE_ID_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Payment';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_payment`
--

LOCK TABLES `quote_payment` WRITE;
/*!40000 ALTER TABLE `quote_payment` DISABLE KEYS */;
INSERT INTO `quote_payment` VALUES (1,5,'2020-03-06 00:04:48','0000-00-00 00:00:00','checkmo',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,NULL,NULL,NULL,'N;',NULL,NULL,NULL),(2,12,'2020-03-12 05:01:38','0000-00-00 00:00:00','checkmo',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,NULL,NULL,NULL,'N;',NULL,NULL,NULL),(3,14,'2020-03-12 05:09:10','0000-00-00 00:00:00','banktransfer',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,0,NULL,NULL,NULL,'N;',NULL,NULL,NULL);
/*!40000 ALTER TABLE `quote_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_shipping_rate`
--

DROP TABLE IF EXISTS `quote_shipping_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_shipping_rate` (
  `rate_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rate Id',
  `address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Address Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `carrier` varchar(255) DEFAULT NULL COMMENT 'Carrier',
  `carrier_title` varchar(255) DEFAULT NULL COMMENT 'Carrier Title',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `method_description` text COMMENT 'Method Description',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `error_message` text COMMENT 'Error Message',
  `method_title` text COMMENT 'Method Title',
  PRIMARY KEY (`rate_id`),
  KEY `QUOTE_SHIPPING_RATE_ADDRESS_ID` (`address_id`),
  CONSTRAINT `QUOTE_SHIPPING_RATE_ADDRESS_ID_QUOTE_ADDRESS_ADDRESS_ID` FOREIGN KEY (`address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Shipping Rate';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_shipping_rate`
--

LOCK TABLES `quote_shipping_rate` WRITE;
/*!40000 ALTER TABLE `quote_shipping_rate` DISABLE KEYS */;
INSERT INTO `quote_shipping_rate` VALUES (3,10,'2020-03-06 00:04:48','0000-00-00 00:00:00','flatrate','Flat Rate','flatrate_flatrate','flatrate',NULL,10.0000,NULL,'Fixed'),(6,25,'2020-03-12 05:01:38','0000-00-00 00:00:00','flatrate','Cultura Mezcal','flatrate_flatrate','flatrate',NULL,5.0000,NULL,'Tarifa Plana'),(9,30,'2020-03-12 05:09:10','0000-00-00 00:00:00','flatrate','Cultura Mezcal','flatrate_flatrate','flatrate',NULL,5.0000,NULL,'Tarifa Plana');
/*!40000 ALTER TABLE `quote_shipping_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `rating_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Id',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `rating_code` varchar(64) NOT NULL COMMENT 'Rating Code',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Position On Storefront',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Rating is active.',
  PRIMARY KEY (`rating_id`),
  UNIQUE KEY `RATING_RATING_CODE` (`rating_code`),
  KEY `RATING_ENTITY_ID` (`entity_id`),
  CONSTRAINT `RATING_ENTITY_ID_RATING_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `rating_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Ratings';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (1,1,'Calidad',0,1),(2,1,'Value',0,0),(3,1,'Price',0,0);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_entity`
--

DROP TABLE IF EXISTS `rating_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_code` varchar(64) NOT NULL COMMENT 'Entity Code',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `RATING_ENTITY_ENTITY_CODE` (`entity_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Rating entities';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_entity`
--

LOCK TABLES `rating_entity` WRITE;
/*!40000 ALTER TABLE `rating_entity` DISABLE KEYS */;
INSERT INTO `rating_entity` VALUES (1,'product'),(2,'product_review'),(3,'review');
/*!40000 ALTER TABLE `rating_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_option`
--

DROP TABLE IF EXISTS `rating_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Option Id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `code` varchar(32) NOT NULL COMMENT 'Rating Option Code',
  `value` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Option Value',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Ration option position on Storefront',
  PRIMARY KEY (`option_id`),
  KEY `RATING_OPTION_RATING_ID` (`rating_id`),
  CONSTRAINT `RATING_OPTION_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Rating options';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_option`
--

LOCK TABLES `rating_option` WRITE;
/*!40000 ALTER TABLE `rating_option` DISABLE KEYS */;
INSERT INTO `rating_option` VALUES (1,1,'1',1,1),(2,1,'2',2,2),(3,1,'3',3,3),(4,1,'4',4,4),(5,1,'5',5,5),(6,2,'1',1,1),(7,2,'2',2,2),(8,2,'3',3,3),(9,2,'4',4,4),(10,2,'5',5,5),(11,3,'1',1,1),(12,3,'2',2,2),(13,3,'3',3,3),(14,3,'4',4,4),(15,3,'5',5,5);
/*!40000 ALTER TABLE `rating_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_option_vote`
--

DROP TABLE IF EXISTS `rating_option_vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_option_vote` (
  `vote_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Vote id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote option id',
  `remote_ip` varchar(16) NOT NULL COMMENT 'Customer IP',
  `remote_ip_long` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Customer IP converted to long integer format',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `review_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Review id',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Percent amount',
  `value` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote option value',
  PRIMARY KEY (`vote_id`),
  KEY `RATING_OPTION_VOTE_OPTION_ID` (`option_id`),
  KEY `RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` (`review_id`),
  CONSTRAINT `RATING_OPTION_VOTE_OPTION_ID_RATING_OPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `rating_option` (`option_id`) ON DELETE CASCADE,
  CONSTRAINT `RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Rating option values';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_option_vote`
--

LOCK TABLES `rating_option_vote` WRITE;
/*!40000 ALTER TABLE `rating_option_vote` DISABLE KEYS */;
INSERT INTO `rating_option_vote` VALUES (1,4,'187.189.198.170',3149776554,NULL,1,1,1,80,4),(2,5,'187.189.91.159',3149749151,NULL,2,1,2,100,5);
/*!40000 ALTER TABLE `rating_option_vote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_option_vote_aggregated`
--

DROP TABLE IF EXISTS `rating_option_vote_aggregated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_option_vote_aggregated` (
  `primary_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Vote aggregation id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `vote_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote dty',
  `vote_value_sum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'General vote sum',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote percent',
  `percent_approved` smallint(6) DEFAULT '0' COMMENT 'Vote percent approved by admin',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`primary_id`),
  KEY `RATING_OPTION_VOTE_AGGREGATED_RATING_ID` (`rating_id`),
  KEY `RATING_OPTION_VOTE_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `RATING_OPTION_VOTE_AGGREGATED_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  CONSTRAINT `RATING_OPTION_VOTE_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Rating vote aggregated';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_option_vote_aggregated`
--

LOCK TABLES `rating_option_vote_aggregated` WRITE;
/*!40000 ALTER TABLE `rating_option_vote_aggregated` DISABLE KEYS */;
INSERT INTO `rating_option_vote_aggregated` VALUES (1,1,1,1,4,80,80,0),(2,1,1,1,4,80,80,1),(3,1,2,1,5,100,0,0),(4,1,2,1,5,100,0,1);
/*!40000 ALTER TABLE `rating_option_vote_aggregated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_store`
--

DROP TABLE IF EXISTS `rating_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_store` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `RATING_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `RATING_STORE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  CONSTRAINT `RATING_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Store';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_store`
--

LOCK TABLES `rating_store` WRITE;
/*!40000 ALTER TABLE `rating_store` DISABLE KEYS */;
INSERT INTO `rating_store` VALUES (1,0),(2,0),(3,0),(1,1);
/*!40000 ALTER TABLE `rating_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_title`
--

DROP TABLE IF EXISTS `rating_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_title` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Rating Label',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `RATING_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `RATING_TITLE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE,
  CONSTRAINT `RATING_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Title';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_title`
--

LOCK TABLES `rating_title` WRITE;
/*!40000 ALTER TABLE `rating_title` DISABLE KEYS */;
INSERT INTO `rating_title` VALUES (1,1,'Calidad');
/*!40000 ALTER TABLE `rating_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_compared_product_index`
--

DROP TABLE IF EXISTS `report_compared_product_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_compared_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `REPORT_COMPARED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `REPORT_COMPARED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `REPORT_COMPARED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_CMPD_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_CMPD_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_COMPARED_PRODUCT_INDEX_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Reports Compared Product Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_compared_product_index`
--

LOCK TABLES `report_compared_product_index` WRITE;
/*!40000 ALTER TABLE `report_compared_product_index` DISABLE KEYS */;
INSERT INTO `report_compared_product_index` VALUES (1,79,NULL,1,NULL,'2020-03-05 22:12:38');
/*!40000 ALTER TABLE `report_compared_product_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_event`
--

DROP TABLE IF EXISTS `report_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_event` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Id',
  `logged_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Logged At',
  `event_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Type Id',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Object Id',
  `subject_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subject Id',
  `subtype` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Subtype',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`event_id`),
  KEY `REPORT_EVENT_EVENT_TYPE_ID` (`event_type_id`),
  KEY `REPORT_EVENT_SUBJECT_ID` (`subject_id`),
  KEY `REPORT_EVENT_OBJECT_ID` (`object_id`),
  KEY `REPORT_EVENT_SUBTYPE` (`subtype`),
  KEY `REPORT_EVENT_STORE_ID` (`store_id`),
  CONSTRAINT `REPORT_EVENT_EVENT_TYPE_ID_REPORT_EVENT_TYPES_EVENT_TYPE_ID` FOREIGN KEY (`event_type_id`) REFERENCES `report_event_types` (`event_type_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_EVENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8 COMMENT='Reports Event Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_event`
--

LOCK TABLES `report_event` WRITE;
/*!40000 ALTER TABLE `report_event` DISABLE KEYS */;
INSERT INTO `report_event` VALUES (1,'2020-03-02 17:43:45',1,1,11,1,1),(2,'2020-03-02 18:32:35',1,1,12,1,1),(3,'2020-03-02 19:30:31',1,1,16,1,1),(4,'2020-03-02 19:31:32',1,1,16,1,1),(5,'2020-03-02 19:35:08',1,1,17,1,1),(6,'2020-03-02 19:37:19',1,1,17,1,1),(7,'2020-03-02 21:06:13',1,1,20,1,1),(8,'2020-03-02 21:10:45',1,1,20,1,1),(9,'2020-03-02 21:11:40',1,1,20,1,1),(10,'2020-03-02 21:15:55',1,1,20,1,1),(11,'2020-03-02 21:19:16',1,1,20,1,1),(12,'2020-03-02 21:20:46',1,1,20,1,1),(13,'2020-03-02 21:25:48',1,1,20,1,1),(14,'2020-03-02 21:39:04',1,1,20,1,1),(15,'2020-03-02 21:40:32',1,1,20,1,1),(16,'2020-03-02 21:41:43',1,1,20,1,1),(17,'2020-03-02 21:42:35',1,1,20,1,1),(18,'2020-03-02 21:45:09',1,1,20,1,1),(19,'2020-03-02 21:46:55',1,1,20,1,1),(20,'2020-03-02 21:49:50',1,1,20,1,1),(21,'2020-03-02 21:50:54',1,1,20,1,1),(22,'2020-03-02 21:58:15',1,1,20,1,1),(23,'2020-03-02 22:02:11',1,1,20,1,1),(24,'2020-03-02 22:05:51',1,1,20,1,1),(25,'2020-03-02 22:07:24',1,1,20,1,1),(26,'2020-03-02 22:08:30',1,1,20,1,1),(27,'2020-03-02 22:10:37',1,1,20,1,1),(28,'2020-03-02 22:13:10',1,1,20,1,1),(29,'2020-03-02 22:15:42',1,1,20,1,1),(30,'2020-03-02 22:17:55',1,1,20,1,1),(31,'2020-03-02 22:19:30',1,1,20,1,1),(32,'2020-03-02 22:32:31',1,2,21,1,1),(33,'2020-03-02 22:46:54',1,1,21,1,1),(34,'2020-03-02 22:47:54',1,1,22,1,1),(35,'2020-03-02 22:50:36',1,1,24,1,1),(36,'2020-03-02 22:51:28',1,2,21,1,1),(37,'2020-03-02 22:53:22',1,1,26,1,1),(38,'2020-03-02 22:56:59',1,2,29,1,1),(39,'2020-03-02 23:01:24',1,1,32,1,1),(40,'2020-03-02 23:04:30',1,1,34,1,1),(41,'2020-03-02 23:09:16',1,1,36,1,1),(42,'2020-03-02 23:16:56',1,1,39,1,1),(43,'2020-03-02 23:49:06',1,1,39,1,1),(44,'2020-03-02 23:51:00',1,1,39,1,1),(45,'2020-03-02 23:57:14',1,1,39,1,1),(46,'2020-03-02 23:58:55',1,1,39,1,1),(47,'2020-03-03 00:03:12',1,1,39,1,1),(48,'2020-03-03 05:23:41',1,1,43,1,1),(49,'2020-03-03 05:24:50',1,1,43,1,1),(50,'2020-03-03 05:25:08',1,1,43,1,1),(51,'2020-03-03 05:37:36',1,1,43,1,1),(52,'2020-03-03 05:48:32',1,1,43,1,1),(53,'2020-03-03 06:03:33',1,1,43,1,1),(54,'2020-03-03 06:04:15',1,1,44,1,1),(55,'2020-03-03 22:37:25',1,1,55,1,1),(56,'2020-03-04 20:21:19',1,1,3,0,1),(57,'2020-03-04 20:21:39',4,1,3,0,1),(58,'2020-03-05 15:27:16',1,1,70,1,1),(59,'2020-03-05 17:13:50',4,1,72,1,1),(60,'2020-03-05 17:14:20',1,1,72,1,1),(61,'2020-03-05 17:44:55',1,1,73,1,1),(62,'2020-03-05 17:46:34',1,1,74,1,1),(63,'2020-03-05 18:40:09',1,1,76,1,1),(64,'2020-03-05 22:12:38',3,1,79,1,1),(65,'2020-03-06 00:02:22',1,1,81,1,1),(66,'2020-03-06 00:03:10',4,1,81,1,1),(67,'2020-03-06 16:52:48',4,1,88,1,1),(68,'2020-03-06 16:54:45',1,1,88,1,1),(69,'2020-03-06 17:57:59',4,1,89,1,1),(70,'2020-03-06 19:08:31',1,1,93,1,1),(71,'2020-03-06 19:08:35',4,1,94,1,1),(72,'2020-03-09 20:41:57',1,1,100,1,1),(73,'2020-03-09 20:43:44',1,1,100,1,1),(74,'2020-03-09 21:17:48',4,2,100,1,1),(75,'2020-03-09 21:17:52',1,4,100,1,1),(76,'2020-03-09 21:20:11',1,1,100,1,1),(77,'2020-03-09 21:26:07',1,2,100,1,1),(78,'2020-03-09 21:26:15',1,3,100,1,1),(79,'2020-03-09 21:36:52',1,3,100,1,1),(80,'2020-03-11 15:30:16',4,1,112,1,1),(81,'2020-03-11 19:07:33',1,2,113,1,1),(82,'2020-03-11 20:40:03',1,2,114,1,1),(83,'2020-03-11 22:14:51',4,2,120,1,1),(84,'2020-03-11 22:14:59',1,2,120,1,1),(85,'2020-03-11 22:19:23',1,2,120,1,1),(86,'2020-03-11 22:20:02',1,2,120,1,1),(87,'2020-03-11 22:49:25',1,3,121,1,1),(88,'2020-03-11 23:01:32',1,1,123,1,1),(89,'2020-03-11 23:53:03',1,1,125,1,1),(90,'2020-03-11 23:55:39',1,5,4,0,1),(91,'2020-03-12 02:51:47',1,5,129,1,1),(92,'2020-03-12 02:52:01',1,1,129,1,1),(93,'2020-03-12 02:54:39',1,1,129,1,1),(94,'2020-03-12 02:55:19',1,1,129,1,1),(95,'2020-03-12 02:55:36',1,2,129,1,1),(96,'2020-03-12 03:04:20',1,2,129,1,1),(97,'2020-03-12 03:04:22',1,1,129,1,1),(98,'2020-03-12 03:33:03',1,2,129,1,1),(99,'2020-03-12 03:34:12',1,1,129,1,1),(100,'2020-03-12 03:36:29',1,2,129,1,1),(101,'2020-03-12 03:36:44',1,1,129,1,1),(102,'2020-03-12 03:45:59',1,1,129,1,1),(103,'2020-03-12 03:46:00',1,2,129,1,1),(104,'2020-03-12 03:48:27',1,1,129,1,1),(105,'2020-03-12 03:48:35',1,2,129,1,1),(106,'2020-03-12 03:50:52',1,2,129,1,1),(107,'2020-03-12 03:50:56',1,1,129,1,1),(108,'2020-03-12 03:55:20',1,2,130,1,1),(109,'2020-03-12 03:55:20',1,1,131,1,1),(110,'2020-03-12 03:59:31',1,2,134,1,1),(111,'2020-03-12 03:59:31',1,1,134,1,1),(112,'2020-03-12 04:09:10',1,2,134,1,1),(113,'2020-03-12 04:09:32',1,1,134,1,1),(114,'2020-03-12 04:13:03',1,2,134,1,1),(115,'2020-03-12 04:13:06',1,1,134,1,1),(116,'2020-03-12 04:13:55',1,2,134,1,1),(117,'2020-03-12 04:13:55',1,1,134,1,1),(118,'2020-03-12 04:19:39',1,2,134,1,1),(119,'2020-03-12 04:19:39',1,1,134,1,1),(120,'2020-03-12 04:25:33',1,2,134,1,1),(121,'2020-03-12 04:29:06',1,2,134,1,1),(122,'2020-03-12 04:46:36',1,1,134,1,1),(123,'2020-03-12 04:49:14',4,1,134,1,1),(124,'2020-03-12 04:49:44',4,1,4,0,1),(125,'2020-03-12 05:02:55',1,2,4,0,1),(126,'2020-03-12 05:03:03',4,2,4,0,1),(127,'2020-03-12 15:48:49',1,1,136,1,1),(128,'2020-03-12 21:57:51',1,1,141,1,1),(129,'2020-03-12 22:15:11',1,1,143,1,1),(130,'2020-03-12 22:27:13',1,1,143,1,1),(131,'2020-03-12 22:37:58',1,1,143,1,1),(132,'2020-03-12 22:39:00',1,1,143,1,1),(133,'2020-03-12 22:54:03',1,1,143,1,1),(134,'2020-03-12 22:57:04',1,11,145,1,1),(135,'2020-03-12 23:09:42',1,1,146,1,1),(136,'2020-03-12 23:11:54',1,1,149,1,1),(137,'2020-03-12 23:13:40',1,1,149,1,1),(138,'2020-03-12 23:18:48',1,1,149,1,1),(139,'2020-03-12 23:25:18',1,1,149,1,1),(140,'2020-03-12 23:26:19',4,11,145,1,1),(141,'2020-03-12 23:26:19',1,11,145,1,1),(142,'2020-03-12 23:27:32',1,1,149,1,1),(143,'2020-03-12 23:31:05',1,1,149,1,1),(144,'2020-03-12 23:33:26',1,1,149,1,1),(145,'2020-03-12 23:39:04',1,1,149,1,1),(146,'2020-03-12 23:45:07',1,1,149,1,1),(147,'2020-03-12 23:55:29',1,1,149,1,1),(148,'2020-03-13 00:00:48',1,1,149,1,1),(149,'2020-03-13 18:24:59',1,9,157,1,1),(150,'2020-03-13 22:55:29',1,1,168,1,1),(151,'2020-03-13 23:33:30',1,5,168,1,1),(152,'2020-03-13 23:54:10',1,18,168,1,1),(153,'2020-03-13 23:54:23',4,18,168,1,1),(154,'2020-03-16 21:08:12',1,18,169,1,1),(155,'2020-03-17 18:00:35',1,1,174,1,1),(156,'2020-03-17 19:21:00',1,5,176,1,1),(157,'2020-03-17 19:26:43',1,1,179,1,1),(158,'2020-03-17 20:05:52',1,5,179,1,1),(159,'2020-03-17 20:06:05',1,11,179,1,1),(160,'2020-03-17 20:06:16',1,1,179,1,1),(161,'2020-03-17 20:10:51',1,1,179,1,1),(162,'2020-03-18 03:46:18',1,11,189,1,1),(163,'2020-03-18 03:46:30',1,1,189,1,1);
/*!40000 ALTER TABLE `report_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_event_types`
--

DROP TABLE IF EXISTS `report_event_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_event_types` (
  `event_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Type Id',
  `event_name` varchar(64) NOT NULL COMMENT 'Event Name',
  `customer_login` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Login',
  PRIMARY KEY (`event_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Reports Event Type Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_event_types`
--

LOCK TABLES `report_event_types` WRITE;
/*!40000 ALTER TABLE `report_event_types` DISABLE KEYS */;
INSERT INTO `report_event_types` VALUES (1,'catalog_product_view',0),(2,'sendfriend_product',0),(3,'catalog_product_compare_add_product',0),(4,'checkout_cart_add_product',0),(5,'wishlist_add_product',0),(6,'wishlist_share',0);
/*!40000 ALTER TABLE `report_event_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_viewed_product_aggregated_daily`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_viewed_product_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_DAILY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_DAILY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Daily';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_viewed_product_aggregated_daily`
--

LOCK TABLES `report_viewed_product_aggregated_daily` WRITE;
/*!40000 ALTER TABLE `report_viewed_product_aggregated_daily` DISABLE KEYS */;
INSERT INTO `report_viewed_product_aggregated_daily` VALUES (1,'2020-03-02',1,1,'Meteoro',39.8000,35,1),(2,'2020-03-02',1,2,'MeXXico Joven',24.9500,3,2),(3,'2020-03-03',1,1,'Meteoro',39.8000,17,1),(4,'2020-03-04',1,1,'Meteoro',39.8000,1,1),(5,'2020-03-05',1,1,'Meteoro',39.8000,5,1),(6,'2020-03-06',1,1,'Meteoro',39.8000,3,1),(7,'2020-03-09',1,1,'Meteoro',39.8000,3,1),(8,'2020-03-09',1,2,'MeXXico Joven',24.9500,1,3),(9,'2020-03-09',1,3,'Sin Piedad 44',27.0000,2,2),(10,'2020-03-09',1,4,'Sin Piedad Cirial',39.7000,1,4),(11,'2020-03-11',1,2,'MeXXico Joven',24.9500,5,1),(12,'2020-03-11',1,3,'Sin Piedad 44',27.0000,1,2),(13,'2020-03-12',1,1,'Meteoro',39.8000,3,1),(14,'2020-03-12',1,5,'Sin Piedad 44 “Petaca”',12.4500,2,2);
/*!40000 ALTER TABLE `report_viewed_product_aggregated_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_viewed_product_aggregated_monthly`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_monthly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_viewed_product_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_MONTHLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_MONTHLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Monthly';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_viewed_product_aggregated_monthly`
--

LOCK TABLES `report_viewed_product_aggregated_monthly` WRITE;
/*!40000 ALTER TABLE `report_viewed_product_aggregated_monthly` DISABLE KEYS */;
INSERT INTO `report_viewed_product_aggregated_monthly` VALUES (1,'2020-03-01',1,1,'Meteoro',39.8000,67,1),(2,'2020-03-01',1,2,'MeXXico Joven',24.9500,9,2),(3,'2020-03-01',1,3,'Sin Piedad 44',27.0000,3,3),(4,'2020-03-01',1,4,'Sin Piedad Cirial',39.7000,1,5),(8,'2020-03-01',1,5,'Sin Piedad 44 “Petaca”',12.4500,2,4);
/*!40000 ALTER TABLE `report_viewed_product_aggregated_monthly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_viewed_product_aggregated_yearly`
--

DROP TABLE IF EXISTS `report_viewed_product_aggregated_yearly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_viewed_product_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REPORT_VIEWED_PRD_AGGRED_YEARLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_VIEWED_PRD_AGGRED_YEARLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Yearly';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_viewed_product_aggregated_yearly`
--

LOCK TABLES `report_viewed_product_aggregated_yearly` WRITE;
/*!40000 ALTER TABLE `report_viewed_product_aggregated_yearly` DISABLE KEYS */;
INSERT INTO `report_viewed_product_aggregated_yearly` VALUES (1,'2020-01-01',1,1,'Meteoro',39.8000,67,1),(2,'2020-01-01',1,2,'MeXXico Joven',24.9500,9,2),(3,'2020-01-01',1,3,'Sin Piedad 44',27.0000,3,3),(4,'2020-01-01',1,4,'Sin Piedad Cirial',39.7000,1,5),(8,'2020-01-01',1,5,'Sin Piedad 44 “Petaca”',12.4500,2,4);
/*!40000 ALTER TABLE `report_viewed_product_aggregated_yearly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_viewed_product_index`
--

DROP TABLE IF EXISTS `report_viewed_product_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_viewed_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `REPORT_VIEWED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `REPORT_VIEWED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `REPORT_VIEWED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `REPORT_VIEWED_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REPORT_VIEWED_PRODUCT_INDEX_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8 COMMENT='Reports Viewed Product Index Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_viewed_product_index`
--

LOCK TABLES `report_viewed_product_index` WRITE;
/*!40000 ALTER TABLE `report_viewed_product_index` DISABLE KEYS */;
INSERT INTO `report_viewed_product_index` VALUES (1,11,NULL,1,1,'2020-03-02 17:43:45'),(2,12,NULL,1,1,'2020-03-02 18:32:35'),(3,16,NULL,1,1,'2020-03-02 19:30:31'),(5,17,NULL,1,1,'2020-03-02 19:35:08'),(7,20,NULL,1,1,'2020-03-02 21:06:13'),(32,21,NULL,2,1,'2020-03-02 22:32:31'),(33,21,NULL,1,1,'2020-03-02 22:46:54'),(34,22,NULL,1,1,'2020-03-02 22:47:54'),(35,24,NULL,1,1,'2020-03-02 22:50:36'),(37,26,NULL,1,1,'2020-03-02 22:53:22'),(38,29,NULL,2,1,'2020-03-02 22:56:59'),(39,32,NULL,1,1,'2020-03-02 23:01:24'),(40,34,NULL,1,1,'2020-03-02 23:04:30'),(41,36,NULL,1,1,'2020-03-02 23:09:16'),(42,39,NULL,1,1,'2020-03-02 23:16:56'),(48,43,NULL,1,1,'2020-03-03 05:23:41'),(54,44,NULL,1,1,'2020-03-03 06:04:15'),(55,55,NULL,1,1,'2020-03-03 22:37:25'),(56,NULL,3,1,1,'2020-03-04 20:21:19'),(57,70,NULL,1,1,'2020-03-05 15:27:16'),(58,72,NULL,1,1,'2020-03-05 17:14:20'),(59,73,NULL,1,1,'2020-03-05 17:44:55'),(60,74,NULL,1,1,'2020-03-05 17:46:34'),(61,76,NULL,1,1,'2020-03-05 18:40:09'),(62,81,NULL,1,1,'2020-03-06 00:02:22'),(63,88,NULL,1,1,'2020-03-06 16:54:45'),(64,93,NULL,1,1,'2020-03-06 19:08:31'),(65,100,NULL,1,1,'2020-03-09 20:41:57'),(67,100,NULL,4,1,'2020-03-09 21:17:52'),(69,100,NULL,2,1,'2020-03-09 21:26:07'),(70,100,NULL,3,1,'2020-03-09 21:26:15'),(72,113,NULL,2,1,'2020-03-11 19:07:33'),(73,114,NULL,2,1,'2020-03-11 20:40:03'),(74,120,NULL,2,1,'2020-03-11 22:14:59'),(77,121,NULL,3,1,'2020-03-11 22:49:25'),(78,123,NULL,1,1,'2020-03-11 23:01:32'),(79,125,NULL,1,1,'2020-03-11 23:53:03'),(80,NULL,4,5,1,'2020-03-11 23:55:39'),(81,129,NULL,5,1,'2020-03-12 02:51:47'),(82,129,NULL,1,1,'2020-03-12 02:52:01'),(85,129,NULL,2,1,'2020-03-12 02:55:36'),(98,130,NULL,2,1,'2020-03-12 03:55:20'),(99,131,NULL,1,1,'2020-03-12 03:55:20'),(100,134,4,2,1,'2020-03-12 03:59:31'),(101,134,4,1,1,'2020-03-12 03:59:31'),(114,136,NULL,1,1,'2020-03-12 15:48:49'),(115,141,NULL,1,1,'2020-03-12 21:57:51'),(116,143,NULL,1,1,'2020-03-12 22:15:11'),(121,145,NULL,11,1,'2020-03-12 22:57:04'),(122,146,NULL,1,1,'2020-03-12 23:09:42'),(123,149,NULL,1,1,'2020-03-12 23:11:54'),(135,157,NULL,9,1,'2020-03-13 18:24:59'),(136,168,NULL,1,1,'2020-03-13 22:55:29'),(137,168,NULL,5,1,'2020-03-13 23:33:30'),(138,168,NULL,18,1,'2020-03-13 23:54:10'),(139,169,NULL,18,1,'2020-03-16 21:08:12'),(140,174,NULL,1,1,'2020-03-17 18:00:35'),(141,176,NULL,5,1,'2020-03-17 19:21:00'),(142,179,NULL,1,1,'2020-03-17 19:26:43'),(143,179,NULL,5,1,'2020-03-17 20:05:52'),(144,179,NULL,11,1,'2020-03-17 20:06:05'),(147,189,NULL,11,1,'2020-03-18 03:46:18'),(148,189,NULL,1,1,'2020-03-18 03:46:30');
/*!40000 ALTER TABLE `report_viewed_product_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporting_counts`
--

DROP TABLE IF EXISTS `reporting_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_counts` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'Item Reported',
  `count` int(10) unsigned DEFAULT NULL COMMENT 'Count Value',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for all count related events generated via the cron job';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporting_counts`
--

LOCK TABLES `reporting_counts` WRITE;
/*!40000 ALTER TABLE `reporting_counts` DISABLE KEYS */;
/*!40000 ALTER TABLE `reporting_counts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporting_module_status`
--

DROP TABLE IF EXISTS `reporting_module_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_module_status` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Module Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Module Name',
  `active` varchar(255) DEFAULT NULL COMMENT 'Module Active Status',
  `setup_version` varchar(255) DEFAULT NULL COMMENT 'Module Version',
  `state` varchar(255) DEFAULT NULL COMMENT 'Module State',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module Status Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporting_module_status`
--

LOCK TABLES `reporting_module_status` WRITE;
/*!40000 ALTER TABLE `reporting_module_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `reporting_module_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporting_orders`
--

DROP TABLE IF EXISTS `reporting_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_orders` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `total` decimal(20,2) DEFAULT NULL COMMENT 'Total From Store',
  `total_base` decimal(20,2) DEFAULT NULL COMMENT 'Total From Base Currency',
  `item_count` int(10) unsigned NOT NULL COMMENT 'Line Item Count',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for all orders';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporting_orders`
--

LOCK TABLES `reporting_orders` WRITE;
/*!40000 ALTER TABLE `reporting_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reporting_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporting_system_updates`
--

DROP TABLE IF EXISTS `reporting_system_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_system_updates` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'Update Type',
  `action` varchar(255) DEFAULT NULL COMMENT 'Action Performed',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for system updates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporting_system_updates`
--

LOCK TABLES `reporting_system_updates` WRITE;
/*!40000 ALTER TABLE `reporting_system_updates` DISABLE KEYS */;
/*!40000 ALTER TABLE `reporting_system_updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporting_users`
--

DROP TABLE IF EXISTS `reporting_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_users` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `type` varchar(255) DEFAULT NULL COMMENT 'User Type',
  `action` varchar(255) DEFAULT NULL COMMENT 'Action Performed',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reporting for user actions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporting_users`
--

LOCK TABLES `reporting_users` WRITE;
/*!40000 ALTER TABLE `reporting_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `reporting_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `review_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Review create date',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity id',
  `entity_pk_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `status_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Status code',
  PRIMARY KEY (`review_id`),
  KEY `REVIEW_ENTITY_ID` (`entity_id`),
  KEY `REVIEW_STATUS_ID` (`status_id`),
  KEY `REVIEW_ENTITY_PK_VALUE` (`entity_pk_value`),
  CONSTRAINT `REVIEW_ENTITY_ID_REVIEW_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `review_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `REVIEW_STATUS_ID_REVIEW_STATUS_STATUS_ID` FOREIGN KEY (`status_id`) REFERENCES `review_status` (`status_id`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Review base information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,'2020-03-11 23:52:41',1,1,1),(2,'2020-03-12 04:25:32',1,2,2);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_detail`
--

DROP TABLE IF EXISTS `review_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_detail` (
  `detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review detail id',
  `review_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Review id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store id',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `detail` text NOT NULL COMMENT 'Detail description',
  `nickname` varchar(128) NOT NULL COMMENT 'User nickname',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  PRIMARY KEY (`detail_id`),
  KEY `REVIEW_DETAIL_REVIEW_ID` (`review_id`),
  KEY `REVIEW_DETAIL_STORE_ID` (`store_id`),
  KEY `REVIEW_DETAIL_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `REVIEW_DETAIL_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  CONSTRAINT `REVIEW_DETAIL_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  CONSTRAINT `REVIEW_DETAIL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Review detail information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_detail`
--

LOCK TABLES `review_detail` WRITE;
/*!40000 ALTER TABLE `review_detail` DISABLE KEYS */;
INSERT INTO `review_detail` VALUES (1,1,0,'sE VE','no se por q no se ve','aDMIN',NULL),(2,2,1,'dfsd','sddsff','Cliente',NULL);
/*!40000 ALTER TABLE `review_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_entity`
--

DROP TABLE IF EXISTS `review_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review entity id',
  `entity_code` varchar(32) NOT NULL COMMENT 'Review entity code',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review entities';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_entity`
--

LOCK TABLES `review_entity` WRITE;
/*!40000 ALTER TABLE `review_entity` DISABLE KEYS */;
INSERT INTO `review_entity` VALUES (1,'product'),(2,'customer'),(3,'category');
/*!40000 ALTER TABLE `review_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_entity_summary`
--

DROP TABLE IF EXISTS `review_entity_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_entity_summary` (
  `primary_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Summary review entity id',
  `entity_pk_value` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Product id',
  `entity_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Entity type id',
  `reviews_count` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Qty of reviews',
  `rating_summary` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Summarized rating',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`primary_id`),
  KEY `REVIEW_ENTITY_SUMMARY_STORE_ID` (`store_id`),
  CONSTRAINT `REVIEW_ENTITY_SUMMARY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review aggregates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_entity_summary`
--

LOCK TABLES `review_entity_summary` WRITE;
/*!40000 ALTER TABLE `review_entity_summary` DISABLE KEYS */;
INSERT INTO `review_entity_summary` VALUES (1,1,1,1,80,1),(2,1,1,1,80,0),(3,2,1,0,0,1);
/*!40000 ALTER TABLE `review_entity_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_status`
--

DROP TABLE IF EXISTS `review_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_status` (
  `status_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Status id',
  `status_code` varchar(32) NOT NULL COMMENT 'Status code',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Review statuses';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_status`
--

LOCK TABLES `review_status` WRITE;
/*!40000 ALTER TABLE `review_status` DISABLE KEYS */;
INSERT INTO `review_status` VALUES (1,'Approved'),(2,'Pending'),(3,'Not Approved');
/*!40000 ALTER TABLE `review_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_store`
--

DROP TABLE IF EXISTS `review_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_store` (
  `review_id` bigint(20) unsigned NOT NULL COMMENT 'Review Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`review_id`,`store_id`),
  KEY `REVIEW_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `REVIEW_STORE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  CONSTRAINT `REVIEW_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review Store';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_store`
--

LOCK TABLES `review_store` WRITE;
/*!40000 ALTER TABLE `review_store` DISABLE KEYS */;
INSERT INTO `review_store` VALUES (1,0),(2,0),(1,1),(2,1);
/*!40000 ALTER TABLE `review_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_bestsellers_aggregated_daily`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_bestsellers_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Daily';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_bestsellers_aggregated_daily`
--

LOCK TABLES `sales_bestsellers_aggregated_daily` WRITE;
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_daily` DISABLE KEYS */;
INSERT INTO `sales_bestsellers_aggregated_daily` VALUES (1,'2020-03-06',1,1,'Meteoro',56.6000,2.0000,1),(2,'2020-03-06',0,1,'Meteoro',56.6000,2.0000,1);
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_bestsellers_aggregated_monthly`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_monthly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_bestsellers_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Monthly';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_bestsellers_aggregated_monthly`
--

LOCK TABLES `sales_bestsellers_aggregated_monthly` WRITE;
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_monthly` DISABLE KEYS */;
INSERT INTO `sales_bestsellers_aggregated_monthly` VALUES (1,'2020-03-01',0,1,'Meteoro',56.6000,2.0000,1),(2,'2020-03-01',1,1,'Meteoro',56.6000,2.0000,1);
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_monthly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_bestsellers_aggregated_yearly`
--

DROP TABLE IF EXISTS `sales_bestsellers_aggregated_yearly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_bestsellers_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Yearly';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_bestsellers_aggregated_yearly`
--

LOCK TABLES `sales_bestsellers_aggregated_yearly` WRITE;
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_yearly` DISABLE KEYS */;
INSERT INTO `sales_bestsellers_aggregated_yearly` VALUES (1,'2020-01-01',0,1,'Meteoro',56.6000,2.0000,1),(2,'2020-01-01',1,1,'Meteoro',56.6000,2.0000,1);
/*!40000 ALTER TABLE `sales_bestsellers_aggregated_yearly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_creditmemo`
--

DROP TABLE IF EXISTS `sales_creditmemo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_creditmemo` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `creditmemo_status` int(11) DEFAULT NULL COMMENT 'Creditmemo Status',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Invoice Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_CREDITMEMO_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_CREDITMEMO_STORE_ID` (`store_id`),
  KEY `SALES_CREDITMEMO_ORDER_ID` (`order_id`),
  KEY `SALES_CREDITMEMO_CREDITMEMO_STATUS` (`creditmemo_status`),
  KEY `SALES_CREDITMEMO_STATE` (`state`),
  KEY `SALES_CREDITMEMO_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_UPDATED_AT` (`updated_at`),
  KEY `SALES_CREDITMEMO_SEND_EMAIL` (`send_email`),
  KEY `SALES_CREDITMEMO_EMAIL_SENT` (`email_sent`),
  CONSTRAINT `SALES_CREDITMEMO_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_CREDITMEMO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_creditmemo`
--

LOCK TABLES `sales_creditmemo` WRITE;
/*!40000 ALTER TABLE `sales_creditmemo` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_creditmemo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_creditmemo_comment`
--

DROP TABLE IF EXISTS `sales_creditmemo_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_creditmemo_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_CREDITMEMO_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_CREDITMEMO_COMMENT_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Comment';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_creditmemo_comment`
--

LOCK TABLES `sales_creditmemo_comment` WRITE;
/*!40000 ALTER TABLE `sales_creditmemo_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_creditmemo_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_creditmemo_grid`
--

DROP TABLE IF EXISTS `sales_creditmemo_grid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_creditmemo_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `state` int(11) DEFAULT NULL COMMENT 'Status',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `order_base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Order Grand Total',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_CREDITMEMO_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_CREDITMEMO_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_CREDITMEMO_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_CREDITMEMO_GRID_STATE` (`state`),
  KEY `SALES_CREDITMEMO_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_STATUS` (`order_status`),
  KEY `SALES_CREDITMEMO_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_CREDITMEMO_GRID_STORE_ID` (`store_id`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_BASE_GRAND_TOTAL` (`order_base_grand_total`),
  KEY `SALES_CREDITMEMO_GRID_ORDER_ID` (`order_id`),
  FULLTEXT KEY `FTI_32B7BA885941A8254EE84AE650ABDC86` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Grid';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_creditmemo_grid`
--

LOCK TABLES `sales_creditmemo_grid` WRITE;
/*!40000 ALTER TABLE `sales_creditmemo_grid` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_creditmemo_grid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_creditmemo_item`
--

DROP TABLE IF EXISTS `sales_creditmemo_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_creditmemo_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax in the creditmemo item over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_CREDITMEMO_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_CREDITMEMO_ITEM_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Item';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_creditmemo_item`
--

LOCK TABLES `sales_creditmemo_item` WRITE;
/*!40000 ALTER TABLE `sales_creditmemo_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_creditmemo_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoice`
--

DROP TABLE IF EXISTS `sales_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoice` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `is_used_for_refund` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Used For Refund',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `can_void_flag` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Void Flag',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_INVOICE_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_INVOICE_STORE_ID` (`store_id`),
  KEY `SALES_INVOICE_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_INVOICE_ORDER_ID` (`order_id`),
  KEY `SALES_INVOICE_STATE` (`state`),
  KEY `SALES_INVOICE_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_UPDATED_AT` (`updated_at`),
  KEY `SALES_INVOICE_SEND_EMAIL` (`send_email`),
  KEY `SALES_INVOICE_EMAIL_SENT` (`email_sent`),
  CONSTRAINT `SALES_INVOICE_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_INVOICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoice`
--

LOCK TABLES `sales_invoice` WRITE;
/*!40000 ALTER TABLE `sales_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoice_comment`
--

DROP TABLE IF EXISTS `sales_invoice_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoice_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_INVOICE_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_INVOICE_COMMENT_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Comment';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoice_comment`
--

LOCK TABLES `sales_invoice_comment` WRITE;
/*!40000 ALTER TABLE `sales_invoice_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoice_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoice_grid`
--

DROP TABLE IF EXISTS `sales_invoice_grid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoice_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(128) DEFAULT NULL COMMENT 'Payment Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_INVOICE_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_INVOICE_GRID_STORE_ID` (`store_id`),
  KEY `SALES_INVOICE_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_INVOICE_GRID_ORDER_ID` (`order_id`),
  KEY `SALES_INVOICE_GRID_STATE` (`state`),
  KEY `SALES_INVOICE_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_INVOICE_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_INVOICE_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_INVOICE_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_INVOICE_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_INVOICE_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  FULLTEXT KEY `FTI_95D9C924DD6A8734EB8B5D01D60F90DE` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Grid';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoice_grid`
--

LOCK TABLES `sales_invoice_grid` WRITE;
/*!40000 ALTER TABLE `sales_invoice_grid` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoice_grid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoice_item`
--

DROP TABLE IF EXISTS `sales_invoice_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoice_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax invoiced over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_INVOICE_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_INVOICE_ITEM_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Item';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoice_item`
--

LOCK TABLES `sales_invoice_item` WRITE;
/*!40000 ALTER TABLE `sales_invoice_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoice_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoiced_aggregated`
--

DROP TABLE IF EXISTS `sales_invoiced_aggregated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoiced_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_INVOICED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_INVOICED_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_INVOICED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoiced_aggregated`
--

LOCK TABLES `sales_invoiced_aggregated` WRITE;
/*!40000 ALTER TABLE `sales_invoiced_aggregated` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoiced_aggregated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoiced_aggregated_order`
--

DROP TABLE IF EXISTS `sales_invoiced_aggregated_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoiced_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_INVOICED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated Order';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoiced_aggregated_order`
--

LOCK TABLES `sales_invoiced_aggregated_order` WRITE;
/*!40000 ALTER TABLE `sales_invoiced_aggregated_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoiced_aggregated_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order`
--

DROP TABLE IF EXISTS `sales_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `state` varchar(32) DEFAULT NULL COMMENT 'State',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `protect_code` varchar(255) DEFAULT NULL COMMENT 'Protect Code',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Canceled',
  `base_discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Invoiced',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `base_shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Canceled',
  `base_shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Invoiced',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Refunded',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `base_subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Canceled',
  `base_subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Invoiced',
  `base_subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Refunded',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Canceled',
  `base_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Invoiced',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `base_total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Canceled',
  `base_total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced',
  `base_total_invoiced_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced Cost',
  `base_total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Offline Refunded',
  `base_total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Online Refunded',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `base_total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Qty Ordered',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Canceled',
  `discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Invoiced',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Canceled',
  `shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Invoiced',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Refunded',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Canceled',
  `subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Invoiced',
  `subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Refunded',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Tax Invoiced',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Total Canceled',
  `total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Total Invoiced',
  `total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Offline Refunded',
  `total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Online Refunded',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty Ordered',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  `can_ship_partially` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially',
  `can_ship_partially_item` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially Item',
  `customer_is_guest` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Is Guest',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `edit_increment` int(11) DEFAULT NULL COMMENT 'Edit Increment',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `forced_shipment_with_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Forced Do Shipment With Invoice',
  `payment_auth_expiration` int(11) DEFAULT NULL COMMENT 'Payment Authorization Expiration',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `quote_id` int(11) DEFAULT NULL COMMENT 'Quote Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `base_total_due` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Due',
  `payment_authorization_amount` decimal(12,4) DEFAULT NULL COMMENT 'Payment Authorization Amount',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `total_due` decimal(12,4) DEFAULT NULL COMMENT 'Total Due',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `increment_id` varchar(32) DEFAULT NULL COMMENT 'Increment Id',
  `applied_rule_ids` varchar(128) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_firstname` varchar(128) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_lastname` varchar(128) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_middlename` varchar(128) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_prefix` varchar(32) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_suffix` varchar(32) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_taxvat` varchar(32) DEFAULT NULL COMMENT 'Customer Taxvat',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `ext_customer_id` varchar(32) DEFAULT NULL COMMENT 'Ext Customer Id',
  `ext_order_id` varchar(32) DEFAULT NULL COMMENT 'Ext Order Id',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `hold_before_state` varchar(32) DEFAULT NULL COMMENT 'Hold Before State',
  `hold_before_status` varchar(32) DEFAULT NULL COMMENT 'Hold Before Status',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `original_increment_id` varchar(32) DEFAULT NULL COMMENT 'Original Increment Id',
  `relation_child_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Id',
  `relation_child_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Real Id',
  `relation_parent_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Id',
  `relation_parent_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Real Id',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `shipping_method` varchar(32) DEFAULT NULL COMMENT 'Shipping Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `store_name` varchar(32) DEFAULT NULL COMMENT 'Store Name',
  `x_forwarded_for` varchar(32) DEFAULT NULL COMMENT 'X Forwarded For',
  `customer_note` text COMMENT 'Customer Note',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `total_item_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Total Item Count',
  `customer_gender` int(11) DEFAULT NULL COMMENT 'Customer Gender',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `coupon_rule_name` varchar(255) DEFAULT NULL COMMENT 'Coupon Sales Rule Name',
  `paypal_ipn_customer_notified` int(11) DEFAULT '0' COMMENT 'Paypal Ipn Customer Notified',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_ORDER_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_ORDER_STATUS` (`status`),
  KEY `SALES_ORDER_STATE` (`state`),
  KEY `SALES_ORDER_STORE_ID` (`store_id`),
  KEY `SALES_ORDER_CREATED_AT` (`created_at`),
  KEY `SALES_ORDER_CUSTOMER_ID` (`customer_id`),
  KEY `SALES_ORDER_EXT_ORDER_ID` (`ext_order_id`),
  KEY `SALES_ORDER_QUOTE_ID` (`quote_id`),
  KEY `SALES_ORDER_UPDATED_AT` (`updated_at`),
  KEY `SALES_ORDER_SEND_EMAIL` (`send_email`),
  KEY `SALES_ORDER_EMAIL_SENT` (`email_sent`),
  CONSTRAINT `SALES_ORDER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  CONSTRAINT `SALES_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order`
--

LOCK TABLES `sales_order` WRITE;
/*!40000 ALTER TABLE `sales_order` DISABLE KEYS */;
INSERT INTO `sales_order` VALUES (1,'new','pending',NULL,'2e26cb4342250119e29984257675e599','Flat Rate - Fixed',0,1,NULL,0.0000,NULL,NULL,NULL,123.2000,10.0000,NULL,NULL,NULL,0.0000,NULL,113.2000,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,1.0000,1.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,123.2000,10.0000,NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,113.2000,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2.0000,NULL,NULL,NULL,1,1,2,0,NULL,1,1,NULL,NULL,NULL,5,1,NULL,NULL,NULL,NULL,0.0000,113.2000,123.2000,NULL,0.0000,113.2000,123.2000,140.0000,NULL,'000000001',NULL,'EUR','juan.op@disandat.mx',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'EUR',NULL,NULL,'EUR',NULL,NULL,NULL,NULL,NULL,'189.213.163.237','flatrate_flatrate','EUR','Main Website\nMain Website Store\n',NULL,NULL,'2020-03-06 00:04:48','2020-03-06 00:04:48',1,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,10.0000,10.0000,NULL,0,NULL),(2,'new','pending',NULL,'9f5e17823670ab1cb042e852a1f7a0dc','Cultura Mezcal - Tarifa Plana',0,1,4,0.0000,NULL,NULL,NULL,243.8000,5.0000,NULL,NULL,NULL,0.0000,NULL,238.8000,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,1.0000,1.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,243.8000,5.0000,NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,238.8000,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.0000,NULL,NULL,NULL,0,1,4,1,NULL,1,1,NULL,NULL,NULL,12,3,NULL,NULL,NULL,NULL,0.0000,238.8000,243.8000,NULL,0.0000,238.8000,243.8000,0.0000,NULL,'000000002',NULL,'EUR','kdodeth@hotmail.com','Cliente','prueba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'EUR',NULL,NULL,'EUR',NULL,NULL,NULL,NULL,NULL,'187.189.198.170','flatrate_flatrate','EUR','Main Website\nMain Website Store\n',NULL,NULL,'2020-03-12 05:01:38','2020-03-12 05:01:39',1,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,5.0000,5.0000,NULL,0,NULL),(3,'new','pending',NULL,'e6afe70bf3838d365e0f698d170e4306','Cultura Mezcal - Tarifa Plana',0,1,4,0.0000,NULL,NULL,NULL,154.7000,5.0000,NULL,NULL,NULL,0.0000,NULL,149.7000,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,1.0000,1.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,154.7000,5.0000,NULL,NULL,NULL,0.0000,NULL,0.0000,0.0000,149.7000,NULL,NULL,NULL,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.0000,NULL,NULL,NULL,0,1,6,1,NULL,1,1,NULL,NULL,NULL,14,5,NULL,NULL,NULL,NULL,0.0000,149.7000,154.7000,NULL,0.0000,149.7000,154.7000,0.0000,NULL,'000000003',NULL,'EUR','kdodeth@hotmail.com','Cliente','prueba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'EUR',NULL,NULL,'EUR',NULL,NULL,NULL,NULL,NULL,'187.189.91.159','flatrate_flatrate','EUR','Main Website\nMain Website Store\n',NULL,NULL,'2020-03-12 05:09:10','2020-03-12 05:09:10',1,NULL,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,5.0000,5.0000,NULL,0,NULL);
/*!40000 ALTER TABLE `sales_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_address`
--

DROP TABLE IF EXISTS `sales_order_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_address` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `customer_address_id` int(11) DEFAULT NULL COMMENT 'Customer Address Id',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `region_id` int(11) DEFAULT NULL COMMENT 'Region Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Postcode',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Lastname',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(255) DEFAULT NULL COMMENT 'City',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `telephone` varchar(255) DEFAULT NULL COMMENT 'Phone Number',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Firstname',
  `address_type` varchar(255) DEFAULT NULL COMMENT 'Address Type',
  `prefix` varchar(255) DEFAULT NULL COMMENT 'Prefix',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middlename',
  `suffix` varchar(255) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_ADDRESS_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_ORDER_ADDRESS_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Address';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_address`
--

LOCK TABLES `sales_order_address` WRITE;
/*!40000 ALTER TABLE `sales_order_address` DISABLE KEYS */;
INSERT INTO `sales_order_address` VALUES (1,1,NULL,NULL,NULL,NULL,NULL,'Mexico','11320','Ortiz','Laguna de Mayrán 166\nEdificio Copenhague 1202, Anahuac, Miguel Hidalgo','Mexico','juan.op@disandat.mx','5552176974','MX','Juan Luis','shipping',NULL,NULL,NULL,'Disandat',NULL,NULL,NULL,NULL,NULL),(2,1,NULL,NULL,NULL,NULL,NULL,'Mexico','11320','Ortiz','Laguna de Mayrán 166\nEdificio Copenhague 1202, Anahuac, Miguel Hidalgo','Mexico','juan.op@disandat.mx','5552176974','MX','Juan Luis','billing',NULL,NULL,NULL,'Disandat',NULL,NULL,NULL,NULL,NULL),(3,2,1,NULL,17,NULL,NULL,'Federated States Of Micronesia','45644','prueba','asdasd','sdfdsf','kdodeth@hotmail.com','4575545','US','Cliente','shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,2,NULL,NULL,17,NULL,NULL,'Federated States Of Micronesia','45644','prueba','asdasd','sdfdsf','kdodeth@hotmail.com','4575545','US','Cliente','billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,3,1,NULL,17,NULL,NULL,'Federated States Of Micronesia','45644','prueba','asdasd','sdfdsf','kdodeth@hotmail.com','4575545','US','Cliente','shipping',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,3,1,NULL,17,NULL,NULL,'Federated States Of Micronesia','45644','prueba','asdasd','sdfdsf','kdodeth@hotmail.com','4575545','US','Cliente','billing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sales_order_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_aggregated_created`
--

DROP TABLE IF EXISTS `sales_order_aggregated_created`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_ORDER_AGGREGATED_CREATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Created';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_aggregated_created`
--

LOCK TABLES `sales_order_aggregated_created` WRITE;
/*!40000 ALTER TABLE `sales_order_aggregated_created` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_aggregated_created` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_aggregated_updated`
--

DROP TABLE IF EXISTS `sales_order_aggregated_updated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_ORDER_AGGREGATED_UPDATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Updated';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_aggregated_updated`
--

LOCK TABLES `sales_order_aggregated_updated` WRITE;
/*!40000 ALTER TABLE `sales_order_aggregated_updated` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_aggregated_updated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_grid`
--

DROP TABLE IF EXISTS `sales_order_grid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `order_currency_code` varchar(255) DEFAULT NULL COMMENT 'Order Currency Code',
  `shipping_name` varchar(255) DEFAULT NULL COMMENT 'Shipping Name',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group` varchar(255) DEFAULT NULL COMMENT 'Customer Group',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `payment_method` varchar(255) DEFAULT NULL COMMENT 'Payment Method',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_ORDER_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_ORDER_GRID_STATUS` (`status`),
  KEY `SALES_ORDER_GRID_STORE_ID` (`store_id`),
  KEY `SALES_ORDER_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `SALES_ORDER_GRID_BASE_TOTAL_PAID` (`base_total_paid`),
  KEY `SALES_ORDER_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `SALES_ORDER_GRID_TOTAL_PAID` (`total_paid`),
  KEY `SALES_ORDER_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `SALES_ORDER_GRID_BILLING_NAME` (`billing_name`),
  KEY `SALES_ORDER_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_ORDER_GRID_CUSTOMER_ID` (`customer_id`),
  KEY `SALES_ORDER_GRID_UPDATED_AT` (`updated_at`),
  FULLTEXT KEY `FTI_65B9E9925EC58F0C7C2E2F6379C233E7` (`increment_id`,`billing_name`,`shipping_name`,`shipping_address`,`billing_address`,`customer_name`,`customer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Grid';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_grid`
--

LOCK TABLES `sales_order_grid` WRITE;
/*!40000 ALTER TABLE `sales_order_grid` DISABLE KEYS */;
INSERT INTO `sales_order_grid` VALUES (1,'pending',1,'Main Website\nMain Website Store\n',NULL,123.2000,NULL,123.2000,NULL,'000000001','EUR','EUR','Juan Luis Ortiz','Juan Luis Ortiz','2020-03-06 00:04:48','2020-03-06 00:04:48','Laguna de Mayrán 166\nEdificio Copenhague 1202, Anahuac, Miguel Hidalgo Mexico Mexico 11320','Laguna de Mayrán 166\nEdificio Copenhague 1202, Anahuac, Miguel Hidalgo Mexico Mexico 11320','Flat Rate - Fixed','juan.op@disandat.mx','0',113.2000,10.0000,'','checkmo',NULL),(2,'pending',1,'Main Website\nMain Website Store\n',4,243.8000,NULL,243.8000,NULL,'000000002','EUR','EUR','Cliente prueba','Cliente prueba','2020-03-12 05:01:38','2020-03-12 05:01:38','asdasd sdfdsf Federated States Of Micronesia 45644','asdasd sdfdsf Federated States Of Micronesia 45644','Cultura Mezcal - Tarifa Plana','kdodeth@hotmail.com','1',238.8000,5.0000,'Cliente prueba','checkmo',NULL),(3,'pending',1,'Main Website\nMain Website Store\n',4,154.7000,NULL,154.7000,NULL,'000000003','EUR','EUR','Cliente prueba','Cliente prueba','2020-03-12 05:09:10','2020-03-12 05:09:10','asdasd sdfdsf Federated States Of Micronesia 45644','asdasd sdfdsf Federated States Of Micronesia 45644','Cultura Mezcal - Tarifa Plana','kdodeth@hotmail.com','1',149.7000,5.0000,'Cliente prueba','banktransfer',NULL);
/*!40000 ALTER TABLE `sales_order_grid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_item`
--

DROP TABLE IF EXISTS `sales_order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Quote Item Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `product_options` text COMMENT 'Product Options',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'No Discount',
  `qty_backordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Backordered',
  `qty_canceled` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Canceled',
  `qty_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Invoiced',
  `qty_ordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `qty_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Refunded',
  `qty_shipped` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Shipped',
  `base_cost` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Cost',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `original_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `base_original_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Original Price',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Invoiced',
  `base_tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Invoiced',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Invoiced',
  `base_discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Invoiced',
  `amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Amount Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Amount Refunded',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Invoiced',
  `base_row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Invoiced',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `ext_order_item_id` varchar(255) DEFAULT NULL COMMENT 'Ext Order Item Id',
  `locked_do_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Invoice',
  `locked_do_ship` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Ship',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `discount_tax_compensation_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Canceled',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `free_shipping` smallint(6) DEFAULT NULL,
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `gift_message_available` int(11) DEFAULT NULL COMMENT 'Gift Message Available',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `SALES_ORDER_ITEM_ORDER_ID` (`order_id`),
  KEY `SALES_ORDER_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_ORDER_ITEM_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_ORDER_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Item';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_item`
--

LOCK TABLES `sales_order_item` WRITE;
/*!40000 ALTER TABLE `sales_order_item` DISABLE KEYS */;
INSERT INTO `sales_order_item` VALUES (1,1,NULL,3,1,'2020-03-06 00:04:48','2020-03-06 00:04:48',1,'simple','a:1:{s:15:\"info_buyRequest\";a:5:{s:4:\"uenc\";s:72:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21lemNhbC1tZXRlb3JvLmh0bWw,\";s:7:\"product\";s:1:\"1\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:3:\"qty\";s:1:\"1\";}}',70.0000,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,NULL,0.0000,0.0000,2.0000,0.0000,0.0000,NULL,56.6000,56.6000,56.6000,56.6000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,113.2000,113.2000,0.0000,0.0000,140.0000,NULL,NULL,NULL,NULL,NULL,56.6000,56.6000,113.2000,113.2000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,NULL,11,1,'2020-03-12 05:01:38','2020-03-12 05:01:38',1,'simple','a:2:{s:15:\"info_buyRequest\";a:6:{s:4:\"uenc\";s:80:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21ldGVvcm8uaHRtbD9vcHRpb25zPWNhcnQ,\";s:7:\"product\";s:1:\"1\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:7:\"options\";a:1:{i:5;s:1:\"9\";}s:3:\"qty\";s:1:\"1\";}s:7:\"options\";a:1:{i:0;a:7:{s:5:\"label\";s:6:\"Unidad\";s:5:\"value\";s:4:\"Caja\";s:11:\"print_value\";s:4:\"Caja\";s:9:\"option_id\";s:1:\"5\";s:11:\"option_type\";s:9:\"drop_down\";s:12:\"option_value\";s:1:\"9\";s:11:\"custom_view\";b:0;}}}',NULL,0,'MT01','Meteoro',NULL,NULL,NULL,0,0,NULL,0.0000,0.0000,1.0000,0.0000,0.0000,NULL,238.8000,238.8000,39.8000,39.8000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,238.8000,238.8000,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,238.8000,238.8000,238.8000,238.8000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,3,NULL,12,1,'2020-03-12 05:09:10','2020-03-12 05:09:10',2,'simple','a:2:{s:15:\"info_buyRequest\";a:6:{s:4:\"uenc\";s:88:\"aHR0cDovL2N1bHR1cmFtZXpjYWwuY29tL21lemNhbC0zL21leHhpY28tam92ZW4uaHRtbD9vcHRpb25zPWNhcnQ,\";s:7:\"product\";s:1:\"2\";s:28:\"selected_configurable_option\";s:0:\"\";s:15:\"related_product\";s:0:\"\";s:7:\"options\";a:1:{i:4;s:1:\"7\";}s:3:\"qty\";s:1:\"1\";}s:7:\"options\";a:1:{i:0;a:7:{s:5:\"label\";s:6:\"Unidad\";s:5:\"value\";s:4:\"Caja\";s:11:\"print_value\";s:4:\"Caja\";s:9:\"option_id\";s:1:\"4\";s:11:\"option_type\";s:9:\"drop_down\";s:12:\"option_value\";s:1:\"7\";s:11:\"custom_view\";b:0;}}}',NULL,0,'MX01','MeXXico Joven',NULL,NULL,NULL,0,0,NULL,0.0000,0.0000,1.0000,0.0000,0.0000,NULL,149.7000,149.7000,24.9500,24.9500,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,149.7000,149.7000,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,149.7000,149.7000,149.7000,149.7000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sales_order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_payment`
--

DROP TABLE IF EXISTS `sales_order_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_payment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Captured',
  `shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Captured',
  `amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Amount Refunded',
  `base_amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid',
  `amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Amount Canceled',
  `base_amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Authorized',
  `base_amount_paid_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid Online',
  `base_amount_refunded_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded Online',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Amount Paid',
  `amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Amount Authorized',
  `base_amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Ordered',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded',
  `amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Amount Ordered',
  `base_amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Canceled',
  `quote_payment_id` int(11) DEFAULT NULL COMMENT 'Quote Payment Id',
  `additional_data` text COMMENT 'Additional Data',
  `cc_exp_month` varchar(12) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_ss_start_year` varchar(12) DEFAULT NULL COMMENT 'Cc Ss Start Year',
  `echeck_bank_name` varchar(128) DEFAULT NULL COMMENT 'Echeck Bank Name',
  `method` varchar(128) DEFAULT NULL COMMENT 'Method',
  `cc_debug_request_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Request Body',
  `cc_secure_verify` varchar(32) DEFAULT NULL COMMENT 'Cc Secure Verify',
  `protection_eligibility` varchar(32) DEFAULT NULL COMMENT 'Protection Eligibility',
  `cc_approval` varchar(32) DEFAULT NULL COMMENT 'Cc Approval',
  `cc_last_4` varchar(100) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_status_description` varchar(32) DEFAULT NULL COMMENT 'Cc Status Description',
  `echeck_type` varchar(32) DEFAULT NULL COMMENT 'Echeck Type',
  `cc_debug_response_serialized` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Serialized',
  `cc_ss_start_month` varchar(128) DEFAULT NULL COMMENT 'Cc Ss Start Month',
  `echeck_account_type` varchar(255) DEFAULT NULL COMMENT 'Echeck Account Type',
  `last_trans_id` varchar(32) DEFAULT NULL COMMENT 'Last Trans Id',
  `cc_cid_status` varchar(32) DEFAULT NULL COMMENT 'Cc Cid Status',
  `cc_owner` varchar(128) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_type` varchar(32) DEFAULT NULL COMMENT 'Cc Type',
  `po_number` varchar(32) DEFAULT NULL COMMENT 'Po Number',
  `cc_exp_year` varchar(4) DEFAULT NULL COMMENT 'Cc Exp Year',
  `cc_status` varchar(4) DEFAULT NULL COMMENT 'Cc Status',
  `echeck_routing_number` varchar(32) DEFAULT NULL COMMENT 'Echeck Routing Number',
  `account_status` varchar(32) DEFAULT NULL COMMENT 'Account Status',
  `anet_trans_method` varchar(32) DEFAULT NULL COMMENT 'Anet Trans Method',
  `cc_debug_response_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Body',
  `cc_ss_issue` varchar(32) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `echeck_account_name` varchar(32) DEFAULT NULL COMMENT 'Echeck Account Name',
  `cc_avs_status` varchar(32) DEFAULT NULL COMMENT 'Cc Avs Status',
  `cc_number_enc` varchar(32) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_trans_id` varchar(32) DEFAULT NULL COMMENT 'Cc Trans Id',
  `address_status` varchar(32) DEFAULT NULL COMMENT 'Address Status',
  `additional_information` text COMMENT 'Additional Information',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_PAYMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_ORDER_PAYMENT_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Payment';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_payment`
--

LOCK TABLES `sales_order_payment` WRITE;
/*!40000 ALTER TABLE `sales_order_payment` DISABLE KEYS */;
INSERT INTO `sales_order_payment` VALUES (1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10.0000,10.0000,NULL,NULL,123.2000,NULL,NULL,NULL,123.2000,NULL,NULL,NULL,NULL,NULL,NULL,'checkmo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a:3:{s:12:\"method_title\";s:19:\"Check / Money order\";s:10:\"payable_to\";N;s:15:\"mailing_address\";N;}'),(2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5.0000,5.0000,NULL,NULL,243.8000,NULL,NULL,NULL,243.8000,NULL,NULL,NULL,NULL,NULL,NULL,'checkmo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a:3:{s:12:\"method_title\";s:19:\"Check / Money order\";s:10:\"payable_to\";N;s:15:\"mailing_address\";N;}'),(3,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5.0000,5.0000,NULL,NULL,154.7000,NULL,NULL,NULL,154.7000,NULL,NULL,NULL,NULL,NULL,NULL,'banktransfer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a:2:{s:12:\"method_title\";s:21:\"Bank Transfer Payment\";s:12:\"instructions\";s:55:\"Cuentas de CulturaMezcal:\r\nBanco\r\nNo. 12345678910121314\";}');
/*!40000 ALTER TABLE `sales_order_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_status`
--

DROP TABLE IF EXISTS `sales_order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_status` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_status`
--

LOCK TABLES `sales_order_status` WRITE;
/*!40000 ALTER TABLE `sales_order_status` DISABLE KEYS */;
INSERT INTO `sales_order_status` VALUES ('canceled','Canceled'),('closed','Closed'),('complete','Complete'),('fraud','Suspected Fraud'),('holded','On Hold'),('payment_review','Payment Review'),('paypal_canceled_reversal','PayPal Canceled Reversal'),('paypal_reversed','PayPal Reversed'),('pending','Pending'),('pending_payment','Pending Payment'),('pending_paypal','Pending PayPal'),('processing','Processing');
/*!40000 ALTER TABLE `sales_order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_status_history`
--

DROP TABLE IF EXISTS `sales_order_status_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_status_history` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `entity_name` varchar(32) DEFAULT NULL COMMENT 'Shows what entity history is bind to.',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_ORDER_STATUS_HISTORY_PARENT_ID` (`parent_id`),
  KEY `SALES_ORDER_STATUS_HISTORY_CREATED_AT` (`created_at`),
  CONSTRAINT `SALES_ORDER_STATUS_HISTORY_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Status History';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_status_history`
--

LOCK TABLES `sales_order_status_history` WRITE;
/*!40000 ALTER TABLE `sales_order_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_status_label`
--

DROP TABLE IF EXISTS `sales_order_status_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_status_label` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`,`store_id`),
  KEY `SALES_ORDER_STATUS_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_ORDER_STATUS_LABEL_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE,
  CONSTRAINT `SALES_ORDER_STATUS_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Label Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_status_label`
--

LOCK TABLES `sales_order_status_label` WRITE;
/*!40000 ALTER TABLE `sales_order_status_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_status_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_status_state`
--

DROP TABLE IF EXISTS `sales_order_status_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_status_state` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `state` varchar(32) NOT NULL COMMENT 'Label',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visible on front',
  PRIMARY KEY (`status`,`state`),
  CONSTRAINT `SALES_ORDER_STATUS_STATE_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_status_state`
--

LOCK TABLES `sales_order_status_state` WRITE;
/*!40000 ALTER TABLE `sales_order_status_state` DISABLE KEYS */;
INSERT INTO `sales_order_status_state` VALUES ('canceled','canceled',1,1),('closed','closed',1,1),('complete','complete',1,1),('fraud','payment_review',0,1),('fraud','processing',0,1),('holded','holded',1,1),('payment_review','payment_review',1,1),('pending','new',1,1),('pending_payment','pending_payment',1,0),('processing','processing',1,1);
/*!40000 ALTER TABLE `sales_order_status_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_tax`
--

DROP TABLE IF EXISTS `sales_order_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_tax` (
  `tax_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `percent` decimal(12,4) DEFAULT NULL COMMENT 'Percent',
  `amount` decimal(12,4) DEFAULT NULL COMMENT 'Amount',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `base_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount',
  `process` smallint(6) NOT NULL COMMENT 'Process',
  `base_real_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Real Amount',
  PRIMARY KEY (`tax_id`),
  KEY `SALES_ORDER_TAX_ORDER_ID_PRIORITY_POSITION` (`order_id`,`priority`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_tax`
--

LOCK TABLES `sales_order_tax` WRITE;
/*!40000 ALTER TABLE `sales_order_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_tax_item`
--

DROP TABLE IF EXISTS `sales_order_tax_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_tax_item` (
  `tax_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Item Id',
  `tax_id` int(10) unsigned NOT NULL COMMENT 'Tax Id',
  `item_id` int(10) unsigned DEFAULT NULL COMMENT 'Item Id',
  `tax_percent` decimal(12,4) NOT NULL COMMENT 'Real Tax Percent For Item',
  `amount` decimal(12,4) NOT NULL COMMENT 'Tax amount for the item and tax rate',
  `base_amount` decimal(12,4) NOT NULL COMMENT 'Base tax amount for the item and tax rate',
  `real_amount` decimal(12,4) NOT NULL COMMENT 'Real tax amount for the item and tax rate',
  `real_base_amount` decimal(12,4) NOT NULL COMMENT 'Real base tax amount for the item and tax rate',
  `associated_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Id of the associated item',
  `taxable_item_type` varchar(32) NOT NULL COMMENT 'Type of the taxable item',
  PRIMARY KEY (`tax_item_id`),
  UNIQUE KEY `SALES_ORDER_TAX_ITEM_TAX_ID_ITEM_ID` (`tax_id`,`item_id`),
  KEY `SALES_ORDER_TAX_ITEM_ITEM_ID` (`item_id`),
  KEY `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` (`associated_item_id`),
  CONSTRAINT `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`associated_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_ORDER_TAX_ITEM_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_ORDER_TAX_ITEM_TAX_ID_SALES_ORDER_TAX_TAX_ID` FOREIGN KEY (`tax_id`) REFERENCES `sales_order_tax` (`tax_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Item';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_tax_item`
--

LOCK TABLES `sales_order_tax_item` WRITE;
/*!40000 ALTER TABLE `sales_order_tax_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_tax_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_payment_transaction`
--

DROP TABLE IF EXISTS `sales_payment_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Transaction Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `payment_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Payment Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `parent_txn_id` varchar(100) DEFAULT NULL COMMENT 'Parent Txn Id',
  `txn_type` varchar(15) DEFAULT NULL COMMENT 'Txn Type',
  `is_closed` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Closed',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `SALES_PAYMENT_TRANSACTION_ORDER_ID_PAYMENT_ID_TXN_ID` (`order_id`,`payment_id`,`txn_id`),
  KEY `SALES_PAYMENT_TRANSACTION_PARENT_ID` (`parent_id`),
  KEY `SALES_PAYMENT_TRANSACTION_PAYMENT_ID` (`payment_id`),
  CONSTRAINT `FK_B99FF1A06402D725EBDB0F3A7ECD47A2` FOREIGN KEY (`parent_id`) REFERENCES `sales_payment_transaction` (`transaction_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_PAYMENT_TRANSACTION_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_PAYMENT_TRANSACTION_PAYMENT_ID_SALES_ORDER_PAYMENT_ENTT_ID` FOREIGN KEY (`payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Payment Transaction';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_payment_transaction`
--

LOCK TABLES `sales_payment_transaction` WRITE;
/*!40000 ALTER TABLE `sales_payment_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_payment_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_refunded_aggregated`
--

DROP TABLE IF EXISTS `sales_refunded_aggregated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_refunded_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_REFUNDED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_REFUNDED_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_REFUNDED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_refunded_aggregated`
--

LOCK TABLES `sales_refunded_aggregated` WRITE;
/*!40000 ALTER TABLE `sales_refunded_aggregated` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_refunded_aggregated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_refunded_aggregated_order`
--

DROP TABLE IF EXISTS `sales_refunded_aggregated_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_refunded_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_REFUNDED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated Order';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_refunded_aggregated_order`
--

LOCK TABLES `sales_refunded_aggregated_order` WRITE;
/*!40000 ALTER TABLE `sales_refunded_aggregated_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_refunded_aggregated_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_sequence_meta`
--

DROP TABLE IF EXISTS `sales_sequence_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_sequence_meta` (
  `meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Prefix',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `sequence_table` varchar(32) NOT NULL COMMENT 'table for sequence',
  PRIMARY KEY (`meta_id`),
  UNIQUE KEY `SALES_SEQUENCE_META_ENTITY_TYPE_STORE_ID` (`entity_type`,`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='sales_sequence_meta';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_sequence_meta`
--

LOCK TABLES `sales_sequence_meta` WRITE;
/*!40000 ALTER TABLE `sales_sequence_meta` DISABLE KEYS */;
INSERT INTO `sales_sequence_meta` VALUES (1,'order',0,'sequence_order_0'),(2,'invoice',0,'sequence_invoice_0'),(3,'creditmemo',0,'sequence_creditmemo_0'),(4,'shipment',0,'sequence_shipment_0'),(5,'order',1,'sequence_order_1'),(6,'invoice',1,'sequence_invoice_1'),(7,'creditmemo',1,'sequence_creditmemo_1'),(8,'shipment',1,'sequence_shipment_1');
/*!40000 ALTER TABLE `sales_sequence_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_sequence_profile`
--

DROP TABLE IF EXISTS `sales_sequence_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_sequence_profile` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `meta_id` int(10) unsigned NOT NULL COMMENT 'Meta_id',
  `prefix` varchar(32) DEFAULT NULL COMMENT 'Prefix',
  `suffix` varchar(32) DEFAULT NULL COMMENT 'Suffix',
  `start_value` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Start value for sequence',
  `step` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Step for sequence',
  `max_value` int(10) unsigned NOT NULL COMMENT 'MaxValue for sequence',
  `warning_value` int(10) unsigned NOT NULL COMMENT 'WarningValue for sequence',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'isActive flag',
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `SALES_SEQUENCE_PROFILE_META_ID_PREFIX_SUFFIX` (`meta_id`,`prefix`,`suffix`),
  CONSTRAINT `SALES_SEQUENCE_PROFILE_META_ID_SALES_SEQUENCE_META_META_ID` FOREIGN KEY (`meta_id`) REFERENCES `sales_sequence_meta` (`meta_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='sales_sequence_profile';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_sequence_profile`
--

LOCK TABLES `sales_sequence_profile` WRITE;
/*!40000 ALTER TABLE `sales_sequence_profile` DISABLE KEYS */;
INSERT INTO `sales_sequence_profile` VALUES (1,1,NULL,NULL,1,1,4294967295,4294966295,1),(2,2,NULL,NULL,1,1,4294967295,4294966295,1),(3,3,NULL,NULL,1,1,4294967295,4294966295,1),(4,4,NULL,NULL,1,1,4294967295,4294966295,1),(5,5,NULL,NULL,1,1,4294967295,4294966295,1),(6,6,NULL,NULL,1,1,4294967295,4294966295,1),(7,7,NULL,NULL,1,1,4294967295,4294966295,1),(8,8,NULL,NULL,1,1,4294967295,4294966295,1);
/*!40000 ALTER TABLE `sales_sequence_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipment`
--

DROP TABLE IF EXISTS `sales_shipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_shipment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `total_weight` decimal(12,4) DEFAULT NULL COMMENT 'Total Weight',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `packages` text COMMENT 'Packed Products in Packages',
  `shipping_label` mediumblob COMMENT 'Shipping Label Content',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_SHIPMENT_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_SHIPMENT_STORE_ID` (`store_id`),
  KEY `SALES_SHIPMENT_TOTAL_QTY` (`total_qty`),
  KEY `SALES_SHIPMENT_ORDER_ID` (`order_id`),
  KEY `SALES_SHIPMENT_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_UPDATED_AT` (`updated_at`),
  KEY `SALES_SHIPMENT_SEND_EMAIL` (`send_email`),
  KEY `SALES_SHIPMENT_EMAIL_SENT` (`email_sent`),
  CONSTRAINT `SALES_SHIPMENT_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALES_SHIPMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_shipment`
--

LOCK TABLES `sales_shipment` WRITE;
/*!40000 ALTER TABLE `sales_shipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_shipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipment_comment`
--

DROP TABLE IF EXISTS `sales_shipment_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_shipment_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_COMMENT_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_SHIPMENT_COMMENT_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Comment';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_shipment_comment`
--

LOCK TABLES `sales_shipment_comment` WRITE;
/*!40000 ALTER TABLE `sales_shipment_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_shipment_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipment_grid`
--

DROP TABLE IF EXISTS `sales_shipment_grid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_shipment_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_increment_id` varchar(32) NOT NULL COMMENT 'Order Increment Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `order_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Order Increment Id',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `billing_name` varchar(128) DEFAULT NULL COMMENT 'Billing Name',
  `shipping_name` varchar(128) DEFAULT NULL COMMENT 'Shipping Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `SALES_SHIPMENT_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  KEY `SALES_SHIPMENT_GRID_STORE_ID` (`store_id`),
  KEY `SALES_SHIPMENT_GRID_TOTAL_QTY` (`total_qty`),
  KEY `SALES_SHIPMENT_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `SALES_SHIPMENT_GRID_SHIPMENT_STATUS` (`shipment_status`),
  KEY `SALES_SHIPMENT_GRID_ORDER_STATUS` (`order_status`),
  KEY `SALES_SHIPMENT_GRID_CREATED_AT` (`created_at`),
  KEY `SALES_SHIPMENT_GRID_UPDATED_AT` (`updated_at`),
  KEY `SALES_SHIPMENT_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `SALES_SHIPMENT_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `SALES_SHIPMENT_GRID_BILLING_NAME` (`billing_name`),
  FULLTEXT KEY `FTI_086B40C8955F167B8EA76653437879B4` (`increment_id`,`order_increment_id`,`shipping_name`,`customer_name`,`customer_email`,`billing_address`,`shipping_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Grid';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_shipment_grid`
--

LOCK TABLES `sales_shipment_grid` WRITE;
/*!40000 ALTER TABLE `sales_shipment_grid` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_shipment_grid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipment_item`
--

DROP TABLE IF EXISTS `sales_shipment_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_shipment_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `SALES_SHIPMENT_ITEM_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Item';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_shipment_item`
--

LOCK TABLES `sales_shipment_item` WRITE;
/*!40000 ALTER TABLE `sales_shipment_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_shipment_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipment_track`
--

DROP TABLE IF EXISTS `sales_shipment_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_shipment_track` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `track_number` text COMMENT 'Number',
  `description` text COMMENT 'Description',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `carrier_code` varchar(32) DEFAULT NULL COMMENT 'Carrier Code',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  KEY `SALES_SHIPMENT_TRACK_PARENT_ID` (`parent_id`),
  KEY `SALES_SHIPMENT_TRACK_ORDER_ID` (`order_id`),
  KEY `SALES_SHIPMENT_TRACK_CREATED_AT` (`created_at`),
  CONSTRAINT `SALES_SHIPMENT_TRACK_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Track';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_shipment_track`
--

LOCK TABLES `sales_shipment_track` WRITE;
/*!40000 ALTER TABLE `sales_shipment_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_shipment_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipping_aggregated`
--

DROP TABLE IF EXISTS `sales_shipping_aggregated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_shipping_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALES_SHPP_AGGRED_PERIOD_STORE_ID_ORDER_STS_SHPP_DESCRIPTION` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `SALES_SHIPPING_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_SHIPPING_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_shipping_aggregated`
--

LOCK TABLES `sales_shipping_aggregated` WRITE;
/*!40000 ALTER TABLE `sales_shipping_aggregated` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_shipping_aggregated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_shipping_aggregated_order`
--

DROP TABLE IF EXISTS `sales_shipping_aggregated_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_shipping_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_C05FAE47282EEA68654D0924E946761F` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated Order';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_shipping_aggregated_order`
--

LOCK TABLES `sales_shipping_aggregated_order` WRITE;
/*!40000 ALTER TABLE `sales_shipping_aggregated_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_shipping_aggregated_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule`
--

DROP TABLE IF EXISTS `salesrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `uses_per_customer` int(11) NOT NULL DEFAULT '0' COMMENT 'Uses Per Customer',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `is_advanced` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Advanced',
  `product_ids` text COMMENT 'Product Ids',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `discount_qty` decimal(12,4) DEFAULT NULL COMMENT 'Discount Qty',
  `discount_step` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Discount Step',
  `apply_to_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Apply To Shipping',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `is_rss` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Rss',
  `coupon_type` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Coupon Type',
  `use_auto_generation` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Use Auto Generation',
  `uses_per_coupon` int(11) NOT NULL DEFAULT '0' COMMENT 'User Per Coupon',
  `simple_free_shipping` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `SALESRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule`
--

LOCK TABLES `salesrule` WRITE;
/*!40000 ALTER TABLE `salesrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_coupon`
--

DROP TABLE IF EXISTS `salesrule_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_coupon` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Coupon Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `usage_limit` int(10) unsigned DEFAULT NULL COMMENT 'Usage Limit',
  `usage_per_customer` int(10) unsigned DEFAULT NULL COMMENT 'Usage Per Customer',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `expiration_date` timestamp NULL DEFAULT NULL COMMENT 'Expiration Date',
  `is_primary` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Primary',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Coupon Code Creation Date',
  `type` smallint(6) DEFAULT '0' COMMENT 'Coupon Code Type',
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `SALESRULE_COUPON_CODE` (`code`),
  UNIQUE KEY `SALESRULE_COUPON_RULE_ID_IS_PRIMARY` (`rule_id`,`is_primary`),
  KEY `SALESRULE_COUPON_RULE_ID` (`rule_id`),
  CONSTRAINT `SALESRULE_COUPON_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_coupon`
--

LOCK TABLES `salesrule_coupon` WRITE;
/*!40000 ALTER TABLE `salesrule_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_coupon_aggregated`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_coupon_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `SALESRULE_COUPON_AGGRED_PERIOD_STORE_ID_ORDER_STS_COUPON_CODE` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_RULE_NAME` (`rule_name`),
  CONSTRAINT `SALESRULE_COUPON_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_coupon_aggregated`
--

LOCK TABLES `salesrule_coupon_aggregated` WRITE;
/*!40000 ALTER TABLE `salesrule_coupon_aggregated` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_coupon_aggregated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_coupon_aggregated_order`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_coupon_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_1094D1FBBCBB11704A29DEF3ACC37D2B` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_ORDER_RULE_NAME` (`rule_name`),
  CONSTRAINT `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated Order';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_coupon_aggregated_order`
--

LOCK TABLES `salesrule_coupon_aggregated_order` WRITE;
/*!40000 ALTER TABLE `salesrule_coupon_aggregated_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_coupon_aggregated_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_coupon_aggregated_updated`
--

DROP TABLE IF EXISTS `salesrule_coupon_aggregated_updated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_coupon_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_7196FA120A4F0F84E1B66605E87E213E` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_RULE_NAME` (`rule_name`),
  CONSTRAINT `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Aggregated Updated';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_coupon_aggregated_updated`
--

LOCK TABLES `salesrule_coupon_aggregated_updated` WRITE;
/*!40000 ALTER TABLE `salesrule_coupon_aggregated_updated` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_coupon_aggregated_updated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_coupon_usage`
--

DROP TABLE IF EXISTS `salesrule_coupon_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_coupon_usage` (
  `coupon_id` int(10) unsigned NOT NULL COMMENT 'Coupon Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`coupon_id`,`customer_id`),
  KEY `SALESRULE_COUPON_USAGE_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `SALESRULE_COUPON_USAGE_COUPON_ID_SALESRULE_COUPON_COUPON_ID` FOREIGN KEY (`coupon_id`) REFERENCES `salesrule_coupon` (`coupon_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_COUPON_USAGE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Usage';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_coupon_usage`
--

LOCK TABLES `salesrule_coupon_usage` WRITE;
/*!40000 ALTER TABLE `salesrule_coupon_usage` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_coupon_usage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_customer`
--

DROP TABLE IF EXISTS `salesrule_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_customer` (
  `rule_customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Customer Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `times_used` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`rule_customer_id`),
  KEY `SALESRULE_CUSTOMER_RULE_ID_CUSTOMER_ID` (`rule_id`,`customer_id`),
  KEY `SALESRULE_CUSTOMER_CUSTOMER_ID_RULE_ID` (`customer_id`,`rule_id`),
  CONSTRAINT `SALESRULE_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_CUSTOMER_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Customer';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_customer`
--

LOCK TABLES `salesrule_customer` WRITE;
/*!40000 ALTER TABLE `salesrule_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_customer_group`
--

DROP TABLE IF EXISTS `salesrule_customer_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `SALESRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `SALESRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_CUSTOMER_GROUP_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Customer Groups Relations';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_customer_group`
--

LOCK TABLES `salesrule_customer_group` WRITE;
/*!40000 ALTER TABLE `salesrule_customer_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_customer_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_label`
--

DROP TABLE IF EXISTS `salesrule_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_label` (
  `label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Label Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  PRIMARY KEY (`label_id`),
  UNIQUE KEY `SALESRULE_LABEL_RULE_ID_STORE_ID` (`rule_id`,`store_id`),
  KEY `SALESRULE_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `SALESRULE_LABEL_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Label';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_label`
--

LOCK TABLES `salesrule_label` WRITE;
/*!40000 ALTER TABLE `salesrule_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_product_attribute`
--

DROP TABLE IF EXISTS `salesrule_product_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_product_attribute` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`rule_id`,`website_id`,`customer_group_id`,`attribute_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID` (`website_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `SALESRULE_PRODUCT_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `SALESRULE_PRD_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_PRD_ATTR_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Product Attribute';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_product_attribute`
--

LOCK TABLES `salesrule_product_attribute` WRITE;
/*!40000 ALTER TABLE `salesrule_product_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_product_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesrule_website`
--

DROP TABLE IF EXISTS `salesrule_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `SALESRULE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `SALESRULE_WEBSITE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  CONSTRAINT `SALESRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Websites Relations';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesrule_website`
--

LOCK TABLES `salesrule_website` WRITE;
/*!40000 ALTER TABLE `salesrule_website` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesrule_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_query`
--

DROP TABLE IF EXISTS `search_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_query` (
  `query_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Query ID',
  `query_text` varchar(255) DEFAULT NULL COMMENT 'Query text',
  `num_results` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Num results',
  `popularity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Popularity',
  `redirect` varchar(255) DEFAULT NULL COMMENT 'Redirect',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `display_in_terms` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Display in terms',
  `is_active` smallint(6) DEFAULT '1' COMMENT 'Active status',
  `is_processed` smallint(6) DEFAULT '0' COMMENT 'Processed status',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated at',
  PRIMARY KEY (`query_id`),
  UNIQUE KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID` (`query_text`,`store_id`),
  KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID_POPULARITY` (`query_text`,`store_id`,`popularity`),
  KEY `SEARCH_QUERY_STORE_ID` (`store_id`),
  KEY `SEARCH_QUERY_IS_PROCESSED` (`is_processed`),
  CONSTRAINT `SEARCH_QUERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Search query table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_query`
--

LOCK TABLES `search_query` WRITE;
/*!40000 ALTER TABLE `search_query` DISABLE KEYS */;
INSERT INTO `search_query` VALUES (1,'mexxico',2,2,NULL,1,1,1,0,'2020-03-11 19:07:28');
/*!40000 ALTER TABLE `search_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_synonyms`
--

DROP TABLE IF EXISTS `search_synonyms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_synonyms` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Synonyms Group Id',
  `synonyms` text NOT NULL COMMENT 'list of synonyms making up this group',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id - identifies the store view these synonyms belong to',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id - identifies the website id these synonyms belong to',
  PRIMARY KEY (`group_id`),
  KEY `SEARCH_SYNONYMS_STORE_ID` (`store_id`),
  KEY `SEARCH_SYNONYMS_WEBSITE_ID` (`website_id`),
  FULLTEXT KEY `SEARCH_SYNONYMS_SYNONYMS` (`synonyms`),
  CONSTRAINT `SEARCH_SYNONYMS_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `SEARCH_SYNONYMS_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table storing various synonyms groups';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_synonyms`
--

LOCK TABLES `search_synonyms` WRITE;
/*!40000 ALTER TABLE `search_synonyms` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_synonyms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sendfriend_log`
--

DROP TABLE IF EXISTS `sendfriend_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sendfriend_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `ip` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer IP address',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Log time',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  PRIMARY KEY (`log_id`),
  KEY `SENDFRIEND_LOG_IP` (`ip`),
  KEY `SENDFRIEND_LOG_TIME` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Send to friend function log storage table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sendfriend_log`
--

LOCK TABLES `sendfriend_log` WRITE;
/*!40000 ALTER TABLE `sendfriend_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sendfriend_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sendgrid_settings`
--

DROP TABLE IF EXISTS `sendgrid_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sendgrid_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Setting ID',
  `key` varchar(255) NOT NULL COMMENT 'Setting Key',
  `value` mediumtext NOT NULL COMMENT 'Setting Value',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SendGrid Settings Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sendgrid_settings`
--

LOCK TABLES `sendgrid_settings` WRITE;
/*!40000 ALTER TABLE `sendgrid_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `sendgrid_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_creditmemo_0`
--

DROP TABLE IF EXISTS `sequence_creditmemo_0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_creditmemo_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_creditmemo_0`
--

LOCK TABLES `sequence_creditmemo_0` WRITE;
/*!40000 ALTER TABLE `sequence_creditmemo_0` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequence_creditmemo_0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_creditmemo_1`
--

DROP TABLE IF EXISTS `sequence_creditmemo_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_creditmemo_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_creditmemo_1`
--

LOCK TABLES `sequence_creditmemo_1` WRITE;
/*!40000 ALTER TABLE `sequence_creditmemo_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequence_creditmemo_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_invoice_0`
--

DROP TABLE IF EXISTS `sequence_invoice_0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_invoice_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_invoice_0`
--

LOCK TABLES `sequence_invoice_0` WRITE;
/*!40000 ALTER TABLE `sequence_invoice_0` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequence_invoice_0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_invoice_1`
--

DROP TABLE IF EXISTS `sequence_invoice_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_invoice_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_invoice_1`
--

LOCK TABLES `sequence_invoice_1` WRITE;
/*!40000 ALTER TABLE `sequence_invoice_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequence_invoice_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_order_0`
--

DROP TABLE IF EXISTS `sequence_order_0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_order_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_order_0`
--

LOCK TABLES `sequence_order_0` WRITE;
/*!40000 ALTER TABLE `sequence_order_0` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequence_order_0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_order_1`
--

DROP TABLE IF EXISTS `sequence_order_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_order_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_order_1`
--

LOCK TABLES `sequence_order_1` WRITE;
/*!40000 ALTER TABLE `sequence_order_1` DISABLE KEYS */;
INSERT INTO `sequence_order_1` VALUES (1),(2),(3);
/*!40000 ALTER TABLE `sequence_order_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_shipment_0`
--

DROP TABLE IF EXISTS `sequence_shipment_0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_shipment_0` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_shipment_0`
--

LOCK TABLES `sequence_shipment_0` WRITE;
/*!40000 ALTER TABLE `sequence_shipment_0` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequence_shipment_0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_shipment_1`
--

DROP TABLE IF EXISTS `sequence_shipment_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_shipment_1` (
  `sequence_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_shipment_1`
--

LOCK TABLES `sequence_shipment_1` WRITE;
/*!40000 ALTER TABLE `sequence_shipment_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequence_shipment_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `session_id` varchar(255) NOT NULL COMMENT 'Session Id',
  `session_expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Date of Session Expiration',
  `session_data` mediumblob NOT NULL COMMENT 'Session Data',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Database Sessions Storage';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setup_module`
--

DROP TABLE IF EXISTS `setup_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setup_module` (
  `module` varchar(50) NOT NULL COMMENT 'Module',
  `schema_version` varchar(50) DEFAULT NULL COMMENT 'Schema Version',
  `data_version` varchar(50) DEFAULT NULL COMMENT 'Data Version',
  PRIMARY KEY (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module versions registry';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setup_module`
--

LOCK TABLES `setup_module` WRITE;
/*!40000 ALTER TABLE `setup_module` DISABLE KEYS */;
INSERT INTO `setup_module` VALUES ('FishPig_WordPress','2.0.9.19','2.0.9.19'),('Magento_AdminNotification','2.0.0','2.0.0'),('Magento_AdvancedPricingImportExport','2.0.0','2.0.0'),('Magento_Authorization','2.0.0','2.0.0'),('Magento_Authorizenet','2.0.0','2.0.0'),('Magento_Backend','2.0.0','2.0.0'),('Magento_Backup','2.0.0','2.0.0'),('Magento_Braintree','2.0.0','2.0.0'),('Magento_Bundle','2.0.2','2.0.2'),('Magento_BundleImportExport','2.0.0','2.0.0'),('Magento_CacheInvalidate','2.0.0','2.0.0'),('Magento_Captcha','2.0.0','2.0.0'),('Magento_Catalog','2.1.4','2.1.4'),('Magento_CatalogImportExport','2.0.0','2.0.0'),('Magento_CatalogInventory','2.0.1','2.0.1'),('Magento_CatalogRule','2.0.1','2.0.1'),('Magento_CatalogRuleConfigurable','2.0.0','2.0.0'),('Magento_CatalogSearch','2.0.0','2.0.0'),('Magento_CatalogUrlRewrite','2.0.0','2.0.0'),('Magento_CatalogWidget','2.0.0','2.0.0'),('Magento_Checkout','2.0.0','2.0.0'),('Magento_CheckoutAgreements','2.0.1','2.0.1'),('Magento_Cms','2.0.1','2.0.1'),('Magento_CmsUrlRewrite','2.0.0','2.0.0'),('Magento_Config','2.0.0','2.0.0'),('Magento_ConfigurableImportExport','2.0.0','2.0.0'),('Magento_ConfigurableProduct','2.0.0','2.0.0'),('Magento_Contact','2.0.0','2.0.0'),('Magento_Cookie','2.0.0','2.0.0'),('Magento_Cron','2.0.0','2.0.0'),('Magento_CurrencySymbol','2.0.0','2.0.0'),('Magento_Customer','2.0.9','2.0.9'),('Magento_CustomerImportExport','2.0.0','2.0.0'),('Magento_Deploy','2.0.0','2.0.0'),('Magento_Developer','2.0.0','2.0.0'),('Magento_Dhl','2.0.0','2.0.0'),('Magento_Directory','2.0.0','2.0.0'),('Magento_Downloadable','2.0.1','2.0.1'),('Magento_DownloadableImportExport','2.0.0','2.0.0'),('Magento_Eav','2.0.0','2.0.0'),('Magento_Email','2.0.0','2.0.0'),('Magento_EncryptionKey','2.0.0','2.0.0'),('Magento_Fedex','2.0.0','2.0.0'),('Magento_GiftMessage','2.0.1','2.0.1'),('Magento_GoogleAdwords','2.0.0','2.0.0'),('Magento_GoogleAnalytics','2.0.0','2.0.0'),('Magento_GoogleOptimizer','2.0.0','2.0.0'),('Magento_GroupedImportExport','2.0.0','2.0.0'),('Magento_GroupedProduct','2.0.1','2.0.1'),('Magento_ImportExport','2.0.1','2.0.1'),('Magento_Indexer','2.0.0','2.0.0'),('Magento_Integration','2.2.0','2.2.0'),('Magento_LayeredNavigation','2.0.0','2.0.0'),('Magento_Marketplace','1.0.0','1.0.0'),('Magento_MediaStorage','2.0.0','2.0.0'),('Magento_Msrp','2.1.3','2.1.3'),('Magento_Multishipping','2.0.0','2.0.0'),('Magento_NewRelicReporting','2.0.0','2.0.0'),('Magento_Newsletter','2.0.0','2.0.0'),('Magento_OfflinePayments','2.0.0','2.0.0'),('Magento_OfflineShipping','2.0.0','2.0.0'),('Magento_PageCache','2.0.0','2.0.0'),('Magento_Payment','2.0.0','2.0.0'),('Magento_Paypal','2.0.0','2.0.0'),('Magento_Persistent','2.0.0','2.0.0'),('Magento_ProductAlert','2.0.0','2.0.0'),('Magento_ProductVideo','2.0.0.2','2.0.0.2'),('Magento_Quote','2.0.4','2.0.4'),('Magento_Reports','2.0.0','2.0.0'),('Magento_RequireJs','2.0.0','2.0.0'),('Magento_Review','2.0.0','2.0.0'),('Magento_Rss','2.0.0','2.0.0'),('Magento_Rule','2.0.0','2.0.0'),('Magento_Sales','2.0.3','2.0.3'),('Magento_SalesInventory','1.0.0','1.0.0'),('Magento_SalesRule','2.0.1','2.0.1'),('Magento_SalesSequence','2.0.0','2.0.0'),('Magento_SampleData','2.0.0','2.0.0'),('Magento_Search','2.0.4','2.0.4'),('Magento_Security','2.0.1','2.0.1'),('Magento_SendFriend','2.0.0','2.0.0'),('Magento_Shipping','2.0.0','2.0.0'),('Magento_Sitemap','2.0.0','2.0.0'),('Magento_Store','2.0.0','2.0.0'),('Magento_Swagger','2.0.0','2.0.0'),('Magento_Swatches','2.0.1','2.0.1'),('Magento_SwatchesLayeredNavigation','2.0.0','2.0.0'),('Magento_Tax','2.0.1','2.0.1'),('Magento_TaxImportExport','2.0.0','2.0.0'),('Magento_Theme','2.0.1','2.0.1'),('Magento_Translation','2.0.0','2.0.0'),('Magento_Ui','2.0.0','2.0.0'),('Magento_Ups','2.0.0','2.0.0'),('Magento_UrlRewrite','2.0.0','2.0.0'),('Magento_User','2.0.1','2.0.1'),('Magento_Usps','2.0.1','2.0.1'),('Magento_Variable','2.0.0','2.0.0'),('Magento_Vault','2.0.2','2.0.2'),('Magento_Version','2.0.0','2.0.0'),('Magento_Webapi','2.0.0','2.0.0'),('Magento_WebapiSecurity','2.0.0','2.0.0'),('Magento_Weee','2.0.0','2.0.0'),('Magento_Widget','2.0.0','2.0.0'),('Magento_Wishlist','2.0.0','2.0.0'),('Plazathemes_Bannerslider','1.0.0','1.0.0'),('Plazathemes_Bestsellerproduct','2.0.0','2.0.0'),('Plazathemes_Blog','2.0.0','2.0.0'),('Plazathemes_Brandslider','1.0.0','1.0.0'),('Plazathemes_Categorytab','2.0.0','2.0.0'),('Plazathemes_Categorytop','2.0.0','2.0.0'),('Plazathemes_Featureproductslider','2.0.0','2.0.0'),('Plazathemes_Hozmegamenu','2.0.0','2.0.0'),('Plazathemes_InstagramGallery','1.0.0','1.0.0'),('Plazathemes_Layout','2.0.0','2.0.0'),('Plazathemes_Loadjs','2.0.0','2.0.0'),('Plazathemes_Newproductslider','2.0.0','2.0.0'),('Plazathemes_Newsletterpopup','2.0.0','2.0.0'),('Plazathemes_Override','2.0.0','2.0.0'),('Plazathemes_Recentproductslider','1.0.0','1.0.0'),('Plazathemes_Template','2.0.0','2.0.0'),('Plazathemes_Testimonial','1.0.0','1.0.0'),('Plazathemes_Themeoptions','2.0.0','2.0.0'),('SendGrid_EmailDeliverySimplified','2.0.0','2.0.0');
/*!40000 ALTER TABLE `setup_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_tablerate`
--

DROP TABLE IF EXISTS `shipping_tablerate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_tablerate` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `website_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `dest_country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Destination coutry ISO/2 or ISO/3 code',
  `dest_region_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Destination Region Id',
  `dest_zip` varchar(10) NOT NULL DEFAULT '*' COMMENT 'Destination Post Code (Zip)',
  `condition_name` varchar(20) NOT NULL COMMENT 'Rate Condition name',
  `condition_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rate condition value',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `cost` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Cost',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `UNQ_D60821CDB2AFACEE1566CFC02D0D4CAA` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Shipping Tablerate';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_tablerate`
--

LOCK TABLES `shipping_tablerate` WRITE;
/*!40000 ALTER TABLE `shipping_tablerate` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping_tablerate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemap`
--

DROP TABLE IF EXISTS `sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemap` (
  `sitemap_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sitemap Id',
  `sitemap_type` varchar(32) DEFAULT NULL COMMENT 'Sitemap Type',
  `sitemap_filename` varchar(32) DEFAULT NULL COMMENT 'Sitemap Filename',
  `sitemap_path` varchar(255) DEFAULT NULL COMMENT 'Sitemap Path',
  `sitemap_time` timestamp NULL DEFAULT NULL COMMENT 'Sitemap Time',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`sitemap_id`),
  KEY `SITEMAP_STORE_ID` (`store_id`),
  CONSTRAINT `SITEMAP_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='XML Sitemap';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemap`
--

LOCK TABLES `sitemap` WRITE;
/*!40000 ALTER TABLE `sitemap` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitemap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `store_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Store Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Sort Order',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Activity',
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `STORE_CODE` (`code`),
  KEY `STORE_WEBSITE_ID` (`website_id`),
  KEY `STORE_IS_ACTIVE_SORT_ORDER` (`is_active`,`sort_order`),
  KEY `STORE_GROUP_ID` (`group_id`),
  CONSTRAINT `STORE_GROUP_ID_STORE_GROUP_GROUP_ID` FOREIGN KEY (`group_id`) REFERENCES `store_group` (`group_id`) ON DELETE CASCADE,
  CONSTRAINT `STORE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Stores';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` VALUES (0,'admin',0,0,'Admin',0,1),(1,'default',1,1,'CULTURA MEZCAL',0,1);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_group`
--

DROP TABLE IF EXISTS `store_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_group` (
  `group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Group Name',
  `root_category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Root Category Id',
  `default_store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Store Id',
  PRIMARY KEY (`group_id`),
  KEY `STORE_GROUP_WEBSITE_ID` (`website_id`),
  KEY `STORE_GROUP_DEFAULT_STORE_ID` (`default_store_id`),
  CONSTRAINT `STORE_GROUP_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Store Groups';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_group`
--

LOCK TABLES `store_group` WRITE;
/*!40000 ALTER TABLE `store_group` DISABLE KEYS */;
INSERT INTO `store_group` VALUES (0,0,'Default',0,0),(1,1,'Main Website Store',2,1);
/*!40000 ALTER TABLE `store_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_website`
--

DROP TABLE IF EXISTS `store_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_website` (
  `website_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Website Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `name` varchar(64) DEFAULT NULL COMMENT 'Website Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Group Id',
  `is_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Defines Is Website Default',
  PRIMARY KEY (`website_id`),
  UNIQUE KEY `STORE_WEBSITE_CODE` (`code`),
  KEY `STORE_WEBSITE_SORT_ORDER` (`sort_order`),
  KEY `STORE_WEBSITE_DEFAULT_GROUP_ID` (`default_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Websites';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_website`
--

LOCK TABLES `store_website` WRITE;
/*!40000 ALTER TABLE `store_website` DISABLE KEYS */;
INSERT INTO `store_website` VALUES (0,'admin','Admin',0,0,0),(1,'base','Main Website',0,1,1);
/*!40000 ALTER TABLE `store_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_calculation`
--

DROP TABLE IF EXISTS `tax_calculation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_calculation` (
  `tax_calculation_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `tax_calculation_rule_id` int(11) NOT NULL COMMENT 'Tax Calculation Rule Id',
  `customer_tax_class_id` smallint(6) NOT NULL COMMENT 'Customer Tax Class Id',
  `product_tax_class_id` smallint(6) NOT NULL COMMENT 'Product Tax Class Id',
  PRIMARY KEY (`tax_calculation_id`),
  KEY `TAX_CALCULATION_TAX_CALCULATION_RULE_ID` (`tax_calculation_rule_id`),
  KEY `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID` (`customer_tax_class_id`),
  KEY `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID` (`product_tax_class_id`),
  KEY `TAX_CALC_TAX_CALC_RATE_ID_CSTR_TAX_CLASS_ID_PRD_TAX_CLASS_ID` (`tax_calculation_rate_id`,`customer_tax_class_id`,`product_tax_class_id`),
  CONSTRAINT `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`customer_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  CONSTRAINT `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`product_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  CONSTRAINT `TAX_CALC_TAX_CALC_RATE_ID_TAX_CALC_RATE_TAX_CALC_RATE_ID` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  CONSTRAINT `TAX_CALC_TAX_CALC_RULE_ID_TAX_CALC_RULE_TAX_CALC_RULE_ID` FOREIGN KEY (`tax_calculation_rule_id`) REFERENCES `tax_calculation_rule` (`tax_calculation_rule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_calculation`
--

LOCK TABLES `tax_calculation` WRITE;
/*!40000 ALTER TABLE `tax_calculation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_calculation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_calculation_rate`
--

DROP TABLE IF EXISTS `tax_calculation_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_calculation_rate` (
  `tax_calculation_rate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Id',
  `tax_country_id` varchar(2) NOT NULL COMMENT 'Tax Country Id',
  `tax_region_id` int(11) NOT NULL COMMENT 'Tax Region Id',
  `tax_postcode` varchar(21) DEFAULT NULL COMMENT 'Tax Postcode',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `rate` decimal(12,4) NOT NULL COMMENT 'Rate',
  `zip_is_range` smallint(6) DEFAULT NULL COMMENT 'Zip Is Range',
  `zip_from` int(10) unsigned DEFAULT NULL COMMENT 'Zip From',
  `zip_to` int(10) unsigned DEFAULT NULL COMMENT 'Zip To',
  PRIMARY KEY (`tax_calculation_rate_id`),
  KEY `TAX_CALCULATION_RATE_TAX_COUNTRY_ID_TAX_REGION_ID_TAX_POSTCODE` (`tax_country_id`,`tax_region_id`,`tax_postcode`),
  KEY `TAX_CALCULATION_RATE_CODE` (`code`),
  KEY `IDX_CA799F1E2CB843495F601E56C84A626D` (`tax_calculation_rate_id`,`tax_country_id`,`tax_region_id`,`zip_is_range`,`tax_postcode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_calculation_rate`
--

LOCK TABLES `tax_calculation_rate` WRITE;
/*!40000 ALTER TABLE `tax_calculation_rate` DISABLE KEYS */;
INSERT INTO `tax_calculation_rate` VALUES (1,'US',12,'*','US-CA-*-Rate 1',8.2500,NULL,NULL,NULL),(2,'US',43,'*','US-NY-*-Rate 1',8.3750,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tax_calculation_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_calculation_rate_title`
--

DROP TABLE IF EXISTS `tax_calculation_rate_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_calculation_rate_title` (
  `tax_calculation_rate_title_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Title Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`tax_calculation_rate_title_id`),
  KEY `TAX_CALCULATION_RATE_TITLE_TAX_CALCULATION_RATE_ID_STORE_ID` (`tax_calculation_rate_id`,`store_id`),
  KEY `TAX_CALCULATION_RATE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_37FB965F786AD5897BB3AE90470C42AB` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  CONSTRAINT `TAX_CALCULATION_RATE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate Title';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_calculation_rate_title`
--

LOCK TABLES `tax_calculation_rate_title` WRITE;
/*!40000 ALTER TABLE `tax_calculation_rate_title` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_calculation_rate_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_calculation_rule`
--

DROP TABLE IF EXISTS `tax_calculation_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_calculation_rule` (
  `tax_calculation_rule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rule Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `calculate_subtotal` int(11) NOT NULL COMMENT 'Calculate off subtotal option',
  PRIMARY KEY (`tax_calculation_rule_id`),
  KEY `TAX_CALCULATION_RULE_PRIORITY_POSITION` (`priority`,`position`),
  KEY `TAX_CALCULATION_RULE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rule';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_calculation_rule`
--

LOCK TABLES `tax_calculation_rule` WRITE;
/*!40000 ALTER TABLE `tax_calculation_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_calculation_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_class`
--

DROP TABLE IF EXISTS `tax_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_class` (
  `class_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Class Id',
  `class_name` varchar(255) NOT NULL COMMENT 'Class Name',
  `class_type` varchar(8) NOT NULL DEFAULT 'CUSTOMER' COMMENT 'Class Type',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Tax Class';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_class`
--

LOCK TABLES `tax_class` WRITE;
/*!40000 ALTER TABLE `tax_class` DISABLE KEYS */;
INSERT INTO `tax_class` VALUES (2,'Taxable Goods','PRODUCT'),(3,'Retail Customer','CUSTOMER');
/*!40000 ALTER TABLE `tax_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_order_aggregated_created`
--

DROP TABLE IF EXISTS `tax_order_aggregated_created`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TAX_ORDER_AGGRED_CREATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `TAX_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`),
  CONSTRAINT `TAX_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_order_aggregated_created`
--

LOCK TABLES `tax_order_aggregated_created` WRITE;
/*!40000 ALTER TABLE `tax_order_aggregated_created` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_order_aggregated_created` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_order_aggregated_updated`
--

DROP TABLE IF EXISTS `tax_order_aggregated_updated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TAX_ORDER_AGGRED_UPDATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  CONSTRAINT `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregated Updated';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_order_aggregated_updated`
--

LOCK TABLES `tax_order_aggregated_updated` WRITE;
/*!40000 ALTER TABLE `tax_order_aggregated_updated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_order_aggregated_updated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theme`
--

DROP TABLE IF EXISTS `theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `theme` (
  `theme_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Theme identifier',
  `parent_id` int(11) DEFAULT NULL COMMENT 'Parent Id',
  `theme_path` varchar(255) DEFAULT NULL COMMENT 'Theme Path',
  `theme_title` varchar(255) NOT NULL COMMENT 'Theme Title',
  `preview_image` varchar(255) DEFAULT NULL COMMENT 'Preview Image',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Theme Featured',
  `area` varchar(255) NOT NULL COMMENT 'Theme Area',
  `type` smallint(6) NOT NULL COMMENT 'Theme type: 0:physical, 1:virtual, 2:staging',
  `code` text COMMENT 'Full theme code, including package',
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Core theme';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `theme`
--

LOCK TABLES `theme` WRITE;
/*!40000 ALTER TABLE `theme` DISABLE KEYS */;
INSERT INTO `theme` VALUES (1,NULL,'Magento/blank','Magento Blank','preview_image_5e59325015c1e.jpeg',0,'frontend',0,'Magento/blank'),(2,1,'Magento/luma','Magento Luma','preview_image_5e59325020ecc.jpeg',0,'frontend',0,'Magento/luma'),(3,NULL,'Magento/backend','Magento 2 backend',NULL,0,'adminhtml',0,'Magento/backend'),(4,1,'thebell/thebell_default','Magento thebell_default','preview_image_5e593b9406dca.jpeg',0,'frontend',0,'thebell/thebell_default'),(5,4,'thebell/thebell3','Magento thebell3','preview_image_5e593b941510b.jpeg',0,'frontend',0,'thebell/thebell3'),(6,4,'thebell/thebell4','Magento thebell4','preview_image_5e593b94225d7.jpeg',0,'frontend',0,'thebell/thebell4'),(7,4,'thebell/thebell2','Magento thebell2','preview_image_5e593b94300f0.jpeg',0,'frontend',0,'thebell/thebell2');
/*!40000 ALTER TABLE `theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theme_file`
--

DROP TABLE IF EXISTS `theme_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `theme_file` (
  `theme_files_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Theme files identifier',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme Id',
  `file_path` varchar(255) DEFAULT NULL COMMENT 'Relative path to file',
  `file_type` varchar(32) NOT NULL COMMENT 'File Type',
  `content` longtext NOT NULL COMMENT 'File Content',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Temporary File',
  PRIMARY KEY (`theme_files_id`),
  KEY `THEME_FILE_THEME_ID_THEME_THEME_ID` (`theme_id`),
  CONSTRAINT `THEME_FILE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Core theme files';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `theme_file`
--

LOCK TABLES `theme_file` WRITE;
/*!40000 ALTER TABLE `theme_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `theme_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation`
--

DROP TABLE IF EXISTS `translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation` (
  `key_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Key Id of Translation',
  `string` varchar(255) NOT NULL DEFAULT 'Translate String' COMMENT 'Translation String',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `translate` varchar(255) DEFAULT NULL COMMENT 'Translate',
  `locale` varchar(20) NOT NULL DEFAULT 'en_US' COMMENT 'Locale',
  `crc_string` bigint(20) NOT NULL DEFAULT '1591228201' COMMENT 'Translation String CRC32 Hash',
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `TRANSLATION_STORE_ID_LOCALE_CRC_STRING_STRING` (`store_id`,`locale`,`crc_string`,`string`),
  CONSTRAINT `TRANSLATION_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Translations';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation`
--

LOCK TABLES `translation` WRITE;
/*!40000 ALTER TABLE `translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ui_bookmark`
--

DROP TABLE IF EXISTS `ui_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ui_bookmark` (
  `bookmark_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Bookmark identifier',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User Id',
  `namespace` varchar(255) NOT NULL COMMENT 'Bookmark namespace',
  `identifier` varchar(255) NOT NULL COMMENT 'Bookmark Identifier',
  `current` smallint(6) NOT NULL COMMENT 'Mark current bookmark per user and identifier',
  `title` varchar(255) DEFAULT NULL COMMENT 'Bookmark title',
  `config` longtext COMMENT 'Bookmark config',
  `created_at` datetime NOT NULL COMMENT 'Bookmark created at',
  `updated_at` datetime NOT NULL COMMENT 'Bookmark updated at',
  PRIMARY KEY (`bookmark_id`),
  KEY `UI_BOOKMARK_USER_ID_NAMESPACE_IDENTIFIER` (`user_id`,`namespace`,`identifier`),
  CONSTRAINT `UI_BOOKMARK_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='Bookmark';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ui_bookmark`
--

LOCK TABLES `ui_bookmark` WRITE;
/*!40000 ALTER TABLE `ui_bookmark` DISABLE KEYS */;
INSERT INTO `ui_bookmark` VALUES (1,1,'design_config_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"default\":{\"visible\":true,\"sorting\":false},\"store_website_id\":{\"visible\":true,\"sorting\":false},\"store_group_id\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"theme_theme_id\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"default\":0,\"store_website_id\":1,\"store_group_id\":2,\"store_id\":3,\"theme_theme_id\":4,\"actions\":5}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,1,'design_config_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"default\":{\"visible\":true,\"sorting\":false},\"store_website_id\":{\"visible\":true,\"sorting\":false},\"store_group_id\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"theme_theme_id\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"default\":0,\"store_website_id\":1,\"store_group_id\":2,\"store_id\":3,\"theme_theme_id\":4,\"actions\":5}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,1,'cms_block_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true,\"is_active\":[\"1\"]}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":false},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":\"asc\"},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8},\"search\":{\"value\":\"\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,'cms_block_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"block_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"block_id\":1,\"title\":2,\"identifier\":3,\"store_id\":4,\"is_active\":5,\"creation_time\":6,\"update_time\":7,\"actions\":8},\"search\":{\"value\":\"\"}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,1,'cms_page_listing','current',0,NULL,'{\"current\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,1,'cms_page_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"search\":{\"value\":\"\"},\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"page_id\":{\"visible\":true,\"sorting\":\"asc\"},\"title\":{\"visible\":true,\"sorting\":false},\"identifier\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":true,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keywords\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"page_layout\":{\"visible\":true,\"sorting\":false},\"is_active\":{\"visible\":true,\"sorting\":false},\"custom_theme\":{\"visible\":false,\"sorting\":false},\"custom_root_template\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"creation_time\":{\"visible\":true,\"sorting\":false},\"update_time\":{\"visible\":true,\"sorting\":false},\"custom_theme_from\":{\"visible\":false,\"sorting\":false},\"custom_theme_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"page_id\":1,\"title\":2,\"identifier\":3,\"page_layout\":4,\"store_id\":5,\"is_active\":6,\"creation_time\":7,\"update_time\":8,\"custom_theme_from\":9,\"custom_theme_to\":10,\"custom_theme\":11,\"custom_root_template\":12,\"meta_title\":13,\"meta_keywords\":14,\"meta_description\":15,\"actions\":16}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,1,'product_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"color\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"featured\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"agave\":{\"visible\":true,\"sorting\":false},\"agua_fermentacion\":{\"visible\":true,\"sorting\":false},\"anio\":{\"visible\":true,\"sorting\":false},\"botellas_por_caja\":{\"visible\":true,\"sorting\":false},\"cocimiento\":{\"visible\":true,\"sorting\":false},\"fermentacion\":{\"visible\":true,\"sorting\":false},\"litros_destilados\":{\"visible\":true,\"sorting\":false},\"maduracion_anios\":{\"visible\":true,\"sorting\":false},\"maestro\":{\"visible\":true,\"sorting\":false},\"maguey\":{\"visible\":true,\"sorting\":false},\"molido\":{\"visible\":true,\"sorting\":false},\"notas_de_cata\":{\"visible\":true,\"sorting\":false},\"no_destilaciones\":{\"visible\":true,\"sorting\":false},\"origen_planta\":{\"visible\":true,\"sorting\":false},\"presentacion_cl\":{\"visible\":true,\"sorting\":false},\"region\":{\"visible\":true,\"sorting\":false},\"terruno\":{\"visible\":true,\"sorting\":false},\"tipo_abocado\":{\"visible\":true,\"sorting\":false},\"tipo_ajuste\":{\"visible\":true,\"sorting\":false},\"tipo_destilador\":{\"visible\":true,\"sorting\":false},\"variedad\":{\"visible\":true,\"sorting\":false},\"grados_alcohol\":{\"visible\":true,\"sorting\":false},\"entrega\":{\"visible\":true,\"sorting\":false},\"estado\":{\"visible\":true,\"sorting\":false},\"filter_maestro\":{\"visible\":true,\"sorting\":false},\"filter_maguey\":{\"visible\":true,\"sorting\":false},\"filter_region\":{\"visible\":true,\"sorting\":false},\"home_producto_destacado\":{\"visible\":true,\"sorting\":false},\"paquete\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"color\":22,\"news_from_date\":23,\"news_to_date\":24,\"custom_design\":25,\"custom_design_from\":26,\"custom_design_to\":27,\"page_layout\":28,\"country_of_manufacture\":29,\"custom_layout\":30,\"url_key\":31,\"msrp\":32,\"tax_class_id\":33,\"gift_message_available\":34,\"featured\":35,\"actions\":36,\"agave\":37,\"agua_fermentacion\":38,\"anio\":39,\"botellas_por_caja\":40,\"cocimiento\":41,\"entrega\":42,\"estado\":43,\"fermentacion\":44,\"filter_maguey\":45,\"filter_region\":46,\"litros_destilados\":47,\"maduracion_anios\":48,\"maestro\":49,\"maguey\":50,\"molido\":51,\"notas_de_cata\":52,\"no_destilaciones\":53,\"origen_planta\":54,\"presentacion_cl\":55,\"region\":56,\"terruno\":57,\"tipo_abocado\":58,\"tipo_ajuste\":59,\"tipo_destilador\":60,\"variedad\":61,\"grados_alcohol\":62,\"home_producto_destacado\":63,\"paquete\":64}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,1,'product_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"color\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"featured\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"color\":22,\"news_from_date\":23,\"news_to_date\":24,\"custom_design\":25,\"custom_design_from\":26,\"custom_design_to\":27,\"page_layout\":28,\"country_of_manufacture\":29,\"custom_layout\":30,\"url_key\":31,\"msrp\":32,\"tax_class_id\":33,\"gift_message_available\":34,\"featured\":35,\"actions\":36}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,1,'customer_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"email\":{\"visible\":true,\"sorting\":false},\"billing_telephone\":{\"visible\":true,\"sorting\":false},\"billing_postcode\":{\"visible\":true,\"sorting\":false},\"billing_region\":{\"visible\":true,\"sorting\":false},\"confirmation\":{\"visible\":true,\"sorting\":false},\"created_in\":{\"visible\":true,\"sorting\":false},\"billing_full\":{\"visible\":false,\"sorting\":false},\"shipping_full\":{\"visible\":false,\"sorting\":false},\"taxvat\":{\"visible\":true,\"sorting\":false},\"billing_street\":{\"visible\":false,\"sorting\":false},\"billing_city\":{\"visible\":false,\"sorting\":false},\"billing_fax\":{\"visible\":false,\"sorting\":false},\"billing_vat_id\":{\"visible\":false,\"sorting\":false},\"billing_company\":{\"visible\":false,\"sorting\":false},\"billing_firstname\":{\"visible\":false,\"sorting\":false},\"billing_lastname\":{\"visible\":false,\"sorting\":false},\"lock_expires\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"group_id\":{\"visible\":true,\"sorting\":false},\"billing_country_id\":{\"visible\":true,\"sorting\":false},\"website_id\":{\"visible\":true,\"sorting\":false},\"gender\":{\"visible\":true,\"sorting\":false},\"created_at\":{\"visible\":true,\"sorting\":false},\"dob\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"search\":{\"value\":\"\"},\"positions\":{\"ids\":0,\"entity_id\":1,\"name\":2,\"email\":3,\"group_id\":4,\"billing_telephone\":5,\"billing_postcode\":6,\"billing_country_id\":7,\"billing_region\":8,\"created_at\":9,\"website_id\":10,\"confirmation\":11,\"created_in\":12,\"billing_full\":13,\"shipping_full\":14,\"dob\":15,\"taxvat\":16,\"gender\":17,\"billing_street\":18,\"billing_city\":19,\"billing_fax\":20,\"billing_vat_id\":21,\"billing_company\":22,\"billing_firstname\":23,\"billing_lastname\":24,\"lock_expires\":25,\"actions\":26}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,1,'customer_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"email\":{\"visible\":true,\"sorting\":false},\"billing_telephone\":{\"visible\":true,\"sorting\":false},\"billing_postcode\":{\"visible\":true,\"sorting\":false},\"billing_region\":{\"visible\":true,\"sorting\":false},\"confirmation\":{\"visible\":true,\"sorting\":false},\"created_in\":{\"visible\":true,\"sorting\":false},\"billing_full\":{\"visible\":false,\"sorting\":false},\"shipping_full\":{\"visible\":false,\"sorting\":false},\"taxvat\":{\"visible\":true,\"sorting\":false},\"billing_street\":{\"visible\":false,\"sorting\":false},\"billing_city\":{\"visible\":false,\"sorting\":false},\"billing_fax\":{\"visible\":false,\"sorting\":false},\"billing_vat_id\":{\"visible\":false,\"sorting\":false},\"billing_company\":{\"visible\":false,\"sorting\":false},\"billing_firstname\":{\"visible\":false,\"sorting\":false},\"billing_lastname\":{\"visible\":false,\"sorting\":false},\"lock_expires\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"group_id\":{\"visible\":true,\"sorting\":false},\"billing_country_id\":{\"visible\":true,\"sorting\":false},\"website_id\":{\"visible\":true,\"sorting\":false},\"gender\":{\"visible\":true,\"sorting\":false},\"created_at\":{\"visible\":true,\"sorting\":false},\"dob\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"search\":{\"value\":\"\"},\"positions\":{\"ids\":0,\"entity_id\":1,\"name\":2,\"email\":3,\"group_id\":4,\"billing_telephone\":5,\"billing_postcode\":6,\"billing_country_id\":7,\"billing_region\":8,\"created_at\":9,\"website_id\":10,\"confirmation\":11,\"created_in\":12,\"billing_full\":13,\"shipping_full\":14,\"dob\":15,\"taxvat\":16,\"gender\":17,\"billing_street\":18,\"billing_city\":19,\"billing_fax\":20,\"billing_vat_id\":21,\"billing_company\":22,\"billing_firstname\":23,\"billing_lastname\":24,\"lock_expires\":25,\"actions\":26}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,1,'sales_order_grid','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"increment_id\":{\"visible\":true,\"sorting\":\"desc\"},\"store_id\":{\"visible\":true,\"sorting\":false},\"billing_name\":{\"visible\":true,\"sorting\":false},\"shipping_name\":{\"visible\":true,\"sorting\":false},\"base_grand_total\":{\"visible\":true,\"sorting\":false},\"grand_total\":{\"visible\":true,\"sorting\":false},\"billing_address\":{\"visible\":false,\"sorting\":false},\"shipping_address\":{\"visible\":false,\"sorting\":false},\"shipping_information\":{\"visible\":false,\"sorting\":false},\"customer_email\":{\"visible\":false,\"sorting\":false},\"subtotal\":{\"visible\":false,\"sorting\":false},\"shipping_and_handling\":{\"visible\":false,\"sorting\":false},\"customer_name\":{\"visible\":false,\"sorting\":false},\"total_refunded\":{\"visible\":false,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"customer_group\":{\"visible\":false,\"sorting\":false},\"payment_method\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"created_at\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"search\":{\"value\":\"\"},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"increment_id\":1,\"store_id\":2,\"created_at\":3,\"billing_name\":4,\"shipping_name\":5,\"base_grand_total\":6,\"grand_total\":7,\"status\":8,\"billing_address\":9,\"shipping_address\":10,\"shipping_information\":11,\"customer_email\":12,\"customer_group\":13,\"subtotal\":14,\"shipping_and_handling\":15,\"customer_name\":16,\"payment_method\":17,\"total_refunded\":18,\"actions\":19}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,1,'sales_order_grid','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"increment_id\":{\"visible\":true,\"sorting\":\"desc\"},\"store_id\":{\"visible\":true,\"sorting\":false},\"billing_name\":{\"visible\":true,\"sorting\":false},\"shipping_name\":{\"visible\":true,\"sorting\":false},\"base_grand_total\":{\"visible\":true,\"sorting\":false},\"grand_total\":{\"visible\":true,\"sorting\":false},\"billing_address\":{\"visible\":false,\"sorting\":false},\"shipping_address\":{\"visible\":false,\"sorting\":false},\"shipping_information\":{\"visible\":false,\"sorting\":false},\"customer_email\":{\"visible\":false,\"sorting\":false},\"subtotal\":{\"visible\":false,\"sorting\":false},\"shipping_and_handling\":{\"visible\":false,\"sorting\":false},\"customer_name\":{\"visible\":false,\"sorting\":false},\"total_refunded\":{\"visible\":false,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"customer_group\":{\"visible\":false,\"sorting\":false},\"payment_method\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"created_at\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"search\":{\"value\":\"\"},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"increment_id\":1,\"store_id\":2,\"created_at\":3,\"billing_name\":4,\"shipping_name\":5,\"base_grand_total\":6,\"grand_total\":7,\"status\":8,\"billing_address\":9,\"shipping_address\":10,\"shipping_information\":11,\"customer_email\":12,\"customer_group\":13,\"subtotal\":14,\"shipping_and_handling\":15,\"customer_name\":16,\"payment_method\":17,\"total_refunded\":18,\"actions\":19}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,1,'sales_order_invoice_grid','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"increment_id\":{\"visible\":true,\"sorting\":\"asc\"},\"order_increment_id\":{\"visible\":true,\"sorting\":false},\"billing_name\":{\"visible\":true,\"sorting\":false},\"base_grand_total\":{\"visible\":true,\"sorting\":false},\"grand_total\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":false,\"sorting\":false},\"billing_address\":{\"visible\":false,\"sorting\":false},\"shipping_address\":{\"visible\":false,\"sorting\":false},\"customer_name\":{\"visible\":false,\"sorting\":false},\"customer_email\":{\"visible\":false,\"sorting\":false},\"shipping_information\":{\"visible\":false,\"sorting\":false},\"subtotal\":{\"visible\":false,\"sorting\":false},\"shipping_and_handling\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"state\":{\"visible\":true,\"sorting\":false},\"customer_group_id\":{\"visible\":false,\"sorting\":false},\"payment_method\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"created_at\":{\"visible\":true,\"sorting\":false},\"order_created_at\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"increment_id\":1,\"created_at\":2,\"order_increment_id\":3,\"order_created_at\":4,\"billing_name\":5,\"state\":6,\"base_grand_total\":7,\"grand_total\":8,\"store_id\":9,\"billing_address\":10,\"shipping_address\":11,\"customer_name\":12,\"customer_email\":13,\"customer_group_id\":14,\"payment_method\":15,\"shipping_information\":16,\"subtotal\":17,\"shipping_and_handling\":18,\"actions\":19},\"search\":{\"value\":\"\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,1,'sales_order_invoice_grid','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"increment_id\":{\"visible\":true,\"sorting\":\"asc\"},\"order_increment_id\":{\"visible\":true,\"sorting\":false},\"billing_name\":{\"visible\":true,\"sorting\":false},\"base_grand_total\":{\"visible\":true,\"sorting\":false},\"grand_total\":{\"visible\":true,\"sorting\":false},\"store_id\":{\"visible\":false,\"sorting\":false},\"billing_address\":{\"visible\":false,\"sorting\":false},\"shipping_address\":{\"visible\":false,\"sorting\":false},\"customer_name\":{\"visible\":false,\"sorting\":false},\"customer_email\":{\"visible\":false,\"sorting\":false},\"shipping_information\":{\"visible\":false,\"sorting\":false},\"subtotal\":{\"visible\":false,\"sorting\":false},\"shipping_and_handling\":{\"visible\":false,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"state\":{\"visible\":true,\"sorting\":false},\"customer_group_id\":{\"visible\":false,\"sorting\":false},\"payment_method\":{\"visible\":false,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"created_at\":{\"visible\":true,\"sorting\":false},\"order_created_at\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"increment_id\":1,\"created_at\":2,\"order_increment_id\":3,\"order_created_at\":4,\"billing_name\":5,\"state\":6,\"base_grand_total\":7,\"grand_total\":8,\"store_id\":9,\"billing_address\":10,\"shipping_address\":11,\"customer_name\":12,\"customer_email\":13,\"customer_group_id\":14,\"payment_method\":15,\"shipping_information\":16,\"subtotal\":17,\"shipping_and_handling\":18,\"actions\":19},\"search\":{\"value\":\"\"}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,2,'product_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"agave\":{\"visible\":true,\"sorting\":false},\"agua_fermentacion\":{\"visible\":true,\"sorting\":false},\"anio\":{\"visible\":true,\"sorting\":false},\"botellas_por_caja\":{\"visible\":true,\"sorting\":false},\"cocimiento\":{\"visible\":true,\"sorting\":false},\"fermentacion\":{\"visible\":true,\"sorting\":false},\"litros_destilados\":{\"visible\":true,\"sorting\":false},\"maduracion_anios\":{\"visible\":true,\"sorting\":false},\"maestro\":{\"visible\":true,\"sorting\":false},\"maguey\":{\"visible\":true,\"sorting\":false},\"molido\":{\"visible\":true,\"sorting\":false},\"notas_de_cata\":{\"visible\":true,\"sorting\":false},\"no_destilaciones\":{\"visible\":true,\"sorting\":false},\"origen_planta\":{\"visible\":true,\"sorting\":false},\"presentacion_cl\":{\"visible\":true,\"sorting\":false},\"region\":{\"visible\":true,\"sorting\":false},\"terruno\":{\"visible\":true,\"sorting\":false},\"tipo_abocado\":{\"visible\":true,\"sorting\":false},\"tipo_ajuste\":{\"visible\":true,\"sorting\":false},\"tipo_destilador\":{\"visible\":true,\"sorting\":false},\"variedad\":{\"visible\":true,\"sorting\":false},\"grados_alcohol\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"color\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"featured\":{\"visible\":false,\"sorting\":false},\"entrega\":{\"visible\":true,\"sorting\":false},\"estado\":{\"visible\":true,\"sorting\":false},\"filter_maestro\":{\"visible\":true,\"sorting\":false},\"filter_maguey\":{\"visible\":true,\"sorting\":false},\"filter_region\":{\"visible\":true,\"sorting\":false},\"home_producto_destacado\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"color\":22,\"news_from_date\":23,\"news_to_date\":24,\"custom_design\":25,\"custom_design_from\":26,\"custom_design_to\":27,\"page_layout\":28,\"country_of_manufacture\":29,\"custom_layout\":30,\"url_key\":31,\"msrp\":32,\"tax_class_id\":33,\"gift_message_available\":34,\"featured\":35,\"agave\":36,\"agua_fermentacion\":37,\"anio\":38,\"botellas_por_caja\":39,\"cocimiento\":40,\"entrega\":41,\"estado\":42,\"fermentacion\":43,\"filter_maestro\":44,\"filter_maguey\":45,\"filter_region\":46,\"litros_destilados\":47,\"maduracion_anios\":48,\"maestro\":49,\"maguey\":50,\"molido\":51,\"notas_de_cata\":52,\"no_destilaciones\":53,\"origen_planta\":54,\"presentacion_cl\":55,\"region\":56,\"terruno\":57,\"tipo_abocado\":58,\"tipo_ajuste\":59,\"tipo_destilador\":60,\"variedad\":61,\"grados_alcohol\":62,\"home_producto_destacado\":63,\"actions\":64}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,2,'product_listing','current',0,NULL,'{\"current\":{\"filters\":{\"applied\":{\"placeholder\":true}},\"columns\":{\"entity_id\":{\"visible\":true,\"sorting\":\"asc\"},\"name\":{\"visible\":true,\"sorting\":false},\"sku\":{\"visible\":true,\"sorting\":false},\"price\":{\"visible\":true,\"sorting\":false},\"websites\":{\"visible\":true,\"sorting\":false},\"qty\":{\"visible\":true,\"sorting\":false},\"short_description\":{\"visible\":false,\"sorting\":false},\"special_price\":{\"visible\":false,\"sorting\":false},\"cost\":{\"visible\":false,\"sorting\":false},\"weight\":{\"visible\":false,\"sorting\":false},\"meta_title\":{\"visible\":false,\"sorting\":false},\"meta_keyword\":{\"visible\":false,\"sorting\":false},\"meta_description\":{\"visible\":false,\"sorting\":false},\"url_key\":{\"visible\":false,\"sorting\":false},\"msrp\":{\"visible\":false,\"sorting\":false},\"agave\":{\"visible\":true,\"sorting\":false},\"agua_fermentacion\":{\"visible\":true,\"sorting\":false},\"anio\":{\"visible\":true,\"sorting\":false},\"botellas_por_caja\":{\"visible\":true,\"sorting\":false},\"cocimiento\":{\"visible\":true,\"sorting\":false},\"fermentacion\":{\"visible\":true,\"sorting\":false},\"litros_destilados\":{\"visible\":true,\"sorting\":false},\"maduracion_anios\":{\"visible\":true,\"sorting\":false},\"maestro\":{\"visible\":true,\"sorting\":false},\"maguey\":{\"visible\":true,\"sorting\":false},\"molido\":{\"visible\":true,\"sorting\":false},\"notas_de_cata\":{\"visible\":true,\"sorting\":false},\"no_destilaciones\":{\"visible\":true,\"sorting\":false},\"origen_planta\":{\"visible\":true,\"sorting\":false},\"presentacion_cl\":{\"visible\":true,\"sorting\":false},\"region\":{\"visible\":true,\"sorting\":false},\"terruno\":{\"visible\":true,\"sorting\":false},\"tipo_abocado\":{\"visible\":true,\"sorting\":false},\"tipo_ajuste\":{\"visible\":true,\"sorting\":false},\"tipo_destilador\":{\"visible\":true,\"sorting\":false},\"variedad\":{\"visible\":true,\"sorting\":false},\"grados_alcohol\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"type_id\":{\"visible\":true,\"sorting\":false},\"attribute_set_id\":{\"visible\":true,\"sorting\":false},\"visibility\":{\"visible\":true,\"sorting\":false},\"status\":{\"visible\":true,\"sorting\":false},\"manufacturer\":{\"visible\":false,\"sorting\":false},\"color\":{\"visible\":false,\"sorting\":false},\"custom_design\":{\"visible\":false,\"sorting\":false},\"page_layout\":{\"visible\":false,\"sorting\":false},\"country_of_manufacture\":{\"visible\":false,\"sorting\":false},\"custom_layout\":{\"visible\":false,\"sorting\":false},\"tax_class_id\":{\"visible\":false,\"sorting\":false},\"gift_message_available\":{\"visible\":false,\"sorting\":false},\"featured\":{\"visible\":false,\"sorting\":false},\"entrega\":{\"visible\":true,\"sorting\":false},\"estado\":{\"visible\":true,\"sorting\":false},\"filter_maestro\":{\"visible\":true,\"sorting\":false},\"filter_maguey\":{\"visible\":true,\"sorting\":false},\"filter_region\":{\"visible\":true,\"sorting\":false},\"home_producto_destacado\":{\"visible\":true,\"sorting\":false},\"actions\":{\"visible\":true,\"sorting\":false},\"thumbnail\":{\"visible\":true,\"sorting\":false},\"special_from_date\":{\"visible\":false,\"sorting\":false},\"special_to_date\":{\"visible\":false,\"sorting\":false},\"news_from_date\":{\"visible\":false,\"sorting\":false},\"news_to_date\":{\"visible\":false,\"sorting\":false},\"custom_design_from\":{\"visible\":false,\"sorting\":false},\"custom_design_to\":{\"visible\":false,\"sorting\":false},\"paquete\":{\"visible\":true,\"sorting\":false}},\"displayMode\":\"grid\",\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":200},\"positions\":{\"ids\":0,\"entity_id\":1,\"thumbnail\":2,\"name\":3,\"type_id\":4,\"attribute_set_id\":5,\"sku\":6,\"price\":7,\"qty\":8,\"visibility\":9,\"status\":10,\"websites\":11,\"short_description\":12,\"special_price\":13,\"special_from_date\":14,\"special_to_date\":15,\"cost\":16,\"weight\":17,\"manufacturer\":18,\"meta_title\":19,\"meta_keyword\":20,\"meta_description\":21,\"color\":22,\"news_from_date\":23,\"news_to_date\":24,\"custom_design\":25,\"custom_design_from\":26,\"custom_design_to\":27,\"page_layout\":28,\"country_of_manufacture\":29,\"custom_layout\":30,\"url_key\":31,\"msrp\":32,\"tax_class_id\":33,\"gift_message_available\":34,\"featured\":35,\"agave\":36,\"agua_fermentacion\":37,\"anio\":38,\"botellas_por_caja\":39,\"cocimiento\":40,\"entrega\":41,\"estado\":42,\"fermentacion\":43,\"filter_maguey\":44,\"filter_region\":45,\"litros_destilados\":46,\"maduracion_anios\":47,\"maestro\":48,\"maguey\":49,\"molido\":50,\"notas_de_cata\":51,\"no_destilaciones\":52,\"origen_planta\":53,\"presentacion_cl\":54,\"region\":55,\"terruno\":56,\"tipo_abocado\":57,\"tipo_ajuste\":58,\"tipo_destilador\":59,\"variedad\":60,\"grados_alcohol\":61,\"home_producto_destacado\":62,\"actions\":63,\"paquete\":64}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,2,'product_attributes_grid','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"columns\":{\"attribute_code\":{\"visible\":true,\"sorting\":\"asc\"},\"frontend_label\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_required\":{\"visible\":true,\"sorting\":false},\"is_user_defined\":{\"visible\":true,\"sorting\":false},\"is_visible\":{\"visible\":true,\"sorting\":false},\"is_global\":{\"visible\":true,\"sorting\":false},\"is_searchable\":{\"visible\":true,\"sorting\":false},\"is_comparable\":{\"visible\":true,\"sorting\":false},\"is_filterable\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"attribute_code\":1,\"frontend_label\":2,\"is_required\":3,\"is_user_defined\":4,\"is_visible\":5,\"is_global\":6,\"is_searchable\":7,\"is_comparable\":8,\"is_filterable\":9}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,2,'product_attributes_grid','current',0,NULL,'{\"current\":{\"columns\":{\"attribute_code\":{\"visible\":true,\"sorting\":\"asc\"},\"frontend_label\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_required\":{\"visible\":true,\"sorting\":false},\"is_user_defined\":{\"visible\":true,\"sorting\":false},\"is_visible\":{\"visible\":true,\"sorting\":false},\"is_global\":{\"visible\":true,\"sorting\":false},\"is_searchable\":{\"visible\":true,\"sorting\":false},\"is_comparable\":{\"visible\":true,\"sorting\":false},\"is_filterable\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"displayMode\":\"grid\",\"positions\":{\"ids\":0,\"attribute_code\":1,\"frontend_label\":2,\"is_required\":3,\"is_user_defined\":4,\"is_visible\":5,\"is_global\":6,\"is_searchable\":7,\"is_comparable\":8,\"is_filterable\":9}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,1,'product_attributes_listing','current',0,NULL,'{\"current\":{\"displayMode\":\"grid\",\"columns\":{\"attribute_code\":{\"visible\":true,\"sorting\":\"asc\"},\"frontend_label\":{\"visible\":true,\"sorting\":false},\"is_filterable\":{\"visible\":true,\"sorting\":false},\"is_required\":{\"visible\":true,\"sorting\":false},\"is_user_defined\":{\"visible\":true,\"sorting\":false},\"is_visible\":{\"visible\":true,\"sorting\":false},\"is_global\":{\"visible\":true,\"sorting\":false},\"is_searchable\":{\"visible\":true,\"sorting\":false},\"is_comparable\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"is_filterable\":1,\"attribute_code\":2,\"frontend_label\":3,\"is_required\":4,\"is_user_defined\":5,\"is_visible\":6,\"is_global\":7,\"is_searchable\":8,\"is_comparable\":9}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,1,'product_attributes_listing','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"displayMode\":\"grid\",\"columns\":{\"attribute_code\":{\"visible\":true,\"sorting\":\"asc\"},\"frontend_label\":{\"visible\":true,\"sorting\":false},\"is_filterable\":{\"visible\":true,\"sorting\":false},\"is_required\":{\"visible\":true,\"sorting\":false},\"is_user_defined\":{\"visible\":true,\"sorting\":false},\"is_visible\":{\"visible\":true,\"sorting\":false},\"is_global\":{\"visible\":true,\"sorting\":false},\"is_searchable\":{\"visible\":true,\"sorting\":false},\"is_comparable\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false}},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20},\"positions\":{\"ids\":0,\"is_filterable\":1,\"attribute_code\":2,\"frontend_label\":3,\"is_required\":4,\"is_user_defined\":5,\"is_visible\":6,\"is_global\":7,\"is_searchable\":8,\"is_comparable\":9}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,1,'product_attributes_grid','default',1,'Default View','{\"views\":{\"default\":{\"label\":\"Default View\",\"index\":\"default\",\"editable\":false,\"data\":{\"displayMode\":\"grid\",\"columns\":{\"attribute_code\":{\"visible\":true,\"sorting\":\"asc\"},\"frontend_label\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_required\":{\"visible\":true,\"sorting\":false},\"is_user_defined\":{\"visible\":true,\"sorting\":false},\"is_visible\":{\"visible\":true,\"sorting\":false},\"is_global\":{\"visible\":true,\"sorting\":false},\"is_searchable\":{\"visible\":true,\"sorting\":false},\"is_comparable\":{\"visible\":true,\"sorting\":false},\"is_filterable\":{\"visible\":true,\"sorting\":false}},\"positions\":{\"ids\":0,\"attribute_code\":1,\"frontend_label\":2,\"is_required\":3,\"is_user_defined\":4,\"is_visible\":5,\"is_global\":6,\"is_searchable\":7,\"is_comparable\":8,\"is_filterable\":9},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20}},\"value\":\"Default View\"}}}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,1,'product_attributes_grid','current',0,NULL,'{\"current\":{\"displayMode\":\"grid\",\"columns\":{\"attribute_code\":{\"visible\":true,\"sorting\":\"asc\"},\"frontend_label\":{\"visible\":true,\"sorting\":false},\"ids\":{\"visible\":true,\"sorting\":false},\"is_required\":{\"visible\":true,\"sorting\":false},\"is_user_defined\":{\"visible\":true,\"sorting\":false},\"is_visible\":{\"visible\":true,\"sorting\":false},\"is_global\":{\"visible\":true,\"sorting\":false},\"is_searchable\":{\"visible\":true,\"sorting\":false},\"is_comparable\":{\"visible\":true,\"sorting\":false},\"is_filterable\":{\"visible\":true,\"sorting\":false}},\"positions\":{\"ids\":0,\"attribute_code\":1,\"frontend_label\":2,\"is_required\":3,\"is_user_defined\":4,\"is_visible\":5,\"is_global\":6,\"is_searchable\":7,\"is_comparable\":8,\"is_filterable\":9},\"paging\":{\"options\":{\"20\":{\"value\":20,\"label\":20},\"30\":{\"value\":30,\"label\":30},\"50\":{\"value\":50,\"label\":50},\"100\":{\"value\":100,\"label\":100},\"200\":{\"value\":200,\"label\":200}},\"value\":20}}}','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ui_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `url_rewrite`
--

DROP TABLE IF EXISTS `url_rewrite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_rewrite` (
  `url_rewrite_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rewrite Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Entity type code',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `request_path` varchar(255) DEFAULT NULL COMMENT 'Request Path',
  `target_path` varchar(255) DEFAULT NULL COMMENT 'Target Path',
  `redirect_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Redirect Type',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `is_autogenerated` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is rewrite generated automatically flag',
  `metadata` varchar(255) DEFAULT NULL COMMENT 'Meta data for url rewrite',
  PRIMARY KEY (`url_rewrite_id`),
  UNIQUE KEY `URL_REWRITE_REQUEST_PATH_STORE_ID` (`request_path`,`store_id`),
  KEY `URL_REWRITE_TARGET_PATH` (`target_path`),
  KEY `URL_REWRITE_STORE_ID_ENTITY_ID` (`store_id`,`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COMMENT='Url Rewrites';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `url_rewrite`
--

LOCK TABLES `url_rewrite` WRITE;
/*!40000 ALTER TABLE `url_rewrite` DISABLE KEYS */;
INSERT INTO `url_rewrite` VALUES (1,'cms-page',1,'no-route','cms/page/view/page_id/1',0,1,NULL,1,NULL),(2,'cms-page',2,'home','cms/page/view/page_id/2',0,1,NULL,1,NULL),(3,'cms-page',3,'enable-cookies','cms/page/view/page_id/3',0,1,NULL,1,NULL),(5,'cms-page',5,'thebell1-jewelry-responsive-magento-theme','cms/page/view/page_id/5',0,1,NULL,1,NULL),(6,'cms-page',6,'thebell2-shoe-responsive-magento-theme','cms/page/view/page_id/6',0,1,NULL,1,NULL),(7,'cms-page',7,'thebell3-furniture-responsive-magento-theme','cms/page/view/page_id/7',0,1,NULL,1,NULL),(8,'cms-page',8,'thebell4-kitchenware-responsive-magento-theme','cms/page/view/page_id/8',0,1,NULL,1,NULL),(9,'cms-page',9,'about-magento-demo-store','cms/page/view/page_id/9',0,1,NULL,1,NULL),(10,'cms-page',10,'new-arrials','cms/page/view/page_id/10',0,1,NULL,1,NULL),(23,'cms-page',11,'home-products','cms/page/view/page_id/11',0,1,NULL,1,NULL),(27,'cms-page',12,'preguntas-frecuentes','cms/page/view/page_id/12',0,1,NULL,1,NULL),(28,'cms-page',4,'politica-privacidad','cms/page/view/page_id/4',0,1,NULL,1,NULL),(61,'category',4,'compra.html','catalog/category/view/id/4',0,1,NULL,1,NULL),(66,'category',3,'mezcal.html','catalog/category/view/id/3',0,1,NULL,1,NULL),(69,'product',2,'mexxico-joven.html','catalog/product/view/id/2',0,1,NULL,1,NULL),(70,'product',2,'mezcal/mexxico-joven.html','catalog/product/view/id/2/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(71,'category',5,'accesorios.html','catalog/category/view/id/5',0,1,NULL,1,NULL),(75,'category',6,'experiencias.html','catalog/category/view/id/6',0,1,NULL,1,NULL),(76,'product',3,'sin-piedad-44.html','catalog/product/view/id/3',0,1,NULL,1,NULL),(77,'product',3,'mezcal/sin-piedad-44.html','catalog/product/view/id/3/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(78,'product',3,'compra/sin-piedad-44.html','catalog/product/view/id/3/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(79,'product',4,'sin-piedad-cirial.html','catalog/product/view/id/4',0,1,NULL,1,NULL),(80,'product',4,'mezcal/sin-piedad-cirial.html','catalog/product/view/id/4/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(81,'product',4,'compra/sin-piedad-cirial.html','catalog/product/view/id/4/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(82,'product',1,'meteoro.html','catalog/product/view/id/1',0,1,NULL,1,NULL),(83,'product',1,'mezcal/meteoro.html','catalog/product/view/id/1/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(84,'product',1,'compra/meteoro.html','catalog/product/view/id/1/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(85,'product',1,'mezcal-meteoro.html','meteoro.html',301,1,NULL,0,'a:0:{}'),(86,'product',1,'mezcal/mezcal-meteoro.html','mezcal/meteoro.html',301,1,NULL,0,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(87,'product',5,'sin-piedad-44-petaca.html','catalog/product/view/id/5',0,1,NULL,1,NULL),(88,'product',5,'mezcal/sin-piedad-44-petaca.html','catalog/product/view/id/5/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(89,'product',5,'compra/sin-piedad-44-petaca.html','catalog/product/view/id/5/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(90,'product',6,'mayalen-etiqueta-negra.html','catalog/product/view/id/6',0,1,NULL,1,NULL),(91,'product',6,'mezcal/mayalen-etiqueta-negra.html','catalog/product/view/id/6/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(92,'product',6,'compra/mayalen-etiqueta-negra.html','catalog/product/view/id/6/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(93,'product',7,'mexxico-petaca.html','catalog/product/view/id/7',0,1,NULL,1,NULL),(94,'product',7,'mezcal/mexxico-petaca.html','catalog/product/view/id/7/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(95,'product',7,'compra/mexxico-petaca.html','catalog/product/view/id/7/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(96,'product',8,'mezcal-chaneque-madrecuixe.html','catalog/product/view/id/8',0,1,NULL,1,NULL),(97,'product',8,'mezcal/mezcal-chaneque-madrecuixe.html','catalog/product/view/id/8/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(98,'product',8,'compra/mezcal-chaneque-madrecuixe.html','catalog/product/view/id/8/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(99,'product',9,'mezcal-chaneque-tobaziche.html','catalog/product/view/id/9',0,1,NULL,1,NULL),(100,'product',9,'mezcal/mezcal-chaneque-tobaziche.html','catalog/product/view/id/9/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(101,'product',9,'compra/mezcal-chaneque-tobaziche.html','catalog/product/view/id/9/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(102,'product',10,'mezcal-chaneque-espadin.html','catalog/product/view/id/10',0,1,NULL,1,NULL),(103,'product',10,'mezcal/mezcal-chaneque-espadin.html','catalog/product/view/id/10/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(104,'product',10,'compra/mezcal-chaneque-espadin.html','catalog/product/view/id/10/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(105,'product',11,'mezcal-chaneque-tobala-silvestre.html','catalog/product/view/id/11',0,1,NULL,1,NULL),(106,'product',11,'mezcal/mezcal-chaneque-tobala-silvestre.html','catalog/product/view/id/11/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(107,'product',11,'compra/mezcal-chaneque-tobala-silvestre.html','catalog/product/view/id/11/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(108,'product',12,'mezcal-chaneque-ensamble.html','catalog/product/view/id/12',0,1,NULL,1,NULL),(109,'product',12,'mezcal/mezcal-chaneque-ensamble.html','catalog/product/view/id/12/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(110,'product',12,'compra/mezcal-chaneque-ensamble.html','catalog/product/view/id/12/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(111,'product',13,'bruxo-1-espadin.html','catalog/product/view/id/13',0,1,NULL,1,NULL),(112,'product',13,'mezcal/bruxo-1-espadin.html','catalog/product/view/id/13/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(113,'product',13,'compra/bruxo-1-espadin.html','catalog/product/view/id/13/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(114,'product',14,'bruxo-2-pechuga-de-maguey.html','catalog/product/view/id/14',0,1,NULL,1,NULL),(115,'product',14,'mezcal/bruxo-2-pechuga-de-maguey.html','catalog/product/view/id/14/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(116,'product',14,'compra/bruxo-2-pechuga-de-maguey.html','catalog/product/view/id/14/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(117,'product',15,'bruxo-3-barril.html','catalog/product/view/id/15',0,1,NULL,1,NULL),(118,'product',15,'mezcal/bruxo-3-barril.html','catalog/product/view/id/15/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(119,'product',15,'compra/bruxo-3-barril.html','catalog/product/view/id/15/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(120,'product',16,'bruxo-4-ensamble.html','catalog/product/view/id/16',0,1,NULL,1,NULL),(121,'product',16,'mezcal/bruxo-4-ensamble.html','catalog/product/view/id/16/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(122,'product',16,'compra/bruxo-4-ensamble.html','catalog/product/view/id/16/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(123,'product',17,'iba-55-mezcal-joven.html','catalog/product/view/id/17',0,1,NULL,1,NULL),(124,'product',17,'mezcal/iba-55-mezcal-joven.html','catalog/product/view/id/17/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(125,'product',17,'compra/iba-55-mezcal-joven.html','catalog/product/view/id/17/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(126,'product',18,'do-a-natalia.html','catalog/product/view/id/18',0,1,NULL,1,NULL),(127,'product',18,'mezcal/do-a-natalia.html','catalog/product/view/id/18/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(128,'product',18,'compra/do-a-natalia.html','catalog/product/view/id/18/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(129,'product',19,'marca-negra-ensamble.html','catalog/product/view/id/19',0,1,NULL,1,NULL),(130,'product',19,'mezcal/marca-negra-ensamble.html','catalog/product/view/id/19/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(131,'product',19,'compra/marca-negra-ensamble.html','catalog/product/view/id/19/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(132,'product',20,'marca-negra-tobala.html','catalog/product/view/id/20',0,1,NULL,1,NULL),(133,'product',20,'mezcal/marca-negra-tobala.html','catalog/product/view/id/20/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(134,'product',20,'compra/marca-negra-tobala.html','catalog/product/view/id/20/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(135,'product',21,'marca-negra-tepextate.html','catalog/product/view/id/21',0,1,NULL,1,NULL),(136,'product',21,'mezcal/marca-negra-tepextate.html','catalog/product/view/id/21/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(137,'product',21,'compra/marca-negra-tepextate.html','catalog/product/view/id/21/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(138,'product',22,'marca-negra-dobadan.html','catalog/product/view/id/22',0,1,NULL,1,NULL),(139,'product',22,'mezcal/marca-negra-dobadan.html','catalog/product/view/id/22/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(140,'product',22,'compra/marca-negra-dobadan.html','catalog/product/view/id/22/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(141,'product',23,'pescador-de-sue-os-arroque-o.html','catalog/product/view/id/23',0,1,NULL,1,NULL),(142,'product',23,'mezcal/pescador-de-sue-os-arroque-o.html','catalog/product/view/id/23/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(143,'product',23,'compra/pescador-de-sue-os-arroque-o.html','catalog/product/view/id/23/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(144,'product',24,'pescador-de-sue-os-cuishe.html','catalog/product/view/id/24',0,1,NULL,1,NULL),(145,'product',24,'mezcal/pescador-de-sue-os-cuishe.html','catalog/product/view/id/24/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(146,'product',24,'compra/pescador-de-sue-os-cuishe.html','catalog/product/view/id/24/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(147,'product',25,'pescador-de-sue-os-tobala.html','catalog/product/view/id/25',0,1,NULL,1,NULL),(148,'product',25,'mezcal/pescador-de-sue-os-tobala.html','catalog/product/view/id/25/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(149,'product',25,'compra/pescador-de-sue-os-tobala.html','catalog/product/view/id/25/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(150,'product',26,'pescador-de-sue-os-coyote.html','catalog/product/view/id/26',0,1,NULL,1,NULL),(151,'product',26,'mezcal/pescador-de-sue-os-coyote.html','catalog/product/view/id/26/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(152,'product',26,'compra/pescador-de-sue-os-coyote.html','catalog/product/view/id/26/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(153,'product',27,'pescador-de-sue-os-tepextate.html','catalog/product/view/id/27',0,1,NULL,1,NULL),(154,'product',27,'mezcal/pescador-de-sue-os-tepextate.html','catalog/product/view/id/27/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(155,'product',27,'compra/pescador-de-sue-os-tepextate.html','catalog/product/view/id/27/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(156,'product',28,'pescador-de-sue-os-bicuixe.html','catalog/product/view/id/28',0,1,NULL,1,NULL),(157,'product',28,'mezcal/pescador-de-sue-os-bicuixe.html','catalog/product/view/id/28/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(158,'product',28,'compra/pescador-de-sue-os-bicuixe.html','catalog/product/view/id/28/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(159,'product',29,'papadiablo-espadin.html','catalog/product/view/id/29',0,1,NULL,1,NULL),(160,'product',29,'mezcal/papadiablo-espadin.html','catalog/product/view/id/29/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(161,'product',29,'compra/papadiablo-espadin.html','catalog/product/view/id/29/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(162,'product',30,'papadiablo-ensamble.html','catalog/product/view/id/30',0,1,NULL,1,NULL),(163,'product',30,'mezcal/papadiablo-ensamble.html','catalog/product/view/id/30/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(164,'product',30,'compra/papadiablo-ensamble.html','catalog/product/view/id/30/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(165,'product',31,'nakawe.html','catalog/product/view/id/31',0,1,NULL,1,NULL),(166,'product',31,'mezcal/nakawe.html','catalog/product/view/id/31/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(167,'product',31,'compra/nakawe.html','catalog/product/view/id/31/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(168,'product',32,'iba-40-mezcal-joven.html','catalog/product/view/id/32',0,1,NULL,1,NULL),(169,'product',32,'mezcal/iba-40-mezcal-joven.html','catalog/product/view/id/32/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(170,'product',32,'compra/iba-40-mezcal-joven.html','catalog/product/view/id/32/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(171,'product',33,'machetazo-papalote.html','catalog/product/view/id/33',0,1,NULL,1,NULL),(172,'product',33,'mezcal/machetazo-papalote.html','catalog/product/view/id/33/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(173,'product',33,'compra/machetazo-papalote.html','catalog/product/view/id/33/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(174,'product',34,'el-hijo-del-santo.html','catalog/product/view/id/34',0,1,NULL,1,NULL),(175,'product',34,'mezcal/el-hijo-del-santo.html','catalog/product/view/id/34/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(176,'product',34,'compra/el-hijo-del-santo.html','catalog/product/view/id/34/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(177,'product',35,'mezcal-chaneque-mexicano.html','catalog/product/view/id/35',0,1,NULL,1,NULL),(178,'product',35,'mezcal/mezcal-chaneque-mexicano.html','catalog/product/view/id/35/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(179,'product',35,'compra/mezcal-chaneque-mexicano.html','catalog/product/view/id/35/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}'),(180,'product',36,'mezcal-chaneque-duranguensis.html','catalog/product/view/id/36',0,1,NULL,1,NULL),(181,'product',36,'mezcal/mezcal-chaneque-duranguensis.html','catalog/product/view/id/36/category/3',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"3\";}'),(182,'product',36,'compra/mezcal-chaneque-duranguensis.html','catalog/product/view/id/36/category/4',0,1,NULL,1,'a:1:{s:11:\"category_id\";s:1:\"4\";}');
/*!40000 ALTER TABLE `url_rewrite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variable`
--

DROP TABLE IF EXISTS `variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable` (
  `variable_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Variable Code',
  `name` varchar(255) DEFAULT NULL COMMENT 'Variable Name',
  PRIMARY KEY (`variable_id`),
  UNIQUE KEY `VARIABLE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variable`
--

LOCK TABLES `variable` WRITE;
/*!40000 ALTER TABLE `variable` DISABLE KEYS */;
/*!40000 ALTER TABLE `variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variable_value`
--

DROP TABLE IF EXISTS `variable_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Value Id',
  `variable_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Variable Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `plain_value` text COMMENT 'Plain Text Value',
  `html_value` text COMMENT 'Html Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `VARIABLE_VALUE_VARIABLE_ID_STORE_ID` (`variable_id`,`store_id`),
  KEY `VARIABLE_VALUE_STORE_ID` (`store_id`),
  CONSTRAINT `VARIABLE_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  CONSTRAINT `VARIABLE_VALUE_VARIABLE_ID_VARIABLE_VARIABLE_ID` FOREIGN KEY (`variable_id`) REFERENCES `variable` (`variable_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variable Value';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variable_value`
--

LOCK TABLES `variable_value` WRITE;
/*!40000 ALTER TABLE `variable_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `variable_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vault_payment_token`
--

DROP TABLE IF EXISTS `vault_payment_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vault_payment_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `public_hash` varchar(128) NOT NULL COMMENT 'Hash code for using on frontend',
  `payment_method_code` varchar(128) NOT NULL COMMENT 'Payment method code',
  `type` varchar(128) NOT NULL COMMENT 'Type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `expires_at` timestamp NULL DEFAULT NULL COMMENT 'Expires At',
  `gateway_token` varchar(255) NOT NULL COMMENT 'Gateway Token',
  `details` text COMMENT 'Details',
  `is_active` tinyint(1) NOT NULL COMMENT 'Is active flag',
  `is_visible` tinyint(1) NOT NULL COMMENT 'Is visible flag',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `VAULT_PAYMENT_TOKEN_HASH_UNIQUE_INDEX_PUBLIC_HASH` (`public_hash`),
  UNIQUE KEY `UNQ_54DCE14AEAEA03B587F9EF723EB10A10` (`payment_method_code`,`customer_id`,`gateway_token`),
  KEY `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`),
  CONSTRAINT `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Vault tokens of payment';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vault_payment_token`
--

LOCK TABLES `vault_payment_token` WRITE;
/*!40000 ALTER TABLE `vault_payment_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `vault_payment_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vault_payment_token_order_payment_link`
--

DROP TABLE IF EXISTS `vault_payment_token_order_payment_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vault_payment_token_order_payment_link` (
  `order_payment_id` int(10) unsigned NOT NULL COMMENT 'Order payment Id',
  `payment_token_id` int(10) unsigned NOT NULL COMMENT 'Payment token Id',
  PRIMARY KEY (`order_payment_id`,`payment_token_id`),
  KEY `FK_4ED894655446D385894580BECA993862` (`payment_token_id`),
  CONSTRAINT `FK_4ED894655446D385894580BECA993862` FOREIGN KEY (`payment_token_id`) REFERENCES `vault_payment_token` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_CF37B9D854256534BE23C818F6291CA2` FOREIGN KEY (`order_payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Order payments to vault token';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vault_payment_token_order_payment_link`
--

LOCK TABLES `vault_payment_token_order_payment_link` WRITE;
/*!40000 ALTER TABLE `vault_payment_token_order_payment_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `vault_payment_token_order_payment_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weee_tax`
--

DROP TABLE IF EXISTS `weee_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weee_tax` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `country` varchar(2) DEFAULT NULL COMMENT 'Country',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT 'State',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`value_id`),
  KEY `WEEE_TAX_WEBSITE_ID` (`website_id`),
  KEY `WEEE_TAX_ENTITY_ID` (`entity_id`),
  KEY `WEEE_TAX_COUNTRY` (`country`),
  KEY `WEEE_TAX_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `WEEE_TAX_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `WEEE_TAX_COUNTRY_DIRECTORY_COUNTRY_COUNTRY_ID` FOREIGN KEY (`country`) REFERENCES `directory_country` (`country_id`) ON DELETE CASCADE,
  CONSTRAINT `WEEE_TAX_ENTITY_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `WEEE_TAX_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Weee Tax';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weee_tax`
--

LOCK TABLES `weee_tax` WRITE;
/*!40000 ALTER TABLE `weee_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `weee_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Widget Id',
  `widget_code` varchar(255) DEFAULT NULL COMMENT 'Widget code for template directive',
  `widget_type` varchar(255) DEFAULT NULL COMMENT 'Widget Type',
  `parameters` text COMMENT 'Parameters',
  PRIMARY KEY (`widget_id`),
  KEY `WIDGET_WIDGET_CODE` (`widget_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Preconfigured Widgets';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget`
--

LOCK TABLES `widget` WRITE;
/*!40000 ALTER TABLE `widget` DISABLE KEYS */;
/*!40000 ALTER TABLE `widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget_instance`
--

DROP TABLE IF EXISTS `widget_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget_instance` (
  `instance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Instance Id',
  `instance_type` varchar(255) DEFAULT NULL COMMENT 'Instance Type',
  `theme_id` int(10) unsigned NOT NULL COMMENT 'Theme id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Widget Title',
  `store_ids` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Store ids',
  `widget_parameters` text COMMENT 'Widget parameters',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  PRIMARY KEY (`instance_id`),
  KEY `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` (`theme_id`),
  CONSTRAINT `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Instances of Widget for Package Theme';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget_instance`
--

LOCK TABLES `widget_instance` WRITE;
/*!40000 ALTER TABLE `widget_instance` DISABLE KEYS */;
INSERT INTO `widget_instance` VALUES (1,'Magento\\Cms\\Block\\Widget\\Block',6,'Comprar por','1','a:1:{s:8:\"block_id\";s:2:\"25\";}',1);
/*!40000 ALTER TABLE `widget_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget_instance_page`
--

DROP TABLE IF EXISTS `widget_instance_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget_instance_page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Page Id',
  `instance_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Id',
  `page_group` varchar(25) DEFAULT NULL COMMENT 'Block Group Type',
  `layout_handle` varchar(255) DEFAULT NULL COMMENT 'Layout Handle',
  `block_reference` varchar(255) DEFAULT NULL COMMENT 'Container',
  `page_for` varchar(25) DEFAULT NULL COMMENT 'For instance entities',
  `entities` text COMMENT 'Catalog entities (comma separated)',
  `page_template` varchar(255) DEFAULT NULL COMMENT 'Path to widget template',
  PRIMARY KEY (`page_id`),
  KEY `WIDGET_INSTANCE_PAGE_INSTANCE_ID` (`instance_id`),
  CONSTRAINT `WIDGET_INSTANCE_PAGE_INSTANCE_ID_WIDGET_INSTANCE_INSTANCE_ID` FOREIGN KEY (`instance_id`) REFERENCES `widget_instance` (`instance_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Instance of Widget on Page';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget_instance_page`
--

LOCK TABLES `widget_instance_page` WRITE;
/*!40000 ALTER TABLE `widget_instance_page` DISABLE KEYS */;
INSERT INTO `widget_instance_page` VALUES (1,1,'pages','catalog_category_view','footer','all','','widget/static_block/default.phtml');
/*!40000 ALTER TABLE `widget_instance_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget_instance_page_layout`
--

DROP TABLE IF EXISTS `widget_instance_page_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget_instance_page_layout` (
  `page_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Page Id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  UNIQUE KEY `WIDGET_INSTANCE_PAGE_LAYOUT_LAYOUT_UPDATE_ID_PAGE_ID` (`layout_update_id`,`page_id`),
  KEY `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID` (`page_id`),
  CONSTRAINT `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID_WIDGET_INSTANCE_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `widget_instance_page` (`page_id`) ON DELETE CASCADE,
  CONSTRAINT `WIDGET_INSTANCE_PAGE_LYT_LYT_UPDATE_ID_LYT_UPDATE_LYT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout updates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget_instance_page_layout`
--

LOCK TABLES `widget_instance_page_layout` WRITE;
/*!40000 ALTER TABLE `widget_instance_page_layout` DISABLE KEYS */;
INSERT INTO `widget_instance_page_layout` VALUES (1,3);
/*!40000 ALTER TABLE `widget_instance_page_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wishlist` (
  `wishlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist ID',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer ID',
  `shared` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sharing flag (0 or 1)',
  `sharing_code` varchar(32) DEFAULT NULL COMMENT 'Sharing encrypted code',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Last updated date',
  PRIMARY KEY (`wishlist_id`),
  UNIQUE KEY `WISHLIST_CUSTOMER_ID` (`customer_id`),
  KEY `WISHLIST_SHARED` (`shared`),
  CONSTRAINT `WISHLIST_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Wishlist main Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishlist`
--

LOCK TABLES `wishlist` WRITE;
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;
INSERT INTO `wishlist` VALUES (1,1,0,'dbafe195bf6f46df50a1dc2f0a1f01b2','2020-02-28 15:36:35'),(2,2,0,'d383d046c35ba1fa4896f82fabaa296f','2020-02-28 19:06:48'),(3,3,0,'106b0f70271ff8049a884c567761f9e1','2020-03-04 20:20:52'),(4,4,0,'c3ec28fc169875918583b7f4a8fbc81a','2020-03-11 23:54:31');
/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist_item`
--

DROP TABLE IF EXISTS `wishlist_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wishlist_item` (
  `wishlist_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist item ID',
  `wishlist_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Wishlist ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Add date and time',
  `description` text COMMENT 'Short description of wish list item',
  `qty` decimal(12,4) NOT NULL COMMENT 'Qty',
  PRIMARY KEY (`wishlist_item_id`),
  KEY `WISHLIST_ITEM_WISHLIST_ID` (`wishlist_id`),
  KEY `WISHLIST_ITEM_PRODUCT_ID` (`product_id`),
  KEY `WISHLIST_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `WISHLIST_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  CONSTRAINT `WISHLIST_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  CONSTRAINT `WISHLIST_ITEM_WISHLIST_ID_WISHLIST_WISHLIST_ID` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`wishlist_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist items';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishlist_item`
--

LOCK TABLES `wishlist_item` WRITE;
/*!40000 ALTER TABLE `wishlist_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `wishlist_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist_item_option`
--

DROP TABLE IF EXISTS `wishlist_item_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wishlist_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `wishlist_item_id` int(10) unsigned NOT NULL COMMENT 'Wishlist Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `FK_A014B30B04B72DD0EAB3EECD779728D6` (`wishlist_item_id`),
  CONSTRAINT `FK_A014B30B04B72DD0EAB3EECD779728D6` FOREIGN KEY (`wishlist_item_id`) REFERENCES `wishlist_item` (`wishlist_item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist Item Option Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishlist_item_option`
--

LOCK TABLES `wishlist_item_option` WRITE;
/*!40000 ALTER TABLE `wishlist_item_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `wishlist_item_option` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-24 15:54:37
