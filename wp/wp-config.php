<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'mezcal_wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'toor');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'toor');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', '127.0.0.1');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '-M7wsZvj4<ky$n= wr+Z:Qzl8~+QNIBA4$T`AcLKWM!Cybc!Wh@J.81N`VQJ;tb?');
define('SECURE_AUTH_KEY', '}<P9OLlng9;<`/EGWda$lcZn*f.)V/N@rXH!|b;<>3`q0+OXv6Q4x8Fb3dkzk;t~');
define('LOGGED_IN_KEY', ')T_^xo|&81 5i29oBPd-K@4B^YFPU/CCVwk]oo4eoONJ;(VoW9LkTMRB:`yxI[Mv');
define('NONCE_KEY', '&3ySk:IQ(o3b0RAQ+gqvdUy/wpW>@skMqEN|&Y-)hf+31gTrCgr];?+p,Z3lbg~!');
define('AUTH_SALT', '!/J`[:QPjJEoQA{G[@5d@annttL@sp9_tt{gB_Da&biHhz9]c^Ndmw(n25Lv9+4P');
define('SECURE_AUTH_SALT', 'O,<?`keUP2`s5u`BEm5nKUt84UkqYCsh2xEC`^^Hkt+ $*!DZ=,GRwHcD5mN7?ug');
define('LOGGED_IN_SALT', 'XfbP0e+Ge3jn8,$T,|[ysag6Z3RMkrkqK)FCc.f0[9D{JN=Wt,#kVP1SxRNe+2mZ');
define('NONCE_SALT', ']JqSyB=|n*2|Yl]G;2jM2)f?xiWw-MzxM#J.QU/u^DZ4Ws d3HTUy5swUOVb4%bf');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', true);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
