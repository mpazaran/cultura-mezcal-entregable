-- WP

UPDATE wp_options SET option_value = replace(option_value, 'http://192.168.15.15/mezcal-web/wp', 'http://192.168.15.31/mezcal-web/wp') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET guid = replace(guid, 'http://192.168.15.15/mezcal-web/wp','http://192.168.15.31/mezcal-web/wp');
UPDATE wp_posts SET post_content = replace(post_content, 'http://192.168.15.15/mezcal-web/wp', 'http://192.168.15.31/mezcal-web/wp');
UPDATE wp_postmeta SET meta_value = replace(meta_value,'http://192.168.15.15/mezcal-web/wp','http://192.168.15.31/mezcal-web/wp');

-- MAGENTO

update core_config_data set value= 'http://192.168.15.31/mezcal-web-dev/' where path = 'web/secure/base_url';
update core_config_data set value= 'http://192.168.15.31/mezcal-web-dev/' where path = 'web/unsecure/base_url';