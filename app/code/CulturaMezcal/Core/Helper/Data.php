<?php

namespace CulturaMezcal\Core\Helper;

use Magento\Backend\Model\Session as AdminSession;
use Magento\Backend\Model\Session\Quote as AdminQuote;
use Magento\Backend\Model\SessionFactory as AdminSessionFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection as ResourceConnectionAlias;
use Magento\Framework\App\State;
use Magento\Framework\DataObject;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\OrderRepository;
use Magento\Store\Api\Data\StoreInterface;

class Data extends AbstractHelper
{
    /**
     * @var State
     */
    private $state;
    /**
     * @var AdminQuote
     */
    private $adminSessionQuote;
    /**
     * @var RequestInterface
     */
    private $httpRequest;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var CustomerSession
     */
    private $session;
    /**
     * @var AdminSession
     */
    private $adminSession;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var CustomerInterface
     */
    private $customer;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;
    /**
     * @var Context
     */
    private $context;
    /**
     * @var ObjectManager
     */
    private $objectManager;
    /**
     * @var Customer|false|null
     */
    private $horeca;
    /**
     * @var StoreInterface
     */
    private $store;
    /**
     * @var AdapterInterface
     */
    private $connection;
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * Customer constructor.
     *
     * @param Context                     $context
     * @param AdminQuote                  $adminSessionQuote
     * @param RequestInterface            $httpRequest
     * @param OrderRepository             $orderRepository
     * @param CheckoutSession             $checkoutSession
     * @param SessionFactory              $session
     * @param AdminSessionFactory         $adminSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param State                       $state
     * @param StoreInterface              $store
     * @param ResourceConnectionAlias     $resourceConnection
     * @param ManagerInterface            $messageManager
     */
    public function __construct(
        Context $context,
        AdminQuote $adminSessionQuote,
        RequestInterface $httpRequest,
        OrderRepository $orderRepository,
        CheckoutSession $checkoutSession,
        SessionFactory $session,
        AdminSessionFactory $adminSession,
        CustomerRepositoryInterface $customerRepository,
        State $state,
        StoreInterface $store,
        ResourceConnectionAlias $resourceConnection,
        ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->state              = $state;
        $this->adminSessionQuote  = $adminSessionQuote;
        $this->httpRequest        = $httpRequest;
        $this->orderRepository    = $orderRepository;
        $this->session            = $session->create();
        $this->adminSession       = $adminSession->create();
        $this->customerRepository = $customerRepository;
        $this->checkoutSession    = $checkoutSession;
        $this->context            = $context;
        $this->objectManager      = ObjectManager::getInstance();
        $this->store              = $store;
        $this->connection         = $resourceConnection->getConnection();
        $this->messageManager     = $messageManager;
    }

    /**
     * @return CustomerInterface|ExtensibleDataInterface
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomer()
    {
        if ($this->customer == null) {
            if ($this->isAdmin()) {
                if ($this->adminSessionQuote->getQuote()->getCustomerId()) {
                    $this->customer = $this->adminSessionQuote->getQuote()->getCustomer();
                } else {
                    if ($orderId = $this->httpRequest->getParam('order_id')) {
                        $order          = $this->orderRepository->get($orderId);
                        $this->customer = $this->customerRepository->getById($order->getCustomerId());
                    }
                }
            } else {
                if (!$this->isLogged()) {
                    throw new NoSuchEntityException();
                }
                $this->customer = $this->session->getCustomer();
            }
        }

        return $this->customer;

    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function isAdmin()
    {
        return $this->state->getAreaCode() === 'adminhtml';
    }

    /**
     * @return Quote
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getQuote()
    {
        return $this->isAdmin() ? $this->adminSessionQuote->getQuote() : $this->checkoutSession->getQuote();
    }

    /**
     * @return bool
     */
    public function isLogged()
    {
        return $this->session->isLoggedIn();
    }

    public function getForwardCustomerGroupId()
    {
        return $this->scopeConfig->getValue('payment/directdebit/forward_customer_group');
    }

    public function isForward()
    {
        if ($this->session->isLoggedIn() && !$this->isAdmin()) {
            return $this->scopeConfig->getValue('payment/directdebit/forward_customer_group') == $this->getCustomer()->getGroupId();
        }

        return false;
    }

    public function isHoreca()
    {
        if ($this->session->isLoggedIn() && !$this->isAdmin()) {
            return $this->scopeConfig->getValue('payment/directdebit/horeca_customer_group') == $this->getCustomer()->getGroupId();
        }

        return false;
    }

    public function getHorecaCustomerGroupId()
    {
        return $this->scopeConfig->getValue('payment/directdebit/horeca_customer_group');
    }

    public function getCustomerSession()
    {
        return $this->session;
    }

    /**
     * @return bool|false|Customer|DataObject|null
     */
    public function getHoreca()
    {
        if ($this->isAdmin()) {
            return false;
        }
        if (null === $this->horeca) {
            $horecaId = $this->checkoutSession->getHorecaId();
            if ($horecaId > 0) {
                /** @var Collection $collection */
                $collection = $this->objectManager->get(Collection::class);

                return $this->horeca = $collection->addFieldToFilter('entity_id', $horecaId)->getFirstItem();
            } else {
                $this->horeca = false;
            }
        }

        return $this->horeca;

    }

    public function removeHorecaFromCheckoutSession()
    {
        $this->checkoutSession->unsHorecaId();
    }

    public function isForwardPresent()
    {
        return $this->checkoutSession->getIsForwardPresent() === '1';
    }

    public function getForwardId()
    {
        return $this->checkoutSession->getForwardId();
    }

    /**
     * @return int
     */
    public function getCurrentStoreId()
    {
        return $this->store->getId();
    }

    /**
     * @return int
     */
    public function getCurrentWebsiteId()
    {
        return $this->store->getWebsiteId();
    }

    public function query($sql, $params)
    {
        return $this->connection->query($sql, $params);
    }

    public function getAttribtueOptionValueByLabel($attribute, $label)
    {
        return $this->query('
        SELECT eaov.value_id FROM eav_attribute ea
JOIN eav_attribute_option eao ON ea.attribute_id = eao.attribute_id
JOIN eav_attribute_option_value eaov ON eao.option_id = eaov.option_id AND eaov.store_id = 0
WHERE ea.attribute_code = ?
AND value = ?', [$attribute, $label])->fetchColumn(0);
    }

    public function getAttributeLabelByIds($attribute, $ids)
    {
        $idsArray = is_array($ids) ? $ids : explode(',', $ids);
        if (empty($idsArray)) {
            return [];
        }
        array_unshift($idsArray, $attribute);
        array_unshift($idsArray, $this->getCurrentStoreId() ?? 0);
        $labels = $this->query('
        SELECT IF(eaov1.value IS NULL, eaov0.value, eaov1.value) as label FROM eav_attribute ea
JOIN eav_attribute_option eao ON ea.attribute_id = eao.attribute_id
JOIN eav_attribute_option_value eaov0 ON eao.option_id = eaov0.option_id AND eaov0.store_id = 0
LEFT JOIN eav_attribute_option_value eaov1 ON eao.option_id = eaov1.option_id AND eaov1.store_id = ?
WHERE ea.attribute_code = ?
AND eao.option_id in (?' . (str_repeat(',?', count($idsArray) - 3)) . ')', $idsArray)->fetchAll();
        $result = [];
        foreach ($labels as $label) {
            $result[] = $label['label'];
        }
        return $result;
    }

    public function isForwardApproved(CustomerInterface $customer)
    {
        return $customer->getCustomAttribute('forward_status')->getValue() == $this->getAttribtueOptionValueByLabel('forward_status', 'Approved');
    }

    public function isHorecaApproved(CustomerInterface $customer)
    {
        return $customer->getCustomAttribute('horeca_status')->getValue() == $this->getAttribtueOptionValueByLabel('horeca_status', 'Approved')
            || $customer->getCustomAttribute('horeca_status')->getValue() == $this->getAttribtueOptionValueByLabel('horeca_status', 'Registered by Forward');
    }

    /**
     * @return ManagerInterface
     */
    public function getMessageManager()
    {
        return $this->messageManager;
    }

    public function isFakeLogin()
    {
        $forwardId = $this->getCustomerSession()->getForwardId();

        if (empty($forwardId)) {
            return false;
        }

        return $forwardId != $this->getCustomerSession()->getCustomerId();
    }
}
