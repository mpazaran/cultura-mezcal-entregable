<?php

namespace CulturaMezcal\Forwards\Plugin\Magento\Quote\Model;

/**
 * Used in order to assign the data for new addresses from checkout
 *
 * @package CulturaMezcal\Forwards\Plugin\Magento\Quote\Model
 */
class ShippingAddressManagement
{

    /**
     * @param \Magento\Quote\Model\ShippingAddressManagement $subject
     * @param                                                $cartId
     * @param \Magento\Quote\Api\Data\AddressInterface       $address
     *
     * @return array
     */
    public function beforeAssign(
        \Magento\Quote\Model\ShippingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {

        $extAttributes = $address->getExtensionAttributes();
        if (!empty($extAttributes)) {
            $address->setNotes($extAttributes->getNotes());
            $address->setExternalNumber($extAttributes->getExternalNumber());
            $address->setInternalNumber($extAttributes->getInternalNumber());
            $address->setDeliveryDays($extAttributes->getDeliveryDays());
            $address->setDeliveryTime($extAttributes->getDeliveryTime());
            $address->setTownId($extAttributes->getTownId());
        }

        return [$cartId, $address];
    }
}
