<?php

namespace CulturaMezcal\Forwards\Plugin\Magento\Quote\Model\Quote;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Quote\Model\Quote\Address as QuoteAddress;

/**
 * Used in order to convert the quote address to customer address
 *
 * @package CulturaMezcal\Forwards\Plugin\Magento\Quote\Model\Quote
 */
class Address
{

    /**
     * @param QuoteAddress     $quoteAddress
     * @param AddressInterface $customerAddress
     *
     * @return AddressInterface
     */
    public function afterExportCustomerAddress(
        QuoteAddress $quoteAddress,
        AddressInterface $customerAddress
    ) {

        $customerAddress->setCustomAttribute('notes', $quoteAddress->getNotes());
        $customerAddress->setCustomAttribute('external_number', $quoteAddress->getExternalNumber());
        $customerAddress->setCustomAttribute('internal_number', $quoteAddress->getInternalNumber());
        $customerAddress->setCustomAttribute('delivery_days', $quoteAddress->getDeliveryDays());
        $customerAddress->setCustomAttribute('delivery_time', $quoteAddress->getDeliveryTime());
        $customerAddress->setCustomAttribute('town_id', $quoteAddress->getTownId());

        return $customerAddress;
    }
}
