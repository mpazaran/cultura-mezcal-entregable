<?php

namespace CulturaMezcal\Forwards\Plugin\Magento\Checkout\Block\Checkout;

use CulturaMezcal\Core\Helper\Data;
use CulturaMezcal\Core\Model\AttributeOptions;
use CulturaMezcal\Forwards\Api\DirectoryCountryRegionTownRepositoryInterface;
use Magento\Checkout\Block\Checkout\LayoutProcessor as ChekcoutLayerprocessor;

class LayoutProcessor
{
    protected $dataHelper;
    /**
     * @var DirectoryCountryRegionTownRepositoryInterface
     */
    private $townRepository;
    /**
     * @var AttributeOptions
     */
    private $attributeOptions;

    /**
     * LayoutProcessor constructor.
     *
     * @param Data                                          $dataHelper
     * @param DirectoryCountryRegionTownRepositoryInterface $townRepository
     * @param AttributeOptions                              $attributeOptions
     */
    public function __construct(
        Data $dataHelper,
        DirectoryCountryRegionTownRepositoryInterface $townRepository,
        AttributeOptions $attributeOptions
    ) {
        $this->dataHelper       = $dataHelper;
        $this->townRepository   = $townRepository;
        $this->attributeOptions = $attributeOptions;
    }

    public function afterProcess(
        ChekcoutLayerprocessor $subject,
        array $jsLayout
    ) {

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['address-list']['component']
            = 'CulturaMezcal_Forwards/js/view/shipping-address/list';


        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['town_id']
            = [
            'component'  => 'Magento_Ui/js/form/element/select',
            'config'     => [
                'customScope' => 'shippingAddress.customAttributes',
                'template'    => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'id'          => 'town-id',
            ],
            'dataScope'  => 'shippingAddress.customAttributes.town_id',
            'label'      => __('Town'),
            'provider'   => 'checkoutProvider',
            'visible'    => true,
            'sortOrder'  => 251,
            'id'         => 'town-id',
            'validation' => [
                'required-entry' => true,
            ],
            'filterBy'   => [
                'target' => "checkoutProvider:shippingAddress.region_id",
                'field'  => 'region_id',
            ],
            'options'    => $this->townRepository->toOptionArray()
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['external_number']
            = [
            'component'   => 'Magento_Ui/js/form/element/abstract',
            'config'      => [
                'customScope' => 'shippingAddress.customAttributes',
                'customEntry' => null,
                'template'    => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
            ],
            'dataScope'   => 'shippingAddress.customAttributes.external_number',
            'label'       => 'External Number',
            'provider'    => 'checkoutProvider',
            'sortOrder'   => 80,
            'validation'  => [
                'required-entry' => true
            ],
            'options'     => [],
            'filterBy'    => null,
            'customEntry' => null,
            'visible'     => true,
            'value'       => ''
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['internal_number']
            = [
            'component'   => 'Magento_Ui/js/form/element/abstract',
            'config'      => [
                'customScope' => 'shippingAddress.customAttributes',
                'customEntry' => null,
                'template'    => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
            ],
            'dataScope'   => 'shippingAddress.customAttributes.internal_number',
            'label'       => 'Internal Number',
            'provider'    => 'checkoutProvider',
            'sortOrder'   => 81,
            'validation'  => [
                'required-entry' => false
            ],
            'options'     => [],
            'filterBy'    => null,
            'customEntry' => null,
            'visible'     => true,
            'value'       => ''
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['delivery_days']
            = [
            'component'  => 'Magento_Ui/js/form/element/multiselect',
            'config'     => [
                'customScope' => 'shippingAddress.customAttributes',
                'template'    => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/multiselect',
                'id'          => 'delivery-days',
            ],
            'dataScope'  => 'shippingAddress.customAttributes.delivery_days',
            'label'      => __('Delivery Days'),
            'provider'   => 'checkoutProvider',
            'visible'    => true,
            'sortOrder'  => 260,
            'id'         => 'delivery-days',
            'validation' => [
                'required-entry' => true,
            ],
            'options'    => $this->attributeOptions->setAttributeCode('delivery_days')->toOptionArray('customer_address')
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['delivery_time']
            = [
            'component'  => 'Magento_Ui/js/form/element/multiselect',
            'config'     => [
                'customScope' => 'shippingAddress.customAttributes',
                'template'    => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/multiselect',
                'id'          => 'delivery-time',
            ],
            'dataScope'  => 'shippingAddress.customAttributes.delivery_time',
            'label'      => __('Delivery Time'),
            'provider'   => 'checkoutProvider',
            'visible'    => true,
            'sortOrder'  => 270,
            'id'         => 'delivery-time',
            'validation' => [
                'required-entry' => true,
            ],
            'options'    => $this->attributeOptions->setAttributeCode('delivery_time')->toOptionArray('customer_address')
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['notes']
            = [
            'component'   => 'Magento_Ui/js/form/element/abstract',
            'config'      => [
                'customScope' => 'shippingAddress.customAttributes',
                'customEntry' => null,
                'template'    => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/textarea',
            ],
            'dataScope'   => 'shippingAddress.customAttributes.notes',
            'label'       => 'Notes',
            'provider'    => 'checkoutProvider',
            'sortOrder'   => 271,
            'validation'  => [
                'required-entry' => false
            ],
            'options'     => [],
            'filterBy'    => null,
            'customEntry' => null,
            'visible'     => true,
            'value'       => '',
            'cols'        => null,
            'rows'        => null,
        ];

        return $jsLayout;
    }
}
