<?php

namespace CulturaMezcal\Forwards\Controller\Horeca;

use CulturaMezcal\Forwards\Logger\Logger;
use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface as RepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface as MessageManager;

/**
 * @package \CulturaMezcal\Forwards\Controller\Horeca
 */
class Delete extends Action
{

    /**
     * @var RepositoryInterface
     */
    private $modelRepository;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var MessageManager
     */
    protected $messageManager;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * Search constructor.
     *
     * @param Context $context
     * @param Session $customerSession
     * @param JsonFactory $resultFactory
     * @param Logger $logger
     * @param RepositoryInterface $modelRepository
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        JsonFactory $resultFactory,
        Logger $logger,
        RepositoryInterface $modelRepository
    ) {
        parent::__construct($context);
        $this->resultFactory   = $resultFactory;
        $this->modelRepository = $modelRepository;
        $this->customerSession = $customerSession;
        $this->messageManager  = $context->getMessageManager();
        $this->logger          = $logger;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        /**
         * This is to create the response as JSON
         */
        $result = $this->resultFactory->create();

        /**
         * If the customer is not logged in the service return an empty response
         */
        if (!$this->customerSession->isLoggedIn()) {
            $result->setData([
                'redirect' => '/customer/account/login'
            ]);
            return $result;
        }

        try {
            $id    = $this->getRequest()->getParam('id');
            $model = $this->modelRepository->get($id);
            $this->modelRepository->delete($model);
            $result->setData(
                [
                    'ok'    => true,
                    'model' => $model->toArray(),
                ]
            );
            $this->messageManager->addSuccessMessage(__('You deleted the HORECA.'));
        } catch (NoSuchEntityException $e) {
            $result->setData(['ok' => false]);
            $this->logger->error($e->getMessage());
            $this->messageManager->addExceptionMessage($e, __('This HORECA no longer exists.'));
         } catch (Exception $e) {
            $result->setData(['ok' => false]);
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting the HORECA.'));
        }

        return $result;

    }

}
