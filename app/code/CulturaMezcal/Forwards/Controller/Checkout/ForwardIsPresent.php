<?php

namespace CulturaMezcal\Forwards\Controller\Checkout;

use CulturaMezcal\Forwards\Logger\Logger;
use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\CustomerRepositoryInterface as RepositoryInterface;
use Magento\Customer\Model\CustomerFactory as ModelFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Message\ManagerInterface as MessageManager;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;

/**
 * @package \CulturaMezcal\Forwards\Controller\Checkout
 */
class ForwardIsPresent extends Action
{

    /**
     * @var RepositoryInterface
     */
    private $modelRepository;

    /**
     * @var ModelFactory
     */
    private $modelFactory;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var SerializerJson $serializerJson
     */
    protected $serializerJson;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @var MessageManager
     */
    protected $messageManager;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * Search constructor.
     *
     * @param Context             $context
     * @param Session             $customerSession
     * @param CheckoutSession     $checkoutSession
     * @param SerializerJson      $serializerJson
     * @param JsonFactory         $resultFactory
     * @param Logger              $logger
     * @param RepositoryInterface $modelRepository
     * @param ModelFactory        $modelFactory
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        SerializerJson $serializerJson,
        JsonFactory $resultFactory,
        Logger $logger,
        RepositoryInterface $modelRepository,
        ModelFactory $modelFactory
    ) {
        parent::__construct($context);
        $this->resultFactory   = $resultFactory;
        $this->modelRepository = $modelRepository;
        $this->modelFactory    = $modelFactory;
        $this->customerSession = $customerSession;
        $this->serializerJson  = $serializerJson;
        $this->messageManager  = $context->getMessageManager();
        $this->logger          = $logger;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        /**
         * This is to create the response as JSON
         */
        $result = $this->resultFactory->create();

        /**
         * If the customer is not logged in the service return an empty response
         */
        if (!$this->customerSession->isLoggedIn()) {
            $result->setData([
                'redirect' => '/customer/account/login'
            ]);

            return $result;
        }

        try {
            $data = $this->serializerJson->unserialize($this->getRequest()->getContent());

            $this->checkoutSession->setIsForwardPresent(!empty($data['present']));

            $result->setData(
                [
                    'ok' => true
                ]
            );

        } catch (Exception $e) {
            $result->setData(['ok' => false]);
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
        }

        return $result;
    }
}
