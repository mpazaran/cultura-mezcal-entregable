<?php

namespace CulturaMezcal\Forwards\Controller\Register;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $pageFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
        parent::__construct($context);
        $this->pageFactory     = $pageFactory;
        $this->customerSession = $customerSession;
    }

    /**
     * Execute action based on request and return result
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     */
    public function execute()
    {
        if (!$this->customerSession->isLoggedIn()) {
            return $this->pageFactory->create();
        } else {
            /**
             * @var $redirectResult \Magento\Framework\Controller\Result\Redirect
             */
            $redirectResult = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $redirectResult->setPath('customer/account');
            return $redirectResult;
        }
    }
}
