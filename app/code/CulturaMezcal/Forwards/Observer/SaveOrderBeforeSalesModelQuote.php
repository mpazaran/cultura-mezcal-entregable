<?php

namespace CulturaMezcal\Forwards\Observer;

use CulturaMezcal\Core\Helper\Data;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

class SaveOrderBeforeSalesModelQuote implements ObserverInterface
{
    /**
     * @var Data
     */
    private $culturaMezcalHelper;

    /**
     * SaveOrderBeforeSalesModelQuote constructor.
     *
     * @param Data $culturaMezcalHelper
     */
    public function __construct(
        Data $culturaMezcalHelper
    ) {
        $this->culturaMezcalHelper = $culturaMezcalHelper;
    }

    /**
     * @param Observer $observer
     *
     * @return $this|void
     */
    public function execute(Observer $observer)
    {
        /* @var Order $order */
        $order = $observer->getEvent()->getData('order');
        $order->setData('forward_id', $this->culturaMezcalHelper->getForwardId());

        if ($this->culturaMezcalHelper->isHoreca()) {
            /**
             * SI ES HORECA PREGUNTAMOS SI SE RECONOCE LA PRESENCIA DEL HORECA
             */
            $order->setData('forward_present', $this->culturaMezcalHelper->isForwardPresent());
        }

        return $this;
    }
}
