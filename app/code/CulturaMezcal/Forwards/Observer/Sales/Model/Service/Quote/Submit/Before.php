<?php

namespace CulturaMezcal\Forwards\Observer\Sales\Model\Service\Quote\Submit;

use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Api\OrderAddressRepositoryInterface;
use Magento\Sales\Model\Order\Address as OrderAddress;

class Before implements ObserverInterface
{

    /**
     * @var OrderAddressRepositoryInterface
     */
    private $addressRepository;
    /**
     * @var AddressRepositoryInterface
     */
    private $customerAddressRepository;

    /**
     * Before constructor.
     *
     * @param OrderAddressRepositoryInterface $addressRepository
     * @param AddressRepositoryInterface   $cuustomeraddressRepository
     */
    public function __construct(
        OrderAddressRepositoryInterface $addressRepository,
        AddressRepositoryInterface $customerAddressRepository
    ) {
        $this->addressRepository         = $addressRepository;
        $this->customerAddressRepository = $customerAddressRepository;
    }

    public function execute(Observer $observer)
    {
        /** @var OrderAddress $quoteAddress */
        $orderAddress = $observer->getEvent()->getData('order')->getShippingAddress();
        /** @var Address $quoteAddress */
        $quoteAddress = $observer->getEvent()->getData('quote')->getShippingAddress();

        $orderAddress->setData('notes', $quoteAddress->getData('notes'));
        $orderAddress->setData('external_number', $quoteAddress->getData('external_number'));
        $orderAddress->setData('internal_number', $quoteAddress->getData('internal_number'));
        $orderAddress->setData('delivery_days', $quoteAddress->getData('delivery_days'));
        $orderAddress->setData('delivery_time', $quoteAddress->getData('delivery_time'));
        $orderAddress->setData('town_id', $quoteAddress->getData('town_id'));

        $this->addressRepository->save($orderAddress);

        return $this;
    }
}
