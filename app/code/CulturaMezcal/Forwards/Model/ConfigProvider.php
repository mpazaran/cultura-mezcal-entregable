<?php

namespace CulturaMezcal\Forwards\Model;

use CulturaMezcal\Core\Helper\Data;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\LayoutInterface;

class ConfigProvider implements ConfigProviderInterface
{
    /** @var LayoutInterface */
    protected $_layout;
    /**
     * @var Data
     */
    private $culturaMezcalHelper;

    /**
     * ConfigProvider constructor.
     *
     * @param LayoutInterface $layout
     * @param Data            $culturaMezcalHelper
     */
    public function __construct(
        LayoutInterface $layout,
        Data $culturaMezcalHelper
    ) {
        $this->_layout             = $layout;
        $this->culturaMezcalHelper = $culturaMezcalHelper;
    }

    public function getConfig()
    {
        return [
            'isHoreca'    => $this->culturaMezcalHelper->isHoreca(),
            'isFakeLogin' => $this->culturaMezcalHelper->isFakeLogin(),
        ];
    }
}
