<?php

namespace CulturaMezcal\Forwards\Model;

use CulturaMezcal\Forwards\Api\Data\HorecaContactInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * This is the table horeca_contact representation in Magento.
 *
 * @package CulturaMezcal\Forwards\Model
 */
class HorecaContact extends AbstractModel implements IdentityInterface, HorecaContactInterface
{
    /**
     * Model cache tag for clear cache in after save and after delete
     */
    const CACHE_TAG = 'cultura_mezcal_forwards_horeca_contact';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * When you use true - all cache will be clean
     *
     * @var string|array|bool
     */
    protected $_cacheTag = 'cultura_mezcal_forwards_horeca_contact';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_eventPrefix = 'cultura_mezcal_forwards_horeca_contact';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\HorecaContact::class);
    }

    /**
     * @inheritdoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * @param int $id
     *
     * @return HorecaContactInterface
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * @return int
     */
    public function getHorecaId()
    {
        return $this->getData('horeca_id');
    }

    /**
     * @param int $horecaId
     *
     * @return HorecaContactInterface
     */
    public function setHorecaId($horecaId)
    {
        return $this->setData('horeca_id', $horecaId);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @param string $name
     *
     * @return HorecaContactInterface
     */
    public function setName($name)
    {
        return $this->setData('name', $name);
    }

    /**
     * @return string
     */
    public function getJobTitle()
    {
        return $this->getData('job_title');
    }

    /**
     * @param string $jobTitle
     *
     * @return HorecaContactInterface
     */
    public function setJobTitle($jobTitle)
    {
        return $this->setData('job_title', $jobTitle);
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->getData('phone');
    }

    /**
     * @param string $phone
     *
     * @return HorecaContactInterface
     */
    public function setPhone($phone)
    {
        return $this->setData('phone', $phone);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->getData('email');
    }

    /**
     * @param string $email
     *
     * @return HorecaContactInterface
     */
    public function setEmail($email)
    {
        return $this->setData('email', $email);
    }

}
