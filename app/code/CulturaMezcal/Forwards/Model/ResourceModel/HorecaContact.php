<?php

namespace CulturaMezcal\Forwards\Model\ResourceModel;

/**
 * Relates the mysql table horeca_contact with the model \CulturaMezcal\Forwards\Model\HorecaContact.
 *
 */
class HorecaContact extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @inheritdoc
     */
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('horeca_contact', 'id');
    }
}

