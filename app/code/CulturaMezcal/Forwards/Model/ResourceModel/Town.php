<?php

namespace CulturaMezcal\Forwards\Model\ResourceModel;

class Town extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{
    /**
     * @var \CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown\CollectionFactory
     */
    protected $_townsFactory;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory               $attrOptionCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory                          $attrOptionFactory
     * @param \CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown\CollectionFactory $townsFactory
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory,
        \CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown\CollectionFactory $townsFactory
    ) {
        $this->_townsFactory = $townsFactory;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * @inheritdoc
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {

        if (!$this->_options) {
            $this->_options = $this->_createCollection()->load()->groupedOptionArray();
        }

        return $this->_options;
    }

    /**
     * @return \Magento\Directory\Model\ResourceModel\Region\Collection
     */
    protected function _createCollection()
    {
        return $this->_townsFactory->create();
    }
}
