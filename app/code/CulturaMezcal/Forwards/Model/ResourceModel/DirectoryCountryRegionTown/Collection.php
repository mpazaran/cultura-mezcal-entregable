<?php

namespace CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown;

/**
 * Collection for \CulturaMezcal\Forwards\Model\DirectoryCountryRegionTown.
 *
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Identifier field name for collection items
     *
     * Can be used by collections with items without defined
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'cultura_mezcal_forwards_directory_country_region_town_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'cultura_mezcal_forwards_directory_country_region_town_collection';
    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    private $regionCollectionFactory;

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->regionCollectionFactory = $regionCollectionFactory;
    }


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\CulturaMezcal\Forwards\Model\DirectoryCountryRegionTown::class, \CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown::class);
    }

    public function groupedOptionArray()
    {
        $result           = [];
        $regionCollection = $this->regionCollectionFactory->create();
        foreach ($this->getItems() as $item) {
            $regionId = $item->getRegionId();
            if (empty($result[$regionId])) {
                /** @var \Magento\Directory\Model\Region $region */
                $region            = $regionCollection->addFieldToFilter('main_table.region_id', $regionId)->getFirstItem();
                $result[$regionId] = [
                    'label' => $region->getName(),
                    'value' => []
                ];
            }
            $result[$item->getRegionId()]['value'][] = [
                'label' => $item->getName(),
                'value' => $item->getId()
            ];
        }

        return $result;
    }
}


