<?php

namespace CulturaMezcal\Forwards\Model;

use CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * This is the table directory_country_region_town representation in Magento.
 *
 * @package CulturaMezcal\Forwards\Model
 */
class DirectoryCountryRegionTown extends AbstractModel implements IdentityInterface, DirectoryCountryRegionTownInterface
{
    /**
     * Model cache tag for clear cache in after save and after delete
     */
    const CACHE_TAG = 'cultura_mezcal_forwards_directory_country_region_town';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * When you use true - all cache will be clean
     *
     * @var string|array|bool
     */
    protected $_cacheTag = 'cultura_mezcal_forwards_directory_country_region_town';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_eventPrefix = 'cultura_mezcal_forwards_directory_country_region_town';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\DirectoryCountryRegionTown::class);
    }

    /**
     * @inheritdoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * @param int $id
     *
     * @return DirectoryCountryRegionTownInterface
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        return $this->getData('region_id');
    }

    /**
     * @param int $regionId
     *
     * @return DirectoryCountryRegionTownInterface
     */
    public function setRegionId($regionId)
    {
        return $this->setData('region_id', $regionId);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @param string $name
     *
     * @return DirectoryCountryRegionTownInterface
     */
    public function setName($name)
    {
        return $this->setData('name', $name);
    }

}
