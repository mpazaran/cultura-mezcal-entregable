<?php

namespace CulturaMezcal\Forwards\Model;

use CulturaMezcal\Forwards\Api\DirectoryCountryRegionTownRepositoryInterface as ApiDirectoryCountryRegionTownRepositoryInterface;
use CulturaMezcal\Forwards\Api\DirectoryCountryRegionTownSearchResultsInterfaceFactory as ApiDirectoryCountryRegionTownSearchResultsInterface;
use CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown as ResourceModelDirectoryCountryRegionTown;
use CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTownFactory as ResourceModelDirectoryCountryRegionTownFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Exception\NoSuchEntityException;


/**
 * This is the repository for DirectoryCountryRegionTown
 *
 * @package CulturaMezcal\Forwards\Model\repository
 */
class DirectoryCountryRegionTownRepository implements ApiDirectoryCountryRegionTownRepositoryInterface
{

    /**
     * @var DirectoryCountryRegionTownFactory
     */
    protected $modelFactory = null;

    /**
     * @var ResourceModelDirectoryCountryRegionTownFactory
     */
    protected $resourceModelFactory = null;

    /**
     * @var ResourceModelDirectoryCountryRegionTown
     */
    protected $resourceModel = null;

    /**
     * @var $searchResultsFactory
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceModelDirectoryCountryRegionTown\CollectionFactory
     */
    protected $collectionFactory = null;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param DirectoryCountryRegionTownFactory                         $modelFactory
     * @param ResourceModelDirectoryCountryRegionTownFactory            $resourceModelFactory
     * @param ResourceModelDirectoryCountryRegionTown\CollectionFactory $collectionFactory
     * @param ApiDirectoryCountryRegionTownSearchResultsInterface       $searchResultsFactory
     * @param CollectionProcessorInterface                              $collectionProcessor
     * @param SearchCriteriaBuilder                                     $searchCriteriaBuilder
     * @param SortOrderBuilder                                          $sortOrderBuilder
     */
    public function __construct(
        DirectoryCountryRegionTownFactory $modelFactory,
        ResourceModelDirectoryCountryRegionTownFactory $resourceModelFactory,
        ResourceModelDirectoryCountryRegionTown\CollectionFactory $collectionFactory,
        ApiDirectoryCountryRegionTownSearchResultsInterface $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder
    ) {
        $this->modelFactory          = $modelFactory;
        $this->resourceModelFactory  = $resourceModelFactory;
        $this->resourceModel         = $resourceModelFactory->create();
        $this->searchResultsFactory  = $searchResultsFactory;
        $this->collectionFactory     = $collectionFactory;
        $this->collectionProcessor   = $collectionProcessor;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
    }

    /**
     * @return \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface
     */
    public function create()
    {
        return $this->modelFactory->create();
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \CulturaMezcal\Forwards\Api\DirectoryCountryRegionTownSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null)
    {
        $searchResults = $this->searchResultsFactory->create();
        $collection    = $this->getCollection();
        if (null !== $searchCriteria) {
            $searchResults->setSearchCriteria($searchCriteria);
            $this->collectionProcessor->process($searchCriteria, $collection);
        }
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults->setItems($collection->getItems());
    }

    /**
     * @return \CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown\Collection
     */
    public function getCollection()
    {
        return $this->collectionFactory->create();
    }

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null)
    {
        return $this->loadModel($this->modelFactory->create(), $value, $field);
    }

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface $model
     * @param mixed                                                                $value
     * @param string                                                               $field
     *
     * @return \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface $model, $value, $field = null)
    {
        $this->resourceModel->load($model, $value, $field);
        if (!$model->getId()) {
            throw new NoSuchEntityException(__('The register not longer exists.'));
        }

        return $model;
    }

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface $model
     *
     * @return \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface
     * @throws \Exception
     */
    public function save(\CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface $model)
    {
        $this->resourceModel->save($model);

        return $model;
    }

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface $model
     *
     * @return \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface
     * @throws \Exception
     */
    public function delete(\CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface $model)
    {
        $this->resourceModel->delete($model);

        return $model;
    }

    /**
     * @param int $id
     *
     * @return \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $model = $this->load($id);
        $this->resourceModel->delete($model);

        return $model;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null)
    {
        $options = [];
        foreach ($this->getList($searchCriteria)->getItems() as $model) {
            $options[] = [
                'value'     => $model->getId(),
                'label'     => $model->getName(),
                'region_id' => $model->getRegionId(),
            ];
        }

        return $options;
    }

}
