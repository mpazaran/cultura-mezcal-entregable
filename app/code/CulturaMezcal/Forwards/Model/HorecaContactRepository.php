<?php

namespace CulturaMezcal\Forwards\Model;

use CulturaMezcal\Forwards\Api\HorecaContactRepositoryInterface as ApiHorecaContactRepositoryInterface;
use CulturaMezcal\Forwards\Api\HorecaContactSearchResultsInterfaceFactory as ApiHorecaContactSearchResultsInterface;
use CulturaMezcal\Forwards\Model\ResourceModel\HorecaContactFactory as ResourceModelHorecaContactFactory;
use CulturaMezcal\Forwards\Model\ResourceModel\HorecaContact as ResourceModelHorecaContact;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Exception\NoSuchEntityException;


/**
 * This is the repository for HorecaContact
 *
 * @package CulturaMezcal\Forwards\Model\repository
 */
class HorecaContactRepository implements ApiHorecaContactRepositoryInterface
{

    /**
     * @var HorecaContactFactory
     */
    protected $modelFactory         = null;

    /**
     * @var ResourceModelHorecaContactFactory
     */
    protected $resourceModelFactory = null;

    /**
     * @var ResourceModelHorecaContact
     */
    protected $resourceModel = null;

    /**
     * @var $searchResultsFactory
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceModelHorecaContact\CollectionFactory
     */
    protected $collectionFactory    = null;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param HorecaContactFactory $modelFactory
     * @param ResourceModelHorecaContactFactory $resourceModelFactory
     * @param ResourceModelHorecaContact\CollectionFactory $collectionFactory
     * @param ApiHorecaContactSearchResultsInterface $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     */
    public function __construct(
        HorecaContactFactory $modelFactory,
        ResourceModelHorecaContactFactory $resourceModelFactory,
        ResourceModelHorecaContact\CollectionFactory $collectionFactory,
        ApiHorecaContactSearchResultsInterface $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder
    )
    {
        $this->modelFactory          = $modelFactory;
        $this->resourceModelFactory  = $resourceModelFactory;
        $this->resourceModel         = $resourceModelFactory->create();
        $this->searchResultsFactory  = $searchResultsFactory;
        $this->collectionFactory     = $collectionFactory;
        $this->collectionProcessor   = $collectionProcessor;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
    }

    /**
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     */
    public function create()
    {
        return $this->modelFactory->create();
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \CulturaMezcal\Forwards\Api\HorecaContactSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null)
    {
        $searchResults = $this->searchResultsFactory->create();
        $collection = $this->getCollection();
        if (null !== $searchCriteria) {
            $searchResults->setSearchCriteria($searchCriteria);
            $this->collectionProcessor->process($searchCriteria, $collection);
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($collection->getItems());
    }

    /**
     * @return \CulturaMezcal\Forwards\Model\ResourceModel\HorecaContact\Collection
     */
    public function getCollection(){
        return $this->collectionFactory->create();
    }

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null) {
        return $this->loadModel($this->modelFactory->create(), $value, $field);
    }

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model, $value, $field = null) {
        $this->resourceModel->load($model, $value, $field);
        if(!$model->getId()) {
            throw new NoSuchEntityException(__('The register not longer exists.'));
        }
        return $model;
    }

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     * @throws \Exception
     */
    public function save(\CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model) {
        $this->resourceModel->save($model);
        return $model;
    }

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     * @throws \Exception
     */
    public function delete(\CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model) {
        $this->resourceModel->delete($model);
        return $model;
    }

    /**
     * @param int $id
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id) {
        $model = $this->load($id);
        $this->resourceModel->delete($model);
        return $model;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null)
    {
        $options = [];
        foreach($this->getList($searchCriteria)->getItems() as $model) {
            $options[] = [
                'value' => $model->getId(),
                'label' => $model->getId(),
            ];
        }

        return $options;
    }

}
