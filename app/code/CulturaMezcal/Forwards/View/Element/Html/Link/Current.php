<?php

namespace CulturaMezcal\Forwards\View\Element\Html\Link;

use Magento\Framework\App\DefaultPathInterface;
use Magento\Framework\View\Element\Template\Context;

class Current extends \Magento\Framework\View\Element\Html\Link\Current
{
    /**
     * @var \CulturaMezcal\Core\Helper\Data
     */
    private $culturaMezcal;

    public function __construct(
        Context $context,
        DefaultPathInterface $defaultPath,
        \CulturaMezcal\Core\Helper\Data $culturaMezcal,
        array $data = []
    ) {
        parent::__construct($context, $defaultPath, $data);
        $this->culturaMezcal = $culturaMezcal;
    }

    protected function _toHtml()
    {
        if (!$this->culturaMezcal->isForward()) {
            return '';
        }

        return parent::_toHtml();
    }


}
