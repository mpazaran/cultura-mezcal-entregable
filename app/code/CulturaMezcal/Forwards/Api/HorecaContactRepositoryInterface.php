<?php

namespace CulturaMezcal\Forwards\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @package CulturaMezcal\Forwards\Api
 */
interface HorecaContactRepositoryInterface extends OptionSourceInterface
{

    /**
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     */
    public function create();

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \CulturaMezcal\Forwards\Api\HorecaContactSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null);

    /**
     * @return \CulturaMezcal\Forwards\Model\ResourceModel\HorecaContact\Collection
     */
    public function getCollection();

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null);

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model, $value, $field = null);

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     */
    public function save(\CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model);

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     */
    public function delete(\CulturaMezcal\Forwards\Api\Data\HorecaContactInterface $model);

    /**
     * @param int $id
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null);

}
