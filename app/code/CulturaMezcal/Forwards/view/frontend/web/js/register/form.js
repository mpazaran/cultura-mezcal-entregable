define([
    "uiComponent",
    "uiRegistry",
    "mage/storage",
    "mage/translate",
    "mage/validation",
    "Magento_Ui/js/modal/alert",
    "Magento_Ui/js/modal/confirm",
    "jquery",
    "ko",
    "CulturaMezcal_Core/js/messages",
    "mage/calendar",
    "mage/loader"
], function (Component, registry, storage, __, validation, alert, confirm, $, ko, messagesContainer) {
    let messages = messagesContainer();
    return Component.extend({

        defaults      : {
            template          : {
                name       : "CulturaMezcal_Forwards/register/form",
                afterRender: function (renderedNodesArray, data) {
                    data.form = $('#forwards_register_form');
                    data.form.validation();
                    $("#dob").calendar({
                        showsTime  : false,
                        dateFormat : "Y-MM-dd",
                        buttonImage: "",
                        yearRange  : "-60y:c+nn",
                        buttonText : "Select Date", maxDate: "-17y", changeMonth: true, changeYear: true, showOn: "both", firstDay: 1
                    });
                    $(".ui-datepicker-trigger.v-middle").remove();
                }
            },
            model             : ko.observable(null),
            loading           : ko.observable(false),
            countries         : ko.observableArray([]),
            jobRegions        : ko.observableArray([]),
            regions           : ko.observableArray([]),
            towns             : ko.observableArray([]),
            jobTowns          : ko.observableArray([]),
            spainRegions      : ko.observableArray([]),
            professions       : ko.observableArray([]),
            workPlaces        : ko.observableArray([]),
            paymentPeriodicity: ko.observableArray([]),
            form              : null,
            forward           : {
                email              : ko.observable(),
                password           : ko.observable(),
                firstname          : ko.observable(),
                lastname           : ko.observable(),
                mothersname        : ko.observable(),
                dob                : ko.observable(),
                phone              : ko.observable(),
                address_street     : ko.observable(),
                external_number    : ko.observable(),
                internal_number    : ko.observable(),
                zip_code           : ko.observable(),
                country_id         : ko.observable('ES'),
                region_id          : ko.observable(),
                town_id            : ko.observable(),
                cif                : ko.observable(),
                bank_account       : ko.observable(),
                profession         : ko.observable(),
                job_country_id     : ko.observable('ES'),
                job_region_id      : ko.observable(),
                job_town_id        : ko.observable(),
                work_place         : ko.observable(),
                payment_periodicity: ko.observable(),
            }
        },
        initialize    : function () {
            this._super();
            window.lala = this;
            let body = $('body');
            this.loading.subscribe(function (value) {
                if (value) {
                    body.trigger('processStart');
                } else {
                    body.trigger('processStop');
                }
            }.bind(this));
            this.forward.country_id.subscribe(function (id) {
                this.loading(true);
                storage.post(
                    "forwards/region/search",
                    JSON.stringify({id: id})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.regions(response.options);
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }.bind(this));
            this.forward.job_country_id.subscribe(function (id) {
                this.loading(true);
                storage.post(
                    "forwards/region/search",
                    JSON.stringify({id: id})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.jobRegions(response.options);
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }.bind(this));
            this.forward.job_region_id.subscribe(function (id) {
                this.loading(true);
                storage.post(
                    "forwards/region_town/search",
                    JSON.stringify({id: id})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.jobTowns(response.options);
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }.bind(this));
            this.forward.region_id.subscribe(function (id) {
                this.loading(true);
                storage.post(
                    "forwards/region_town/search",
                    JSON.stringify({id: id})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.towns(response.options);
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }.bind(this));
        },
        initObservable: function () {
            var result = this._super()
                .observe([
                    'countries',
                    'jobRegions',
                    'spainRegions',
                    'professions',
                    'workPlaces',
                    'paymentPeriodicity',
                    'towns',
                    'jobTowns'
                ]);

            this.jobRegions(this.spainRegions());
            this.regions(this.spainRegions());
            return result;
        },
        save          : function (back) {
            return function () {
                if (this.form.validation('isValid')) {
                    this.loading(true);
                    storage.post(
                        "forwards/register/save",
                        ko.toJSON(this.forward)
                    ).done(
                        function (response) {
                            if (response.ok) {
                                this.model(response.model);
                            }
                        }.bind(this)
                    ).fail(
                        function (response) {
                        }.bind(this)
                    ).complete(
                        function (response) {
                            if (undefined !== response.responseJSON.redirect) {
                                window.location = response.responseJSON.redirect;
                            }
                            this.loading(false);
                            messages.timedClear();
                        }.bind(this)
                    );
                }
            }.bind(this);
        },
    });
});
