define([
    "jquery",
    "ko",
    "uiComponent",
    "mage/storage",
    "mage/translate"
], function ($, ko, Component, storage, __) {
    return Component.extend({
            loading       : ko.observable(false),
            default       : {
                towns         : ko.observableArray([]),
                townId        : ko.observable(0),
                originalTownId: 0,
            },
            initialize    : function () {
                this._super();
                $('#region_id').change(this.updateTowns.bind(this));
                let body = $('body');
                this.loading.subscribe(function (value) {
                    if (value) {
                        body.trigger('processStart');
                    } else {
                        body.trigger('processStop');
                    }
                }.bind(this));
            },
            initObservable: function () {
                let result = this._super()
                    .observe(['towns', 'townId']);
                this.originalTownId = this.townId();
                this.updateTowns(true);
                return result;
            },
            updateTowns   : function (initialLoad) {
                this.loading(true);
                storage.post(
                    "forwards/region_town/search",
                    JSON.stringify({id: $('#region_id').val()})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.towns(response.options);
                            if (initialLoad !== undefined) {
                                this.townId(this.originalTownId);
                            }
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        //messages.timedClear();
                    }.bind(this)
                );
            }
        }
    );
});
