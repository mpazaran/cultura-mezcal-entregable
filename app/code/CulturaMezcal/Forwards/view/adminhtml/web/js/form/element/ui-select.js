define([
    'jquery',
    'Magento_Ui/js/form/element/ui-select',
    'mage/translate'
], function ($, Select, $t) {
    'use strict';
    return Select.extend({

        defaults: {
            loadingValuePlaceholder: $t('Loading...')
        },
        initialize: function () {
            this._super();

            this.loading(true);
            this.isDisplayMissingValuePlaceholder(false);
            $.ajax({
                url       : BASE_URL.split('/').slice(0, -2).join('/') + '/' + this.searchUrl,
                type      : 'get',
                dataType  : 'json',
                context   : this,
                data      : {
                    id: this.value()
                },
                success   : $.proxy(this.success, this),
                error     : $.proxy(this.error, this),
                beforeSend: $.proxy(this.beforeSend, this),
                complete  : $.proxy(this.complete, this, null, 1)
            });

            return this;
        },

        /**
         * Submit request to load data
         *
         * @param {String} searchKey
         * @param {Number} page
         */
        processRequest: function (searchKey, page) {
            this.loading(true);
            this.currentSearchKey = searchKey;
            $.ajax({
                url       : BASE_URL.split('/').slice(0, -2).join('/') + '/' + this.searchUrl,
                type      : 'get',
                dataType  : 'json',
                context   : this,
                data      : {
                    searchKey: searchKey,
                    page     : page,
                    limit    : this.pageLimit,
                },
                success   : $.proxy(this.success, this),
                error     : $.proxy(this.error, this),
                beforeSend: $.proxy(this.beforeSend, this),
                complete  : $.proxy(this.complete, this, searchKey, page)
            });
        },

        /**
         * Set caption
         */
        setCaption: function () {
            var length, caption = '';

            if(this.loading()){
                return this.loadingValuePlaceholder;
            }

            if (!_.isArray(this.value()) && this.value()) {
                length = 1;
            } else if (this.value()) {
                length = this.value().length;
            } else {
                this.value([]);
                length = 0;
            }
            this.warn(caption);

            //check if option was removed
            if (this.isDisplayMissingValuePlaceholder && length && !this.getSelected().length) {
                caption = this.missingValuePlaceholder.replace('%s', this.value());
                this.placeholder(caption);
                this.warn(caption);

                return this.placeholder();
            }

            if (length > 1) {
                this.placeholder(length + ' ' + this.selectedPlaceholders.lotPlaceholders);
            } else if (length && this.getSelected().length) {
                this.placeholder(this.getSelected()[0].label);
            } else {
                this.placeholder(this.selectedPlaceholders.defaultPlaceholder);
            }

            return this.placeholder();
        },
    });
});


