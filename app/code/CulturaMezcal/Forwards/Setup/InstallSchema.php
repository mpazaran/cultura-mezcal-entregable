<?php

namespace CulturaMezcal\Forwards\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;


class InstallSchema implements InstallSchemaInterface
{

    /**
     * @inheritdoc
     * @throws Zend_Db_Exception
     * @throws LocalizedException
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $setup->getConnection();

        $tableName = $setup->getTable('directory_country_region_town');

        if ($connection->isTableExists($tableName)) {
            $connection->dropTable($tableName);
        }

        $table = $connection->newTable($tableName)
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    Table::OPTION_NULLABLE => false,
                    Table::OPTION_IDENTITY => true,
                    Table::OPTION_UNSIGNED => true,
                ],
                'ID'
            )
            ->addColumn(
                'region_id',
                Table::TYPE_INTEGER,
                null,
                [
                    Table::OPTION_NULLABLE => false,
                    Table::OPTION_UNSIGNED => true,
                ],
                'REGION ID'
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,
                [
                    Table::OPTION_NULLABLE => false,
                    Table::OPTION_LENGTH   => 255
                ],
                'Full Name'
            )->addIndex(
                'idx_primary',
                ['id'],
                ['type' => AdapterInterface::INDEX_TYPE_PRIMARY]
            );

        $connection->createTable($table);

        $tableName = $setup->getTable('horeca_contact');

        if ($connection->isTableExists($tableName)) {
            $connection->dropTable($tableName);
        }

        $table = $connection->newTable($tableName)
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    Table::OPTION_NULLABLE => false,
                    Table::OPTION_IDENTITY => true,
                    Table::OPTION_UNSIGNED => true,
                ],
                'ID'
            )
            ->addColumn(
                'horeca_id',
                Table::TYPE_INTEGER,
                null,
                [
                    Table::OPTION_NULLABLE => false,
                ],
                'HORECA'
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,
                [
                    Table::OPTION_NULLABLE => false,
                    Table::OPTION_LENGTH   => 255
                ],
                'Full Name'
            )
            ->addColumn(
                'job_title',
                Table::TYPE_TEXT,
                255,
                [
                    Table::OPTION_NULLABLE => false,
                    Table::OPTION_LENGTH   => 255
                ],
                'Job Title'
            )
            ->addColumn(
                'phone',
                Table::TYPE_TEXT,
                255,
                [
                    Table::OPTION_NULLABLE => false,
                    Table::OPTION_LENGTH   => 255
                ],
                'Phone'
            )
            ->addColumn(
                'email',
                Table::TYPE_TEXT,
                255,
                [
                    Table::OPTION_NULLABLE => true,
                    Table::OPTION_LENGTH   => 255
                ],
                'Email'
            )->addIndex(
                'idx_primary',
                ['id'],
                ['type' => AdapterInterface::INDEX_TYPE_PRIMARY]
            );

        $connection->createTable($table);

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'forward_present',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => false,
                'default'  => 0,
                'length'   => '1',
                'comment'  => 'Is Forward Present'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'forward_present',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => false,
                'default'  => 0,
                'length'   => '1',
                'comment'  => 'Is Forward Present'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'commission_paid',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => false,
                'default'  => 0,
                'length'   => '1',
                'comment'  => 'Commission Paid'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'forward_id',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => true,
                'default'  => 0,
                'comment'  => 'Forward ID'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'forward_id',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => true,
                'default'  => 0,
                'comment'  => 'Forward ID'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'commission_paid',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => false,
                'default'  => 0,
                'length'   => '1',
                'comment'  => 'Commission Paid'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'commission_paid',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => false,
                'default'  => 0,
                'length'   => '1',
                'comment'  => 'Commission Paid'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'notes',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => '',
                'comment'  => 'Notes'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'external_number',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => 0,
                'length'   => '255',
                'comment'  => 'External Numbers'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'internal_number',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => 0,
                'length'   => '255',
                'comment'  => 'Internal Numbers'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'delivery_days',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => 0,
                'length'   => '255',
                'comment'  => 'Delivery Days'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'delivery_time',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => 0,
                'length'   => '255',
                'comment'  => 'Delivery Time'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'town_id',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => false,
                'default'  => 0,
                'length'   => '11',
                'comment'  => 'Town'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_address'),
            'notes',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => '',
                'comment'  => 'Notes'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_address'),
            'external_number',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => 0,
                'length'   => '255',
                'comment'  => 'External Numbers'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_address'),
            'internal_number',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => 0,
                'length'   => '255',
                'comment'  => 'Internal Numbers'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_address'),
            'delivery_days',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => 0,
                'length'   => '255',
                'comment'  => 'Delivery Days'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_address'),
            'delivery_time',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => 0,
                'length'   => '255',
                'comment'  => 'Delivery Time'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_address'),
            'town_id',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => false,
                'default'  => 0,
                'length'   => '11',
                'comment'  => 'Town'
            ]
        );

        $setup->endSetup();
    }
}
