<?php

namespace CulturaMezcal\Forwards\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Image\Factory as ImageFactory;
use Magento\Framework\UrlInterface;

class Media extends AbstractHelper
{

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var ImageFactory
     */
    private $imageFactory;

    /**
     * @var string
     */
    private $mediaUrl;

    /**
     * @param Context       $context
     * @param DirectoryList $directoryList
     * @param ImageFactory  $imageFactory
     */
    public function __construct(
        Context $context,
        DirectoryList $directoryList,
        ImageFactory $imageFactory
    ) {
        parent::__construct($context);
        $this->directoryList = $directoryList;
        $this->imageFactory  = $imageFactory;
    }

    public function getMediaUrl()
    {
        if (null === $this->mediaUrl) {
            return $this->mediaUrl = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
        }

        return $this->mediaUrl;
    }

    /**
     * Return the url for an image inside of media directory
     *
     * @param $path
     *
     * @return string
     */
    public function getImageUrl($path)
    {
        return $this->getMediaUrl() . $path;
    }

    /**
     * Create a thumbnail image, and stores it in media direcotry,
     *
     * @param     $relativePath
     * @param int $width
     * @param int $height
     *
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getThumbnailUrl($relativePath, $width = 175, $height = 175)
    {
        $relativePath          = '/' . trim($relativePath, '/');
        $absolutePath          = $this->directoryList->getPath('media') . $relativePath;
        $thumbnailPath         = 'thumbnail/' . $width . 'x' . $height . $relativePath;
        $thumbnailAbsolutePath = $this->directoryList->getPath('media') . '/' . $thumbnailPath;

        if (!is_file($thumbnailAbsolutePath)) {
            $targetThumbnailAbsolutePath = dirname($thumbnailAbsolutePath);
            if (!is_dir($targetThumbnailAbsolutePath)) {
                /**
                 * Work around to proper create directory with correct permissions
                 */
                $tmp = umask();
                umask(0);
                mkdir($targetThumbnailAbsolutePath, 0777, true);
                umask($tmp);
            }
            $processor = $this->getImageProcessor($absolutePath);
            $processor->resize($width, $height);
            $processor->save($thumbnailAbsolutePath);
        }

        return $this->getMediaUrl() . $thumbnailPath;

    }

    /**
     * @param $absolutePath
     *
     * @return \Magento\Framework\Image
     */
    public function getImageProcessor($absolutePath)
    {
        return $this->imageFactory->create($absolutePath);
    }

    /**
     * splits the info in the valid accepted files
     *
     * @param $string
     *
     * @return array
     */
    public function extractImageParams($string)
    {
        $parts     = explode(',', $string);
        $extension = preg_replace('/^data:image\\/([a-z]{3,4});base64$/', '$1', $parts[0] ?? '');

        return [
            0 < preg_match('/^(jpg|jpeg|gif|png)$/', $extension) ? $extension : false,
            base64_decode($parts[1] ?? null)
        ];
    }

    /**
     * @param string $targetFile
     * @param int    $height
     * @param int    $width
     *
     * @return bool
     */
    public function validateImageSize($targetFile, $height = 3000, $width = 4000)
    {
        $imageSizeData = getimagesize($targetFile);

        return $imageSizeData[0] <= $height || $imageSizeData[1] <= $width;
    }

    /**
     * @param string $file
     *
     * @return string
     * @throws FileSystemException
     */
    public function createDirectoryForFile($file)
    {
        $targetFile = $this->directoryList->getPath('media') . '/' . trim($file, '/');

        $targetDir = dirname($targetFile);

        if (!is_dir($targetDir)) {
            /**
             * Fix to create directory with right permissions
             */
            $tmp = umask();
            umask(0);
            mkdir($targetDir, 0777, true);
            umask($tmp);

            if (!is_dir($targetDir)) {
                throw new FileSystemException(__('Cant\'t create directory for the file %1.', $targetDir));
            }
        }

        return $targetFile;
    }

    /**
     * @param string $value
     *
     * @return string
     * @throws FileSystemException
     */
    public function confirmFile($value)
    {
        $file       = str_replace('tmp:', '', trim($value, '/'));
        $sourceFile = $this->directoryList->getPath('media') . '/tmp/' . $file;

        if (is_file($sourceFile)) {
            $targetFile = $this->directoryList->getPath('media') . '/' . $file;
            $this->createDirectoryForFile($file);
            if (rename($sourceFile, $targetFile)) {
                return $file;
            }
            throw new FileSystemException(__('Cant\'t move the file %1.', $targetFile));
        }


        throw new FileSystemException(__('File not exists %1.', $sourceFile));
    }

    /**
     * @param string $value
     *
     * @return string
     * @throws FileSystemException
     */
    public function unConfirmFile($value)
    {
        $file       = str_replace('tmp:', '', trim($value, '/'));
        $sourceFile = $this->directoryList->getPath('media') . '/' . $file;

        if (is_file($sourceFile)) {
            $targetFile = $this->directoryList->getPath('media') . '/tmp/' . $file;
            $this->createDirectoryForFile($file);
            if (rename($sourceFile, $targetFile)) {
                return $file;
            }
            throw new FileSystemException(__('Cant\'t move the file %1.', $targetFile));
        }


        throw new FileSystemException(__('File not exists %1.', $sourceFile));
    }

    /**
     * @param $filePath
     *
     * @throws FileSystemException
     */
    public function deleteFile($filePath)
    {

        $file = $this->directoryList->getPath('media') . '/' . $filePath;

        if (is_file($file)) {
            if (is_writable($file)) {
                if (unlink($file)) {
                    return;
                }
            }
            throw new FileSystemException(__('Cant\'t delete the file %1.', $targetFile));
        }

        throw new FileSystemException(__('File not exists %1.', $file));
    }

    /**
     * @param $filePath
     *
     * @return bool
     * @throws FileSystemException
     */
    public function fileExists($filePath)
    {
        return is_file($this->directoryList->getPath('media') . '/' . $filePath);
    }

    /**
     * @param $filePath
     *
     * @return false|int
     * @throws FileSystemException
     */
    public function getFileSize($filePath)
    {
        return filesize($this->directoryList->getPath('media') . '/' . $filePath);
    }

    /**
     * @param $filePath
     *
     * @return array
     * @throws FileSystemException
     */
    public function getFileData($filePath)
    {
        return [
            'name' => basename($filePath),
            'url'  => $this->getImageUrl($filePath),
            'size' => $this->getFileSize($filePath)
        ];
    }
}
