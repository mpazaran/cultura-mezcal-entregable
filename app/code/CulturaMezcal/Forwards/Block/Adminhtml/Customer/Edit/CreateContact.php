<?php

namespace CulturaMezcal\Forwards\Block\Adminhtml\Customer\Edit;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Block\Adminhtml\Edit\GenericButton;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveAndContinueButton
 */
class CreateContact extends GenericButton implements ButtonProviderInterface
{
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry           $registry
     * @param AccountManagementInterface            $customerAccountManagement
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        AccountManagementInterface $customerAccountManagement
    ) {
        parent::__construct($context, $registry);
        $this->customerAccountManagement = $customerAccountManagement;
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        $customerId = $this->getCustomerId();
        $canModify  = !$customerId || !$this->customerAccountManagement->isReadonly($this->getCustomerId());
        $data       = [];
        if ($canModify) {
            $data = [
                'label'      => __('Create New Contact'),
                'class'      => 'save',
                'on_click'   => sprintf("location.href = '%s';", $this->urlBuilder->getUrl('forwards/horeca_contact/new', ['horeca_id' => $this->getCustomerId()])),
                'sort_order' => 10,
            ];
        }

        return $data;
    }
}
