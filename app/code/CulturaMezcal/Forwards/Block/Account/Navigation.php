<?php

namespace CulturaMezcal\Forwards\Block\Account;

use CulturaMezcal\Core\Helper\Data;
use Magento\Customer\Block\Account\SortLinkInterface;
use Magento\Framework\View\Element\Template\Context;

/**
 * @package CulturaMezcal\Customer\Block\Account
 */
class Navigation extends \Magento\Customer\Block\Account\Navigation
{
    /**
     * @var Data
     */
    private $culturaMezcal;

    /**
     * Navigation constructor.
     *
     * @param Context $context
     * @param Data    $data
     * @param array   $data
     */
    public function __construct(
        Context $context,
        Data $culturaMezcal,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->culturaMezcal = $culturaMezcal;
    }


    /**
     * {@inheritdoc}
     * @since 101.0.0
     */
    public function getLinks()
    {

        if ($this->culturaMezcal->isFakeLogin()) {
            $allowed      = [
                'customer-account-navigation-account-link'   => true,
                'customer-account-navigation-orders-link'    => true,
                'customer-account-navigation-wish-list-link' => true,
            ];
            $links        = $this->_layout->getChildBlocks($this->getNameInLayout());
            $sortableLink = [];
            foreach ($links as $key => $link) {
                if (!isset($allowed[$key])) {
                    unset($links[$key]);
                    continue;
                }
                if ($link instanceof SortLinkInterface) {
                    $sortableLink[] = $link;
                    unset($links[$key]);
                }
            }
            usort($sortableLink, [$this, "compare"]);

            return array_merge($sortableLink, $links);
        } else {
            return parent::getLinks();
        }
    }

    /**
     * Compare sortOrder in links.
     *
     * @param SortLinkInterface $firstLink
     * @param SortLinkInterface $secondLink
     *
     * @return int
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function compare(SortLinkInterface $firstLink, SortLinkInterface $secondLink): int
    {
        return $secondLink->getSortOrder() <=> $firstLink->getSortOrder();
    }
}


