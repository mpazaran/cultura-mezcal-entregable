<?php

namespace CulturaMezcal\Forwards\Block;

use CulturaMezcal\Core\Helper\Data;
use Magento\Framework\View\Element\Html\Link as MagentoLink;
use Magento\Framework\View\Element\Template;

/**
 * Class Link
 */
class HorecasLink extends MagentoLink
{

    /**
     * @var Data
     */
    private $culturaMezcal;

    public function __construct(
        Template\Context $context,
        Data $culturaMezcal,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->culturaMezcal = $culturaMezcal;
    }


    /**
     * @return string
     */
    protected function _toHtml()
    {

        if ($this->culturaMezcal->isForward()) {
            return '<li><a ' . $this->getLinkAttributes() . ' >' . $this->escapeHtml(__('HORECAS')) . '</a></li>';
        }

        return '';
    }

}
